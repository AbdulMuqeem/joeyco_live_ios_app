//
//  FaqViewController.swift
//  Joey
//
//  Created by Muhammad Anas on 28/09/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

import UIKit


class FaqViewController: NavigationContainerViewController,
    UITableViewDelegate,
UITableViewDataSource,JSONHelperDelegate {
    
    
    override func parsedObjects(_ action: String!, objects list: NSMutableArray!) {
                self.tableViewData = list! as [AnyObject]
                 self.tableView.reloadData()
                 print(tableViewData.count )
                
    }
    
   
    
  // MARK: - Delegate method - Table view

    
    @IBOutlet weak var screen_main_label_title: UILabel!
       
    
    @IBOutlet weak var tableView: UITableView!
    
    // Data model: These strings will be the data for the table view cells
     var tableViewData = [AnyObject]()
    
     
     // cell reuse id (cells that scroll out of view can be reused
     let cellReuseIdentifier = "genericCell"
    
    
    //JSON Helper
    var jsonHelper = JSONHelper()


    
       override func viewDidLoad() {
           super.viewDidLoad()

           // Do any additional setup after loading the view.
           
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
        
        //Setting Delegate
        jsonHelper = JSONHelper(self, andDelegate: self)
        jsonHelper.delegate = self;
        
        //Call Webservice
        callWebservice()
              
       }
    
    
    func callWebservice() {
        jsonHelper.faqFetchApi()
        
    }
    
//    // MARK: - Delegate method - Webservices
//    override func parsedObjects(_ action: String!, objects list: NSMutableArray!) {
//
//
//
//        if(action == "actionFaq_fetch")
//        {self.tableViewData = list! as [AnyObject]
//         self.tableView.reloadData()
//         print(tableViewData.count )
//        }}
       
    
     // number of rows in table view
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return self.tableViewData.count
       }
       
       // create a cell for each table view row
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
           // create a new cell if needed or reuse an old one
        let cell:GenericTableViewCells = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! GenericTableViewCells
        
        //Structure :
        //From server 1 array is being passed
        //That 1 array contains multiple values of same store.
        //Now we are just showing store value from just first value
        let storeFirstObjName = self.tableViewData[indexPath.row]

//        let obj = storeFirstObjName.object(at: 1) as? [String:            AnyObject]
        if let title = storeFirstObjName["name"]{
            cell.title_generic.text = "\(title)"
        }
        
           return cell
       }
       
       // method to run when table view cell is tapped
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    
        let storeFirstObjName = self.tableViewData[indexPath.row]

        let storyboard = UIStoryboard(name: "Storyboard_iPhone", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FaqDescriptionViewController") as? FaqDescriptionViewController
//        vc?.modalPresentationStyle = .fullScreen

        
        if let title = storeFirstObjName["name"]{
            
            vc?.decsTitle = "\(title)"
        }

        if let id = storeFirstObjName["id"]{
            vc?.sprintId = id
        }

        self.present(vc!, animated: true)
//                let vc = storyboard.instantiateViewController(withIdentifier: "TestAutoLayoutViewController") as? TestAutoLayoutViewController
//                self.present(vc!, animated: true,completion: nil)
        
        }
    

   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
