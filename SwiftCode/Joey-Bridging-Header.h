//
//  Joey-Bridging-Header.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 06/07/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#ifndef Joey_Bridging_Header_h
#define Joey_Bridging_Header_h


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <JoeyCoUtilities/JoeyCoUtilities.h>
#import <GoogleMaps/GoogleMaps.h>
#import <sqlite3.h>
#import <CoreLocation/CoreLocation.h>
#import <Crashlytics/Crashlytics.h>
#import "TFHpple.h"
#import "JSQMessages.h"

#import "AppDelegate.h"
#import "Common.h"
#import "JSONHelper.h"
#import "UrlInfo.h"
#import "DatabaseHelper.h"
#import "LocationManager.h"

#import "SWRevealViewController.h"
#import "JTCalendar.h"

#import "ReturnOrderVC.h"

#import "NavigationContainerViewController.h"
#import "SideMenuViewController.h"
#import "TutorialScrollViewController.h"
#import "LoginViewController.h"
#import "StartPageViewController.h"
#import "OffDutyViewController.h"
#import "OrderViewController.h"
#import "NewOrdersViewController.h"
#import "ActiveOrdersViewController.h"
#import "OpenOrderDetailViewController.h"
#import "CameraViewController.h"
#import "CameraVC_takePhoto.h"
#import "AccountViewController.h"
#import "ComplainSection.h"
#import "ScanQR.h"
#import "CallNow.h"
#import "Chat_tawkTo.h"
#import "ItinearyOrdersViewController.h"
#import "ItineraryOrder.h"


#import "DepositViewController.h"
#import "ScheduleViewController.h"
#import "ScheduleEdit1ViewController.h"
#import "ScheduleEdit2ViewController.h"
#import "ScheduleFullViewController.h"
#import "ChatChannelsViewController.h"
#import "ChatChannelMessagesViewController.h"
#import "ChatMessagesViewController.h"
#import "ImageViewController.h"
#import "SignatureViewController.h"
#import "ScannerViewController.h"
#import "BarcodeViewController.h"
#import "SummaryViewController.h"
#import "Summary2ViewController.h"
#import "SummaryOrdersViewController.h"
#import "SummaryShiftsViewController.h"
#import "SummaryDetailViewController.h"
#import "SignUpViewController.h"

#import "Joey.h"
#import "Order.h"
#import "Vehicle.h"
#import "NewShift.h"
#import "Channel.h"
#import "Message.h"
#import "Summary.h"
#import "SummaryRecord.h"

#import "OrderTableCell.h"
#import "TwoLabelsTableCell.h"
#import "TaskCompleteTableCell.h"
#import "ConfirmationTableCell.h"
#import "OrderCollectionCell.h"
#import "FourLabelsTableViewCell.h"

#import "RoundedButton.h"
#import "CustomButton.h"
#import "CustomKeyboardToolbar.h"
#import "CustomTextField.h"
#import "WhiteTextField.h"
#import "CustomScrollView.h"
#import "SlidingPagesDataSource.h"
#import "SlidingPagesViewController.h"
#import "GraySegmentedControl.h"
#import "CustomTableView.h"
#import "SignatureView.h"

#endif /* Joey_Bridging_Header_h */
