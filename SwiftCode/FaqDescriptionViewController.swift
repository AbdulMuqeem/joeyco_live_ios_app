//
//  FaqDescriptionViewController.swift
//  Joey
//
//  Created by Muhammad Anas on 29/09/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

import UIKit


struct CellData {
    var opened = Bool()
    var title: String = ""
    var desc: [String] = []
    
}
class FaqDescriptionViewController: UIViewController, UITableViewDelegate,
                                    UITableViewDataSource,JSONHelperDelegate {
    
    
    
    var faqObj: RemoteConfigValue?
    var sprintId: Any? = 0
    var decsTitle: String = "Demo"
    var tempSprintId: Int = 0   //used to convert sprintId from Any to type Int
    //    let cur:[CGFloat] = [100]
    
    
    @IBOutlet weak var title_lbl: UILabel!
    
    @IBOutlet weak var descTableView: UITableView!
    // Data model: These strings will be the data for the table view cells
    var tableViewData = [AnyObject]()
    var dummyTableViewData = [CellData]()
    
    //JSON Helper
    var jsonHelper = JSONHelper()
    
    
    func parsedObjects(_ action: String!, objects list: NSMutableArray!) {
        self.tableViewData = list! as [AnyObject]
        
        var title: String = "title"
        var description: [String] = []
        
        for i in tableViewData {
            
            if let value = i["faq_title"] {
                title = "\(value)"
            }
            
            if let value = i["faq_description"] {
                description = ["\(value)"]
            }
                        
            dummyTableViewData.append(CellData(opened: false,title:title, desc: description))
            //       let storeFirstObjName = self.tableViewData[indexPath.section]
            //            dummyTableViewData = [CellData(opened: false,title:(i["faq_title"] as? String)! ,desc: ["faq_title"]),
            //                                  CellData(opened: false,title: (i["faq_title"] as? String)!,desc: ["asasfasfk ljjsldkf lskdfgj ds;flgj kdflsgj dklsfg kdf kldsfj asdf 1"]),
            //                                  CellData(opened: false,title: (i["faq_title"] as? String)!,desc: ["asaskdlfgj kldsjg kljsdfk ldkfsgj kdfjg kldfg dkf sfasfk ljasdf 1"])]
        }
        self.descTableView.reloadData()
        
        print(tableViewData.count)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //commented by Me
//        self.descTableView.delegate = self
//        self.descTableView.dataSource = self
        
       
        //        dummyTableViewData = [CellData(opened: false,title: "aiod idfoug difguoid ",desc: ["asasfasf sdfgdf gdfg kljasdf 1"]),
        //        CellData(opened: false,title: "what do you want to do ",desc: ["asasfasfk ljjsldkf lskdfgj ds;flgj kdflsgj dklsfg kdf kldsfj asdf 1"]),
        //        CellData(opened: false,title: "a3isud fiog oisduiof iodsfgu  idf dfgio oiudf",desc: ["asaskdlfgj kldsjg kljsdfk ldkfsgj kdfjg kldfg dkf sfasfk ljasdf 1"])]
        
       
        self.title_lbl.text = self.decsTitle
        if let id = self.sprintId {
            self.tempSprintId = id as! Int
        }
        
        
        //Setting Delegate
//        jsonHelper = JSONHelper(self, andDelegate: self)
//        jsonHelper.delegate = self;
        
        //commented by Me
//        callWebservice()
        
    }
    
    
    func callWebservice() {
        jsonHelper.faqDescFetchApi(Int32(tempSprintId))
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dummyTableViewData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dummyTableViewData[section].opened == true {
            return dummyTableViewData[section].desc.count + 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "expandedIdentifierCell") as? ExpandableCell  else {
                return UITableViewCell()
            }
            cell.cellTitle?.text = dummyTableViewData[indexPath.section].title
            //            cell.cellTitle?.text = storeFirstObjName["faq_title"] as? String
            
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "expandedIdentifierCellView") as? ExpandableCell  else {
                return UITableViewCell()
            }
            cell.cellTitle?.text = dummyTableViewData[indexPath.section].desc[indexPath.row - 1]
            //            cell.cellTitle?.text = storeFirstObjName["faq_description"] as? String
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "expandedIdentifierCell") as? ExpandableCell
        //        UIView.animate(withDuration: 2.0, animations: {
        //            cell?.arImage.transform = cell?.arImage.transform.rotated(by: CGFloat(Double.pi / 2)) as! CGAffineTransform
        //        })
        
        
        if indexPath.section < self.descTableView.numberOfSections{
            if indexPath.row < self.descTableView.numberOfRows(inSection: indexPath.section){
                if dummyTableViewData[indexPath.section].opened == true {
                    dummyTableViewData[indexPath.section].opened = false
                    let sections = IndexSet.init(integer: indexPath.section)
                    tableView.reloadSections(sections, with: .none)
                }
                else{
                    dummyTableViewData[indexPath.section].opened = true
                    let sections = IndexSet.init(integer: indexPath.section)
                    tableView.reloadSections(sections, with: .none)
                }
            }
        }
        // cell?.arImage?.rotate(degrees: .pi)
        
        
        
    }
    
    //    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
    ////             switch indexPath.section {
    ////      case 0:
    ////          switch indexPath.row {
    ////          case 0:
    ////              return [44, 44, 44]
    ////          case 1:
    ////            return [50]
    ////          case 2:
    ////              return [33, 33, 33]
    ////          default:
    ////              break
    ////          }
    ////      default:
    ////          break
    ////      }
    ////      return nil
    //
    //        print(" return [50]")
    //        return [150]
    //
    //    }
    //
    //    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
    //        let cell1 = self.descTableView.dequeueReusableCell(withIdentifier: "expandablecellviewidentifier") as! ExpandableCellView
    // let storeFirstObjName = self.tableViewData[indexPath.row]
    //        print(" cell1.cellTitle?.tex")
    //
    //
    //           // set the text from the data model
    //        cell1.descViewCellLbl?.text = storeFirstObjName["faq_description"] as? String
    //
    ////        let cell2 = descTableView.dequeueReusableCell(withIdentifier: "expandedIdentifierCell") as! ExpandableCell
    ////        cell2.cellTitle.text = "Second Expanded Cell"
    ////
    ////        let cell3 = descTableView.dequeueReusableCell(withIdentifier: "expandedIdentifierCell") as! ExpandableCell
    ////        cell3.cellTitle.text = "Third Expanded Cell"
    ////
    ////
    ////       switch indexPath.section {
    ////              case 0:
    ////                  switch indexPath.row {
    ////                  case 0:
    ////                      return [cell1, cell2, cell3]
    ////                  case 1:
    ////                      return [cell1]
    ////                    case 2:
    ////                       return [cell3, cell1]
    ////
    ////                  default:
    ////                      break
    ////                  }
    ////
    ////       default:
    ////        break
    ////        }
    ////              return nil
    //        return [cell1]
    //    }
    //
    //    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
    //        return self.tableViewData.count
    //    }
    //    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //               return 40
    //    }
    //
    //
    //
    //
    //    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //
    //                   // create a new cell if needed or reuse an old one
    //        let cell:ExpandableCell = self.descTableView.dequeueReusableCell(withIdentifier: "expandedIdentifierCell") as! ExpandableCell
    //         let storeFirstObjName = self.tableViewData[indexPath.row]
    //                cell.cellTitle?.text = storeFirstObjName["faq_title"] as? String
    //
    ////        cell.cellTitle.text=self.Dummycategories[indexPath.row]
    //                   return cell
    //    }
    //    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
    //           if let cell = expandedCell as? ExpandableCell {
    //               print("\(cell.cellTitle.text ?? "")")
    //           }
    //       }
    //
    //       func expandableTableView(_ expandableTableView: ExpandableTableView, titleForHeaderInSection section: Int) -> String? {
    //           return nil
    //       }
    //       func expandableTableView(_ expandableTableView: ExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
    //           return 0
    //       }
    ////     @objc(expandableTableView:didCloseRowAt:) func expandableTableView(_ expandableTableView: UITableView, didCloseRowAt indexPath: IndexPath) {
    ////           let cell = expandableTableView.cellForRow(at: indexPath)
    ////           cell?.contentView.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
    ////           cell?.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
    ////       }
    //
    //       func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
    //           return true
    //       }
    //
    ////       func expandableTableView(_ expandableTableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
    //////           let cell = expandableTableView.cellForRow(at: indexPath)
    //////           cell?.contentView.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    //////           cell?.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    ////       }
    //
    //    /*
    //    // MARK: - Navigation
    //
    //    // In a storyboard-based application, you will often want to do a little preparation before navigation
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //        // Get the new view controller using segue.destination.
    //        // Pass the selected object to the new view controller.
    //    }
    //    */
    
}
