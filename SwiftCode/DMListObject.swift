//
//  DMListObject.swift
//  Joey
//
//  Created by Muhammad Faiz Masroor on 06/07/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

import Foundation


// MARK: - DMListObject
struct DMListObject: Codable {
    let id: Int
    let storeName: String
    let sprintID, vendorID: Int
    let courier, joeyName, vendorName, pickupAddress: String
    let dropoffAddress: String
    let weight: Int
    let truckTractor, track, status: String

    enum CodingKeys: String, CodingKey {
        case id
        case storeName = "store_name"
        case sprintID = "sprint_id"
        case vendorID = "vendor_id"
        case courier = "Courier"
        case joeyName = "joey_name"
        case vendorName = "vendor_name"
        case pickupAddress = "pickup_address"
        case dropoffAddress = "dropoff_address"
        case weight = "Weight"
        case truckTractor = "Truck/tractor"
        case track = "Track"
        case status = "Status"
    }
}
