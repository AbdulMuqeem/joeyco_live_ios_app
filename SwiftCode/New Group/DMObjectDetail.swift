// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let dMObjectDetail = try? newJSONDecoder().decode(DMObjectDetail.self, from: jsonData)

import Foundation

// MARK: - DMObjectDetail
struct DMObjectDetail: Codable {
    let id: Int
    let storeName: String
    let sprintID, vendorID: Int
    let courier, joeyName, vendorName, pickupAddress: String
    let dropoffAddress: String
    let weight: Int
    let truckTractor, track, status: String

    enum CodingKeys: String, CodingKey {
        case id
        case storeName = "store_name"
        case sprintID = "sprint_id"
        case vendorID = "vendor_id"
        case courier = "Courier"
        case joeyName = "joey_name"
        case vendorName = "vendor_name"
        case pickupAddress = "pickup_address"
        case dropoffAddress = "dropoff_address"
        case weight = "Weight"
        case truckTractor = "Truck/tractor"
        case track = "Track"
        case status = "Status"
    }
}
