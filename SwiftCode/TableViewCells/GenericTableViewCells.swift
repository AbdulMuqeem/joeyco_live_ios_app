//
//  GenericTableViewCells.swift
//  Joey
//
//  Created by Muhammad Faiz Masroor on 05/07/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

import UIKit

class GenericTableViewCells: UITableViewCell {

    @IBOutlet weak var title_generic: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
