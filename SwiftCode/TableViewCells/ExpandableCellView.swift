//
//  ExpandableCellView.swift
//  Joey
//
//  Created by Muhammad Faiz Masroor on 30/09/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

import UIKit

class ExpandableCellView: UITableViewCell {

    @IBOutlet weak var descViewCellLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
