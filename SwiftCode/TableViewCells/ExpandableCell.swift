//
//  ExpandableCell.swift
//  Joey
//
//  Created by Muhammad Faiz Masroor on 29/09/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

import UIKit
class ExpandableCell: UITableViewCell {
    

    @IBOutlet weak var arImage: UIImageView!
    @IBOutlet weak var cellTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
