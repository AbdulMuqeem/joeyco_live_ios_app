//
//  DigitalManifestDetail.swift
//  Joey
//
//  Created by Muhammad Faiz Masroor on 06/07/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

import UIKit
import SpreadsheetView

class DigitalManifestDetail: UIViewController,SpreadsheetViewDataSource,SpreadsheetViewDelegate,JSONHelperDelegate {
 
    
    @IBOutlet weak var spreadsheetView: SpreadsheetView!
    
    @IBAction func confirmBtnAction(_ sender: Any) {
        
        openSignatureScreen()
        
    }
    var tableData = [AnyObject]()
    
    //JSON Helper
     var jsonHelper = JSONHelper()
     var  alertHelper = AlertHelper();


    
    let Header = [
    "id", "Courier/Driver Name", "Pickup Address", "Dropoff Address", "Weight", "Truck/Tractor#", "Track#", "Status"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let obj = self.tableData[0]
        var store_title = obj["store_name"] as? String

        self.title = store_title!
      //  navigationController?.navigationBar.bar = UIColor.white
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
      

        
//        var emptyDictionary = [Dictionary<String,AnyObject>]()
//
//       // tableData.append(emptyDictionary as AnyObject)
//
//        self.tableData.append(self.tableData[1])
        
         //Setting Delegate
        jsonHelper = JSONHelper(self, andDelegate: self)
        jsonHelper.delegate = self;
        spreadsheetView.dataSource = self
        spreadsheetView.delegate = self

        
        spreadsheetView.register(HourCell.self, forCellWithReuseIdentifier: String(describing: HourCell.self))
        spreadsheetView.register(ChannelCell.self, forCellWithReuseIdentifier: String(describing: ChannelCell.self))
        spreadsheetView.register(BlankCell.self, forCellWithReuseIdentifier: String(describing: BlankCell.self))

    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }

            func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
                return Header.count
            }

            func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
                return tableData.count + 1
            }

            func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
                
                if( column == 0) {
                     return 50
                }
                else if(  column == 4) {
                     return 50
                }
                    
                    
              
                    else if(  column == 2) {
                         return 180
                    }
                    else if(  column == 3) {
                         return 180
                    }
                    else if(  column == 7) {
                         return 150
                    }
                else
                {
                     return 80
                }
             
            }

            func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
              return 40
            }
        
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
//        if indexPath.column == 0 && indexPath.row == 0 {
//            return nil
//        }
    

            
        if indexPath.column >= 0 && indexPath.row == 0 {
               let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: HourCell.self), for: indexPath) as! HourCell
               cell.label.text = Header[indexPath.column]
               cell.gridlines.top = .solid(width: 1, color: .white)
               cell.gridlines.bottom = .solid(width: 1, color: .white)
               return cell
           }
        
        
        if indexPath.column >= 0 && indexPath.row > 0
        {
             let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: ChannelCell.self), for: indexPath) as! ChannelCell
            
            
            let obj = self.tableData[indexPath.row - 1]

//            let obj = storeFirstObjName.object(at: indexPath.row) as? [String: AnyObject]

//            cell.label.text  = Header[indexPath.column]
//
//             cell.label.text  = "faiz"
            if indexPath.column == 0
            {
                
               // cell.label.text = Header[indexPath.row]
                
                let  identity = obj["id"] as? NSNumber
                
//                print (identity as Any)
                cell.label.text  = String.init(describing: identity!)
            }   else   if indexPath.column == 1
            {
                let courierName = obj["Courier"] as? String
                let JoeyName = obj["joey_name"] as? String
                cell.label.text  = courierName! + JoeyName!
            }
                
             else   if indexPath.column == 2
                {
                    
                 let cellValue = obj["pickup_address"] as? String
                  cell.label.text  = cellValue!

                }
                else   if indexPath.column == 3
                              {
                                  
                               let cellValue = obj["dropoff_address"] as? String
                                cell.label.text  = cellValue!

                              }
                else   if indexPath.column == 4
                              {
                                  
                 let cellValue = obj["Weight"] as? NSNumber
                 cell.label.text  = String.init(describing: cellValue!)

                              }
                else   if indexPath.column == 5
                                             {
                                                 
                        let cellValue = obj["Truck/tractor"] as? String
                        cell.label.text  = cellValue!

                                             }
                else   if indexPath.column == 6
                {
                                                               
                  let cellValue = obj["Track"] as? String
                cell.label.text  = cellValue!

          }
                else   if indexPath.column == 7
                              {
                                                                             
                let cellValue = obj["Status"] as? String
                cell.label.text  = cellValue!

                        }
                
            else
            {
                cell.label.text = ""
            }
              return cell
        }

               return spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: BlankCell.self), for: indexPath)

    }

    /*
   
     Open Signature Screen
    */

    func openSignatureScreen()
    {
    
        
        //getting Vendor ID and Sprint ID (Would be used as a TASK ID)
        let obj = self.tableData[0]
        let taskID = obj["sprint_id"] as? NSNumber
        let vendorID = obj["vendor_id"] as? NSNumber
        
        let  joeyId = UrlInfo.getJoeyId() as NSNumber

        
        
    let controller = UIStoryboard.init(name: "Storyboard_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignatureViewController") as? SignatureViewController
        
  controller?.signatureCompletionBlock = { image in
    
    if image != nil {
        let hasInternet = self.jsonHelper.hasInternetConnection(false)
             if hasInternet {
                
                let encodedImage = image?.pngData()?.base64EncodedString(options: .lineLength64Characters)
                
                let jsonDictionary = [
                    "task_id": taskID!,
                    "vendor_id": vendorID!,
                    "joey_id": String.init(describing: joeyId),
                     "image": encodedImage ?? ""
                    ] as [String : Any]
                
                var error: Error? = nil
                var jsonData: Data? = nil
                do {
                    jsonData = try JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
                } catch {
                }
                if error != nil {
                    // If any error occurs then just display its description on the console.
                    print("\(error?.localizedDescription ?? "")")
                }

                
                self.jsonHelper.digitalManifestConfirm(bySignatureApi: jsonData )
      
              } else {
                self.alertHelper.showAlert(withOneButton: NSLocalizedString("Picture not sent", comment: ""), message: NSLocalizedString("Picture confirmation not sent. Please try again.", comment: ""), from: self, buttonTitle: NSLocalizedString("Ok", comment: ""), withHandler: nil)

              }
  }
    
        }
        
     

        DispatchQueue.main.async {
              //your code block
             self.navigationController?.pushViewController(controller!, animated: true)
        }
        }
        
       
    func parsedObjects(_ action: String!, objects list: NSMutableArray!) {
        
        self.alertHelper.showAlert(withOneButton: "Success", message: "Confirmation has been completed", from: self, buttonTitle: "Ok", withHandler: {
            self.navigationController?.popViewController(animated: true)
        })
        
        
//        self.alertHelper.showAlert(withOneButton: NSLocalizedString("Success", comment: ""), message: "Confirmation has completed", comment: ""), from: self, buttonTitle: NSLocalizedString("Ok", comment: ""), withHandler: {
//
//            self.navigationController?.popViewController(animated: true)
//        })
        
     }
    
    func errorMessage(_ action: String!, message: String!) {
        
          self.alertHelper.showAlert(withOneButton: "Error", message: message, from: self, buttonTitle: "ok", withHandler: nil)
    }
    
    
}
