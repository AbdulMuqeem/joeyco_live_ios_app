//
//  ImageHelper.h
//  Joey
//
//  Created by Katia Maeda on 2014-10-22.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

#define IMAGE_MAX_SIZE (1024 * 600)
#define IMAGE_SIDE 1024

typedef void (^ImageDownloadCompletionBlock)(NSURL *imageUrl, BOOL success);

@interface ImageHelper : NSOperation <NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate>

@property (nonatomic, strong) NSString *downloadUrl;
@property (nonatomic, strong) NSString *authorization;
@property (strong) ImageDownloadCompletionBlock completionAction;

-(BOOL)startDownload;
+(NSString *)imagePath:(NSString *)imageUrl;
+(CAGradientLayer *)gradient:(CGRect)imageFrame;
+(void)deleteCache;

+(void)parallaxEffectForView:(UIView *)aView depth:(CGFloat)depth;

+(UIImage *)compress:(UIImage *)original;

+(void)loadImage:(NSString *)imageUrl toGMSMarker:(GMSMarker *)marker authorization:(NSString *)authorization;
+(void)loadImage:(NSString *)imageString toImageView:(UIImageView *)imageView authorization:(NSString *)authorization;
+(void)loadImage:(NSString *)imageUrl toButton:(UIButton *)button authorization:(NSString *)authorization;
+(void)loadImage:(NSString *)imageUrl authorization:(NSString *)authorization withCompletionHandler:(void (^)(UIImage *image))completionHandler;

+(void)saveImage:(UIImage *)image;

+(UIImage *)getRating:(int)value total:(int)total;

@end

