//
//  NSDictionaryHelper.h
//  Joey
//
//  Created by Katia Maeda on 2014-11-20.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary(Helper)

- (id)objForKey:(id)aKey;
- (NSNumber *)numberForKey:(id)aKey;
- (NSDictionary *)dictionaryForKey:(id)aKey;
- (NSArray *)arrayForKey:(id)aKey;

@end
