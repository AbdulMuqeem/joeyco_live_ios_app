//
//  AlertHelper.h
//  Joey
//
//  Created by Katia Maeda on 2014-11-21.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum : NSInteger {
    AlertTypeNone,
    AlertTypeActivityIndicator,
    AlertTypeProgressBar
} AlertType;

@interface AlertHelper : NSObject

-(void)showAlertNoButtons:(NSString *)title message:(NSString *)message dismissAfter:(CGFloat)seconds from:(UIViewController *)viewController;
-(void)showAlertNoButtons:(NSString *)title type:(AlertType)type message:(NSString *)message dismissAfter:(CGFloat)seconds from:(UIViewController *)viewController;
-(void)dismissCustomAlert;

-(void)showAlertWithButtons:(NSString *)title message:(NSString *)message from:(UIViewController *)viewController withOkHandler:(void (^)(void))okHandler;
-(void)showAlertWithOneButton:(NSString *)title message:(NSString *)message from:(UIViewController *)viewController buttonTitle:(NSString *)buttonTitle withHandler:(void (^)(void))okHandler;
-(void)showAlertWithField:(NSString *)title message:(NSString *)message from:(UIViewController *)viewController fieldHint:(NSString *)hint withOkHandler:(void (^)(UITextField *))okHandler;
-(void)showLoadingView:(UIViewController *)viewController;
-(void)showProgressView:(UIViewController *)viewController message:(NSString *)message totalSent:(int64_t)totalSent totalToSend:(int64_t)totalToSend;
-(void)dismissAlert;

@end

@interface CustomAlert : UIView

@property (nonatomic, weak) AlertHelper *delegate;
@property (nonatomic, strong) UIView *customAlertView;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) UITextView *messageView;
@property (nonatomic) BOOL isShowing;
@property (nonatomic) BOOL allowDismiss;

-(id)alert:(UIView *)view type:(AlertType)type withTitle:(NSString *)title withMessage:(NSString *)message progress:(CGFloat)progress allowDismiss:(BOOL)allowDismiss;
-(void)dismissCustomAlert;
-(void)showAlert;
-(void)updateProgress:(CGFloat)progress message:(NSString *)message;

@end