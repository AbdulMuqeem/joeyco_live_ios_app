//
//  NSNumber+Helper.h
//  Joey
//
//  Created by Katia Maeda on 2014-11-06.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#pragma mark - NSNumberHelper Category

@interface NSNull(Helper)

- (float)floatValue;
- (int)intValue;
- (BOOL)boolValue;

@end

