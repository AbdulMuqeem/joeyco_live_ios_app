//
//  GoogleMapDirection.h
//  Joey
//
//  Created by Katia Maeda on 2014-12-17.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "GoogleMapRouteInfo.h"

typedef enum
{
    TravelModeDriving,
    TravelModeBicycling,
    TravelModeTransit,
    TravelModeWalking
}TravelMode;

@interface MapHelper : NSObject <UIAlertViewDelegate>

+(void)getDirectionBetweenLocations:(NSArray *)locations travelMode:(TravelMode)travelMode succeeded:(void(^)(GMSPolyline *polyline))success failed:(void (^)(NSString *errorMsg))failure;
+(void)getGoogleMapRouteInfo:(NSArray *)locations travelMode:(TravelMode)travelMode succeeded:(void(^)(GoogleMapRouteInfo *info))success failed:(void (^)(NSString *errorMsg))failure;

+(double)getDistanceBetweenLocations:(CLLocationCoordinate2D)coord1 and:(CLLocationCoordinate2D) coord2;
+(void)verifyLocationAuthorization;

+(NSString *)stringFromDistance:(NSInteger)distance;

+(GMSCoordinateBounds *)mapFitAllMarkers:(NSArray*)markers;

@end
