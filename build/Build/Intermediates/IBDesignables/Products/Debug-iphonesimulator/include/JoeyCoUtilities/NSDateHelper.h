//
//  NSDateHelper.h
//  Joey
//
//  Created by Katia Maeda on 2015-01-16.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateHelper : NSObject

+(BOOL)isToday:(NSDate *)date;
+(BOOL)isTomorrow:(NSDate *)date;
+(BOOL)isLater:(NSDate *)date plusMinutes:(int)minutes;
+(BOOL)isLater:(NSDate *)date plusSeconds:(int)seconds;
+(BOOL)isLater:(NSDate *)date plusHours:(int)minutes;
+(NSDate *)date:(NSDate *)date plusSeconds:(int)seconds;
+(NSDate *)date:(NSDate *)date plusMinutes:(int)minutes;
+(NSDate *)date:(NSDate *)date plusDays:(int)days;

+(NSDate *)dateFromString:(NSString *)stringDate;
+(NSDate *)dateFromUTCString:(NSString *)stringDate;
+(NSDate *)dateFromNumber:(NSInteger)integerDate;
+(NSString *)stringFromDate:(NSDate *)date;
+(NSString *)stringDateFromDate:(NSDate *)date;
+(NSString *)stringDateFromDate2:(NSDate *)date;
+(NSString *)stringDateFromDate3:(NSDate *)date;
+(NSString *)stringDateFromDate4:(NSDate *)date;
+(NSString *)stringDateTimeFromDate:(NSDate *)date;
+(NSString *)stringDateTimeFromDate2:(NSDate *)date;
+(NSString *)stringDateTimeFromDate3:(NSDate *)date;
+(NSString *)stringDateTimeFromDate4:(NSDate *)date;
+(NSString *)stringTimeFromDate:(NSDate *)date;
+(NSString *)stringTimeFromDate2:(NSDate *)date;
+(NSString *)stringFromCountDown:(NSInteger)countDown;
+(NSString *)stringFromTime:(NSInteger)time;
+(NSInteger)getTime:(NSDate *)date;
+(NSInteger)getFirstHour:(NSDate *)date;
+(NSInteger)getLastHour:(NSDate *)date;
+(NSString *)getFirstHourString:(NSDate *)date;
+(NSString *)getLastHourString:(NSDate *)date;

@end
