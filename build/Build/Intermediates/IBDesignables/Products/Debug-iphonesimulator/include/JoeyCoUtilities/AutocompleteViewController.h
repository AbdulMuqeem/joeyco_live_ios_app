//
//  AutocompleteViewController.h
//  Customer
//
//  Created by Katia Maeda on 2016-04-18.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>

@protocol AutocompleteDelegate <NSObject>
@required
- (void)didAutocompleteWithPlace:(GMSPlace *)place;
@end



@interface AutocompleteViewController : UIViewController

@property (nonatomic, weak) id<AutocompleteDelegate> delegate;

@end
