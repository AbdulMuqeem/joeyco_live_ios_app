//
//  GoogleMapRouteInfo.h
//  Joey
//
//  Created by Katia Maeda on 2015-02-12.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface GoogleMapRouteInfo : NSObject

@property (nonatomic, strong) NSNumber *totalDuration;
@property (nonatomic, strong) NSMutableArray *durations;

@property (nonatomic, strong) NSNumber *totalDistance;
@property (nonatomic, strong) NSMutableArray *distances;

@property (nonatomic, strong) GMSPolyline *polyline;

-(void)addTotalDistance:(NSNumber *)totalDistance;
-(void)addTotalDuration:(NSNumber *)totalDuration;

@end
