//
//  NSStringHelper.h
//  Joey
//
//  Created by Katia Maeda on 2014-11-28.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Helper)

- (NSString *)filterString:(NSString *)validDigits;
- (NSString *)maskString:(NSString *)mask;
- (BOOL)validateEmail;
- (BOOL)validatePhoneNumber;
- (BOOL)validatePostalCode;
- (NSString *)convertHtmlEntity;
-(int)occurrencesOfString:(NSString *)subString;

@end
