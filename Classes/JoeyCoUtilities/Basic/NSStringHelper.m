//
//  NSStringHelper.m
//  Joey
//
//  Created by Katia Maeda on 2014-11-28.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "NSStringHelper.h"

@implementation NSString(Helper)

- (NSString *)filterString:(NSString *)validDigits
{
    NSCharacterSet *validDigitsSet = [NSCharacterSet characterSetWithCharactersInString:validDigits];
    NSArray *nonDigits = [self componentsSeparatedByCharactersInSet:validDigitsSet];
    
    NSString *newString = self;
    for (NSString *separator in nonDigits)
    {
        newString = [newString stringByReplacingOccurrencesOfString:separator withString:@""];
    }

    return newString;
}

- (NSString *)maskString:(NSString *)mask
{
    NSString *result = @"";
    if (!self || self.length == 0)
    {
        return result;
    }
    
    NSInteger length = [mask length];
    NSInteger j = 0;

    for (int i = 0; i < length; i++)
    {
        char c = [mask characterAtIndex:i];
        
        if (c != '#')
        {
            result = [result stringByAppendingString:[NSString stringWithFormat:@"%c" , c]];
        }
        else
        {
            
            result = [result stringByAppendingString:[NSString stringWithFormat:@"%c" , [self characterAtIndex:j]]];
            j++;
            if (j >= [self length])
            {
                j = [self length];
            }
        }
    }
    
    return result;
}

-(BOOL)validateEmail
{
//    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *emailRegex = @"(([^<>()\\[\\]\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (BOOL)validatePhoneNumber
{
    NSString *str=@"\\d{10,15}";
    NSPredicate *no=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",str];
    return [no evaluateWithObject:self];
}

- (BOOL)validatePostalCode
{
    NSString *str=@"([ABCEGHJKLMNPRSTVXY]\\d[ABCEGHJKLMNPRSTVWXYZ]) ?(\\d[ABCEGHJKLMNPRSTVWXYZ]\\d)";
    NSPredicate *no=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",str];
    return [no evaluateWithObject:self];
}

- (NSString *)convertHtmlEntity
{
//    NSData *stringData = [original dataUsingEncoding:NSUTF8StringEncoding];
//    NSDictionary *options = @{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType};
//    NSAttributedString *decodedString = [[NSAttributedString alloc] initWithString:original attributes:options];
//    return decodedString.string;
    
//    NSRange rangeOfHTMLEntity = [self rangeOfString:@"&#"];
//    if( NSNotFound == rangeOfHTMLEntity.location ) {
//        return self;
//    }
//    
//    
//    NSMutableString* answer = [[NSMutableString alloc] init];
//
//    NSScanner* scanner = [NSScanner scannerWithString:self];
//    [scanner setCharactersToBeSkipped:nil]; // we want all white-space
//    
//    while( ![scanner isAtEnd] ) {
//        
//        NSString* fragment;
//        [scanner scanUpToString:@"&#" intoString:&fragment];
//        if( nil != fragment ) { // e.g. '&#38; B'
//            [answer appendString:fragment];
//        }
//        
//        if( ![scanner isAtEnd] ) { // implicitly we scanned to the next '&#'
//            
//            int scanLocation = (int)[scanner scanLocation];
//            [scanner setScanLocation:scanLocation+2]; // skip over '&#'
//            
//            int htmlCode;
//            if( [scanner scanInt:&htmlCode] ) {
//                char c = htmlCode;
//                [answer appendFormat:@"%c", c];
//                
//                scanLocation = (int)[scanner scanLocation];
//                [scanner setScanLocation:scanLocation+1]; // skip over ';'
//                
//            } else {
//                // err ? 
//            }
//        }
//    }
//    
//    return answer;
    
    return self;
}

-(int)occurrencesOfString:(NSString *)subString
{
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:subString options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:self options:0 range:NSMakeRange(0, [self length])];
    return (int)numberOfMatches;
}

@end
