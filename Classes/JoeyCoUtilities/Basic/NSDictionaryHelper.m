//
//  NSDictionaryHelper.m
//  Joey
//
//  Created by Katia Maeda on 2014-11-20.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "NSDictionaryHelper.h"

@implementation NSDictionary(Helper)

- (id)objForKey:(id)aKey
{
    if (!aKey)
    {
        NSLog(@"NULL trying to retrieve from dicionary with nil key");
        return nil;
    }
    
    id obj = [self objectForKey:aKey];
    if (!obj) return @"";
    if ([obj isEqual:[NSNull null]]) return @"";
    
    return obj;
}

- (NSNumber *)numberForKey:(id)aKey
{
    if (!aKey)
    {
        NSLog(@"NULL trying to retrieve from dicionary with nil key");
        return nil;
    }
    
    id obj = [self objectForKey:aKey];
    if (!obj) return [NSNumber numberWithInt:0];
    if ([obj isEqual:[NSNull null]]) return [NSNumber numberWithInt:0];
    
    if ([obj isKindOfClass:[NSString class]])
    {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        return [formatter numberFromString:obj];
    }
    
    if ([obj isKindOfClass:[NSNumber class]])
    {
        return obj;
    }
    
    return [NSNumber numberWithInt:0];
}

- (NSDictionary *)dictionaryForKey:(id)aKey
{
    if (!aKey)
    {
        NSLog(@"NULL trying to retrieve from dicionary with nil key");
        return nil;
    }
    
    id obj = [self objectForKey:aKey];
    if (!obj || [obj isEqual:[NSNull null]] || [obj isKindOfClass:[NSString class]])
    {
        if ([aKey isEqualToString:@"response"])
        {
            //NSLog(@"Big Problem: response null");
        }
        return nil;
    }

    if ([obj isKindOfClass:[NSDictionary class]])
    {
        return obj;
    }
    
    return nil;
}

- (NSArray *)arrayForKey:(id)aKey
{
    if (!aKey)
    {
        NSLog(@"NULL trying to retrieve from dicionary with nil key");
        return nil;
    }
    
    id obj = [self objectForKey:aKey];
    if (!obj || [obj isEqual:[NSNull null]] || [obj isKindOfClass:[NSString class]])
    {
        if ([aKey isEqualToString:@"response"])
        {
            NSLog(@"Big Problem: response null");
        }
        return nil;
    }
    
    if ([obj isKindOfClass:[NSArray class]])
    {
        return obj;
    }
    
    return nil;
}

@end
