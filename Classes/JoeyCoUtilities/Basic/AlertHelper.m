//
//  AlertHelper.m
//  Joey
//
//  Created by Katia Maeda on 2014-11-21.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "AlertHelper.h"

@interface AlertHelper () <UIAlertViewDelegate>

@property (nonatomic, weak) UIViewController *viewController;

@property (nonatomic, strong) UIAlertController *alertController;
@property (nonatomic, strong) UIAlertView *alertView;
@property (nonatomic, strong) CustomAlert *customAlert;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) float timerSeconds;

@property (copy) void (^okClickHandler)(void);
@property (copy) void (^ok2ClickHandler)(UITextField *);

@end

@implementation AlertHelper

- (id)init
{
    return self;
}

-(void)dealloc
{
    [self.timer invalidate];
    self.alertView.delegate = nil;
}

-(void)showAlertWithButtons:(NSString *)title message:(NSString *)message from:(UIViewController *)viewController withOkHandler:(void (^)(void))okHandler
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissAlert];
        
        if ([UIAlertController class])
        {
            self.alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil) style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 if(okHandler) okHandler();
                                                             }];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
            
            [self.alertController addAction:okAction];
            [self.alertController addAction:cancelAction];
            
            [viewController presentViewController:self.alertController animated:YES completion:nil];
            
        }
        else
        {
            #ifndef WIDGET_APP_EXTENSIONS
            self.alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
            self.okClickHandler = okHandler;
            [self.alertView show];
            #endif
        }
    });
}

-(void)showAlertWithOneButton:(NSString *)title message:(NSString *)message from:(UIViewController *)viewController buttonTitle:(NSString *)buttonTitle withHandler:(void (^)(void))okHandler
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissAlert];
        
        if ([UIAlertController class])
        {
            self.alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 if(okHandler) okHandler();
                                                             }];
            [self.alertController addAction:okAction];
            
            [viewController presentViewController:self.alertController animated:YES completion:nil];
            
        }
        else
        {
            #ifndef WIDGET_APP_EXTENSIONS
            self.alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:buttonTitle, nil];
            self.okClickHandler = okHandler;
            [self.alertView show];
            #endif
        }
    });
}

-(void)showAlertWithField:(NSString *)title message:(NSString *)message from:(UIViewController *)viewController fieldHint:(NSString *)hint withOkHandler:(void (^)(UITextField *))okHandler
{
    dispatch_async(dispatch_get_main_queue(), ^{
    [self dismissAlert];
    
    if ([UIAlertController class])
    {
        self.alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil) style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             if(okHandler) okHandler(self.alertController.textFields.firstObject);
                                                         }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action") style:UIAlertActionStyleCancel handler:nil];
        
        [self.alertController addAction:okAction];
        [self.alertController addAction:cancelAction];
        [self.alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
         {
             textField.placeholder = hint;
         }];
        
            [viewController presentViewController:self.alertController animated:YES completion:nil];
        
    }
    else
    {
#ifndef WIDGET_APP_EXTENSIONS
        self.alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
        self.alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        UITextField *textField = [self.alertView textFieldAtIndex:0];
        textField.placeholder = hint;
        self.ok2ClickHandler = okHandler;
        [self.alertView show];
#endif
    }
        });
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    NSString *okTitle = NSLocalizedString(@"Ok", nil);
    if ([[buttonTitle lowercaseString] isEqualToString:[okTitle lowercaseString]])
    {
        if (self.okClickHandler)
        {
            self.okClickHandler();
        }
        else if (self.ok2ClickHandler)
        {
            if (alertView.alertViewStyle == UIAlertViewStylePlainTextInput)
            {
                self.ok2ClickHandler([alertView textFieldAtIndex:0]);
            }
        }
    }
}

-(void)dismissAlert
{
    if ([UIAlertController class])
    {
        [self.alertController dismissViewControllerAnimated:NO completion:nil];
    }
    
    if (self.alertView)
    {
        [self.alertView dismissWithClickedButtonIndex:0 animated:YES];
        self.alertView = nil;
    }
    
    [self dismissCustomAlert];
    [self.timer invalidate];
}

#pragma mark - CustomAlert

-(void)showLoadingView:(UIViewController *)viewController
{
    [self dismissAlert];
    
    NSString *alertTitle = NSLocalizedString(@"Please wait", nil);
    NSString *alertMessage = NSLocalizedString(@"Processing\n", nil);
    [self showAlertNoButtons:alertTitle type:AlertTypeActivityIndicator message:alertMessage dismissAfter:-1 from:viewController allowDismiss:NO];
}

-(void)showProgressView:(UIViewController *)viewController message:(NSString *)message totalSent:(int64_t)totalSent totalToSend:(int64_t)totalToSend
{
    NSString *alertTitle = NSLocalizedString(@"Processing", nil);
    NSString *alertMessage = message;

    self.viewController = viewController;
    self.timerSeconds = -1;
    
    if (!self.customAlert)
    {
        [self dismissAlert];
        self.customAlert = [[CustomAlert alloc] alert:viewController.view type:AlertTypeProgressBar withTitle:alertTitle withMessage:alertMessage progress:(CGFloat)totalSent/totalToSend allowDismiss:NO];
        self.customAlert.delegate = self;
        [self.customAlert showAlert];
    }
    else
    {
        [self.customAlert updateProgress:(CGFloat)totalSent/totalToSend message:message];
    }
}

-(void)showAlertNoButtons:(NSString *)title message:(NSString *)message dismissAfter:(CGFloat)seconds from:(UIViewController *)viewController
{
    [self showAlertNoButtons:title type:AlertTypeNone message:message dismissAfter:seconds from:viewController];
}

-(void)showAlertNoButtons:(NSString *)title type:(AlertType)type message:(NSString *)message dismissAfter:(CGFloat)seconds from:(UIViewController *)viewController
{
    [self showAlertNoButtons:title type:type message:message dismissAfter:seconds from:viewController progress:0 allowDismiss:YES];
}

-(void)showAlertNoButtons:(NSString *)title type:(AlertType)type message:(NSString *)message dismissAfter:(CGFloat)seconds from:(UIViewController *)viewController allowDismiss:(BOOL)allowDismiss
{
    [self showAlertNoButtons:title type:type message:message dismissAfter:seconds from:viewController progress:0 allowDismiss:allowDismiss];
}

-(void)showAlertNoButtons:(NSString *)title type:(AlertType)type message:(NSString *)message dismissAfter:(CGFloat)seconds from:(UIViewController *)viewController progress:(CGFloat)progress allowDismiss:(BOOL)allowDismiss
{
    [self dismissAlert];
    
    self.viewController = viewController;
    self.timerSeconds = seconds;
    
    self.customAlert = [[CustomAlert alloc] alert:viewController.view type:type withTitle:title withMessage:message progress:progress allowDismiss:allowDismiss];
    self.customAlert.delegate = self;

    [self.customAlert showAlert];
    if (self.timerSeconds > 0)
    {
        [self.timer invalidate];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:self.timerSeconds target:self selector:@selector(dismissCustomAlert) userInfo:nil repeats:NO];
    }
}

-(void)dismissCustomAlert
{
    [self.customAlert dismissCustomAlert];
    self.customAlert = nil;
}

-(void)backgroundClicked
{
    [self.timer invalidate];
    [self dismissCustomAlert];
}

@end

@implementation CustomAlert

-(id)alert:(UIView *)view type:(AlertType)type withTitle:(NSString *)title withMessage:(NSString *)message progress:(CGFloat)progress allowDismiss:(BOOL)allowDismiss
{
    UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
    CGRect windowRect = appWindow.frame;

    self.customAlertView = [[UIView alloc] initWithFrame:windowRect];

    UIView *backgroundView = [[UIView alloc] initWithFrame:windowRect];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0.3;
    [self.customAlertView addSubview:backgroundView];
    
    CGFloat alertMargim = 12;
    CGFloat alertWidth = 4*windowRect.size.width/5;
    CGFloat alertHeight = windowRect.size.height - 2*alertMargim;
    CGFloat alertX = alertWidth - 2*alertMargim;

    UITextView *titleView = [[UITextView alloc] init];
    [titleView setEditable:NO];
    [titleView setSelectable:NO];
    titleView.text = title;
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [UIColor blackColor];
    titleView.userInteractionEnabled = NO;
    titleView.font = [UIFont boldSystemFontOfSize:15];
    CGSize titleViewFrame = [titleView sizeThatFits:CGSizeMake(alertX, alertHeight)];
    titleView.frame = CGRectMake(alertWidth/2 - titleViewFrame.width/2, alertMargim, titleViewFrame.width, titleViewFrame.height);

    self.messageView = [[UITextView alloc] init];
    self.messageView.text = message;
    self.messageView.textAlignment = NSTextAlignmentCenter;
    self.messageView.textColor = [UIColor blackColor];
    self.messageView.userInteractionEnabled = NO;
    self.messageView.font = [UIFont systemFontOfSize:13];
    CGSize messageViewFrame = [self.messageView sizeThatFits:CGSizeMake(alertX, alertHeight)];
    self.messageView.frame = CGRectMake(0, alertMargim + titleViewFrame.height, alertWidth, messageViewFrame.height);
    
    CGFloat loaderHeight = 0;
    UIActivityIndicatorView *activityIndicator = nil;
    self.progressView = nil;
    if (type == AlertTypeActivityIndicator)
    {
        activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.center = CGPointMake(alertWidth/2, self.messageView.frame.origin.y + self.messageView.frame.size.height);
        activityIndicator.color = [UIColor blackColor];
        [activityIndicator startAnimating];
        loaderHeight = 25;
    } else if (type == AlertTypeProgressBar)
    {
        self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
        CGSize progressViewFrame = CGSizeMake(alertWidth - 2*alertMargim, 15);
        self.progressView.frame = CGRectMake(alertMargim, self.messageView.frame.origin.y + self.messageView.frame.size.height, progressViewFrame.width, progressViewFrame.height);
        self.progressView.progress = progress;
        loaderHeight = 25;
    }
    
    alertHeight = 2*alertMargim + titleViewFrame.height + messageViewFrame.height + loaderHeight;

    CGPoint alertPoint = CGPointMake(windowRect.size.width/2 - alertWidth/2, windowRect.size.height/2 - alertHeight/2);

    UIView *alertView = [[UIView alloc] initWithFrame:CGRectMake(alertPoint.x, alertPoint.y, alertWidth, alertHeight)];
    alertView.backgroundColor = [UIColor whiteColor];
    alertView.layer.cornerRadius = 10.0;
    alertView.alpha = 0.9;

    [alertView addSubview:titleView];
    [alertView addSubview:self.messageView];
    if (activityIndicator != nil)
    {
        [alertView addSubview:activityIndicator];
    }
    if (self.progressView != nil)
    {
        [alertView addSubview:self.progressView];
    }
    [self.customAlertView addSubview:alertView];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognizer)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [self.customAlertView addGestureRecognizer:tapGesture];

    [appWindow addSubview:self.customAlertView];
    
    [self.customAlertView setAlpha:0.0];
    self.isShowing = NO;
    self.allowDismiss = allowDismiss;
    
    return self;
}

-(void)showAlert
{
    [self.customAlertView setAlpha:1.0];
    self.isShowing = YES;
}

- (void)dismissCustomAlert
{
    self.isShowing = NO;
    [UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.customAlertView setAlpha:0.0];
                     }
                     completion:^(BOOL finished) {
                         [self.customAlertView removeFromSuperview];
                     }];
}

-(void)tapRecognizer
{
    if (self.allowDismiss)
    {
        [self.delegate backgroundClicked];
    }
}

-(void)updateProgress:(CGFloat)progress message:(NSString *)message
{
    if (self.progressView)
    {
        self.progressView.progress = progress;
        self.messageView.text = message;
    }
}

@end
