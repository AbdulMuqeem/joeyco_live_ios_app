//
//  CryptographyHelper.h
//  JoeyCoUtilities
//
//  Created by Katia Maeda on 2016-01-12.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CryptographyHelper : NSObject

+(NSString *)getHashWithKey:(NSString *)key data:(NSString *)data;

@end
