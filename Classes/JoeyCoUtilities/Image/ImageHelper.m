//
//  ImageHelper.m
//  Joey
//
//  Created by Katia Maeda on 2014-10-22.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ImageHelper.h"


@implementation ImageHelper
{
    NSURLSessionDownloadTask *sessionDownloadTask;
}

-(BOOL)startDownload
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *urls = [fileManager URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
    NSURL *documentsDirectory = [urls objectAtIndex:0];
    NSString *imageName = [self.downloadUrl lastPathComponent];
    NSRange range = [imageName rangeOfString:@"pin=1"];
    if (NSNotFound != range.location)
    {
        NSArray<NSString *> *splited = [imageName componentsSeparatedByString:@"?"];
        imageName = [NSString stringWithFormat:@"pin-%@", [splited firstObject]];
    }

    NSURL *destinationUrl = [documentsDirectory URLByAppendingPathComponent:imageName];
    
    if ([fileManager fileExistsAtPath:destinationUrl.path])
    {
        return YES;
    }

    NSOperationQueue *operationQueue = [NSOperationQueue new];
    [operationQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
    [operationQueue addOperation:self];
    
    return NO;
}

+(NSString *)imagePath:(NSString *)imageUrl
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *urls = [fileManager URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
    NSURL *documentsDirectory = [urls objectAtIndex:0];
    
    NSString *imageName = [imageUrl lastPathComponent];
    NSRange range = [imageName rangeOfString:@"pin=1"];
    if (NSNotFound != range.location)
    {
        NSArray<NSString *> *splited = [imageName componentsSeparatedByString:@"?"];
        imageName = [NSString stringWithFormat:@"pin-%@", [splited firstObject]];
    }
    
    NSURL *destinationUrl = [documentsDirectory URLByAppendingPathComponent:imageName];
    
    return destinationUrl.path;
}

+(CAGradientLayer *)gradient:(CGRect)imageFrame
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, imageFrame.size.width, imageFrame.size.height);

    UIColor *centerColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    UIColor *endColor = [UIColor colorWithRed:.24 green:.24 blue:.24 alpha:.7];
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[centerColor CGColor],
                       (id)[centerColor CGColor],
                       (id)[endColor CGColor],
                       nil];
    
    return gradient;
}

+(void)deleteCache
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    if ([paths count] > 0)
    {
        NSLog(@"Path: %@", [paths objectAtIndex:0]);
        
        NSError *error = nil;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Remove Documents directory and all the files
        BOOL deleted = [fileManager removeItemAtPath:[paths objectAtIndex:0] error:&error];
        
        if (deleted != YES || error != nil)
        {
            // Deal with the error...
        }
        else
        {
            // Recreate the Documents directory
            [fileManager createDirectoryAtPath:[paths objectAtIndex:0] withIntermediateDirectories:NO attributes:nil error:&error];
        }
    }
}


+(UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(void)saveImage:(UIImage *)image
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Image.png"];
    [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
}

+(UIImage *)getRating:(int)value total:(int)total
{
    CGFloat starWidth = 16;
    CGFloat starHeight = 14;
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(total*starWidth, starHeight), NO, 0.0);
    UIImage *starOff = [UIImage imageNamed:@"rating-star-off.png"];
    UIImage *starOn = [UIImage imageNamed:@"rating-star-on.png"];
    CGFloat posX = 0;
    for (int i = 0; i < value; i++)
    {
        [starOn drawInRect:CGRectMake(posX, 0, starWidth, starHeight)];
        posX += starWidth;
    }
    for (int i = value; i < total; i++)
    {
        [starOff drawInRect:CGRectMake(posX, 0, starWidth, starHeight)];
        posX += starWidth;
    }
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Compressing

+(UIImage *)compress:(UIImage *)original
{
    if (!original)
    {
        return nil;
    }
    
    // Resize
    CGSize originalSize = original.size;
    CGFloat resizeHeight = IMAGE_SIDE;
    CGFloat resizedWidth = originalSize.width * IMAGE_SIDE / originalSize.height;
    
    if (originalSize.width > originalSize.height)
    {
        resizedWidth = IMAGE_SIDE;
        resizeHeight = originalSize.height * IMAGE_SIDE / originalSize.width;
    }
    
    CGSize newSize = CGSizeMake(resizedWidth, resizeHeight);
    UIGraphicsBeginImageContext(newSize);
    [original drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Compress
    CGFloat quality = 1.0;
    NSData *imageData = UIImageJPEGRepresentation(resizedImage, quality);
    
    while (imageData.length > IMAGE_MAX_SIZE)
    {
        quality -= 0.1;
        imageData = UIImageJPEGRepresentation(resizedImage, quality);
    }
    
    return [UIImage imageWithData:imageData];
}

#pragma mark - NSOperation

- (void)start
{
    if (!self.isCancelled)
    {
        NSURLSessionConfiguration *backgroundConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *backgroundSession = [NSURLSession sessionWithConfiguration:backgroundConfiguration delegate:self delegateQueue:nil];

        NSURL *downloadURL = [NSURL URLWithString:self.downloadUrl];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:downloadURL];
        if (self.authorization && ![self.authorization isEqualToString:@""])
        {
            [request setValue:self.authorization forHTTPHeaderField:@"Authorization"];
        }
        
        sessionDownloadTask = [backgroundSession downloadTaskWithRequest:request];
        [sessionDownloadTask resume];
    }
}

#pragma mark - NSURLSessionDownloadDelegate
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *urls = [fileManager URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
    NSURL *documentsDirectory = [urls objectAtIndex:0];
    
    NSURL *originalUrl = [[downloadTask originalRequest] URL];
    NSString *imageName = [originalUrl lastPathComponent];
    NSRange range = [originalUrl.absoluteString rangeOfString:@"pin=1"];
    if (NSNotFound != range.location)
    {
        NSArray<NSString *> *splited = [imageName componentsSeparatedByString:@"?"];
        imageName = [NSString stringWithFormat:@"pin-%@", [splited firstObject]];
    }
    
    NSURL *destinationUrl = [documentsDirectory URLByAppendingPathComponent:imageName];
    NSError *error;
    
    [fileManager removeItemAtURL:destinationUrl error:NULL];
    BOOL success = [fileManager copyItemAtURL:location toURL:destinationUrl error:&error];
    if (success)
    {
        if (self.completionAction)
        {
            self.completionAction(destinationUrl, success);
        }
    }
    else
    {
        // try again
        error = nil;
        success = [fileManager copyItemAtURL:location toURL:destinationUrl error:&error];
        
        if (success && self.completionAction)
        {
            self.completionAction(destinationUrl, success);
        }
        else
        {
            NSLog(@"Image error: %@", [error localizedDescription]);
        }
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
}

#pragma mark - NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    sessionDownloadTask = nil;
}

#pragma mark - Parallax

+(void)parallaxEffectForView:(UIView *)aView depth:(CGFloat)depth
{
    UIInterpolatingMotionEffect *effectX;
    UIInterpolatingMotionEffect *effectY;
    effectX = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x"
                                                              type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    effectY = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y"
                                                              type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    
    
    effectX.maximumRelativeValue = @(depth);
    effectX.minimumRelativeValue = @(-depth);
    effectY.maximumRelativeValue = @(depth);
    effectY.minimumRelativeValue = @(-depth);
    
    [aView addMotionEffect:effectX];
    [aView addMotionEffect:effectY];
}

#pragma mark - Load images

+(void)loadImage:(NSString *)imageUrl toGMSMarker:(GMSMarker *)marker authorization:(NSString *)authorization
{
    marker.icon = nil;
    
    if (!imageUrl)
    {
        return;
    }
    
    // Download Image
    ImageHelper *operation = [ImageHelper new];
    operation.downloadUrl = imageUrl;
    operation.authorization = authorization;
    operation.completionAction = ^(NSURL *imageUrl, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success)
            {
                marker.icon = [UIImage imageWithContentsOfFile:[imageUrl path]];
            }
        });
    };
    BOOL alreadyDownloaded = [operation startDownload];
    
    if (alreadyDownloaded)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            marker.icon = [UIImage imageWithContentsOfFile:[ImageHelper imagePath:imageUrl]];
        });
    }
}

+(void)loadImage:(NSString *)imageString toImageView:(UIImageView *)imageView authorization:(NSString *)authorization
{
    imageView.image = nil;
    
    if (!imageString)
    {
        return;
    }
    
    // Download Image
    ImageHelper *operation = [ImageHelper new];
    operation.downloadUrl = imageString;
    operation.authorization = authorization;
    operation.completionAction = ^(NSURL *imageUrl, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success)
            {
                imageView.image = [UIImage imageWithContentsOfFile:[imageUrl path]];
            }
        });
    };
    BOOL alreadyDownloaded = [operation startDownload];
    
    if (alreadyDownloaded)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            imageView.image = [UIImage imageWithContentsOfFile:[ImageHelper imagePath:imageString]];
        });
    }
}

/*Aspect fit 
 set [self.button setContentMode:UIViewContentModeScaleAspectFit];*/
+(void)loadImage:(NSString *)imageUrl toButton:(UIButton *)button authorization:(NSString *)authorization
{
    [button setBackgroundImage:nil forState:UIControlStateNormal];
    
    if (!imageUrl)
    {
        return;
    }
    
    ImageHelper *operation = [ImageHelper new];
    operation.downloadUrl = imageUrl;
    operation.authorization = authorization;
    operation.completionAction = ^(NSURL *imageUrl, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success)
            {
                UIImage *image = [[UIImage imageWithContentsOfFile:[imageUrl path]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                [button setImage:image forState:UIControlStateNormal];
            }
        });
    };
    BOOL alreadyDownloaded = [operation startDownload];
    
    if (alreadyDownloaded)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *image = [[UIImage imageWithContentsOfFile:[ImageHelper imagePath:imageUrl]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            [button setImage:image forState:UIControlStateNormal];
        });
    }
}

+(void)loadImage:(NSString *)imageUrl authorization:(NSString *)authorization withCompletionHandler:(void (^)(UIImage *image))completionHandler
{
    if (!imageUrl)
    {
        return;
    }
    
    ImageHelper *operation = [ImageHelper new];
    operation.downloadUrl = imageUrl;
    operation.authorization = authorization;
    operation.completionAction = ^(NSURL *imageUrl, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success)
            {
                UIImage *loadedImage = [[UIImage imageWithContentsOfFile:[imageUrl path]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                completionHandler(loadedImage);
            }
        });
    };
    BOOL alreadyDownloaded = [operation startDownload];
    
    if (alreadyDownloaded)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *loadedImage = [[UIImage imageWithContentsOfFile:[ImageHelper imagePath:imageUrl]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            completionHandler(loadedImage);
        });
    }
}

@end
