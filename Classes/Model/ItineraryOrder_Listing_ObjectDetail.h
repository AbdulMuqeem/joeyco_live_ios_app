//
//  ItineraryOrder_STORE.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 17/05/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Task.h"
#import "Joey.h"
#import "Location_itinearyOrders.h"

NS_ASSUME_NONNULL_BEGIN

@interface ItineraryOrder_Listing_ObjectDetail : NSObject

@property (strong, nonatomic) NSNumber *taskId;
@property (strong, nonatomic) NSNumber *sprintId;

@property (strong, nonatomic) NSString *num;
@property (strong, nonatomic) NSString *pin;
@property (nonatomic) BOOL confirm_signature;
@property (nonatomic) BOOL confirm_pin;
@property (nonatomic) BOOL confirm_image;

@property (nonatomic) BOOL has_picked;
@property (nonatomic) BOOL returned;


@property (strong, nonatomic) NSString *hub_title;
@property (strong, nonatomic) NSString *hub_address;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *start_time;
@property (strong, nonatomic) NSString *end_time;
@property (strong, nonatomic) NSString *merchant_order_num;
@property (strong, nonatomic) NSString *tracking_id;

@property (strong, nonatomic) Contact *contact;
@property (nonatomic, strong) Location_itinearyOrders *location;
@property (strong, nonatomic) NSMutableArray *confirmations;


@end

NS_ASSUME_NONNULL_END
