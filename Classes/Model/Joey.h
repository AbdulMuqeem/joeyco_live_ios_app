//
//  Joey.h
//  JoeyCo
//
//  Created by Katia Maeda on 2014-09-28.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "Address.h"

@interface Joey : NSObject //UserInfo

@property (nonatomic, strong) NSNumber *joeyId;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *about;
@property (nonatomic, strong) NSNumber *vehicleId;
@property (nonatomic, strong) NSNumber *duty;
@property (nonatomic, strong) NSString *image;

@property (nonatomic, strong) NSString *publicKey;
@property (nonatomic, strong) NSString *privateKey;

@property (nonatomic, strong) Address *location;

@property (nonatomic, strong) NSString *address;

@end
