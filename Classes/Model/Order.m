//
//  Order.m
//  JoeyCo
//
//  Created by Katia Maeda on 2014-09-28.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "Order.h"
#import "OrderStatus.h"
#import <CoreLocation/CoreLocation.h>
#import "JSONHelper.h"

@implementation Order
{
    
    JSONHelper *json;

}

-(NSMutableArray *)tasksLocationList
{
    NSMutableArray *list = [NSMutableArray array];
    for (Task *task in self.tasks)
    {
        if (![task isAllConfirmed])
        {
            CLLocationCoordinate2D locationCoordinate = CLLocationCoordinate2DMake([task.location.locationLatitude doubleValue], [task.location.locationLongitude doubleValue]);
            CLLocation *location = [[CLLocation alloc] initWithLatitude:locationCoordinate.latitude longitude:locationCoordinate.longitude];
            [list addObject:location];
        }
   }
    
    return list;
}

-(NSMutableArray *)pickupLocationList
{
    NSMutableArray *pickupList = [NSMutableArray array];
    for (Task *task in self.tasks)
    {
        if ([task.type isEqualToString:@TASK_PICKUP])
        {
            CLLocationCoordinate2D locationCoordinate = CLLocationCoordinate2DMake([task.location.locationLatitude doubleValue], [task.location.locationLongitude doubleValue]);
            CLLocation *location = [[CLLocation alloc] initWithLatitude:locationCoordinate.latitude longitude:locationCoordinate.longitude];
            [pickupList addObject:location];
        }
    }
    
    return pickupList;
}

-(NSMutableArray *)pickupList
{
    NSMutableArray *pickupList = [NSMutableArray array];
    for (Task *task in self.tasks)
    {
        if ([task.type isEqualToString:@TASK_PICKUP])
        {
            [pickupList addObject:task];
        }
    }
    
    return pickupList;
}

-(NSMutableArray *)dropOffList
{
    NSMutableArray *pickupList = [NSMutableArray array];
    for (Task *task in self.tasks)
    {
        if ([task.type isEqualToString:@TASK_DROP_OFF])
        {
            [pickupList addObject:task];
        }
    }
    
    return pickupList;
}

-(Task *)getNextTask
{
    Task *nextTask = nil;

    for (Task *task in self.tasks)
    {
        if (![task isAllConfirmed])
        {
            nextTask = task;
            break;
        }
    }
    
    return nextTask;
}




/*
 Status Updates - CANCELLATION
 */

-(NSMutableArray *)arr_cancellation_status :(UIViewController *)VC{
    
    NSMutableArray *status = [[ NSMutableArray alloc]init];
    
    json = [[JSONHelper alloc] init:VC andDelegate:nil];
    
    
    
    for (NSDictionary *obj_orderStatus in self.cancellation_status)
    {
        
         [status addObject:[json ParseOrderStatusObject:obj_orderStatus]];

    }
    return status;
}




/*
 Status Updates - DELAY
 */

-(NSMutableArray *)arr_delay_status :(UIViewController *)VC{
    
    NSMutableArray *status = [[ NSMutableArray alloc]init];
    
    json = [[JSONHelper alloc] init:VC andDelegate:nil];
    
    
    
    for (NSDictionary *obj_orderStatus in self.delay_status)
    {
        
        [status addObject:[json ParseOrderStatusObject:obj_orderStatus]];
        
    }
    return status;
}






/*
 Status Updates - PICK
 */

-(NSMutableArray *)arr_pickup_status :(UIViewController *)VC{
    
    NSMutableArray *status = [[ NSMutableArray alloc]init];
    
    json = [[JSONHelper alloc] init:VC andDelegate:nil];
    
    
    
    for (NSDictionary *obj_orderStatus in self.pickup_status)
    {
        
        [status addObject:[json ParseOrderStatusObject:obj_orderStatus]];
        
    }
    return status;
}






/*
 Status Updates - DROP
 */

-(NSMutableArray *)arr_dropoff_status :(UIViewController *)VC{
    
    NSMutableArray *status = [[ NSMutableArray alloc]init];
    
    json = [[JSONHelper alloc] init:VC andDelegate:nil];
    
    
    
    for (NSDictionary *obj_orderStatus in self.dropoff_status)
    {
        
        [status addObject:[json ParseOrderStatusObject:obj_orderStatus]];
        
    }
    return status;
}


@end
