//
//  Task.h
//  Joey
//
//  Created by Katia Maeda on 2015-02-12.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contact.h"
#import "Address.h"

#define TASK_PICKUP "pickup"
#define TASK_DROP_OFF "dropoff"
#define TASK_RETURN "return"


#define ORDER_Cancellation_Status "cancellation_status"
#define ORDER_Delay_Status "delay_status"

#define ORDER_Pickup_Status "pickup_status"
#define ORDER_Dropoff_Status "dropoff_status"


#define PAYMENT_COLLECT "collect"
#define PAYMENT_MAKE "make"

@class Order;

@interface Task : NSObject

@property (strong, nonatomic) NSNumber *taskId;
@property (strong, nonatomic) Order *order;
@property (strong, nonatomic) NSNumber *orderId;
@property (strong, nonatomic) NSString *num;

@property (strong, nonatomic) NSString *pin;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *taskDescription;
@property (strong, nonatomic) NSNumber *dueTime;

@property (strong, nonatomic) NSNumber *sprintId;
@property (strong, nonatomic) NSNumber *statusId;

@property (strong, nonatomic) Contact *contact;

@property (nonatomic, strong) Address *location;

@property (strong, nonatomic) NSString *merchant_order_num;
@property (strong, nonatomic) NSString *start_time;
@property (strong, nonatomic) NSString *end_time;
@property (strong, nonatomic) NSString *address_line2;


@property (strong, nonatomic) NSString *paymentType;
@property (strong, nonatomic) NSNumber *paymentAmount;

@property (strong, nonatomic) NSNumber *charge;
@property (strong, nonatomic) NSNumber *serviceCharge;
@property (strong, nonatomic) NSNumber *weightCharge;
@property (strong, nonatomic) NSNumber *weightValue;
@property (strong, nonatomic) NSString *weightUnit;

@property (strong, nonatomic) NSMutableArray *confirmations;

-(BOOL)isAllConfirmed;

@end
