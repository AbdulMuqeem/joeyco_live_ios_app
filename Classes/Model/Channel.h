//
//  Channel.h
//  Joey
//
//  Created by Katia Maeda on 2016-01-28.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Message.h"

@interface Channel : NSObject

@property (nonatomic, strong) NSNumber *channelId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *visibility;
@property (nonatomic, strong) NSNumber *members;
@property (nonatomic, strong) Message *lastMessage;

@end
