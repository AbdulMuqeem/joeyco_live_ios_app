//
//  NewShift.h
//  Joey
//
//  Created by Katia Maeda on 2015-12-02.
//  Copyright © 2015 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Zone.h"
#import "Vehicle.h"

@interface NewShift : NSObject

@property (nonatomic, strong) Zone *zone;
@property (nonatomic, strong) Vehicle *vehicle;

-(instancetype)initWithZone:(Zone *)zone vehicle:(Vehicle *)vehicle;

@end
