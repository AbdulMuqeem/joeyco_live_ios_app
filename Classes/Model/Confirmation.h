//
//  Confirmation.h
//  Joey
//
//  Created by Katia Maeda on 2015-02-12.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Task.h"

#define CONFIRMATION_PIN "pin"
#define CONFIRMATION_ORDER_NUMBER "order-number"
#define CONFIRMATION_CART_ITEMS "cart-items"
#define CONFIRMATION_CONFIRMATION_LIST_ITEMS "sprint-task-items"

#define CONFIRMATION_SEAL "seal"
#define CONFIRMATION_IMAGE "image"
#define CONFIRMATION_DEFAULT "default"
#define CONFIRMATION_AMOUNT "amount"
#define CONFIRMATION_CHARGE_CARD "charge_card"
#define CONFIRMATION_SIGNATURE "signature"

#define CONFIRMATION_INPUT_TYPE_TEXT "text/plain"
#define CONFIRMATION_INPUT_TYPE_IMAGE "image/jpeg"

typedef enum : NSInteger {
    ConfirmationStatusNotConfirmed,
    ConfirmationStatusSent,
    ConfirmationStatusNotSent
} ConfirmationStatus;

@interface Confirmation : NSObject

@property (nonatomic, strong) NSNumber *confirmationId;
@property (nonatomic, weak) Task *task;
@property (nonatomic, strong) NSNumber *taskId;
@property (nonatomic, strong) NSNumber *orderId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *confirmationDescription;
@property (nonatomic, strong) NSString *inputType;
@property (nonatomic, strong) NSNumber *isConfirmed;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *method;

@property (nonatomic, strong) NSData *savedImage;
@property (nonatomic, strong) NSString *savedInputField;

@end
