//
//  Message.h
//  Joey
//
//  Created by Katia Maeda on 2015-02-13.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contact.h"

@interface Message : NSObject

@property (nonatomic, strong) NSNumber *messageId;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) Contact *contact;

@end
