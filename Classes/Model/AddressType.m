//
//  AddressType.m
//  Joey
//
//  Created by Katia Maeda on 2015-01-12.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "AddressType.h"

@implementation AddressType

-(void)setName:(NSString *)name
{
    _name = name;
    
    if ([name isEqualToString:@ADDRESS_TYPE_APARTMENT])
    {
        self.imageOn = @"apartment-green.png";
        self.imageOff = @"apartment-gray.png";
    }
    else if ([name isEqualToString:@ADDRESS_TYPE_CAMPUS])
    {
        self.imageOn = @"campus-green.png";
        self.imageOff = @"campus-gray.png";
    }
    else if ([name isEqualToString:@ADDRESS_TYPE_HOTEL])
    {
        self.imageOn = @"hotel-green.png";
        self.imageOff = @"hotel-gray.png";
    }
    else if ([name isEqualToString:@ADDRESS_TYPE_HOUSE])
    {
        self.imageOn = @"house-green.png";
        self.imageOff = @"house-gray.png";
    }
    else if ([name isEqualToString:@ADDRESS_TYPE_OFFICE])
    {
        self.imageOn = @"office-green.png";
        self.imageOff = @"office-gray.png";
    }
}

@end
