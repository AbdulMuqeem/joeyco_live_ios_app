//
//  Location_itinearOrders.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 17/05/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Location_itinearyOrders : NSObject

@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *address_line2;

@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@end

NS_ASSUME_NONNULL_END
