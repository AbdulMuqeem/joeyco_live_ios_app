//
//  ItineraryOrder.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 17/05/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import "ItineraryOrder.h"
#import "LocationObjectForBirdView.h"
#import "Prefix.pch"

@implementation ItineraryOrder



-(int)getTotalNumberOfItemsPicked{

       
       NSMutableArray * dataset = [[NSMutableArray alloc]init];
    
    int totalNumberOfItemsPicked =0;
      
       
       //Extracting all data from both array data set [Store and Hub]
       
       for (id i in self.arr_itineraryDetailObj_Hub )
       {
           [dataset addObject:i];
       }
       
       for (id i in self.arr_itineraryDetailObj_store )
       {
           [dataset addObject:i];
       }
       
       for (id objDetail in dataset )
       {
           
           ItineraryOrder_Listing_ObjectDetail * obj = objDetail;
           
           if (obj.has_picked ) {
               
               totalNumberOfItemsPicked ++;
           }
           
       }
       
       return totalNumberOfItemsPicked;
}

-(NSString *)getHeadingForTableViewSectionHeader{

    NSString *SectionHeaderName =@"";
    
    if([self.arr_itineraryDetailObj_store count]>0 )
       
       {
           SectionHeaderName =@"Store";
           return SectionHeaderName;

       }
       
       
       if([self.arr_itineraryDetailObj_Hub count]>0 )
       {
           SectionHeaderName =@"Hub";

            return SectionHeaderName;

       }
    return SectionHeaderName;
}


-(NSMutableArray *)getDataForTableView{

    NSMutableArray * tableData = [[NSMutableArray alloc]init];
    
    if([self.arr_itineraryDetailObj_store count]>0 )
    
    {
        tableData =self.arr_itineraryDetailObj_store;
        return tableData;

    }
    
    
    if([self.arr_itineraryDetailObj_Hub count]>0 )
    {
               tableData =self.arr_itineraryDetailObj_Hub;
               return tableData;

    }
    return tableData;

}

-(NSMutableArray *)getTrackingIds{
  
    NSMutableArray * trackingIds = [[NSMutableArray alloc]init];
    
    NSMutableArray * dataSetForTrackingIds = [[NSMutableArray alloc]init];
   
    
    //Extracting all data from both array data set [Store and Hub]
    
    for (id i in self.arr_itineraryDetailObj_Hub )
    {
        [dataSetForTrackingIds addObject:i];
    }
    
    for (id i in self.arr_itineraryDetailObj_store )
    {
        [dataSetForTrackingIds addObject:i];
    }
    
    for (id objDetail in dataSetForTrackingIds )
    {
        
        ItineraryOrder_Listing_ObjectDetail * obj = objDetail;
        
        if ([self isEmpty:obj.tracking_id] == NO) {
            
            [trackingIds addObject:obj.tracking_id];
        }
        
    }
    
    /*
     Removing duplicates
     */
    NSOrderedSet *finalItems = [[NSOrderedSet alloc]initWithArray:trackingIds];
    trackingIds = [[NSMutableArray alloc]initWithArray:[finalItems array]];
    
    return trackingIds;
    
    
}


-(NSMutableArray *)getLocationInformation{
  
    NSMutableArray * locationinfo = [[NSMutableArray alloc]init];
    
    NSMutableArray * dataSet = [[NSMutableArray alloc]init];
   
    
    //Extracting all data from both array data set [Store and Hub]
    
    for (id i in self.arr_itineraryDetailObj_Hub )
    {
        [dataSet addObject:i];
    }
    
    for (id i in self.arr_itineraryDetailObj_store )
    {
        [dataSet addObject:i];
    }
    
    for (id objDetail in dataSet )
    {
        
        ItineraryOrder_Listing_ObjectDetail * obj = objDetail;
        
        NSMutableDictionary *locationInfo = [[NSMutableDictionary alloc]initWithDictionary:obj.location];

        
        LocationObjectForBirdView *birdViewDetail = [[LocationObjectForBirdView alloc]init];
        birdViewDetail.latitude=  [locationInfo objForKey:@"latitude"];
        birdViewDetail.longitude=[locationInfo objForKey:@"longitude" ];
        birdViewDetail.MarkerTitle = obj.num;
        
        if ([self isEmpty:[birdViewDetail.latitude stringValue]] == NO) {
            [locationinfo addObject:birdViewDetail];
        }
        
    }
    
    /*
     Removing duplicates
     */
    NSOrderedSet *finalItems = [[NSOrderedSet alloc]initWithArray:locationinfo];
    locationinfo = [[NSMutableArray alloc]initWithArray:[finalItems array]];
    
    return locationinfo;
    
    
}


-(BOOL)isEmpty:(NSString *)str
{
    if(str.length==0 || [str isKindOfClass:[NSNull class]] || [str isEqualToString:@""]||[str  isEqualToString:NULL]||[str isEqualToString:@"(null)"]||str==nil || [str isEqualToString:@"<null>"]){
        return YES;
    }
    return NO;
}

@end
