//
//  SummaryRecords.h
//  Joey
//
//  Created by Katia Maeda on 2016-05-20.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SummaryRecord : NSObject

@property (nonatomic, strong) NSNumber *payment_method;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSNumber *balance;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *recordsDescription;
@property (nonatomic, strong) NSNumber *recordsId;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *num;
@property (nonatomic, strong) NSString *shift;
@property (nonatomic, strong) NSNumber *distance;
@property (nonatomic, strong) NSString *duration;


@end
