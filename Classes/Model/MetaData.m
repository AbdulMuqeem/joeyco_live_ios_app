//
//  MetaData.m
//  Joey
//
//  Created by Katia Maeda on 2015-02-12.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "MetaData.h"

@implementation MetaOption

-(id)init:(NSString *)key value:(NSString *)value
{
    self = [super init];
    
    if(self)
    {
        self.optionId = key;
        self.value = value;
    }
    
    return self;
}

@end

@implementation MetaStatus

-(id)init:(NSString *)key value:(NSNumber *)value
{
    self = [super init];
    
    if(self)
    {
        self.statusId = key;
        self.value = value;
    }
    
    return self;
}

@end

@implementation MetaData

@end
