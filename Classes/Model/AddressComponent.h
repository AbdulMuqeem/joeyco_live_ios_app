//
//  AddressComponents.h
//  Joey
//
//  Created by Katia Maeda on 2014-10-12.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface AddressComponent : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *type;

-(id)init:(NSString *)name type:(NSString *)type;

@end
