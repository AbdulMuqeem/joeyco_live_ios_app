//
//  Shift.h
//  Joey
//
//  Created by Katia Maeda on 2015-02-12.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Zone.h"
#import "Vehicle.h"

@interface Shift : NSObject

@property (nonatomic, strong) NSNumber *shiftId;
@property (nonatomic, strong) NSString *num;
@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;
@property (nonatomic, strong) NSDate *realStartTime;
@property (nonatomic, strong) NSDate *realEndTime;

@property (nonatomic, strong) Zone *zone;
@property (nonatomic, strong) Vehicle *vehicle;

@property (nonatomic, strong) NSNumber *occupancy;
@property (nonatomic, strong) NSNumber *capacity;
@property (nonatomic, strong) NSNumber *can_cancel; //Newly added



@property (nonatomic, strong) NSArray *joeys;

@property (nonatomic, strong) NSNumber *wage;
@property (nonatomic, strong) NSString *joeyNotes;

@end
