//
//  Address.h
//  Joey
//
//  Created by Katia Maeda on 2014-11-11.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressComponent.h"

#define ADDRESS_ADDRESS "address"
#define ADDRESS_CITY "city"
#define ADDRESS_DIVISION "division"
#define ADDRESS_COUNTRY "country"
#define ADDRESS_POSTAL_CODE "postal_code"
#define ADDRESS_BUZZER "buzzer"
#define ADDRESS_SUITE "suite"

@interface Address : NSObject

@property (nonatomic, strong) NSNumber *addressId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *AddressType;


@property (nonatomic, strong) NSNumber *locationLatitude;
@property (nonatomic, strong) NSNumber *locationLongitude;

@property (nonatomic, strong) AddressComponent *address;
@property (nonatomic, strong) AddressComponent *city;
@property (nonatomic, strong) AddressComponent *division;
@property (nonatomic, strong) AddressComponent *country;
@property (nonatomic, strong) AddressComponent *postalCode;
@property (nonatomic, strong) AddressComponent *buzzer;
@property (nonatomic, strong) AddressComponent *suite;

-(NSString *)getSimpleAddress;
-(NSString *)getAddress;

-(void)setAddressComponent:(AddressComponent *)component;

-(NSString *)GetAddressType:(NSDictionary *)addressData; ///Business Or Residential

@end
