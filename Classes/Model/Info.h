//
//  Info.h
//  Joey
//
//  Created by Katia Maeda on 2015-02-13.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Info : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, strong) NSString *value2;

-(id)init:(NSString *)title value:(NSString *)value value2:(NSString *)value2;

@end
