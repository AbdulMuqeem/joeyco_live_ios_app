//
//  Contact.h
//  Joey
//
//  Created by Katia Maeda on 2015-01-06.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Address.h"

@interface Contact : NSObject

@property (strong, nonatomic) NSNumber *contactId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *type;

@end
