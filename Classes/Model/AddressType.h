//
//  AddressType.h
//  Joey
//
//  Created by Katia Maeda on 2015-01-12.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ADDRESS_TYPE_APARTMENT "apartment"
#define ADDRESS_TYPE_CAMPUS "campus"
#define ADDRESS_TYPE_HOTEL "hotel"
#define ADDRESS_TYPE_HOUSE "house"
#define ADDRESS_TYPE_OFFICE "office"

@interface AddressType : NSObject

@property (nonatomic, strong) NSNumber *typeId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *price;

@property (nonatomic, strong) NSString *imageOn;
@property (nonatomic, strong) NSString *imageOff;

@end
