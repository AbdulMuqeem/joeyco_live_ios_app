//
//  LocationObjectForBirdView.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 24/06/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LocationObjectForBirdView : UIView
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, strong) NSString *MarkerTitle;
@property (nonatomic, strong) NSString *isChecked;


@end

NS_ASSUME_NONNULL_END
