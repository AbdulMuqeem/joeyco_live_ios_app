//
//  Summary.h
//  Joey
//
//  Created by Katia Maeda on 2016-05-20.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Summary : NSObject

@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) NSNumber *totalDistance;
@property (nonatomic, strong) NSString *totalDuration;
@property (nonatomic, strong) NSNumber *numCustom;
@property (nonatomic, strong) NSNumber *numRemotePos;
@property (nonatomic, strong) NSNumber *numQuick;
@property (nonatomic, strong) NSNumber *numThirdParty;
@property (nonatomic, strong) NSNumber *numCustomer;
@property (nonatomic, strong) NSNumber *numTotalOrders;
@property (nonatomic, strong) NSNumber *numCompletedShifts;
@property (nonatomic, strong) NSNumber *numTransfers;
@property (nonatomic, strong) NSNumber *numStatements;

@property (nonatomic, strong) NSNumber *cashCollected;
@property (nonatomic, strong) NSNumber *paymentsMade;
@property (nonatomic, strong) NSNumber *earnings;
@property (nonatomic, strong) NSNumber *wages;
@property (nonatomic, strong) NSNumber *wagesOwed;
@property (nonatomic, strong) NSNumber *tips;
@property (nonatomic, strong) NSNumber *transfers;
@property (nonatomic, strong) NSNumber *adjustments;
@property (nonatomic, strong) NSNumber *planPayments;
@property (nonatomic, strong) NSNumber *invoicePayments;
@property (nonatomic, strong) NSNumber *statementPayments;
@property (nonatomic, strong) NSNumber *bonuses;
@property (nonatomic, strong) NSString *totalHours;
@property (nonatomic, strong) NSString *ordersPerHour;
@property (nonatomic, strong) NSNumber *earningsPerHour;
@property (nonatomic, strong) NSNumber *startingBalance;
@property (nonatomic, strong) NSNumber *endingBalance;
@property (nonatomic, strong) NSString *averageDuration;
@property (nonatomic, strong) NSNumber *averageDistance;
@property (nonatomic, strong) NSNumber *maxDistance;

@property (strong, nonatomic) NSMutableArray *records;
@property (strong, nonatomic) NSMutableArray *shifts;

@end
