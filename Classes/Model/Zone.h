//
//  Zone.h
//  Joey
//
//  Created by Katia Maeda on 2015-02-12.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Zone : NSObject

@property (nonatomic, strong) NSNumber *zoneId;
@property (nonatomic, strong) NSString *num;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *centerLatitude;
@property (nonatomic, strong) NSNumber *centerLongitude;
@property (nonatomic, strong) NSNumber *radius;

@end
