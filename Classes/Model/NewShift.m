//
//  NewShift.m
//  Joey
//
//  Created by Katia Maeda on 2015-12-02.
//  Copyright © 2015 JoeyCo. All rights reserved.
//

#import "NewShift.h"

@implementation NewShift

-(instancetype)initWithZone:(Zone *)zone vehicle:(Vehicle *)vehicle
{
    self = [super init];
    if (self)
    {
        self.zone = zone;
        self.vehicle = vehicle;
    }
    return self;
}

@end
