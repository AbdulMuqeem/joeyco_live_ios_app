//
//  Task.m
//  Joey
//
//  Created by Katia Maeda on 2015-02-12.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "Task.h"
#import "Confirmation.h"

@implementation Task

-(BOOL)isAllConfirmed
{
    BOOL result = YES;
    
    if (self.confirmations.count == 0)
    {
        return NO;
    }
    
    for (Confirmation *confirmation in self.confirmations)
    {
        if (![confirmation.isConfirmed boolValue])
        {
            result = NO;
            break;
        }
    }
    
    return result;
}

@end
