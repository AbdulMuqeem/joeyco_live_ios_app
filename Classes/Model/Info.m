//
//  Info.m
//  Joey
//
//  Created by Katia Maeda on 2015-02-13.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "Info.h"

@implementation Info

-(id)init:(NSString *)title value:(NSString *)value value2:(NSString *)value2
{
    self = [super init];
    
    if(self)
    {
        self.title = title;
        self.value = value;
        self.value2 = value2;
    }
    
    return self;
}

@end
