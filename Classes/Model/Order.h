//
//  Order.h
//  JoeyCo
//
//  Created by Katia Maeda on 2014-09-28.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"
#import "Joey.h"

typedef enum : NSInteger {
    MapTypeSettingNormal,
    MapTypeSettingHybrid
} MapTypeSetting;

@interface Order : NSObject

@property (strong, nonatomic) NSNumber *orderId;
@property (strong, nonatomic) NSString *num;
@property (strong, nonatomic) NSNumber *status;
@property (strong, nonatomic) NSNumber *distance;
@property (strong, nonatomic) NSNumber *distanceAllowance;
@property (strong, nonatomic) NSNumber *eta;
@property (strong, nonatomic) NSString * remaintime;


@property (strong, nonatomic) Joey *joey;

@property (strong, nonatomic) NSNumber *distanceCharge;
@property (strong, nonatomic) NSNumber *totalTaskCharge;
@property (strong, nonatomic) NSNumber *subtotal;
@property (strong, nonatomic) NSNumber *tax;
@property (strong, nonatomic) NSNumber *total;
@property (strong, nonatomic) NSString *tip;

@property (strong, nonatomic) NSMutableArray *tasks;

@property (strong, nonatomic) NSMutableArray *cancellation_status;
@property (strong, nonatomic) NSMutableArray *delay_status;
@property (strong, nonatomic) NSMutableArray *pickup_status;
@property (strong, nonatomic) NSMutableArray *dropoff_status;


@property (strong, nonatomic) NSNumber *totalDistance;
@property (strong, nonatomic) NSNumber *totalDuration;



//Order Status Init
-(NSMutableArray *)arr_cancellation_status :(UIViewController *)VC;
-(NSMutableArray *)arr_delay_status :(UIViewController *)VC;
-(NSMutableArray *)arr_pickup_status :(UIViewController *)VC;
-(NSMutableArray *)arr_dropoff_status :(UIViewController *)VC;


-(NSMutableArray *)tasksLocationList;
-(NSMutableArray *)pickupLocationList;
-(NSMutableArray *)pickupList;
-(NSMutableArray *)dropOffList;
-(Task *)getNextTask;

@end
