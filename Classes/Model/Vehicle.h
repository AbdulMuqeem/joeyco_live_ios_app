//
//  Vehicle.h
//  Joey
//
//  Created by Katia Maeda on 2014-12-11.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

#define VEHICLE_BICYCLE "Bicycle"
#define VEHICLE_SCOOTER "Scooter"
#define VEHICLE_CAR "Car"
#define VEHICLE_TRUCK "Truck"
#define VEHICLE_SUV "SUV"
#define VEHICLE_VAN "Van"

@interface Vehicle : NSObject

@property (nonatomic, strong) NSNumber *vehicleId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *price;

@property (nonatomic, strong) NSString *imageOn;
@property (nonatomic, strong) NSString *imageOff;

@end
