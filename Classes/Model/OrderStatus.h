//
//  OrderStatus.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 07/12/2018.
//  Copyright © 2018 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderStatus : NSObject

@property (nonatomic, strong) NSNumber *orderStatusID;
@property (nonatomic, strong) NSString *orderDescription;
@property (nonatomic, strong) NSNumber *orderActive;
@end

NS_ASSUME_NONNULL_END
