//
//  Address.m
//  Joey
//
//  Created by Katia Maeda on 2014-11-11.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "Address.h"
#import "AddressComponent.h"

@implementation Address

-(void)setAddressComponent:(AddressComponent *)component
{
    if ([component.type isEqualToString:@ADDRESS_ADDRESS])
    {
        self.address = component;
    }
    else if ([component.type isEqualToString:@ADDRESS_CITY])
    {
        self.city = component;
    }
    else if ([component.type isEqualToString:@ADDRESS_DIVISION])
    {
        self.division = component;
    }
    else if ([component.type isEqualToString:@ADDRESS_COUNTRY])
    {
        self.country = component;
    }
    else if ([component.type isEqualToString:@ADDRESS_POSTAL_CODE])
    {
        self.postalCode = component;
    }
    else if ([component.type isEqualToString:@ADDRESS_BUZZER])
    {
        self.buzzer = component;
    }
    else if ([component.type isEqualToString:@ADDRESS_SUITE])
    {
        self.suite = component;
    }
}

-(NSString *)getSimpleAddress
{
    NSString *address = self.address.name;
    NSString *postalCode = self.postalCode.name;

    return [NSString stringWithFormat:@"%@, %@", address, postalCode];
}

-(NSString *)getAddress
{
    NSString *address = self.address.name;
    NSString *postalCode = self.postalCode.name;
    NSString *city = self.city.name;
    NSString *division = self.division.name;
    
    return [NSString stringWithFormat:@"%@, %@\n%@, %@", address, postalCode, city, division];
}

@end
