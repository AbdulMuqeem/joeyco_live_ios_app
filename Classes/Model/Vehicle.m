//
//  Vehicle.m
//  Joey
//
//  Created by Katia Maeda on 2014-12-11.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "Vehicle.h"

@implementation Vehicle

-(void)setName:(NSString *)name
{
    _name = name;
    
    if ([name isEqualToString:@VEHICLE_BICYCLE])
    {
        self.imageOn = @"bike-green.png";
        self.imageOff = @"bike.png";
    }
    else if ([name isEqualToString:@VEHICLE_CAR])
    {
        self.imageOn = @"car-green.png";
        self.imageOff = @"car.png";
    }
    else if ([name isEqualToString:@VEHICLE_VAN])
    {
        self.imageOn = @"van-green.png";
        self.imageOff = @"van.png";
    }
    else if ([name isEqualToString:@VEHICLE_SCOOTER])
    {
        self.imageOn = @"scooter-green.png";
        self.imageOff = @"scooter.png";
    }
    else if ([name isEqualToString:@VEHICLE_TRUCK])
    {
        self.imageOn = @"truck-green.png";
        self.imageOff = @"truck.png";
    }
    else if ([name isEqualToString:@VEHICLE_SUV])
    {
        self.imageOn = @"suv-green.png";
        self.imageOff = @"suv.png";
    }
}

@end
