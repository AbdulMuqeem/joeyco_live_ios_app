//
//  ItineraryOrder.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 17/05/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Task.h"
#import "Joey.h"
#import "Location_itinearyOrders.h"
#import "ItineraryOrder_Listing_ObjectDetail.h"
NS_ASSUME_NONNULL_BEGIN

@interface ItineraryOrder : NSObject

@property (nonatomic, strong) NSMutableArray *arr_itineraryDetailObj_store;

@property (nonatomic, strong) NSMutableArray *arr_itineraryDetailObj_Hub;

@property (strong, nonatomic) NSMutableArray *pickup_delay_status;
@property (strong, nonatomic) NSMutableArray *dropoff_delay_status;
@property (strong, nonatomic) NSMutableArray *pickup_return_status;
@property (strong, nonatomic) NSMutableArray *dropoff_return_status;
@property (strong, nonatomic) NSMutableArray *dropoff_status;

-(NSMutableArray *)getTrackingIds;
-(NSMutableArray *)getDataForTableView;
-(NSString *)getHeadingForTableViewSectionHeader;
-(NSMutableArray *)getLocationInformation;
-(int)getTotalNumberOfItemsPicked;

@end

NS_ASSUME_NONNULL_END
