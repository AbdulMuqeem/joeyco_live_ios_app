//
//  AddressComponents.m
//  Joey
//
//  Created by Katia Maeda on 2014-10-12.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "AddressComponent.h"

@implementation AddressComponent

-(id)init:(NSString *)name type:(NSString *)type
{
    self = [super init];
    
    if(self)
    {
        self.name = name;
        self.type = type;
    }
    
    return self;
}

@end
