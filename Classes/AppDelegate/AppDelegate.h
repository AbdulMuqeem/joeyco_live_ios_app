//
//  AppDelegate.h
//  JoeyCo
//
//  Created by Katia Maeda on 2014-09-27.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
@import Firebase;


#define NotificationsWithDeviceToken "NotificationsWithDeviceToken"
#define NotificationsReceivedNewOrder "NotificationsReceivedNewOrder"
#define NotificationsReceivedAcceptedOrder "NotificationsReceivedAcceptedOrder"
#define NotificationsReceivedOrderTransferred "NotificationsReceivedOrderTransferred"
#define NotificationsOrderMessage "NotificationsOrderMessage"
#define NotificationsReceivedNewMessage "NotificationsReceivedNewMessage"
#define NotificationsReceivedShiftUpdated "NotificationsReceivedShiftUpdated"
#define NotificationsReceivedShiftNotify "NotificationsReceivedShiftNotify"
#define NotificationsReloadEndWorkButton "NotificationsReloadEndWorkButton"

#define NotificationsConfirmationDone "NotificationsConfirmationDone"
#define NotificationsConfirmationDoneImageUpload "NotificationsConfirmationDoneImageUpload"


#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@import PushKit;

@protocol PushKitEventDelegate <NSObject>

- (void)credentialsUpdated:(PKPushCredentials *)credentials;
- (void)credentialsInvalidated;
- (void)incomingPushReceived:(PKPushPayload *)payload withCompletionHandler:(void (^)(void))completion;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;

//@property (strong, nonatomic) UINavigationController *firstViewController;

@end

