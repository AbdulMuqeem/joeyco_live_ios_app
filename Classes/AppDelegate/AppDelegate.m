//
//  AppDelegate.m
//  JoeyCo
//
//  Created by Katia Maeda on 2014-09-27.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "AppDelegate.h"
#import "Prefix.pch"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
@import Firebase;
#import <CoreLocation/CoreLocation.h>

#import "CallNow.h"
#import <ScanditBarcodeScanner/ScanditBarcodeScanner.h>

@import GooglePlaces;
@interface AppDelegate () <PKPushRegistryDelegate>
@property (nonatomic, weak) id<PushKitEventDelegate> pushKitEventDelegate;
@property (nonatomic, strong) PKPushRegistry *voipRegistry;@end

@implementation AppDelegate

//
//
//-(NSString *)convertTime:(int)time
//{
//    if (time < 1) return @"";
//    float hours = floor(time / 60);
//    float minutes = (time % 60);
//    if (hours == 0) return [NSString stringWithFormat:@"%.0f min", minutes];
//    return [NSString stringWithFormat:@"%.0f hr %.0f min", hours, minutes];
//}


-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

//    NTPServer *server = [NTPServer defaultServer];
//    NSDate *date = [server dateWithError:nil];
//    NSLog(@"TIME_STAMP : %@", date);
    
   // NSLog(@"TIME_FZ: %@",[self convertTime:78.2]);
    
//    //Twilio
//    [self setupPushRegistry];
    
    //Firebase analytics
    [FIRApp configure];
    
    [FIRMessaging messaging].delegate = self;
    
    // Map and GPS
    [GMSServices provideAPIKey:@"AIzaSyCgvW-OI6o8iv56qIzLR6KA0H3J8hC-ACs"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyCOPi-R0Jl30-nuei4Pm8_DFAIyWNmXxLs"];
    
    
    [SBSLicense setAppKey:@"AZ2f0LqBLmoGCmYsnRx/rzEMQWGoPfl35FsAWcZZDEemVLzLRnKn/5Efos9tAzokYnHMH3Zhc3spdlL8KGMVZ4IMeCjyayWeXyVJIbc7nhEcenN3wH43Ga13j4RZC4n2PEMMxLZDds/YNbhO+wu4lQyEfK8spcsi5P4rjKGQ8KwuQX5b/U0Sia1/g/qWnc85A7bFJdHnOMXIu5YzcxNyKsQTF6gm/8kgMTsHo6Yx2iF6ebzw7CjvKOy9h3LP5F6QqdnHLrClyBFTxTba7RefctO/uLypJhntLcYE3l2N5wmlwg/8WXEECo3vB+Tx8+r1lV8DiduJ7k1rbCioWz/vAXbQFv1L63Sm2FX0eNlcRQE//55pcl54miwzxULBaFeqhm/9UuyTiPrSArsyVgFcFWUdeqx5zR9sT0qAxo3W8JPJC0T9Yxtj0kNFnSp+2L+Kr+q/hXZwlXPgh39UcJ8VkAaIWJV6lzAjDLhANOZ8j6D5a4wh5OUGNWt/ATGk0egI1ROSmt2cbcR0iUPs44cioS8igGxTZ6WOfhohJZygpvIBmUIrHINGyafvwZpgtE4JXi9eGYj8xWWQdTkVRN9K/qkoBUHagb6I6wbCv7G0yxnAY0uZAE397V2ET7lGDB4K7eGgrEkP8u/L56LYeY116fbg6Qh9hI2oOoebIYr/iGR9/Bw3vaLIX74UDAo9RTMe+WgR1+xgaRq9NWVyop5Hw1iEfmJFZ+olI3V4laMaIAM+zTt7dBRvKW/XA5NJ1fg4aDBznlbLoWZqOWAEOoXTpUCzAPQ7YOydmDwyEt7SWGUk4ghTlnuNY3nHcvyW"];

    
    [DatabaseHelper shared];
    
    // Let the device know we want to receive push notifications
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerForRemoteNotifications];
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    }
    
//    // One Signal Push Notification *Starts
//    // Replace '11111111-2222-3333-4444-0123456789ab' with your OneSignal App ID.
//    [OneSignal initWithLaunchOptions:launchOptions
//                               appId:@"b7ad69f7-cbdb-4266-b73e-e5f3d1e037c0"
//            handleNotificationAction:nil
//                            settings:@{kOSSettingsKeyAutoPrompt: @false}];
//    OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNotification;
//    
//    // Recommend moving the below line to prompt for push after informing the user about
//    //   how your app will use them.
//    [OneSignal promptForPushNotificationsWithUserResponse:^(BOOL accepted) {
//        NSLog(@"User accepted notifications: %d", accepted);
//    }];
//    
//    
//    // One Signal Push Notification *Ends

    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    
    
    
//    [GMSServices provideAPIKey:@"AIzaSyAhyYLROXuYlqyCHnHWzfIJf5T_etAV2Y8"];
//    [GMSPlacesClient provideAPIKey:@"AIzaSyAhyYLROXuYlqyCHnHWzfIJf5T_etAV2Y8"];

    Joey *joey = [[DatabaseHelper shared] getJoey];
    if (joey)
    {
        [UrlInfo setJoeyId:[joey.joeyId integerValue]];
    }

    // This UIApplicationLaunchOptionsLocationKey key enables the location update even when
    // the app has been killed/terminated (Not in th background) by iOS or the user.
    
    [LocationManager shared];
    
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey])
    {
        [[LocationManager shared] startLaunchMode];
    }
    else
    {
        [self navigateTo: @"StartPageViewController"];
    }

    [Fabric with:@[CrashlyticsKit]];
    
    return YES;
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"deviceToken"];
//    [[NSNotificationCenter defaultCenter] postNotificationName:
//     @"FCMToken" object:nil userInfo:dataDict];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsWithDeviceToken object:nil userInfo:dataDict];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}


-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    //Called when a notification is delivered to a foreground app.
    
    NSLog(@"*********JOEY CO PUSH NOTIFICATION RECEIVED Userinfo_ %@",notification.request.content.userInfo);
    
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];

    
    Joey *joey = [[DatabaseHelper shared] getJoey];
    if (!joey)
    {
        return;
    }
    
    NSDictionary *userInfo =notification.request.content.userInfo;
    
    id aps = [userInfo objectForKey:@"aps"];
    if ([aps isKindOfClass:[NSDictionary class]])
    {
        MetaStatus *newOrderStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_NEW];
        MetaStatus *orderTransferStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_TRANSFER];
        MetaStatus *orderAcceptedStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_ACCEPT_JOEY];
        MetaStatus *orderVendorEditting = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_VENDOR_EDITTING];
        MetaStatus *orderVendorCancelled = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_VENDOR_CANCELLED];
        MetaStatus *orderAdminCancelled = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_ADMIN_CANCELLED];
        MetaStatus *orderUserCancelled = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_USER_CANCELLED];
        MetaStatus *messageStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_CHAT_MESSAGE];
        MetaStatus *shiftUpdateStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_JOEY_SHIFT_UPDATE];
        MetaStatus *shiftNotifyStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_JOEY_SHIFT_NOTIFY];
        
        NSDictionary *alert = [aps objectForKey:@"alert"];
     //   NSString *action = [alert objectForKey:@"action-loc-key"];
        NSString *action = [alert objectForKey:@"loc-key"];

        int actionNumber = [action intValue];
        
        NSString *message = [alert objectForKey:@"body"];
        if (!message) {
            message = @"";
        }
        NSDictionary *messageDict = [NSDictionary dictionaryWithObject:message forKey:@"message"];
        
        NSLog(@"New message %d", actionNumber);
        if (actionNumber == [newOrderStatus.value intValue])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedNewOrder object:nil userInfo:nil];
        }
        else if (actionNumber == [orderTransferStatus.value intValue])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedOrderTransferred object:nil userInfo:nil];
        }
        else if (actionNumber == [orderAcceptedStatus.value intValue])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedAcceptedOrder object:nil userInfo:nil];
        }
        else if (actionNumber == [orderVendorEditting.value intValue])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsOrderMessage object:nil userInfo:messageDict];
        }
        else if (actionNumber == [orderVendorCancelled.value intValue])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsOrderMessage object:nil userInfo:messageDict];
        }
        else if (actionNumber == [orderAdminCancelled.value intValue])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsOrderMessage object:nil userInfo:messageDict];
        }
        else if (actionNumber == [orderUserCancelled.value intValue])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsOrderMessage object:nil userInfo:messageDict];
        }
        else if (actionNumber == [messageStatus.value intValue])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedNewMessage object:nil userInfo:nil];
            if (state == UIApplicationStateInactive || state == UIApplicationStateBackground)
            {
                [self navigateTo: @"ChatChannelsViewController"];
            }
        }
        else if (actionNumber == [shiftUpdateStatus.value intValue])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedShiftUpdated object:nil userInfo:nil];
            if (state == UIApplicationStateInactive || state == UIApplicationStateBackground)
            {
                [self navigateTo: @"ScheduleViewController"];
            }
        }
        else if (actionNumber == [shiftNotifyStatus.value intValue])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedShiftNotify object:nil userInfo:nil];
            if (state == UIApplicationStateInactive || state == UIApplicationStateBackground)
            {
                [self navigateTo: @"ScheduleViewController"];
            }
        }
    }
    completionHandler(UNNotificationPresentationOptionAlert);
    
    

}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    
    //Called to let your app know which action was selected by the user for a given notification.
    
    NSLog(@"*********JOEY CO PUSH NOTIFICATION RECEIVED  %@",response.notification.request.content.userInfo);
    
}



- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[LocationManager shared] startBackgroundMode];
    
    
    
    if ([[NSProcessInfo processInfo] isLowPowerModeEnabled]) {

    UILocalNotification *notification_power = [[UILocalNotification alloc]init];
        notification_power.repeatInterval = NSDayCalendarUnit;
        [notification_power setAlertBody:@"Please Turn off power save mode so your location can be fetched"];
    [notification_power setSoundName: UILocalNotificationDefaultSoundName];

        [notification_power setFireDate:[NSDate dateWithTimeIntervalSinceNow:1]];
        [notification_power setTimeZone:[NSTimeZone  defaultTimeZone]];
        [application setScheduledLocalNotifications:[NSArray arrayWithObject:notification_power]];
    }
    
    
    
    if([CLLocationManager locationServicesEnabled])
       {
           NSLog(@"Enabled");

           if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
           {
                NSLog(@"Permission Denied");
               
               UILocalNotification *notification = [[UILocalNotification alloc]init];
                   notification.repeatInterval = NSDayCalendarUnit;
                   [notification setAlertBody:@"Please Turn On your location from settings"];
               [notification setSoundName: UILocalNotificationDefaultSoundName];

                   [notification setFireDate:[NSDate dateWithTimeIntervalSinceNow:2]];
                   [notification setTimeZone:[NSTimeZone  defaultTimeZone]];
                   [application setScheduledLocalNotifications:[NSArray arrayWithObject:notification]];
           }
       }
 


}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [[LocationManager shared] endBackgroundMode];
    [[LocationManager shared] startUpdatingLocation];

}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    CallNow *callEnd = [CallNow shared];
    
    
    if (callEnd.call && callEnd.call.state == TVOCallStateConnected) {
        [callEnd.call disconnect];
    }

}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

-(void)dealloc
{
//    self.firstViewController = nil;
}

#pragma mark - Notifications

//- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
//{
//    NSLog(@"My token is: %@", deviceToken);
//    NSMutableString *token = [NSMutableString stringWithFormat:@"%@", deviceToken];
//    [token replaceOccurrencesOfString:@" " withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [token length])];
//    [token replaceOccurrencesOfString:@"<" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [token length])];
//    [token replaceOccurrencesOfString:@">" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [token length])];
//
//    NSDictionary *deviceInfo = [NSDictionary dictionaryWithObject:token forKey:@"deviceToken"];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsWithDeviceToken object:nil userInfo:deviceInfo];
//}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}
//
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
//{
//    Joey *joey = [[DatabaseHelper shared] getJoey];
//    if (!joey)
//    {
//        return;
//    }
//
//    id aps = [userInfo objectForKey:@"aps"];
//    if ([aps isKindOfClass:[NSDictionary class]])
//    {
//        MetaStatus *newOrderStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_NEW];
//        MetaStatus *orderTransferStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_TRANSFER];
//        MetaStatus *orderAcceptedStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_ACCEPT_JOEY];
//        MetaStatus *orderVendorEditting = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_VENDOR_EDITTING];
//        MetaStatus *orderVendorCancelled = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_VENDOR_CANCELLED];
//        MetaStatus *orderAdminCancelled = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_ADMIN_CANCELLED];
//        MetaStatus *orderUserCancelled = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_USER_CANCELLED];
//        MetaStatus *messageStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_CHAT_MESSAGE];
//        MetaStatus *shiftUpdateStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_JOEY_SHIFT_UPDATE];
//        MetaStatus *shiftNotifyStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_JOEY_SHIFT_NOTIFY];
//
//        NSDictionary *alert = [aps objectForKey:@"alert"];
//        NSString *action = [alert objectForKey:@"action-loc-key"];
//        int actionNumber = [action intValue];
//
//        NSString *message = [alert objectForKey:@"body"];
//        if (!message) {
//            message = @"";
//        }
//        NSDictionary *messageDict = [NSDictionary dictionaryWithObject:message forKey:@"message"];
//
//        NSLog(@"New message %d", actionNumber);
//        if (actionNumber == [newOrderStatus.value intValue])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedNewOrder object:nil userInfo:nil];
//        }
//        else if (actionNumber == [orderTransferStatus.value intValue])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedOrderTransferred object:nil userInfo:nil];
//        }
//        else if (actionNumber == [orderAcceptedStatus.value intValue])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedAcceptedOrder object:nil userInfo:nil];
//        }
//        else if (actionNumber == [orderVendorEditting.value intValue])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsOrderMessage object:nil userInfo:messageDict];
//        }
//        else if (actionNumber == [orderVendorCancelled.value intValue])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsOrderMessage object:nil userInfo:messageDict];
//        }
//        else if (actionNumber == [orderAdminCancelled.value intValue])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsOrderMessage object:nil userInfo:messageDict];
//        }
//        else if (actionNumber == [orderUserCancelled.value intValue])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsOrderMessage object:nil userInfo:messageDict];
//        }
//        else if (actionNumber == [messageStatus.value intValue])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedNewMessage object:nil userInfo:nil];
//            if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground)
//            {
//                [self navigateTo: @"ChatChannelsViewController"];
//            }
//        }
//        else if (actionNumber == [shiftUpdateStatus.value intValue])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedShiftUpdated object:nil userInfo:nil];
//            if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground)
//            {
//                [self navigateTo: @"ScheduleViewController"];
//            }
//        }
//        else if (actionNumber == [shiftNotifyStatus.value intValue])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedShiftNotify object:nil userInfo:nil];
//            if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground)
//            {
//                [self navigateTo: @"ScheduleViewController"];
//            }
//        }
//    }
//}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo     fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
 {
     
     
     
     Joey *joey = [[DatabaseHelper shared] getJoey];
     if (!joey)
     {
         return;
     }

     id aps = [userInfo objectForKey:@"aps"];
     if ([aps isKindOfClass:[NSDictionary class]])
     {
         MetaStatus *newOrderStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_NEW];
         MetaStatus *orderTransferStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_TRANSFER];
         MetaStatus *orderAcceptedStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_ACCEPT_JOEY];
         MetaStatus *orderVendorEditting = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_VENDOR_EDITTING];
         MetaStatus *orderVendorCancelled = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_VENDOR_CANCELLED];
         MetaStatus *orderAdminCancelled = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_ADMIN_CANCELLED];
         MetaStatus *orderUserCancelled = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_USER_CANCELLED];
         MetaStatus *messageStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_CHAT_MESSAGE];
         MetaStatus *shiftUpdateStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_JOEY_SHIFT_UPDATE];
         MetaStatus *shiftNotifyStatus = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_JOEY_SHIFT_NOTIFY];
         
         NSDictionary *alert = [aps objectForKey:@"alert"];
      //   NSString *action = [alert objectForKey:@"action-loc-key"];
         NSString *action = [alert objectForKey:@"loc-key"];

         int actionNumber = [action intValue];
         
         NSString *message = [alert objectForKey:@"body"];
         if (!message) {
             message = @"";
         }
         NSDictionary *messageDict = [NSDictionary dictionaryWithObject:message forKey:@"message"];
         
         NSLog(@"New message %d", actionNumber);
         if (actionNumber == [newOrderStatus.value intValue])
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedNewOrder object:nil userInfo:nil];
         }
         else if (actionNumber == [orderTransferStatus.value intValue])
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedOrderTransferred object:nil userInfo:nil];
         }
         else if (actionNumber == [orderAcceptedStatus.value intValue])
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedAcceptedOrder object:nil userInfo:nil];
         }
         else if (actionNumber == [orderVendorEditting.value intValue])
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsOrderMessage object:nil userInfo:messageDict];
         }
         else if (actionNumber == [orderVendorCancelled.value intValue])
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsOrderMessage object:nil userInfo:messageDict];
         }
         else if (actionNumber == [orderAdminCancelled.value intValue])
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsOrderMessage object:nil userInfo:messageDict];
         }
         else if (actionNumber == [orderUserCancelled.value intValue])
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsOrderMessage object:nil userInfo:messageDict];
         }
         else if (actionNumber == [messageStatus.value intValue])
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedNewMessage object:nil userInfo:nil];
             if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground)
             {
                 [self navigateTo: @"ChatChannelsViewController"];
             }
         }
         else if (actionNumber == [shiftUpdateStatus.value intValue])
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedShiftUpdated object:nil userInfo:nil];
             if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground)
             {
                 [self navigateTo: @"ScheduleViewController"];
             }
         }
         else if (actionNumber == [shiftNotifyStatus.value intValue])
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReceivedShiftNotify object:nil userInfo:nil];
             if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground)
             {
                 [self navigateTo: @"ScheduleViewController"];
             }
         }
     }
     
     completionHandler(UIBackgroundFetchResultNewData);

 }

-(void)navigateTo:(NSString *)controllerName
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
    SWRevealViewController *rootViewController = (SWRevealViewController *)self.window.rootViewController;
    UINavigationController *navController = [storyboard instantiateViewControllerWithIdentifier:@"RootNavigationController"];
    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:controllerName];
    
    
    //Twilio things
    self.pushKitEventDelegate = controller;
    [self initializePushKit];
    
    [navController setViewControllers:@[controller]];
    [rootViewController setFrontViewController:navController animated:NO];
}

- (void)initializePushKit {
    self.voipRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
    self.voipRegistry.delegate = self;
    self.voipRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
}


#pragma mark - PKPushRegistryDelegate
- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(NSString *)type {
    NSLog(@"pushRegistry:didUpdatePushCredentials:forType:");

    if ([type isEqualToString:PKPushTypeVoIP]) {
        if (self.pushKitEventDelegate && [self.pushKitEventDelegate respondsToSelector:@selector(credentialsUpdated:)]) {
            [self.pushKitEventDelegate credentialsUpdated:credentials];
        }
    }
}

- (void)pushRegistry:(PKPushRegistry *)registry didInvalidatePushTokenForType:(PKPushType)type {
    NSLog(@"pushRegistry:didInvalidatePushTokenForType:");

    if ([type isEqualToString:PKPushTypeVoIP]) {
        if (self.pushKitEventDelegate && [self.pushKitEventDelegate respondsToSelector:@selector(credentialsInvalidated)]) {
            [self.pushKitEventDelegate credentialsInvalidated];
        }
    }
}

- (void)pushRegistry:(PKPushRegistry *)registry
didReceiveIncomingPushWithPayload:(PKPushPayload *)payload
             forType:(NSString *)type {
    NSLog(@"pushRegistry:didReceiveIncomingPushWithPayload:forType:");
    
    if (self.pushKitEventDelegate &&
        [self.pushKitEventDelegate respondsToSelector:@selector(incomingPushReceived:withCompletionHandler:)]) {
        [self.pushKitEventDelegate incomingPushReceived:payload withCompletionHandler:nil];
    }
}

/**
 * This delegate method is available on iOS 11 and above. Call the completion handler once the
 * notification payload is passed to the `TwilioVoice.handleNotification()` method.
 */
- (void)pushRegistry:(PKPushRegistry *)registry
didReceiveIncomingPushWithPayload:(PKPushPayload *)payload
             forType:(PKPushType)type
withCompletionHandler:(void (^)(void))completion {
    NSLog(@"pushRegistry:didReceiveIncomingPushWithPayload:forType:withCompletionHandler:");
    
    if (self.pushKitEventDelegate &&
        [self.pushKitEventDelegate respondsToSelector:@selector(incomingPushReceived:withCompletionHandler:)]) {
        [self.pushKitEventDelegate incomingPushReceived:payload withCompletionHandler:completion];
    }

    if ([[NSProcessInfo processInfo] operatingSystemVersion].majorVersion >= 13) {
        /*
         * The Voice SDK processes the call notification and returns the call invite synchronously. Report the incoming call to
         * CallKit and fulfill the completion before exiting this callback method.
         */
        completion();
    }
}


@end
