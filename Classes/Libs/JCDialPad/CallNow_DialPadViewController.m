//
//  ExampleViewController.m
//  JCDialPadDemo
//
//  Created by Aron Bury on 18/01/2014.
//  Copyright (c) 2014 Aron Bury. All rights reserved.
//

#import "CallNow_DialPadViewController.h"
#import "JCDialPad.h"
#import "JCPadButton.h"
#import "FontasticIcons.h"

@implementation CallNow_DialPadViewController



     
- (void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(callStatus:) name:@"Notification_call_status" object:nil];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithImage: [UIImage imageNamed:@"backArrow.png"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(backPressed:)];

    self.navigationItem.leftBarButtonItem = backButton;

    [self.navigationItem setHidesBackButton:NO];
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                         forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];

    
    
    self.view.buttons = [[JCDialPad defaultButtons] arrayByAddingObjectsFromArray:@[]];
//
   // self.view.buttons = [JCDialPad defaultButtons];
    
    
    self.view.delegate = self;
    
//    UIImageView* backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wallpaper"]];
//	backgroundView.contentMode = UIViewContentModeScaleAspectFill;
    
	//[self.view setBackgroundView:backgroundView];
    
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    self.edgesForExtendedLayout=UIRectEdgeNone;

}
- (void)backPressed:(id)sender
{
    self.callNowVC.isComingFromDialpadScreen = YES;
    
    [self dismissViewControllerAnimated:YES completion:nil];
};

- (JCPadButton *)twilioButton
{
    UIImage *twilioIcon = [UIImage imageNamed:@"Twilio"];
    UIImageView *iconView = [[UIImageView alloc] initWithImage:twilioIcon];
    iconView.contentMode = UIViewContentModeScaleAspectFit;
    JCPadButton *twilioButton = [[JCPadButton alloc] initWithInput:@"T" iconView:iconView subLabel:@""];
    return twilioButton;
}

- (JCPadButton *)callButton
{
    
    UIImage *twilioIcon = [UIImage imageNamed:@"white_back"];
       UIImageView *iconView = [[UIImageView alloc] initWithImage:twilioIcon];
       iconView.contentMode = UIViewContentModeScaleAspectFit;
       JCPadButton *twilioButton = [[JCPadButton alloc] initWithInput:@"T" iconView:iconView subLabel:@""];
       return twilioButton;
//    FIIconView *iconView = [[FIIconView alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
//    iconView.backgroundColor = [UIColor clearColor];
//    iconView.icon = [FIFontAwesomeIcon backIcon];
//    iconView.padding = 15;
//    iconView.iconColor = [UIColor whiteColor];
//    JCPadButton *callButton = [[JCPadButton alloc] initWithInput:@"P" iconView:iconView subLabel:@""];
//    callButton.backgroundColor = [UIColor colorWithRed:0.261 green:0.837 blue:0.319 alpha:1.000];
//    callButton.borderColor = [UIColor colorWithRed:0.261 green:0.837 blue:0.319 alpha:1.000];
//    return callButton;
}

- (BOOL)dialPad:(JCDialPad *)dialPad shouldInsertText:(NSString *)text forButtonPress:(JCPadButton *)button
{
    
    NSLog(@"*******DIAL_TEXT :%@", text);
    
//    [self.callNowVC.call disconnect]; //----------?>>>>>>>>>>>>>>>>changee itttttt

    if (self.callNowVC.call && self.callNowVC.call.state == TVOCallStateConnected) {

    [self.callNowVC.call sendDigits:text  ] ;
    
    }
//    {
//
//
//
//        ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
//                                                            iconImg:[UIImage imageNamed:@"call_end"]
//                                                    backgroundStyle:ZHPopupViewBackgroundType_Blur
//                                                              title:@"Call Ended"
//                                                            content:@"Call has been disconnected"
//                                                       buttonTitles:@[@"Ok"]
//                                                confirmBtnTextColor:nil otherBtnTextColor:nil
//                                                 buttonPressedBlock:^(NSInteger btnIdx) {
//
//
//                                                     ///Go back
//                                                     [self dismissViewControllerAnimated:YES completion:nil] ;
//
//
//                                                 }];
//
//        popupView.contentTextAlignment  = NSTextAlignmentCenter;
//
//        [popupView present];
//    }
//

    
//    if ([text isEqualToString:@"T"]) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Respond to button presses!"
//                                                        message:@"Check out the JCDialPadDelegate protocol to do this"
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//        return NO;
//    } else if ([text isEqualToString:@"P"]) {
//        NSURL *callURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", dialPad.rawText]];
//        [[UIApplication sharedApplication] openURL:callURL];
//        return NO;
//    }
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
//



- (void)callDisconnected {
    
    ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                               iconImg:[UIImage imageNamed:@"call_end"]
                                                       backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                                 title:@"Call Ended"
                                                               content:@"Call has been disconnected"
                                                          buttonTitles:@[@"Ok"]
                                                   confirmBtnTextColor:nil otherBtnTextColor:nil
                                                    buttonPressedBlock:^(NSInteger btnIdx) {
                                                        
        
                                                        self.callNowVC.isComingFromDialpadScreen = YES;
        
                                                        ///Go back
                                                        [self dismissViewControllerAnimated:YES completion:nil] ;
                                                          
                                                        
                                                    }];
           
           popupView.contentTextAlignment  = NSTextAlignmentCenter;
           
           [popupView present];
}


/**
 
 Twilio call delegates
 
 */


#pragma mark - Notification
-(void) callStatus:(NSNotification *) notification
{
    
    [self callDisconnected];

}




@end
