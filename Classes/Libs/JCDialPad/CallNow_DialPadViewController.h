//
//  ExampleViewController.h
//  JCDialPadDemo
//
//  Created by Aron Bury on 18/01/2014.
//  Copyright (c) 2014 Aron Bury. All rights reserved.
//

#import "JCDialPad.h"
#import "CallNow.h"
@interface CallNow_DialPadViewController : UIViewController <JCDialPadDelegate,CallNowDelegate>

@property (strong, nonatomic) JCDialPad *view;
@property (strong, nonatomic) CallNow *callNowVC;

@property (weak, nonatomic) id<CallNowDelegate> delegate;


@end
