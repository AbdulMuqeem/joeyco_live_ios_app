//
//  BarcodeViewController.m
//  Joey
//
//  Created by Katia Maeda on 2016-02-25.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface BarcodeViewController () <UITableViewDataSource, UITableViewDelegate, ScannerViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *barcodeList;

@end

@implementation BarcodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.barcodeList = [NSMutableArray array];
    [self.barcodeList addObject:NSLocalizedString(@"Select to scan", nil)];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.barcodeList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"BarcodeTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSString *barcodeString = [self.barcodeList objectAtIndex:indexPath.row];
    cell.textLabel.text = barcodeString;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.barcodeList.count - 1)
    {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        ScannerViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ScannerViewController"];
        viewController.delegate = self;
        [self.navigationController pushViewController:viewController animated:YES];
    });
}

#pragma mark - ScannerViewControllerDelegate

-(void)scanViewController:(ScannerViewController *)scannerViewController didSuccessfullyScan:(NSString *)aScannedValue
{
    [self.barcodeList removeLastObject];
    
    if (aScannedValue)
    {
        [self.barcodeList addObject:aScannedValue];
    }
    
    [self.barcodeList addObject:NSLocalizedString(@"Select to scan", nil)];
    [self.tableView reloadData];
}

@end
