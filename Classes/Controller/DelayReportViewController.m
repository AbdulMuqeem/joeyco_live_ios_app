//
//  DelayReportViewController.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 16/11/2018.
//  Copyright © 2018 JoeyCo. All rights reserved.
//

#import "DelayReportViewController.h"
#import "OrderCancellationReasonCell.h"
#import "DelayOrderCell.h"
#import "OrderStatus.h"
#import "Prefix.pch"

@interface DelayReportViewController ()<UITableViewDataSource,UITableViewDelegate,JSONHelperDelegate>
{
    JSONHelper *json;
    
}
@end

@implementation DelayReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    //Setting by default:
    
    _selectedIndex = -1;
    
    //Hide Cells from bottom
    self.tableVIew.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];


}

-(void) viewWillAppear:(BOOL)animated{
    self.preferredContentSize= CGSizeMake(self.view.frame.size.width, [self createHeight:self.delayStatusObj]);
}

-(CGFloat)createHeight:(NSMutableArray *)arr{
    
    CGFloat cellHeight = 130;
    
    if ([arr count] == 1) {
        
        cellHeight = 130;
        return cellHeight;
    }
    else  if ([arr count] == 2) {
        cellHeight = 180;
        return cellHeight;
        
        
    }
    else  if ([arr count] == 3) {
        cellHeight = 250;
        return cellHeight;
        
        
    }
    else  if ([arr count] == 4) {
        cellHeight = 290;
        return cellHeight;
        
    }
    
    else  if ([arr count] > 4) {
        cellHeight = 320;
        return cellHeight;
        
    }
    
    return cellHeight;
    
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"DelayOrderCelllReuse";
    
    DelayOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (!cell) cell = [[DelayOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    __weak DelayOrderCell *weakCell = cell;
    
    OrderStatus * order = [self.delayStatusObj objectAtIndex:indexPath.row];
    cell.reason_label.text = order.orderDescription;
    
    
    
    //Setting check and uncheck image
    if (_selectedIndex == indexPath.row) {
        [cell.radioButton  setImage:[UIImage imageNamed:@"radio-checked.png"] forState:UIControlStateNormal];
        
    }
    else{
        [cell.radioButton  setImage:[UIImage imageNamed:@"radio-unchecked.png"] forState:UIControlStateNormal];
        
    }
    
    [cell.radioButton  addTarget:self action:@selector(RadioButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

    
    return weakCell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return [self.delayStatusObj count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Saving selected index
    self.selectedIndex =(int) indexPath.row;
    
    [self.tableVIew reloadData];
    
    
    
    /**
     Sending tapped data*/
    [NSNotificationCenter.defaultCenter postNotificationName:@"getTappedData" object:[self.delayStatusObj objectAtIndex:indexPath.row]];
    
    
    [self updateToServer:indexPath.row];
}

- (IBAction)closeButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)RadioButtonTapped:(id)sender {
    
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.tableVIew];
    NSIndexPath *indexPath = [self.tableVIew indexPathForRowAtPoint:buttonPosition];
    
    NSLog(@"ROW TAPPED : %ld",(long)indexPath.row);
    
    
    //Saving selected index
    self.selectedIndex =(int) indexPath.row;
    
    [self.tableVIew reloadData];
    
    
    
    
    //Update To server
    [self updateToServer:indexPath.row];
    
    
    /**
     Sending tapped data*/
    [NSNotificationCenter.defaultCenter postNotificationName:@"getTappedData" object:[self.delayStatusObj objectAtIndex:self.selectedIndex]];
    
    
    //Set Image on the cell tapped
   // DelayOrderCell *cell = [self.tableVIew cellForRowAtIndexPath:indexPath];
   // [cell.radioButton  setImage:[UIImage imageNamed:@"radio-checked.png"] forState:UIControlStateNormal];
    
}


/*
 Update To server
 */
- (void)updateToServer:(NSInteger)indexPathRow{
    
    
    OrderStatus * CancelModel = [self.delayStatusObj objectAtIndex:indexPathRow];
    
   // [json updateSystemStatus:[self.order.orderId stringValue] statusCode:CancelModel.orderStatusID];
  
    
    NSString *type_task = [self.taskType length]== 0 ? @"":self.taskType;
      
      
      [json updateSystemStatus:[self.task.sprintId  stringValue] statusCode:[CancelModel.orderStatusID stringValue] TaskId:[self.task.taskId stringValue] type:type_task];

}
//
//- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list {
//    
//}





/**
 On response of Update Status
 */
- (void)parsedObjects:(NSString *)action objects:(NSMutableDictionary *)responseString {
    
    
    
    ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                        iconImg:[UIImage imageNamed:@"send"]
                                                backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                          title:@"Success"
                                                        content:[responseString objectForKey:@"message"]
                                                   buttonTitles:@[@"Ok"]
                                            confirmBtnTextColor:nil otherBtnTextColor:nil
                                             buttonPressedBlock:^(NSInteger btnIdx) {
                                                 
                                                 [self dismissViewControllerAnimated:YES completion:nil];
                                                 
                                                 
                                             }];
    
    popupView.contentTextAlignment  = NSTextAlignmentCenter;
    
    [popupView present];
    
}

@end


