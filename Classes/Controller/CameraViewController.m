//
//  CameraViewController.m
//  Customer
//
//  Created by Katia Maeda on 2015-01-30.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface CameraViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    AlertHelper *alertHelper;
}

@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@property (nonatomic, weak) IBOutlet RoundedButton *takePictureButton;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *takePictureButtonWidth;

@property (nonatomic, strong) UIBarButtonItem *doneButton;

@end

@implementation CameraViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    alertHelper = [[AlertHelper alloc] init];
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        self.takePictureButton.hidden = YES;
        self.takePictureButtonWidth.constant = 0;
    }
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Upload", nil) style:UIBarButtonItemStylePlain target:self action:@selector(doneAction)];
    [self.doneButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15.0f]} forState: UIControlStateNormal];
    self.doneButton.tintColor = [UIColor colorWithRed:0.87 green:1.0 blue:0 alpha:1.0];
    self.navigationItem.rightBarButtonItem = nil;
    
    
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:backTitle style:UIBarButtonItemStylePlain target:self action:@selector(navigatingBack)];
//    [backButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:25.0f]} forState: UIControlStateNormal];
//    backButton.tintColor = [UIColor colorWithRed:0.87 green:1.0 blue:0 alpha:1.0];
//
//
    UIImage *backBtnGreen = [UIImage imageNamed:@"backBtnGreen.png"];
    UIButton *btnImg = [UIButton buttonWithType:UIButtonTypeCustom];
    btnImg.bounds = CGRectMake( 0, 0, backBtnGreen.size.width, backBtnGreen.size.height );
    [btnImg setImage:backBtnGreen forState:UIControlStateNormal];

    UIBarButtonItem *backBarBtn = [[UIBarButtonItem alloc] initWithImage:backBtnGreen style:UIBarButtonItemStylePlain target:self action:@selector(navigatingBack)];

    self.navigationItem.leftBarButtonItem = backBarBtn;
    
    self.imageView.image = self.image;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [alertHelper dismissAlert];
}

-(void)navigatingBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)doneAction
{
    self.pictureSelectedCompletion(self.imageView.image);
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)takePictureAction:(id)sender
{
    
    
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:picker animated:YES completion:NULL];
    });
}

- (IBAction)selectPictureAction:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:picker animated:YES completion:NULL];
    });
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:NULL];

    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    UIImage *compressedImage = [ImageHelper compress:originalImage];

    dispatch_async(dispatch_get_main_queue(), ^{
        self.imageView.image = compressedImage;
        self.navigationItem.rightBarButtonItem = self.doneButton;
    });
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

@end
