//
//  ComplainSection.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 22/03/2019.
//  Copyright © 2019 JoeyCo. All rights reserved.
//



#import "NavigationContainerViewController.h"

@import RSKPlaceholderTextView;


@interface ComplainSection : NavigationContainerViewController

@property (weak, nonatomic) IBOutlet UILabel *lbl_joeyID;
@property (weak, nonatomic) IBOutlet UITextField *textfield_orderId;
@property (weak, nonatomic) IBOutlet RSKPlaceholderTextView *textView_desc_issue;
@property (weak, nonatomic) IBOutlet UITextField *textfield_issueType;

- (IBAction)submitAction:(id)sender;

@end

