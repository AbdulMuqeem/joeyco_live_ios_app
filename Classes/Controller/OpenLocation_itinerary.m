//
//  OpenLocation_itinerary.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 23/06/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import "OpenLocation_itinerary.h"
#import "Prefix.pch"
#import "OpenLocationBottomSheet.h"
#import "MDCBottomSheetController.h"

#import "LocationObjectForBirdView.h"

@interface OpenLocation_itinerary ()<GMSMapViewDelegate>
{
    GMSCameraPosition *camera;
    GMSMapView *mapView;
}
@property(nonatomic,strong) MDCBottomSheetController *bottomSheetDropOff;

@end

@implementation OpenLocation_itinerary
NSInteger trackingCount= 0;
- (void)initallMarkers{
    self.arr_allMarkers = [[NSMutableArray alloc]init];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Initializing list of all markers
//    self.arr_allMarkers = [[NSMutableArray alloc]init];
 
 
    lastJoeyLocation = [[LocationManager shared] lastKnownLocation];

     camera = [GMSCameraPosition cameraWithLatitude:lastJoeyLocation.coordinate.latitude
                                                            longitude:lastJoeyLocation.coordinate.longitude
                                                                 zoom:14];
    mapView = [GMSMapView mapWithFrame:self.view.frame camera:camera];
    mapView.settings.zoomGestures = YES;
    mapView.settings.myLocationButton = YES;
    mapView.trafficEnabled = YES;
    mapView.delegate=self;
    mapView.padding =UIEdgeInsetsMake(0, 0, 40, 0);
    

    mapView.myLocationEnabled = YES;
    
//    [mapView addSubview:self.closeBtn];
    [self.view addSubview:mapView];
    
    //Adding my current location
    [self createMyLocationMarker];

    //CHeck if its bird view on just single view
    
    if(_is_BirdView){
        
        for (id i in _arr_allMarkers)
        {
            LocationObjectForBirdView *obj = i;
            int count = [self getRouteCount:obj];
            if(count != -1){
                [self addMutlipleMarkerToMap:obj routeCount:count];
            }
            
        }
//        ItineraryOrder *itn = (ItineraryOrder *)[_arr_allMarkers objectAtIndex:0];
//        [mapView animateToLocation:CLLocationCoordinate2DMake(itn.lat,self.singleMarkerLongitude)];

    }
    else
    {
        //Adding marker of the item tapped
        [self addSingleMarkerToMap];
        
    }
    
 

}

-(void)addSingleMarkerToMap{
    
       UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,90,100)];
      
    
       UILabel *label = [UILabel new];
        label.backgroundColor = [UIColor whiteColor];
    [label.font fontWithSize:11.0];
    
    label.text = self.markerTitle_singleMarker;
       [label sizeToFit];
    
     UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"default_restaurants.png"]];
    pinImageView.frame = CGRectMake(0,label.frame.size.height + 8, 40 , 40);
     
    [view addSubview:pinImageView];
    [view addSubview:label];

       //i.e. customize view to get what you need


       UIImage *markerIcon = [self imageFromView:view];
      // marker.icon = markerIcon;
    
    
    // Creates a marker in the center of the map.
      GMSMarker *marker = [[GMSMarker alloc] init];
      marker.position = CLLocationCoordinate2DMake(self.singleMarkerLatitude, self.singleMarkerLongitude);
     // marker.title = self.markerTitle_singleMarker;
     // marker.icon = [UIImage imageNamed:@"default_restaurants.png"];
      marker.icon = markerIcon;
      marker.map = mapView;
    [mapView animateToLocation:CLLocationCoordinate2DMake(self.singleMarkerLatitude,self.singleMarkerLongitude)];

}


-(void)addMutlipleMarkerToMap:(LocationObjectForBirdView *)obj routeCount:(NSInteger)count{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,110,120)];
      
//    UIView *counterVIew = [UIView new];
    UILabel *counter = [UILabel new];
    UILabel *label = [UILabel new];
    counter.backgroundColor = [UIColor redColor];
    counter.textColor = [UIColor whiteColor];
    NSString *strX=[NSString stringWithFormat:@"%d",count];
    [counter.font fontWithSize:11.0];
    counter.text = strX;
    if([strX isEqual: @"1"]){
        counter.hidden = YES;
        
    }else{
        label.hidden = YES;
    }
//    counter.layer.cornerRadius = 50;
//    counter.layer.masksToBounds = YES;
    [[counter layer] setCornerRadius:10.0f];
    [[counter layer] setMasksToBounds:YES];
    if (@available(iOS 14.0, *)) {
        counter.textAlignment = UIListContentTextAlignmentCenter;
    } else {
        counter.textAlignment = UITextAlignmentCenter;
        // Fallback on earlier versions
    }
//    [counterVIew addSubview:counter];
    //    [counter.layer setCornerRadius:100.0];

//    [counter sizeToFit];
    
     
        label.backgroundColor = [UIColor whiteColor];
    [label.font fontWithSize:8.0];
  

    //Setting title above marker
    label.text = obj.MarkerTitle;
       [label sizeToFit];
    
    
     UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"default_restaurants.png"]];
    pinImageView.frame = CGRectMake(0,label.frame.size.height + 8, 40 , 40);
    
    [pinImageView addSubview:counter ];
    counter.frame = CGRectMake(pinImageView.frame.size.width -20,-10,25,25);

     
    [view addSubview:pinImageView];
    [view addSubview:label];
//    [view addSubview:counter];
    
       //i.e. customize view to get what you need


       UIImage *markerIcon = [self imageFromView:view];
      // marker.icon = markerIcon;
    
    
    // Creates a marker in the center of the map.
      GMSMarker *marker = [[GMSMarker alloc] init];
      marker.position = CLLocationCoordinate2DMake([obj.latitude doubleValue], [obj.longitude doubleValue]);
     // marker.title = self.markerTitle_singleMarker;
     // marker.icon = [UIImage imageNamed:@"default_restaurants.png"];
      marker.icon = markerIcon;
      marker.map = mapView;
    
}

-(NSString*) latitude:(double)lat longitude:(double)lng {
    
    
//    NSMutableArray *fileNames = [NSMutableArray array];
        NSString *allTrackingIds= @"";
     trackingCount= 0;
    NSNumber *lt = [NSNumber numberWithDouble:lat];
    NSNumber *lg = [NSNumber numberWithDouble:lng];
    
    for(id i in _arr_allMarkers){
        LocationObjectForBirdView *obj = i;
        if([lt isEqual: obj.latitude] && [lg isEqual: obj.longitude]){
            trackingCount++;
        allTrackingIds = [allTrackingIds stringByAppendingString:obj.MarkerTitle];
        allTrackingIds = [allTrackingIds stringByAppendingString:@"\n"];
        }
//        [allTrackingIds stringByAppendingString:obj.MarkerTitle];
//        i
//        [fileNames addObject: [NSString stringWithFormat:, i]];lat    double    45.479140000000001
    }
    
    return allTrackingIds;
}

-(int)getRouteCount:(LocationObjectForBirdView *)obj {
    int counter = 0;
    if([obj.isChecked isEqual: @"1"]){
        return -1;
    }
    for (id i in _arr_allMarkers){
        LocationObjectForBirdView *obja = i;
        if([obja.latitude isEqual: obj.latitude] && [obja.longitude isEqual: obj.longitude]){
            obja.isChecked = @"1";
            counter++;
        }
       
    }
    return counter;
}
-(void)createMyLocationMarker{
    // Creates a marker in the center of the map.
      GMSMarker *marker = [[GMSMarker alloc] init];
      marker.position = CLLocationCoordinate2DMake(lastJoeyLocation.coordinate.latitude, lastJoeyLocation.coordinate.longitude);
      marker.title = @"Your location";
      marker.icon = [UIImage imageNamed:@"customer.png"];
      marker.map = mapView;
//    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lastJoeyLocation.coordinate.latitude
//                                                            longitude:lastJoeyLocation.coordinate.longitude
//                                                                 zoom:16];
//    NSLog(@"%s","current location");
//    NSLog(@"%f",lastJoeyLocation.coordinate.latitude);
//    NSLog(@"%f",lastJoeyLocation.coordinate.longitude);
//
//
//    [mapView setCamera:camera];
}


#pragma mark -Google Map Delegate method

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    
    [self openBottomSheet:marker];
    
    return YES;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    [self openBottomSheet:marker];

}

-(void)openBottomSheet:(GMSMarker *)marker{
    
    // View controller the bottom sheet will hold
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
        
            OpenLocationBottomSheet * vc = [storyboard instantiateViewControllerWithIdentifier:@"OpenLocationBottomSheet"];
    
            vc.latitude= marker.position.latitude;
            vc.longitude= marker.position.longitude;
   
    vc.trackingIds= [self latitude:marker.position.latitude longitude:marker.position.longitude];
    if(trackingCount > 1){
        vc.isMultipleIDs = @"1";
    }else{
        vc.isMultipleIDs = @"0";
    }
//    vc.trackingIds= @"an \\n as \n klasjd  \n qasldf";
        

        // Initialize the bottom sheet with the view controller just created
        self.bottomSheetDropOff = [[MDCBottomSheetController alloc] initWithContentViewController:vc];
      
         [self presentViewController:self.bottomSheetDropOff animated:true completion:nil];
}


- (IBAction)closeMap:(id)sender {
 [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)viewWillDisappear:(BOOL)animated{
    _arr_allMarkers = nil;
}




- (UIImage *)imageFromView:(UIView *) view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (IBAction)closeBtn_action:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
