//
//  Checklist_bottomSheetVC.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 02/06/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import "Checklist_bottomSheetVC.h"
#import "OrderCancellationReasonCell.h"
#import "DelayOrderCell.h"
#import "OrderStatus.h"
#import "Prefix.pch"
#import "ScanditScanViewController.h"
@interface Checklist_bottomSheetVC ()<UITableViewDelegate,UITableViewDataSource,JSONHelperDelegate>
{
    BOOL firstLayout;
    AlertHelper *alertHelper;
    JSONHelper *json;
}

@end

@implementation Checklist_bottomSheetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Set title and subtitle
    self.bottomSheet_title.text = self.text_bottomSheet_title;
    self.bottomSheet_subtitle.text = self.text_bottomSheet_subtitle;
    
    
    json = [[JSONHelper alloc] init:self andDelegate:self];
      
    
      //Setting by default:
      
      _selectedIndex = -1;
     
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
  static NSString *cellIdentifier = @"DelayOrderCelllReuse";
       
       DelayOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
       
       if (!cell) cell = [[DelayOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
       [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
       
       __weak DelayOrderCell *weakCell = cell;
       
    
    
    OrderStatus * CancelModel = [self.itemsArray objectAtIndex:indexPath.row];
    cell.reason_label.text = CancelModel.orderDescription;
    
    
    //Setting check and uncheck image
    if (_selectedIndex == indexPath.row) {
        [cell.radioButton  setImage:[UIImage imageNamed:@"radio-checked.png"] forState:UIControlStateNormal];
        
    }
    else{
        [cell.radioButton  setImage:[UIImage imageNamed:@"radio-unchecked.png"] forState:UIControlStateNormal];
        
    }
    
    [cell.radioButton  addTarget:self action:@selector(RadioButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

    
    return weakCell;
    
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.itemsArray count];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //Saving selected index
    self.selectedIndex =(int) indexPath.row;
    
    
     
     [self dismissViewControllerAnimated:YES completion:nil];
    
    
    //Delegate method
    [self.delegate optionSelected:(int)indexPath.row checklistObj:self];
    
//    /**
//     Sending tapped data*/
//    [NSNotificationCenter.defaultCenter postNotificationName:@"GetTappedData_checklist" object:[self.itemsArray objectAtIndex:indexPath.row]];
    
      
      
    
    
    
   // [self updateToServer:indexPath.row];
        
    
}

- (IBAction)RadioButtonTapped:(id)sender {
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:buttonPosition];
    
    NSLog(@"ROW TAPPED : %ld",(long)indexPath.row);
    
    
    //Saving selected index
    self.selectedIndex =(int) indexPath.row;
    
    
    
  //  [self.tblView reloadData];
    

     
    //Update To server
  //  [self updateToServer:indexPath.row];
    
   
       
      
    
     
    
    /**
     Sending tapped data*/
    [NSNotificationCenter.defaultCenter postNotificationName:@"GetTappedData_checklist" object:[self.itemsArray objectAtIndex:self.selectedIndex]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
       
    
    
}

//
///*
// Update To server
// */
//- (void)updateToServer:(NSInteger)indexPathRow{
//
//
//    OrderStatus * CancelModel = [self.cancelStatusObj objectAtIndex:indexPathRow];
//
//    NSString *type_task = [self.taskType length]== 0 ? @"":self.taskType;
//
//
//    [json updateSystemStatus:[self.task.sprintId  stringValue] statusCode:[CancelModel.orderStatusID stringValue] TaskId:[self.task.taskId stringValue] type:type_task];
//}
//



/**
 On response of Update Status
 */
- (void)parsedObjects:(NSString *)action objects:(NSMutableDictionary *)responseString {
    
    
    
    ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                        iconImg:[UIImage imageNamed:@"send"]
                                                backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                          title:@"Success"
                                                        content:[responseString objectForKey:@"message"]
                                                   buttonTitles:@[@"Ok"]
                                            confirmBtnTextColor:nil otherBtnTextColor:nil
                                             buttonPressedBlock:^(NSInteger btnIdx) {
                                                 
                                                 [self dismissViewControllerAnimated:YES completion:nil];
                                                 
                                                 
                                             }];
    
    popupView.contentTextAlignment  = NSTextAlignmentCenter;
    
    [popupView present];
    
}


-(void) viewWillAppear:(BOOL)animated{
    self.preferredContentSize= CGSizeMake(self.view.frame.size.width, [self createHeight:self.itemsArray]);
}

-(CGFloat)createHeight:(NSMutableArray *)arr{
    
    CGFloat cellHeight = 130;
    
    if ([arr count] == 1) {
        
        cellHeight = 130;
        return cellHeight;
    }
    else  if ([arr count] == 2) {
        cellHeight = 180;
        return cellHeight;
        
        
    }
    else  if ([arr count] == 3) {
        cellHeight = 250;
        return cellHeight;
        
        
    }
    else  if ([arr count] == 4) {
        cellHeight = 290;
        return cellHeight;
        
    }
    
    else  if ([arr count] > 4) {
        cellHeight = 320;
        return cellHeight;
        
    }
    
    return cellHeight;
    
}

- (IBAction)closeButton:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}



@end
