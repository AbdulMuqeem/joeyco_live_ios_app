//
//  OpenLocationBottomSheet.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 13/08/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import "OpenLocationBottomSheet.h"
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)


@interface OpenLocationBottomSheet ()
@property (weak, nonatomic) IBOutlet UILabel *multidropslbl;
@property (weak, nonatomic) IBOutlet UILabel *trackingids;
@property (weak, nonatomic) IBOutlet UIScrollView *trackingIdsScrollView;

@end

@implementation OpenLocationBottomSheet

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([_isMultipleIDs isEqual:@"0"]){
        _multidropslbl.hidden = YES;
        _trackingIdsScrollView.hidden= YES;
    }else{
        self.preferredContentSize= CGSizeMake(self.view.frame.size.width,400);
    }
    
    
    if (_is_itinerary == YES) {
        _multidropslbl.hidden = NO;

    }else
    {_multidropslbl.hidden = YES;

    }
    
    _trackingIdsScrollView.scrollEnabled = YES;
    _trackingIdsScrollView.contentSize = CGSizeMake(375, 500);
    // Do any additional setup after loading the view.
    
//    for(id i in self.trackingIds){
//        _trackingids.text= i;
//    }
    _trackingids.text= self.trackingIds;
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleCloseBtnTap:)];
       tapGesture.numberOfTapsRequired = 1;
       [self.view addGestureRecognizer:tapGesture];
    
    
}
- (void)viewWillAppear:(BOOL)animated{
   
}

- (IBAction)googleMapsAction:(id)sender {
    [self openGoogleMaps];
}

- (IBAction)wazeAction:(id)sender {
    [self openWaze];
}

- (IBAction)closeButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}


- (void)handleCloseBtnTap:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateRecognized) {
        // handling code
        [self closeButton:sender];
    }
}


-(void)openWaze
{
  //  Task *task = self.task;
    
    
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"waze://"]]) // Waze is installed. Launch Waze and start navigation
    {
        NSString *urlStr = [NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes", self.latitude, self.longitude];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    }
    else // Waze is not installed. Launch AppStore to install Waze app
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
    }
}


-(void)openGoogleMaps
{
    
   // Task *task = self.task;

    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps-x-callback://"]]) // Waze is installed. Launch Waze and start navigation
    {
        NSString *urlStr = [NSString stringWithFormat:@"comgooglemaps://?saddr=&daddr=%f,%f&directionsmode=driving&x-success=sourceapp://?resume=true&x-source=AirApp", self.latitude, self.longitude];
        
//         NSString *urlStr = [NSString stringWithFormat:@"comgooglemaps-x-callback://?saddr=&daddr=%f,%f&directionsmode=driving", [task.location.locationLatitude doubleValue], [task.location.locationLongitude doubleValue]];
//
//        NSString *urlStr = @"comgooglemaps-x-callback://?daddr=John+F.+Kennedy+International+Airport,+Van+Wyck+Expressway,+Jamaica,+New+York&x-success=sourceapp://?resume=true&x-source=AirApp";
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    }
    else // Waze is not installed. Launch AppStore to install Waze app
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://apps.apple.com/us/app/google-maps-transit-food/id585027354"]];
    }
}
- (IBAction)closeBtnAction:(id)sender {
[self dismissViewControllerAnimated:YES completion:nil];
}

@end
