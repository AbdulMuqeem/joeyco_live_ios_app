//
//  OpenLocationBottomSheet.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 13/08/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"
NS_ASSUME_NONNULL_BEGIN

@interface OpenLocationBottomSheet : UIViewController

@property(strong, nonatomic)NSMutableArray * responseData;

@property (nonatomic, assign) BOOL is_itinerary;

@property (nonatomic, assign) CGFloat latitude;
@property (nonatomic, assign) CGFloat longitude;
@property (nonatomic, assign) NSString *trackingIds;
@property (nonatomic, assign) NSString *isMultipleIDs;



- (IBAction)closeButton:(id)sender;
- (IBAction)wazeAction:(id)sender;
- (IBAction)googleMapsAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
