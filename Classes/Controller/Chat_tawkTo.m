//
//  Chat_tawkTo.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 15/05/2019.
//  Copyright © 2019 JoeyCo. All rights reserved.
//

#import "Prefix.pch"
#import "Urlinfo.h"
@interface Chat_tawkTo ()

@end

@implementation Chat_tawkTo

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *urlString = @"http://www.joeyco.com";
    
    
    if ([ [UrlInfo getEnvironment] isEqualToString:@""])
    {
       // urlString = @"https://joey.joeyco.com/joey/chat/"+UrlInfo.joeyId";
        urlString = [NSString stringWithFormat:@"https://joey.joeyco.com/joey/chat/%d",[[UrlInfo getJoeyId] intValue]];

    }
    else   if ([ [UrlInfo getEnvironment] isEqualToString:@"s"])

    {
      //  mWebView.loadUrl("https://joey.staging.joeyco.com/joey/chat/"+UrlInfo.joeyId);
        urlString = [NSString stringWithFormat:@"https://joey.staging.joeyco.com/joey/chat/%d",[[UrlInfo getJoeyId] intValue]];
        
    }
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];

    _webView = [[WKWebView alloc] initWithFrame:self.view.frame];
    [_webView loadRequest:urlRequest];
        _webView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height - 45);
        [self.view addSubview:_webView];
    
//    self.webView.frame = self.view.bounds;
//    [_webView loadRequest:urlRequest];
    

}


@end
