//
//  ScanditScanViewController.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 19/05/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import "ScanditScanViewController.h"
#import "QuartzCore/QuartzCore.h"



@interface ScanditScanViewController ()<JSONHelperDelegate,SBSScanDelegate,SBSProcessFrameDelegate >{
    
    int validItemsCounted ;
    JSONHelper *json;
    AlertHelper *alertHelper;


}

@property(nonatomic,strong)SBSBarcodePicker *picker;

@end

@implementation ScanditScanViewController


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    
   //  [self dismissVC];
    
//    if([self isBeingDismissed])
//    {
//        [self dismissVC];
//    }
}

-(void)dismissVC{
    
 //   [self.picker  stopScanning];

    
    //to notify root vc
    [self.delegate scanViewScreenBeingDismissed];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //extract tracking ids'
//    self.tracking_ids
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    
    
    //Alert view controller
     alertHelper = [[AlertHelper alloc] init];

    
    //init JSON
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    //Hide the count label initially
    self.items_count.hidden = YES;
    

    //Init array for storing valid tracking id which matched parcel/scanned item
    self.valid_scanned_tracking_ids = [[NSMutableArray alloc]init];
    
    
    //Init array with tracking and time stamp for server upload
    self.trackingIdWithTimeStamp = [[NSMutableArray alloc]init];

    //Valid items
    validItemsCounted =0;
    

    [self setupScanner];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // Switch camera on to start streaming frames. The camera is started asynchronously and will
    // take some time to completely turn on.
//    self.barcodeCapture.enabled = YES;
//    [self.camera switchToDesiredState:SDCFrameSourceStateOn];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    // Switch camera off to stop streaming frames. The camera is stopped asynchronously and will
    // take some time to completely turn off. Until it is completely stopped, it is still possible
    // to receive further results, hence it's a good idea to first disable barcode capture as well.
//    self.barcodeCapture.enabled = NO;
//    [self.camera switchToDesiredState:SDCFrameSourceStateOff];
}


- (void)setupScanner {
    SBSScanSettings *scanSettings = [SBSScanSettings defaultSettings];
    // Enable the relevant symbologies.
    [scanSettings setSymbology:SBSSymbologyEAN13 enabled:YES];
    [scanSettings setSymbology:SBSSymbologyCode39 enabled:YES];
    [scanSettings setSymbology:SBSSymbologyUPC12 enabled:YES];
    [scanSettings setSymbology:SBSSymbologyUPCE enabled:YES];
    [scanSettings setSymbology:SBSSymbologyPDF417 enabled:YES];
//    [scanSettings setSymbology:SBSSymbologyDatamatrix enabled:YES];
    [scanSettings setSymbology:SBSSymbologyQR enabled:YES];
    [scanSettings setSymbology:SBSSymbologyITF enabled:YES];
    [scanSettings setSymbology:SBSSymbologyCode128 enabled:YES];
    [scanSettings setSymbology:SBSSymbologyCode93 enabled:YES];
    [scanSettings setSymbology:SBSSymbologyMSIPlessey enabled:YES];
    [scanSettings setSymbology:SBSSymbologyGS1Databar enabled:YES];
    [scanSettings setSymbology:SBSSymbologyGS1DatabarExpanded enabled:YES];
    [scanSettings setSymbology:SBSSymbologyCodabar enabled:YES];
    [scanSettings setSymbology:SBSSymbologyTwoDigitAddOn enabled:YES];
    [scanSettings setSymbology:SBSSymbologyAztec enabled:YES];
    [scanSettings setSymbology:SBSSymbologyFiveDigitAddOn enabled:YES];
    [scanSettings setSymbology:SBSSymbologyCode11 enabled:YES];
    [scanSettings setSymbology:SBSSymbologyMaxiCode enabled:YES];
    [scanSettings setSymbology:SBSSymbologyGS1DatabarLimited enabled:YES];
    [scanSettings setSymbology:SBSSymbologyCode25 enabled:YES];
    [scanSettings setSymbology:SBSSymbologyMicroPDF417 enabled:YES];
    [scanSettings setSymbology:SBSSymbologyRM4SCC enabled:YES];
    [scanSettings setSymbology:SBSSymbologyMicroPDF417 enabled:YES];
    [scanSettings setSymbology:SBSSymbologyKIX enabled:YES];
    [scanSettings setSymbology:SBSSymbologyDotCode enabled:YES];
    [scanSettings setSymbology:SBSSymbologyMicroQR enabled:YES];
    [scanSettings setSymbology:SBSSymbologyCode32 enabled:YES];


    
     // Enable code rejection
     //  scanSettings.codeRejectionEnabled = YES;
    
    //Enable restrict area
    [scanSettings setActiveScanningArea:CGRectMake(0, 0.48, 1, 0.04)];
   
    // Enable Matrix Scanning.
    scanSettings.matrixScanEnabled = NO;
    // The maximum number of codes to be decoded every frame.
    scanSettings.maxNumberOfCodesPerFrame = 1;
    
    //Filtering out duplicates
   // scanSettings.codeDuplicateFilter =  -1;
    scanSettings.codeDuplicateFilter = 2000;

    // Create the picker, set the listener, add the picker to the view hierarchy and start it.
//    SBSBarcodePicker *picker = [[SBSBarcodePicker alloc] initWithSettings:scanSettings];
  
    self.picker = [[SBSBarcodePicker alloc] initWithSettings:scanSettings];

     self.picker .scanDelegate = self;
    // Set the GUI style accordingly.
      self.picker .overlayController.guiStyle = SBSGuiStyleLaser;
    [self addChildViewController: self.picker ];
     self.picker .view.frame = self.pickerParentView.bounds;
   // [self.view addSubview:picker.view];
    [self.pickerParentView addSubview: self.picker .view];
   // [self.view bringSubviewToFront:self.close_btn];
    
    self.close_btn.layer.zPosition = 1;

    [  self.picker  didMoveToParentViewController:self];
    // Set the process frame listener.
      self.picker .processFrameDelegate = self;
    [  self.picker  startScanning];
}


/*
 
 NOT USED ANY MORE BECAUSE WE HAVE DISABLED MATRIX SCAN
 */


- (void)barcodePicker:(SBSBarcodePicker *)aPicker didProcessFrame:(CMSampleBufferRef)frame session:(SBSScanSession *)session {
    
//     //  for (SBSCode *trackedCode in session.newlyRecognizedCodes) {
//        //    __block BOOL shouldReject = NO;
//
//          for (SBSCode *trackedCode in session.newlyRecognizedCodes) {
//
//
//           //THis is check is to show red and green boxes
//                 if ([self searchItem:trackedCode.data]== NO){
//
//                     [session rejectCode:trackedCode];
//
//                     dispatch_sync(dispatch_get_main_queue(), ^{
//
//                [self setBackgroundColors_invalid:self.picker.overlayController.view];
//
//                        });
//
//                 }
//
//        }
  //     }
    /*
    for (NSNumber *identifier in session.trackedCodes) {
        SBSTrackedCode *trackedCode = session.trackedCodes[identifier];
        
        
        
//        //THis is check is to show red and green boxes
//        if ([self searchItem:trackedCode.data]== NO){
//
//            [session rejectTrackedCode:trackedCode];
//        }

        //First search if the scanned item belong to your list of webservice  or not
        //Then, checking if this has been already marked and counted or not
                        
        if ([self searchItem:trackedCode.data] && [self itemScannedAlready:trackedCode.data] == NO) {
                     [self updateTotalCount];
                     [self addItemInVerifiedTrackingCodeList:trackedCode.data];
            
            
            //checking if its BULK PICKUP scan or just single Drop off
                  
                  if (self.is_singleScan == YES) {
                     

                      //if drop off then open a bottom sheet
                      if(self.is_pickup){
                        
                          //Call Webservice and send that tracking id immeditaely to server
                         [json ItineraryBulkScanApi:self.trackingIdWithTimeStamp];
                      }
                      else
                      {
                          //Its Single Scan . And would be used for drop off
                         //so after perfect match just move back to itinery orders and open BOttom sheet
                            
                          
                          // This will wait to finish
                          dispatch_sync(dispatch_get_main_queue(), ^{
                              // Update the UI on the main thread.
                              
                              [self dismissViewControllerAnimated:YES completion:^{
                                  [self.delegate openSheetOnDismissOfScanScreen:self.modelData];
                              }];

                          });
                          

                   
                      }
                      
                  }
        }
        
        
      
     
      
//        if([trackedCode.data isEqualToString:@"12345"])
//        {
//             [session rejectTrackedCode:trackedCode];
//        }
        // Reject all Code39 barcodes.
//        if (trackedCode.symbology == SBSSymbologyCode39) {
//            [session rejectTrackedCode:trackedCode];
//        }
    }
     
     
     */
}

- (void)barcodePicker:(SBSBarcodePicker *)picker didScan:(SBSScanSession *)session {

    
    NSArray *recognized = session.newlyRecognizedCodes;
    SBSCode *trackedCode = [recognized firstObject];
//
    
//    for (SBSCode *trackedCode in session.trackedCodes) {

 //   for (SBSCode *trackedCode in session.newlyRecognizedCodes) {
       // __block BOOL shouldReject = NO;
        
        if ([self searchItem:trackedCode.data]== NO){
                   [session rejectCode:trackedCode];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                  
                [self setBackgroundColors_invalid:self.picker.overlayController.view];
      
                });
            
        }
    else
    {
        dispatch_sync(dispatch_get_main_queue(), ^{

        //Show Greeen color blinking
        [self setBackgroundColors_valid:self.picker.overlayController.view];
        
        });

    }
                          
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            //Core logic to add valid codes
            [self addingValidItems:trackedCode];
           // shouldReject = [self shouldRejectCode:code];
        });
        
       
  //  }
    
    
    
     
      
      
    
    
    // Number of expected barcodes.
//    int numExpectedCodes = 4000;
//  //  int numExpectedCodes = (int)[self.tracking_ids count];
//    // Get all the scanned barcodes from the session.
//    NSArray *allCodes = session.allRecognizedCodes;
//    // If the number of scanned codes is greater than or equal to the number of expected barcodes,
//    // pause the scanning and clear the session (to remove recognized barcodes).
//    if ([allCodes count] >= numExpectedCodes) {
//        // Stop scanning or pause and clear the session.
//        [session stopScanning];
//        // ...
//    }
//
//    for (NSNumber *identifier in session.trackedCodes) {
//           SBSTrackedCode *trackedCode = session.trackedCodes[identifier];
//
//        if ([self searchItem:trackedCode.data]) {
//               [self updateTotalCount];
//               [self addItemInVerifiedTrackingCodeList:trackedCode.data];
//           }
//           else
//           {
//               [session rejectTrackedCode:trackedCode];
//           }
//    }
    
}

- (IBAction)close_scanner:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)updateTotalCount{
    dispatch_async( dispatch_get_main_queue(), ^{

    
    self.items_count.hidden = NO;
    
    [self updateItemCount];
    
        self.items_count.text = [NSString stringWithFormat:@"%d out of %d items scanned",self->validItemsCounted,(int)[self.tracking_ids count]];
        
    });
}

-(BOOL)searchItem:(NSString *)searchedItemName{
    
   
    BOOL itemExists = NO;

    for (int i = 0; i < [self.tracking_ids count];i++ )
    {
        NSString *item = [self.tracking_ids objectAtIndex:i];


        if ([searchedItemName isEqualToString:item])
        {
            
            itemExists =YES;
        }
    }

    
    return itemExists;
    
}


-(BOOL)itemScannedAlready:(NSString *)searchedItemName{
    
   
    BOOL itemExists = NO;

    for (int i = 0; i < [self.valid_scanned_tracking_ids count];i++ )
    {
        NSString *item = [self.valid_scanned_tracking_ids objectAtIndex:i];


        if ([searchedItemName isEqualToString:item])
        {
            
            itemExists =YES;
        }
    }

    
    return itemExists;
    
}

-(void)addItemInVerifiedTrackingCodeList :(NSString *)item{
    
    double timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSNumber *timeStampToString = [NSNumber numberWithDouble:timestamp];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:item forKey:@"tracking_id"];
    [dict setObject:[timeStampToString stringValue]forKey:@"time_stamp"];

    
    //Used for sending to server
    [self.trackingIdWithTimeStamp addObject:dict];
    
    //Used for scanning
    [self.valid_scanned_tracking_ids addObject:item];
}

-(void)updateItemCount{
    
    //Increase total count
      validItemsCounted ++;
}
- (IBAction)upload:(id)sender {

    //Call Webservice
    [json ItineraryBulkScanApi:self.trackingIdWithTimeStamp];
}

#pragma mark -Webservice Delegate methods
- (void)parsedObjects:(NSString *)action objects:(NSString *)responseString {
    
    //Open success pop up
    [self openDialog:responseString];
    
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{

    [alertHelper showAlertWithOneButton:@"Error" message:message from:self buttonTitle:@"Ok" withHandler:^{
        
        if([self isBeingPresented]){
                       [self dismissViewControllerAnimated:YES completion:nil];
                 }
                 //else if([self isMovingToParentViewController]){
                     else {

                         [self.navigationController popViewControllerAnimated:YES];
                 }
    }];
    
   
          
}
-(void)openDialog:(NSString *)responseString {
    
    
    ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                        iconImg:[UIImage imageNamed:@"send"]
                                                backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                          title:@"Response"
                                                        content:responseString
                                                   buttonTitles:@[@"Ok"]
                                            confirmBtnTextColor:nil otherBtnTextColor:nil
                                             buttonPressedBlock:^(NSInteger btnIdx) {
                                                 
                                                 
//        if([self isBeingPresented]){
//              [self dismissViewControllerAnimated:YES completion:nil];
//        }
//        //else if([self isMovingToParentViewController]){
//            else {
//
//            [[self navigationController]popViewControllerAnimated:YES];
//        }
//        }];
    
    
    
        if([self isBeingPresented]){
            
            
        [self dismissViewControllerAnimated:YES completion:^{
     //CALLING DELEGATE METHOD
     //to refresh data with latest webservice data
     [self.delegate scanViewScreenBeingDismissed];
            }];
        }
        //else if([self isMovingToParentViewController]){
            else {
                [CATransaction begin];
                [CATransaction setCompletionBlock:^{
                    // handle completion here
                    
                     //CALLING DELEGATE METHOD
                    //       //to refresh data with latest webservice data
                            [self.delegate scanViewScreenBeingDismissed];
                
                }];

                [self.navigationController popViewControllerAnimated:YES];

                [CATransaction commit];
                
                
//     [CATransaction begin];
//     [CATransaction setCompletionBlock:^{
//
//        //CALLING DELEGATE METHOD
//       //to refresh data with latest webservice data
//         [self.delegate scanViewScreenBeingDismissed];
//
//     }]
//
//
//
//
//                [[self.navigationController popViewControllerAnimated:YES]];
//
//
//                    [CATransaction commit];
      }
    }];
      
    
    
    popupView.contentTextAlignment  = NSTextAlignmentCenter;
    
    [popupView present];
    
}


/*
 Adding valid items
 */

-(void)addingValidItems: (SBSCode *)trackedCode{
    
     //First search if the scanned item belong to your list of webservice  or not
          //Then, checking if this has been already marked and counted or not
                          
          if ([self searchItem:trackedCode.data] && [self itemScannedAlready:trackedCode.data] == NO) {
              
            
              //Update the counter
               [self updateTotalCount];
               [self addItemInVerifiedTrackingCodeList:trackedCode.data];
              
             
              
              //checking if its BULK PICKUP scan or just single Drop off
                    
                    if (self.is_singleScan == YES) {
                       

                        //if drop off then open a bottom sheet
                        if(self.is_pickup){
                          
                            //Call Webservice and send that tracking id immeditaely to server
                           [json ItineraryBulkScanApi:self.trackingIdWithTimeStamp];
                        }
                        else
                        {
                            //Its Single Scan . And would be used for drop off
                           //so after perfect match just move back to itinery orders and open BOttom sheet
                              

                            
                            [self.navigationController popViewControllerAnimated:YES];
                         [self.delegate openSheetOnDismissOfScanScreen:self.modelData];

//
//

                     
                        }
                        
                    }
          }
}




#pragma mark - Adding overlay to overlayController
-(void)addOverlayToCameraView{
    
}


#pragma mark - Remove overlay frm overlayController
-(void)removeOverlayFromCameraView{
    
    
}



#pragma mark - Set background colors

-(void)setBackgroundColors_invalid:(UIView *)OverlayView{
    if (@available(iOS 11.0, *)) {
       OverlayView.backgroundColor = [UIColor redColor];
        
        //Animate View for few seconds
        [self blinkView:OverlayView];
        
        
    } else {
        // Fallback on earlier versions
    }

}

-(void)setBackgroundColors_valid :(UIView *)OverlayView{
    if (@available(iOS 11.0, *)) {
        OverlayView.backgroundColor = [UIColor greenColor];
        
        
        //Animate View for few seconds
        [self blinkView:OverlayView];
        
    } else {
        // Fallback on earlier versions
    }

}


#pragma mark - Blinking Animation

-(void)blinkView:(UIView *)viewToAnimate{
    
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation setFromValue:[NSNumber numberWithFloat:1.0]];
    [animation setToValue:[NSNumber numberWithFloat:0.0]];
    [animation setDuration:0.4f];
    [animation setTimingFunction:[CAMediaTimingFunction
                                  functionWithName:kCAMediaTimingFunctionLinear]];
    [animation setAutoreverses:YES];
    [animation setRepeatCount:3];

    
    
    //To remove blinking colors
    [CATransaction setAnimationDuration:0.4f];
    [CATransaction setCompletionBlock:^{
        viewToAnimate.backgroundColor = [UIColor clearColor];
        
    }];

    [CATransaction begin];
    [[viewToAnimate layer] addAnimation:animation forKey:@"opacity"];
    [CATransaction commit];

    
    
}

@end
