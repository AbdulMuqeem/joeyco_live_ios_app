//
//  Copyright (c) 2018 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <UIKit/UIKit.h>

@protocol google_SCANNERDelegate;

#import "Prefix.pch"
#import "ItineraryOrder_Listing_ObjectDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoogleMLKITBarcodeController : UIViewController
{
    id<google_SCANNERDelegate> delegate;
}

@property (nonatomic) BOOL is_singleScan;
@property (nonatomic) BOOL is_pickup;

@property (weak, nonatomic) id<google_SCANNERDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *pickerParentView;
@property (weak, nonatomic) IBOutlet UIButton *upload_btn;
@property (weak, nonatomic) IBOutlet UILabel *items_count;
@property (weak, nonatomic) IBOutlet UIButton *close_btn;
@property(strong, nonatomic)NSMutableArray *tracking_ids;
@property(strong, nonatomic)NSMutableArray *valid_scanned_tracking_ids;
@property(strong, nonatomic)NSMutableArray *trackingIdWithTimeStamp;

@property(nonatomic,strong) ItineraryOrder_Listing_ObjectDetail * modelData;

- (IBAction)close_scanner:(id)sender;

@end


@protocol google_SCANNERDelegate
@optional
-(void)scanViewScreenBeingDismissed;

-(void)openSheetOnDismissOfScanScreen :(ItineraryOrder_Listing_ObjectDetail * )modelData;
@end

NS_ASSUME_NONNULL_END
