//
//  SideMenuViewController.m
//  Customer
//
//  Created by Katia Maeda on 2014-10-10.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface SideMenuViewController () <JSONHelperDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
}

@property (nonatomic, weak) IBOutlet UIButton *endWorkButton;

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    [self reloadEndWorkButton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadEndWorkButton) name:@NotificationsReloadEndWorkButton object:nil];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@NotificationsReloadEndWorkButton object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [alertHelper dismissAlert];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

-(void)goToHome
{
    [self performSegueWithIdentifier:@"ShowHomeViewController" sender:nil];
}

-(IBAction)logoutAction:(id)sender
{
    __unsafe_unretained typeof(self) weakSelf = self;
    [alertHelper showAlertWithButtons:NSLocalizedString(@"Press OK to logout", nil) message:@"" from:self withOkHandler:^(void) {
        if (weakSelf && [weakSelf isKindOfClass:[SideMenuViewController class]])
        {
            [weakSelf performSelector:@selector(jsonLogout) withObject:nil afterDelay:0.1];
        }
    }];
}

-(IBAction)endWorkAction:(id)sender
{
    __unsafe_unretained typeof(self) weakSelf = self;
    [alertHelper showAlertWithButtons:NSLocalizedString(@"End work", nil) message:NSLocalizedString(@"Are you sure you want to end?", nil) from:self withOkHandler:^(void) {
        if (weakSelf && [weakSelf isKindOfClass:[SideMenuViewController class]])
        {
            [weakSelf performSelector:@selector(jsonEndDuty) withObject:nil afterDelay:0.1];
        }
    }];
}

-(void)reloadEndWorkButton
{
    [json personal];
}

-(void)jsonLogout
{
    NSString *deviceToken = [[DatabaseHelper shared] getSetting:@"deviceToken"];
    [json logout:deviceToken];
}

-(void)jsonEndDuty
{
    [json endDuty];
}

#pragma mark - JSONHelperDelegate methods

-(void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    if ([action isEqualToString:@ACTION_LOGOUT])
    {
        // delete user data from database
        [[DatabaseHelper shared] deleteJoeyAndOrderData];
        
        [[LocationManager shared] stopUpdatingLocation];
        [self goToHome];
    }
    else if ([action isEqualToString:@ACTION_END_DUTY])
    {
        [[DatabaseHelper shared] updateSetting:joeyFieldDuty value:0];
        
        [self goToHome];
        [self reloadEndWorkButton];
    }
    else if ([action isEqualToString:@ACTION_PERSONAL_DATA])
    {
        Joey *joey = [list firstObject];
        
        // save on database
        if (joey)
        {
            self.endWorkButton.hidden = ([joey.duty boolValue]) ? NO : YES;
        }
    }
}

-(void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertNoButtons:NSLocalizedString(@"Error", nil) type:AlertTypeNone message:message dismissAfter:3.0 from:self];
}

@end
