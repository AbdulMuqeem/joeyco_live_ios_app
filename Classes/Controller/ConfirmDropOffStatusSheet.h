//
//  ConfirmDropOffViewController.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 15/04/2019.
//  Copyright © 2019 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ConfirmDropOffDelegate;

#import "MaterialBottomSheet.h"
#import "DelayReportViewController.h"
#import "Order.h"
#import "ItineraryOrder_Listing_ObjectDetail.h"
#import "CameraVC_takePhoto.h"
#import "Task.h"
#import "MetaData.h"
#import "JSONHelper.h"

NS_ASSUME_NONNULL_BEGIN

@interface ConfirmDropOffStatusSheet : UIViewController<ImageConfirmDropOffDelegate>

{
    id<ConfirmDropOffDelegate> delegate;
}

@property(nonatomic,assign)BOOL is_itinerary;


@property (weak, nonatomic) id<ConfirmDropOffDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *bottomSheet_title;
@property (weak, nonatomic) IBOutlet UILabel *bottomSheet_subtitle;


- (IBAction)closeButton:(id)sender;
- (IBAction)RadioButtonTapped:(id)sender;


@property(nonatomic,strong)NSMutableArray * delayStatusObj;

@property (weak, nonatomic) IBOutlet UITableView *tableVIew;
@property(nonatomic,assign)int selectedIndex;


//Order
@property(nonatomic,strong)Order * order;
@property(nonatomic,strong)Task *  order_task;
@property(nonatomic,strong)NSString * taskType;

//Model Data of clicked obj
@property(nonatomic,strong)ItineraryOrder_Listing_ObjectDetail * modelData;

@end


@protocol ConfirmDropOffDelegate

-(void)dropStatusCompleted :(ItineraryOrder_Listing_ObjectDetail *)obj;

@end

NS_ASSUME_NONNULL_END
