//
//  ScheduleEdit1ViewController.m
//  Joey
//
//  Created by Katia Maeda on 2015-12-02.
//  Copyright © 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface ScheduleEdit1ViewController () <JSONHelperDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    GMSMarker *joeyMarker;
    Joey *joey;
    CLLocation *lastJoeyLocation;
    
    AlertHelper *alertHelper;
    JSONHelper *json;
}

@property (nonatomic, weak) IBOutlet GMSMapView *mapView;

@property (nonatomic, strong) NSArray *zoneList;
@property (nonatomic, weak) IBOutlet UIButton *zoneButton;
@property (nonatomic, strong) Zone *zoneSelected;
@property (nonatomic, strong) NSArray *vehicleList;
@property (nonatomic, weak) IBOutlet UIButton *vehicleButton;
@property (nonatomic, strong) Vehicle *vehicleSelected;

@property (nonatomic, weak) IBOutlet UIView *pickerView;
@property (nonatomic, weak) IBOutlet UIPickerView *picker;
@property (nonatomic, strong) UIButton *buttonSelected;

@end

@implementation ScheduleEdit1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    joey = [[DatabaseHelper shared] getJoey];
    
    // Initiate GMSMapView
    self.mapView.settings.myLocationButton = YES;
    self.mapView.myLocationEnabled = YES;
    self.mapView.mapType = [self setMapType];
    lastJoeyLocation = [[LocationManager shared] lastKnownLocation];

    MetaData *metaData = [[DatabaseHelper shared] getFullMetaData];
    if (metaData && ![NSDateHelper isLater:metaData.lastUpdated plusHours:24] && metaData.zones.count > 0)
    {
        [self reloadData:metaData];
    }
    else
    {
        if (!lastJoeyLocation || [lastJoeyLocation isEqual:[NSNull null]])
        {
            [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Could not get user location", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        }
        
        [json metaData:lastJoeyLocation.coordinate.latitude longitude:lastJoeyLocation.coordinate.longitude];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [alertHelper dismissAlert];
}

-(void)dealloc
{
    [self superDealloc];
    
    self.picker.delegate = nil;
    self.picker.dataSource = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reloadData:(MetaData *)metaData
{
    self.zoneList = metaData.zones;
    self.zoneSelected = [self.zoneList firstObject];
    [self.zoneButton setTitle:self.zoneSelected.name forState:UIControlStateNormal];
    
    self.vehicleList = metaData.vehicles;
    self.vehicleSelected = [self.vehicleList firstObject];
    [self.vehicleButton setTitle:self.vehicleSelected.name forState:UIControlStateNormal];
    
    [self reloadMap];
}

#pragma mark - Map

-(void)showJoeyMarker
{
    if (!lastJoeyLocation || [lastJoeyLocation isEqual:[NSNull null]])
    {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (joeyMarker)
        {
            joeyMarker.map = nil;
            joeyMarker = nil;
        }
        
        CLLocationCoordinate2D joeyLocation = CLLocationCoordinate2DMake(lastJoeyLocation.coordinate.latitude, lastJoeyLocation.coordinate.longitude);

        joeyMarker = [[GMSMarker alloc] init];
        joeyMarker.position = joeyLocation;
        joeyMarker.map = self.mapView;
        joeyMarker.icon = [UIImage imageNamed:@"joey_run.png"];
    });
}

-(void)reloadMap
{
    [self.mapView clear];
    if (self.zoneSelected)
    {
        CLLocationCoordinate2D zonePosition = CLLocationCoordinate2DMake([self.zoneSelected.centerLatitude doubleValue], [self.zoneSelected.centerLongitude doubleValue]);
        
        GMSCircle *circle = [[GMSCircle alloc] init];
        circle.radius = [self.zoneSelected.radius intValue]; // Meters
        circle.position = zonePosition;
        circle.fillColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.33];
        circle.strokeWidth = .8;
        circle.strokeColor = [UIColor blackColor];
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = zonePosition;
        
        circle.map = self.mapView;
        marker.map = self.mapView;
        self.mapView.camera = [GMSCameraPosition cameraWithTarget:zonePosition zoom:13];
    }
    [self showJoeyMarker];
}

#pragma mark - Filter Picker

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.zoneButton == self.buttonSelected)
    {
        return self.zoneList.count;
    }
    
    return self.vehicleList.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = nil;
    if (self.zoneButton == self.buttonSelected)
    {
        if (row < 0 || row >= self.zoneList.count)
        {
            return @"";
        }
        
        Zone *zone = [self.zoneList objectAtIndex:row];
        title = zone.name;
    }
    else
    {
        if (row < 0 || row >= self.vehicleList.count)
        {
            return @"";
        }
        
        Vehicle *vehicle = [self.vehicleList objectAtIndex:row];
        title = vehicle.name;
    }
    
    return title;
}

-(IBAction)selectButton:(id)sender
{
    if (self.pickerView.hidden)
    {
        [self openSelectedStatusAnimation:self.pickerView];
    }
    else if (self.buttonSelected == sender)
    {
        [self closeSelectedStatusAnimation:self.pickerView];
    }
    
    self.buttonSelected = sender;
    [self.picker reloadAllComponents];
}

-(IBAction)selectPickerValue:(id)sender
{
    NSInteger row = [self.picker selectedRowInComponent:0];
    
    NSString *title = nil;
    if (self.zoneButton == self.buttonSelected)
    {
        if (row < 0 || row >= self.zoneList.count)
        {
            return;
        }
        self.zoneSelected = [self.zoneList objectAtIndex:row];
        title = self.zoneSelected.name;
        [self reloadMap];
    }
    else
    {
        if (row < 0 || row >= self.vehicleList.count)
        {
            return;
        }
        self.vehicleSelected = [self.vehicleList objectAtIndex:row];
        title = self.vehicleSelected.name;
    }

    [self.buttonSelected setTitle:title forState:UIControlStateNormal];
    [self closeSelectedStatusAnimation:self.pickerView];
}

#pragma mark - Animations

-(void)openSelectedStatusAnimation:(UIView *)view
{
    if (!view.hidden)
    {
        return;
    }
    
    view.hidden = NO;
    
    CGRect frame = view.frame;
    frame.origin.y -= frame.size.height;
    
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.frame = frame;
                     }
                     completion:nil];
}

-(void)closeSelectedStatusAnimation:(UIView *)view
{
    if (view.hidden)
    {
        return;
    }
    
    view.hidden = NO;
    
    CGRect frame = view.frame;
    frame.origin.y += frame.size.height;
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         view.hidden = YES;
                     }];
}

#pragma mark - JSONHelperDelegate

- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    [super parsedObjects:action objects:list];
    
    if ([action isEqualToString:@ACTION_GET_META_DATA])
    {
        if (list)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                MetaData *metaData = [list firstObject];
                [[DatabaseHelper shared] insertMetaData:metaData];
                [[DatabaseHelper shared] insertVehicles:metaData.vehicles];
                [[DatabaseHelper shared] insertZones:metaData.zones];
                
                [self reloadData:metaData];
            });
        }
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

#pragma mark - Navigation

-(IBAction)availableSlotsAction:(id)sender
{
    BOOL hasInternet = [json hasInternetConnection:NO];
    if (hasInternet)
    {
        NewShift *newShift = [[NewShift alloc] initWithZone:self.zoneSelected vehicle:self.vehicleSelected];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            ScheduleEdit2ViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ScheduleEdit2ViewController"];
            viewController.shift = newShift;
            [self.navigationController pushViewController:viewController animated:YES];
        });
    }
}

@end
