//
//  OrderStatusUpdates.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 24/10/2018.
//  Copyright © 2018 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UpdateStatusCell.h"
#import "ReturnOrderVC.h"
#import "MaterialBottomSheet.h"
#import "DelayReportViewController.h"
#import "Order.h"
#import "Task.h"
#import "MetaData.h"
NS_ASSUME_NONNULL_BEGIN

@interface OrderStatusUpdates : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *updateStatusTableView;
@property(nonatomic,strong)NSMutableArray * Arr_StatusObj;


@property(nonatomic,strong)Order * order;
@property(nonatomic,strong)Task *  task;
@property(nonatomic,strong)NSString * taskType;


- (IBAction)ProceedToCancelScreen:(id)sender;
- (IBAction)ReportDelay:(id)sender;

@end

NS_ASSUME_NONNULL_END
