//
//  SummaryDetailViewController.h
//  Joey
//
//  Created by Katia Maeda on 2016-05-25.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "NavigationContainerViewController.h"
#import "SummaryRecord.h"

@interface SummaryDetailViewController : NavigationContainerViewController

@property (nonatomic, strong) Shift *shift;
@property (nonatomic, strong) SummaryRecord *record;

@end

@interface ItemDetail : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *value;
-(instancetype)init:(NSString *)title value:(NSString *)value;
-(instancetype)init:(NSString *)title value:(NSString *)value defaultValue:(NSString *)defaultValue;

@end