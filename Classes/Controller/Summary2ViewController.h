//
//  Summary2ViewController.h
//  Joey
//
//  Created by Katia Maeda on 2016-05-20.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Summary.h"

@interface Summary2ViewController : UIViewController

@property (nonatomic, strong) Summary *summary;

@end

@interface Item : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *value;
-(instancetype)init:(NSString *)title value:(NSString *)value;
-(instancetype)init:(NSString *)title value:(NSString *)value defaultValue:(NSString *)defaultValue;

@end