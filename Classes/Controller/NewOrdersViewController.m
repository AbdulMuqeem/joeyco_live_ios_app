//
//  NewOrdersViewController.m
//  Joey
//
//  Created by Katia Maeda on 2015-11-06.
//  Copyright © 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface NewOrdersViewController () <JSONHelperDelegate>
{
    CLLocation *lastJoeyLocation;
    GMSMarker *joeyMarker;
    TravelMode mapRouteSetting;
    BOOL firstLocationUpdate;
    NSDate *lastUpdateLocationInfo;
    
    NSTimer *readInTimer;
    
    JSONHelper *json;
    AlertHelper *alertHelper;
}

@property (nonatomic, weak) IBOutlet GMSMapView *mapView;

@property (nonatomic, weak) IBOutlet UITableView *orderTable;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *orderTableHeight;

@property(nonatomic, strong) NSArray *orderList;

@end

@implementation NewOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    lastJoeyLocation = [[LocationManager shared] lastKnownLocation];
    
    json = [[JSONHelper alloc] init:self andDelegate:self];
    alertHelper = [[AlertHelper alloc] init];
    
    // Initiate GMSMapView
    self.mapView.settings.myLocationButton = YES;
    self.mapView.myLocationEnabled = YES;
    self.mapView.mapType = [self.delegate setMapType];
    
    if (lastJoeyLocation && ![lastJoeyLocation isEqual:[NSNull null]])
    {
        self.mapView.camera = [GMSCameraPosition cameraWithTarget:lastJoeyLocation.coordinate zoom:14];
    }
    
    firstLocationUpdate = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    int route = [[DatabaseHelper shared] getMapRouteData];
    mapRouteSetting = (route == 0) ? TravelModeDriving : TravelModeBicycling;

    [readInTimer invalidate];
    readInTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(reloadData) userInfo:nil repeats:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [alertHelper dismissAlert];
    [readInTimer invalidate];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Location

-(void)updateLastLocation:(CLLocation *)newLocation
{
    lastJoeyLocation = newLocation;
    [self showJoeyMarker];
    [self updateLocationInfo];
}

-(void)showJoeyMarker
{
    if (!lastJoeyLocation || [lastJoeyLocation isEqual:[NSNull null]])
    {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!firstLocationUpdate)
        {
            firstLocationUpdate = YES;
            self.mapView.camera = [GMSCameraPosition cameraWithTarget:lastJoeyLocation.coordinate zoom:14];
        }
        
        if (joeyMarker)
        {
            joeyMarker.map = nil;
            joeyMarker = nil;
        }
        
        joeyMarker = [[GMSMarker alloc] init];
        joeyMarker.position = CLLocationCoordinate2DMake(lastJoeyLocation.coordinate.latitude, lastJoeyLocation.coordinate.longitude);
        joeyMarker.icon = [UIImage imageNamed:@"joey_run.png"];
        joeyMarker.map = self.mapView;
    });
}

-(void)updateLocationInfo
{
//    if (!lastJoeyLocation || [lastJoeyLocation isEqual:[NSNull null]])
//    {
//        return;
//    }
//
//    if (self.orderList.count <= 0 || (lastUpdateLocationInfo && ![NSDateHelper isLater:lastUpdateLocationInfo plusSeconds:5]))
//    {
//        return;
//    }
//    
//    lastUpdateLocationInfo = [NSDate date];
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        for (Order *order in self.orderList)
//        {
//            NSMutableArray *locations = [order tasksLocationList];
//            [locations insertObject:lastJoeyLocation atIndex:0];
//            
//            __weak typeof(self) weakSelf = self;
//            [MapHelper getGoogleMapRouteInfo:locations travelMode:mapRouteSetting succeeded:^(GoogleMapRouteInfo *info) {
//                order.totalDistance = info.totalDistance;
//                order.totalDuration = info.totalDuration;
//                
//                Order *lastOrder = [self.orderList lastObject];
//                if (lastOrder == order)
//                {
//                    [weakSelf reloadData];
//                }
//            } failed:^(NSString *errorMsg) {
//                NSLog(@"%@", errorMsg);
//            }];
//        }
//    });
}

#pragma mark - layout

-(void)updateOrders:(NSArray *)orders
{
    self.orderList = orders;
    [self reloadData];
    [self updateLocationInfo];
}

-(void)reloadData
{
    [readInTimer invalidate];
    [self.orderTable reloadData];
    self.orderTableHeight.constant = self.orderTable.contentSize.height;
    
    CGRect frame = self.orderTable.frame;
    if (frame.origin.y + self.orderTable.contentSize.height > self.view.frame.size.height)
    {
        self.orderTableHeight.constant = self.view.frame.size.height - frame.origin.y - 20;
    }
}

#pragma mark - JSONHelperDelegate - Getting Accepted Order

-(void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    if ([action isEqualToString:@ACTION_ACCEPT_ORDER])
    {
        [self.delegate getOrderList];
    }
}

-(void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

#pragma mark - UITableView methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.orderList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"OrderListCell";
    
    OrderTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[OrderTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    __weak OrderTableCell *weakCell = cell;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (indexPath.row < self.orderList.count)
        {
            Order *order = [self.orderList objectAtIndex:indexPath.row];
            
            weakCell.orderNumTxt.text = order.num;
            weakCell.pickupTxt.text = [NSString stringWithFormat:NSLocalizedString(@"Number of Pickup : %ld", nil), (long)order.pickupList.count];
            weakCell.droppOffTxt.text = [NSString stringWithFormat:NSLocalizedString(@"Drop Off : %ld", nil), (long)order.dropOffList.count];
            
            Task *firsTask = [order.pickupList firstObject];
            NSInteger countDown = [firsTask.dueTime doubleValue] - [[NSDate date] timeIntervalSince1970];
            if (countDown > 0)
            {
                weakCell.readyInTxt.text = [NSString stringWithFormat:NSLocalizedString(@"Ready in %@", nil), [NSDateHelper stringFromCountDown:countDown]];
                weakCell.readyInTxt.textColor = [UIColor colorWithRed:0.83 green:0.4 blue:0.15 alpha:1.0];
                [readInTimer invalidate];
                readInTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(reloadData) userInfo:nil repeats:NO];
            }
            else
            {
                weakCell.readyInTxt.text = NSLocalizedString(@"Ready to pick up", nil);
                weakCell.readyInTxt.textColor = [UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0];
            }
            
            weakCell.travelDistanceTxt.text = [MapHelper stringFromDistance:[order.distance doubleValue]];
            weakCell.travelTimeTxt.text = [self convertTime:[order.eta intValue]];
            
            
            weakCell.acceptBtn.buttonData = order;
            [weakCell.acceptBtn addTarget:self action:@selector(showConfirmAcceptOrderAlert:) forControlEvents:UIControlEventTouchUpInside];
        }
    });
    
    return weakCell;
}

-(NSString *)convertTime:(int)time
{
    if (time < 1) return @"";
    float hours = floor(time / 60);
    float minutes = (time % 60);
    if (hours == 0) return [NSString stringWithFormat:@"%.0f min", minutes];
    return [NSString stringWithFormat:@"%.0f hr %.0f min", hours, minutes];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (indexPath.row < 0 || indexPath.row >= self.orderList.count)
        {
            return;
        }
        
        Order *order = [self.orderList objectAtIndex:indexPath.row];
        OpenOrderDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OpenOrderDetailViewController"];
        viewController.order = order;
        [self.navigationController pushViewController:viewController animated:YES];
    });
}

-(void)showConfirmAcceptOrderAlert:(id)sender
{
    RoundedButton *button = (RoundedButton *)sender;
    Order *order = (Order *)button.buttonData;
    
    __unsafe_unretained typeof(self) weakSelf = self;
    [alertHelper showAlertWithButtonsYesCancel:NSLocalizedString(@"Do you want to accept this order?" , nil) message:NSLocalizedString(@"Click yes to accept!", nil) from:self withOkHandler:^(void) {
        if (weakSelf && [weakSelf isKindOfClass:[NewOrdersViewController class]])
        {
            [weakSelf performSelector:@selector(acceptOrder:) withObject:order afterDelay:.1];
        }
    }];
}

-(void)acceptOrder:(Order *)order
{
    if (!lastJoeyLocation || [lastJoeyLocation isEqual:[NSNull null]])
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Order not accepted", nil) message:NSLocalizedString(@"Could not get user location", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    if (!order || !order.orderId)
    {
        CLS_LOG(@"Problem accepting order:%@", order.orderId);
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Order not accepted", nil) message:NSLocalizedString(@"Could not get order data", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    if (![json hasInternetConnection:YES]) return;
    
    NSMutableArray *locations = [order pickupLocationList];
    [locations insertObject:lastJoeyLocation atIndex:0];
    
    __unsafe_unretained typeof(self) weakSelf = self;
    [MapHelper getGoogleMapRouteInfo:locations travelMode:mapRouteSetting succeeded:^(GoogleMapRouteInfo *info) {
        if (![info isKindOfClass:[GoogleMapRouteInfo class]]) {
            return;
        }
        
        NSMutableArray *ordersArray = [NSMutableArray array];
        int eta = 0;
        
        NSMutableArray *pickupList = [order pickupList];
        for (int i = 0; i < pickupList.count; i++)
        {
            Task *task = [pickupList objectAtIndex:i];
            
            int duration = 0;
            if (i < info.durations.count)
            {
                duration = [[info.durations objectAtIndex:i] intValue];
            }

            eta += duration;
            
            if (!task || !task.taskId)
            {
                CLS_LOG(@"Problem accepting order, task:%@/ order:%@", task.taskId, task.orderId);
            }
            
            NSMutableDictionary *pickup = [NSMutableDictionary dictionaryWithDictionary:@{@"id":task.taskId, @"eta":[NSNumber numberWithInt:eta]}];
            [ordersArray addObject:pickup];
        }
        
        NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":[UrlInfo getJoeyId], @"orders":ordersArray}];
        NSMutableDictionary *orderDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"dictionary":jsonDictionary, @"orderId":order.orderId}];
        
        if (weakSelf && [weakSelf isKindOfClass:[NewOrdersViewController class]])
        {
            [weakSelf performSelector:@selector(jsonAcceptOrder:) withObject:orderDictionary afterDelay:0.1];
        }
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@", errorMsg);
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Order not accepted", nil) message:errorMsg from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
    }];
}

-(void)jsonAcceptOrder:(NSMutableDictionary *)dictionary
{
    NSMutableDictionary *jsonDictionary = [dictionary objectForKey:@"dictionary"];
    NSNumber *orderId = [dictionary objectForKey:@"orderId"];
    [json acceptOrder:[orderId intValue] pickupInfo:jsonDictionary];
}

@end
