//
//  ScannerViewController.h
//  Joey
//
//  Created by Katia Maeda on 2016-02-24.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@protocol ScannerViewControllerDelegate;

@interface ScannerViewController : NavigationContainerViewController <AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic, weak) id<ScannerViewControllerDelegate> delegate;

@property (assign, nonatomic) BOOL touchToFocusEnabled;
@property (weak, nonatomic) IBOutlet UIView *redBlinkingLine;
@property (weak, nonatomic) IBOutlet UIView *transparentWindowView;
@property (weak, nonatomic) IBOutlet UIView *greyBackgroundView;


-(BOOL)isCameraAvailable;
-(void)startScanning;
-(void)stopScanning;

@end

@protocol ScannerViewControllerDelegate <NSObject>

@required
-(void)scanViewController:(ScannerViewController *)scannerViewController didSuccessfullyScan:(NSString *) aScannedValue;

@end

