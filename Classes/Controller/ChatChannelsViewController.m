//
//  DepositViewController.m
//  Joey
//
//  Created by Katia Maeda on 2015-02-23.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface ChatChannelsViewController () <UITableViewDataSource, UITableViewDelegate, JSONHelperDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
}

@property (nonatomic, weak) IBOutlet UITableView *chatsTable;
@property (nonatomic, strong) NSMutableArray *chatsList;

@end

@implementation ChatChannelsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [json chatChannels];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotificationWithMessage:) name:@NotificationsReceivedNewMessage object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [alertHelper dismissAlert];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@NotificationsReceivedNewMessage object:nil];
}

-(void)dealloc
{
    [self superDealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)applicationWillEnterForeground:(NSNotification *)notification
{
    [json chatChannels];
}

#pragma mark - Notification

-(void)receivedNotificationWithMessage:(NSNotification *)notification
{
    [json chatChannels];
}

#pragma mark - JSONHelperDelegate

- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    [super parsedObjects:action objects:list];
    
    if ([action isEqualToString:@ACTION_CHAT_CHANNELS])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.chatsList = list;
            [self.chatsTable reloadData];
        });
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

- (void) errorMessage:(NSString *)action message:(NSString *)message fields:(NSArray *)fieldsArray
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

#pragma mark - UITableView methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.chatsList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 73;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ChatTableViewCell";
    
    FourLabelsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[FourLabelsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }

    __weak FourLabelsTableViewCell *weakCell = cell;

    dispatch_async(dispatch_get_main_queue(), ^{
        if (indexPath.row >= self.chatsList.count)
        {
            return;
        }
        
        Channel *channel = [self.chatsList objectAtIndex:indexPath.row];
        weakCell.label1.text = channel.name;
        
        Message *message = channel.lastMessage;
        weakCell.label2.text = message.contact.name;
        
        if ([message.type isEqualToString:@"image"])
        {
            weakCell.label3.text = NSLocalizedString(@"Image", nil);
        }
        else
        {
            weakCell.label3.text = message.message;
        }

        weakCell.label4.text = [NSDateHelper stringDateTimeFromDate4:message.date];
    });

    return weakCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Channel *channel = [self.chatsList objectAtIndex:indexPath.row];
    
    dispatch_async(dispatch_get_main_queue(), ^{
//        ChatChannelMessagesViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatChannelMessagesViewController"];
//        viewController.channel = channel;
//        [self.navigationController pushViewController:viewController animated:YES];
        
        ChatMessagesViewController *viewController = [[ChatMessagesViewController alloc] init];
        viewController.channel = channel;
        [self.navigationController pushViewController:viewController animated:YES];
    });
}

@end