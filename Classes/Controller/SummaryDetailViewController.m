//
//  SummaryDetailViewController.m
//  Joey
//
//  Created by Katia Maeda on 2016-05-25.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface SummaryDetailViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *detailTable;
@property (nonatomic, strong) NSMutableArray *detailList;

@end

@implementation SummaryDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setShift:(Shift *)shift
{
    _shift = shift;
    
    self.detailList = [NSMutableArray array];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Date", nil) value:[NSDateHelper stringDateFromDate:shift.startTime]]];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Shift ID", nil) value:shift.num defaultValue:@"-"]];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Zone ID", nil) value:shift.zone.num defaultValue:@"-"]];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Scheduled Start Time", nil) value:[NSDateHelper stringTimeFromDate2:shift.startTime] defaultValue:@"-"]];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Scheduled End Time", nil) value:[NSDateHelper stringTimeFromDate2:shift.endTime] defaultValue:@"-"]];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Joey Start Time", nil) value:[NSDateHelper stringTimeFromDate2:shift.realStartTime] defaultValue:@"-"]];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Joey End Time", nil) value:[NSDateHelper stringTimeFromDate2:shift.realEndTime] defaultValue:@"-"]];

    NSString *wageString = [NSString stringWithFormat:@"$%.2f", [shift.wage floatValue]];
    if (!shift.realStartTime) {
        wageString = NSLocalizedString(@"No Show", nil);
    } else if (!shift.realEndTime) {
        wageString = NSLocalizedString(@"Shift Still In Progress", nil);
    }
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Wages Earned", nil) value:wageString defaultValue:@"-"]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.detailTable reloadData];
    });
}

-(void)setRecord:(SummaryRecord *)record
{
    _record = record;
    
    self.detailList = [NSMutableArray array];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Date", nil) value:[NSDateHelper stringDateFromDate:record.date]]];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Time", nil) value:[NSDateHelper stringTimeFromDate:record.date]]];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"ID", nil) value:record.num defaultValue:@"-"]];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Associated Shift", nil) value:record.shift defaultValue:@"-"]];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Description", nil) value:record.recordsDescription defaultValue:@"-"]];
    
    NSString *paymentMethod = @"";
    switch ([record.payment_method integerValue]) {
        case 39: paymentMethod = NSLocalizedString(@"Cash Payment", nil); break;
        case 40: paymentMethod = NSLocalizedString(@"Online Payment", nil); break;
        case 41: paymentMethod = NSLocalizedString(@"Credit Card Payment", nil); break;
        case 62: paymentMethod = NSLocalizedString(@"No Payment", nil); break;
        case 65: paymentMethod = NSLocalizedString(@"Payment At Door", nil); break;
    }
    
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Payment Type", nil) value:paymentMethod defaultValue:@"-"]];
    
    NSString *distanceString = [NSString stringWithFormat:@"%.2f km", [record.distance floatValue]/1000];
    if (!record.distance || [record.distance floatValue] == 0) {
        distanceString = @"";
    }
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Distance", nil) value:distanceString defaultValue:@"-"]];
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Duration", nil) value:record.duration defaultValue:@"-"]];
    
    [self.detailList addObject:[[ItemDetail alloc] init:NSLocalizedString(@"Amount", nil) value:[NSString stringWithFormat:@"%.2f", [record.amount floatValue]] defaultValue:@"-"]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.detailTable reloadData];
    });
}

#pragma mark UITableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.detailList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"SummaryDetailTableViewCell";
    TwoLabelsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (!cell) cell = [[TwoLabelsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    __weak TwoLabelsTableCell *weakCell = cell;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        ItemDetail *detail = [self.detailList objectAtIndex:indexPath.row];
        
        weakCell.label1.text = detail.title;
        weakCell.label2.text = detail.value;
        weakCell.label2Width.constant = [weakCell.label2 sizeThatFits:CGSizeMake(1000, weakCell.label2.frame.size.height)].width;
    });
    
    return weakCell;
}

@end

@implementation ItemDetail

-(instancetype)init:(NSString *)title value:(NSString *)value
{
    _title = title;
    _value = value;
    return self;
}

-(instancetype)init:(NSString *)title value:(NSString *)value defaultValue:(NSString *)defaultValue
{
    _title = title;
    _value = (!value || [value isEqualToString:@""]) ? defaultValue : value;
    return self;
}

@end
