//
//  OffDutyViewController.m
//  Joey
//
//  Created by Katia Maeda on 2015-11-05.
//  Copyright © 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface OffDutyViewController () <JSONHelperDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
    CLLocation *lastJoeyLocation;
}

@property (nonatomic, weak) IBOutlet UIButton *sandwichButton;
@property (nonatomic, weak) IBOutlet UIButton *pizzaButton;
@property (nonatomic, weak) IBOutlet UIButton *masterCardButton;
@property (nonatomic, weak) IBOutlet UIButton *depositCardButton;
@property (nonatomic, weak) IBOutlet UIButton *cleanButton;
@property (nonatomic, weak) IBOutlet UIButton *changeButton;
@property (nonatomic) BOOL isSelectedSandwichButton;
@property (nonatomic) BOOL isSelectedPizzaButton;
@property (nonatomic) BOOL isSelectedMasterCardButton;
@property (nonatomic) BOOL isSelectedDepositCardButton;
@property (nonatomic) BOOL isSelectedCleanButton;
@property (nonatomic) BOOL isSelectedChangeButton;

@property (nonatomic, strong) NSArray *vehicleList;
@property (nonatomic, weak) IBOutlet UIPickerView *vehiclePicker;
@property (nonatomic) NSInteger vehicleSelected;

@end

@implementation OffDutyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    lastJoeyLocation = [[LocationManager shared] lastKnownLocation];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    Joey *joey = [[DatabaseHelper shared] getJoey];
    if (joey)
    {
        [json personal];
        
        MetaData *metaData = [[DatabaseHelper shared] getFullMetaData];
        if (!metaData || [NSDateHelper isLater:metaData.lastUpdated plusHours:24])
        {
            [json metaData:lastJoeyLocation.coordinate.latitude longitude:lastJoeyLocation.coordinate.longitude];
        }
        else
        {
            self.vehicleList = metaData.vehicles;
            [self.vehiclePicker reloadAllComponents];
        }
        
        self.isSelectedSandwichButton = NO;
        self.isSelectedPizzaButton = NO;
        self.isSelectedMasterCardButton = NO;
        self.isSelectedDepositCardButton = NO;
        self.isSelectedCleanButton = NO;
        self.isSelectedChangeButton = NO;
        
        [self.sandwichButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.pizzaButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.masterCardButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.depositCardButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.cleanButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.changeButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [self setupCheckboxes];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
       // [self.navigationController presentViewController:controller animated:NO completion:nil];
    if (@available(iOS 13.0, *)) {
              [controller setModalPresentationStyle: UIModalPresentationFullScreen];
          } else {
              // Fallback on earlier versions
              
          }
        [self.navigationController presentViewController:controller animated:YES completion:nil];
    
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [alertHelper dismissAlert];
}

-(void)dealloc
{
    [self superDealloc];
    
    self.vehiclePicker.delegate = nil;
    self.vehiclePicker.dataSource = nil;
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupCheckboxes
{
    [self.sandwichButton setImage:[UIImage imageNamed:(self.isSelectedSandwichButton ? @"checkbox-checked.png" : @"checkbox-unchecked.png")] forState:UIControlStateNormal];
    [self.pizzaButton setImage:[UIImage imageNamed:(self.isSelectedPizzaButton ? @"checkbox-checked.png" : @"checkbox-unchecked.png")] forState:UIControlStateNormal];
    [self.masterCardButton setImage:[UIImage imageNamed:(self.isSelectedMasterCardButton ? @"checkbox-checked.png" : @"checkbox-unchecked.png")] forState:UIControlStateNormal];
    [self.depositCardButton setImage:[UIImage imageNamed:(self.isSelectedDepositCardButton ? @"checkbox-checked.png" : @"checkbox-unchecked.png")] forState:UIControlStateNormal];
    [self.cleanButton setImage:[UIImage imageNamed:(self.isSelectedCleanButton ? @"checkbox-checked.png" : @"checkbox-unchecked.png")] forState:UIControlStateNormal];
    [self.changeButton setImage:[UIImage imageNamed:(self.isSelectedChangeButton ? @"checkbox-checked.png" : @"checkbox-unchecked.png")] forState:UIControlStateNormal];
}

-(void)toggleButton:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if (button == self.sandwichButton)
    {
        self.isSelectedSandwichButton = !self.isSelectedSandwichButton;
    }
    else if (button == self.pizzaButton)
    {
        self.isSelectedPizzaButton = !self.isSelectedPizzaButton;
    }
    else if (button == self.masterCardButton)
    {
        self.isSelectedMasterCardButton = !self.isSelectedMasterCardButton;
    }
    else if (button == self.depositCardButton)
    {
        self.isSelectedDepositCardButton = !self.isSelectedDepositCardButton;
    }
    else if (button == self.cleanButton)
    {
        self.isSelectedCleanButton = !self.isSelectedCleanButton;
    }
    else if (button == self.changeButton)
    {
        self.isSelectedChangeButton = !self.isSelectedChangeButton;
    }
    
    [self setupCheckboxes];
}

-(IBAction)startDutyAction
{
    NSInteger row = [self.vehiclePicker selectedRowInComponent:0];
    
    if (row < 0 || row >= self.vehicleList.count)
    {
        return;
    }
    
    if (!self.isSelectedSandwichButton || !self.isSelectedPizzaButton || !self.isSelectedMasterCardButton ||
        !self.isSelectedDepositCardButton || !self.isSelectedCleanButton || !self.isSelectedChangeButton) {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Checklist", nil) message:NSLocalizedString(@"Need to check all items of the checklist.", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    lastJoeyLocation = [[LocationManager shared] lastKnownLocation];
    if (!lastJoeyLocation || [lastJoeyLocation isEqual:[NSNull null]])
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Could not get user location", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    Vehicle *vehicle = [self.vehicleList objectAtIndex:row];
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"vehicle_id":vehicle.vehicleId, @"latitude":[NSNumber numberWithDouble:lastJoeyLocation.coordinate.latitude], @"longitude":[NSNumber numberWithDouble:lastJoeyLocation.coordinate.longitude]}];
    
    [json startDuty:jsonDictionary];
}

#pragma mark - Filter Picker

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.vehicleList.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (row < 0 || row >= self.vehicleList.count)
    {
        return @"";
    }
    Vehicle *item = [self.vehicleList objectAtIndex:row];
    return item.name;
}

#pragma mark - JSONHelperDelegate

-(void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    [super parsedObjects:action objects:list];
    
    if ([action isEqualToString:@ACTION_GET_META_DATA])
    {
        if (list)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                MetaData *metaData = [list firstObject];
                [[DatabaseHelper shared] insertMetaData:metaData];
                [[DatabaseHelper shared] insertVehicles:metaData.vehicles];
                [[DatabaseHelper shared] insertZones:metaData.zones];
                
                self.vehicleList = metaData.vehicles;
                [self.vehiclePicker reloadAllComponents];
            });
        }
    }
    else if ([action isEqualToString:@ACTION_PERSONAL_DATA])
    {
        Joey *joey = [list firstObject];
        
        // save on database
        if (joey)
        {
            [[DatabaseHelper shared] updateJoey:joey];
            [UrlInfo setJoeyId:[joey.joeyId integerValue]];
            
            if ([joey.duty boolValue])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
                    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"OrderViewController"];
                    [self.navigationController setViewControllers:@[controller]];
                });
            }
            else
            {
                for (int i = 0; i < self.vehicleList.count; i++)
                {
                    Vehicle *vehicle = [self.vehicleList objectAtIndex:i];
                    if ([vehicle.vehicleId intValue] == [joey.vehicleId intValue])
                    {
                        [self.vehiclePicker selectRow:i inComponent:0 animated:YES];
                        break;
                    }
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReloadEndWorkButton object:nil userInfo:nil];
            }
        }
    }
    else if ([action isEqualToString:@ACTION_START_DUTY])
    {
        [[DatabaseHelper shared] updateSetting:joeyFieldDuty value:1];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            OrderViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderViewController"];
            [self.navigationController setViewControllers:@[viewController] animated:NO];
        });
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@NotificationsReloadEndWorkButton object:nil userInfo:nil];
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

- (void) errorMessage:(NSString *)action message:(NSString *)message fields:(NSArray *)fieldsArray
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

@end
