//
//  SummaryOrdersViewController.m
//  Joey
//
//  Created by Katia Maeda on 2016-05-20.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface SummaryOrdersViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *ordersTable;
@property(nonatomic, strong) NSMutableArray* tableViewSections;
@property(nonatomic, strong) NSMutableDictionary* tableViewCells;

@end

@implementation SummaryOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setSummary:(Summary *)summary
{
    _summary = summary;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setupDataSource];
        [self.ordersTable reloadData];
    });
}

-(void)setupDataSource
{
    self.tableViewSections = [NSMutableArray arrayWithCapacity:0];
    self.tableViewCells = [NSMutableDictionary dictionaryWithCapacity:0];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.timeZone = calendar.timeZone;
    [dateFormatter setDateFormat:@"MMMM dd, yyyy"];
    
    NSUInteger dateComponents = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSInteger previousYear = -1;
    NSInteger previousMonth = -1;
    NSInteger previousDay = -1;
    NSMutableArray *tableViewCellsForSection = nil;
    
    for (SummaryRecord *record in self.summary.records)
    {
        NSDate *date = record.date;
        
        NSDateComponents *components = [calendar components:dateComponents fromDate:date];
        NSInteger year = [components year];
        NSInteger month = [components month];
        NSInteger day = [components day];
        if (year != previousYear || month != previousMonth || day != previousDay)
        {
            NSString *sectionHeading = [dateFormatter stringFromDate:date];
            [self.tableViewSections addObject:sectionHeading];
            tableViewCellsForSection = [NSMutableArray arrayWithCapacity:0];
            [self.tableViewCells setObject:tableViewCellsForSection forKey:sectionHeading];
            
            previousYear = year;
            previousMonth = month;
            previousDay = day;
        }
        [tableViewCellsForSection addObject:record];
    }
}

#pragma mark UITableView Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.tableViewSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id key = [self.tableViewSections objectAtIndex:section];
    NSArray *tableViewCellsForSection = [self.tableViewCells objectForKey:key];
    return tableViewCellsForSection.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *nameView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 28)];
    nameView.backgroundColor = [UIColor colorWithWhite:.9 alpha:1.0];
    __weak UIView *weakView = nameView;

    NSString *string = [self.tableViewSections objectAtIndex:section];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 6, tableView.frame.size.width-30, 16)];
    headerLabel.font = [UIFont fontWithName:@"CenturyGothic-Bold" size:14.0f];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.text = string;
    [weakView addSubview:headerLabel];

    return weakView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"SummaryOrderTableViewCell";
    TwoLabelsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (!cell) cell = [[TwoLabelsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    __weak TwoLabelsTableCell *weakCell = cell;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        id key = [self.tableViewSections objectAtIndex:indexPath.section];
        NSArray *tableViewCellsForSection = [self.tableViewCells objectForKey:key];
        SummaryRecord *record = [tableViewCellsForSection objectAtIndex:indexPath.row];
        
        weakCell.label1.text = [NSString stringWithFormat:@"%@ | $%.2f", record.num, [record.amount floatValue]];
        weakCell.label2.text = [NSDateHelper stringTimeFromDate:record.date];
    });
    
    return weakCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    id key = [self.tableViewSections objectAtIndex:indexPath.section];
    NSArray *tableViewCellsForSection = [self.tableViewCells objectForKey:key];
    SummaryRecord *record = [tableViewCellsForSection objectAtIndex:indexPath.row];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        SummaryDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SummaryDetailViewController"];
        viewController.record = record;
        [self.navigationController pushViewController:viewController animated:YES];
    });
}

@end
