//
//  ComplainSection.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 22/03/2019.
//  Copyright © 2019 JoeyCo. All rights reserved.
//



#import "Prefix.pch"
#import "ZHPopupView.h"
#import "GeneralBottomSheetClass.h"
#import "MaterialBottomSheet.h"
#import "GeneralBottomSheetClass.h"

#define kRandomText @"\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\""

@interface ComplainSection ()<JSONHelperDelegate,UITextFieldDelegate>
{
        
    JSONHelper *json;
    
}

@end

@implementation ComplainSection

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    //init JSON
    json = [[JSONHelper alloc] init:self andDelegate:self];

    
    //Setting joey #
    self.lbl_joeyID.text = [NSString stringWithFormat:@"Joey ID # %@",[UrlInfo getJoeyId]];
    
    
    //Setting border across textview
    self.textView_desc_issue.layer.borderWidth = 1.0f;
    self.textView_desc_issue.layer.borderColor = [[UIColor colorWithRed:0.917f green:0.917f blue:0.917f alpha:1] CGColor];
    [[self.textView_desc_issue layer] setCornerRadius:10];
    [self.textView_desc_issue setKeyboardType:UIKeyboardTypeASCIICapable];
    
    
    
    
    //Set delegate
    [self.textfield_orderId setDelegate:self];
    [self.textfield_issueType setDelegate:self];

    
    //Set Textfield focusable
    [self.textfield_orderId becomeFirstResponder];
    

   // [self.textView_desc_issue setDelegate:self];
    
    
    // add observer in controller(s) where you want to receive data
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getTappedData:) name:@"getTappedData" object:nil];

  
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    Boolean returnType = NO;
    
    if(textField == self.textfield_orderId){
        if (textField.text.length < 10 || string.length == 0){
            returnType = YES;
        }
        else{
                returnType = NO;
        }
    }
    
    return returnType ;
   
}

- (IBAction)submitAction:(id)sender {
    
    if ([self validateTextField:self.textfield_orderId.text]) {
        
        [self submitFormToServer];
    }
    else{
        
        NSLog(@"TEXT FIELD NOT VALIDATED ");
    }

    
    
}


/*
 Validate Text field
 **/
-(BOOL)validateTextField:(NSString * )textData{
    
    BOOL validatedResponse = NO;
    
    if (textData && textData.length > 0)
    {
        /* not empty - do something */
        validatedResponse =  YES;
    }
    else
    {
        /* what ever */
        validatedResponse = NO;

    }
    
    return validatedResponse;
    
}


-(void)submitFormToServer{
    

    [json complainSectionApi:self.textfield_orderId.text
                     joey_id:[NSString stringWithFormat:@"%@",[UrlInfo getJoeyId]]
                  issue_type:self.textfield_issueType.text
                  desc_issue:self.textView_desc_issue.text];
    
}



/**
 On response of complain section success
 */
- (void)parsedObjects:(NSString *)action objects:(NSString *)responseString {
    
    
    ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                        iconImg:[UIImage imageNamed:@"send"]
                                                backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                          title:@"Success"
                                                        content:responseString
                                                   buttonTitles:@[@"Ok"]
                                            confirmBtnTextColor:nil otherBtnTextColor:nil
                                             buttonPressedBlock:^(NSInteger btnIdx) {
                                                 
                                                 [self clearAllTextField];
                                                 
                                                 [self.view.window endEditing:YES];
                                                 
                                                 

                                             }];
    
    popupView.contentTextAlignment  = NSTextAlignmentCenter;
    
    [popupView present];
    
}


/**
 Clear All edit text
 */
-(void)clearAllTextField{
    
    self.textfield_orderId.text = @"";
     self.textfield_issueType.text = @"";
    self.textView_desc_issue.text = @"";
    
    
}


/*
 Detect if textfield is tap
 **/

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    if (textField.tag == 8001) {
        
        [self openOptions];
        
        return NO;
    }
    else
    {
     
        
        return YES;
    }
    
  
}



/**
 Open options
 */
-(void)openOptions{
    
    // View controller the bottom sheet will hold
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
    GeneralBottomSheetClass * bottomSheet = [storyboard instantiateViewControllerWithIdentifier:@"GeneralBottomSheetController"];
    
    
    bottomSheet.dataSourceArray = [[NSMutableArray alloc]init];
    [bottomSheet.dataSourceArray addObject:@"Finance Issue"];
    [bottomSheet.dataSourceArray addObject:@"Tech Issue"];
    

    
    // Initialize the bottom sheet with the view controller just created
    MDCBottomSheetController *MDC = [[MDCBottomSheetController alloc] initWithContentViewController:bottomSheet];
    
   
   // [self presentViewController:MDC animated:true completion:nil];
    
//    if (@available(iOS 13.0, *)) {
//                   [MDC setModalPresentationStyle: UIModalPresentationFullScreen];
//               } else {
//                   // Fallback on earlier versions
//
//               }
     [self.navigationController presentViewController:MDC animated:YES completion:nil];
}



-(void) getTappedData:(NSNotification *) notification {
   
    id selectedRowData = notification.object ; // some custom object that was passed with notification fire.
    
    
    /**
     Setting data in textfield
     */
    self.textfield_issueType.text = selectedRowData;
    
    
    
    NSLog(@"TAPPED BOTTOM SHEET CELL : %@",selectedRowData);
}


@end
