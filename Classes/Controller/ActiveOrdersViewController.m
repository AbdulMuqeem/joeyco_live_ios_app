//
//  ActiveOrdersViewController.m
//  Joey
//
//  Created by Katia Maeda on 2015-11-06.
//  Copyright © 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"
#import "CallNow.h"
#import "MaterialBottomSheet.h"
#import "OrderStatusUpdates.h"
#import "MM_SUPERMARKET/MMCallAPI.h"
#import "ReturnOrderVC.h"
#import "ConfirmDropOffStatusSheet.h"
#import "UIView+Toast.h"
#import "CallNow_DialPadViewController.h"
#import "OpenLocationBottomSheet.h"
#import <QuartzCore/QuartzCore.h>

#define CLOSED_TASK_HEIGHT 80

@interface ActiveOrdersViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate, JSONHelperDelegate, GMSMapViewDelegate, UITextFieldDelegate>
{
    BOOL firstLayout;
    AlertHelper *alertHelper;
    JSONHelper *json;
    CustomKeyboardToolbar *toolbar;
    
    CLLocation *lastJoeyLocation;
    TravelMode mapRouteSetting;
    NSTimer *readInTimer;
    GMSMarker *joeyMarker;
    GMSPolyline *routePolyline;
    
    BOOL largeFontSize;
    
    PaneStatus paneStatus;
    CGFloat panInitialPositionY;
    CGFloat panInitialPositionX;
    
    Confirmation *currentConfirmation;

    
    RoundedButton *checklistButton;
    
    
}

@property(nonatomic,strong) MDCBottomSheetController *bottomSheetDropOff;

@property(nonatomic, strong) Task *TaskOpenedBeforeConfirmation;

@property(nonatomic, strong) NSMutableArray *orders;

@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (nonatomic, strong) NSMutableArray *polylines;

@property (nonatomic, weak) IBOutlet UICollectionView *ordersCollection;
@property (nonatomic, weak) IBOutlet UIScrollView *ordersTabs;
@property (nonatomic, strong) NSIndexPath *orderSelected;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *ordersCollectionHeight;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *ordersCollectionY;
@property (nonatomic, strong) NSMutableArray *cellHeight;
@property (nonatomic, strong) NSMutableArray *cellReadyInTxt;
@property (nonatomic, strong) NSMutableArray *currentTaskArray;

@property (nonatomic, weak) IBOutlet UIView *bgConfirmationView;
@property (nonatomic, weak) IBOutlet CustomScrollView *confirmationView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *confirmationViewHeight;
@property (nonatomic, weak) IBOutlet UILabel *confirmationTypeTxt;
@property (nonatomic, weak) IBOutlet RoundedButton *confirmationPhoneButton;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *confirmationPhoneButtonHeight;
@property (nonatomic, weak) IBOutlet UILabel *confirmationReadyInTxt;
@property (nonatomic, weak) IBOutlet UIView *confirmationDescriptionView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *confirmationDescriptionViewHeight;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *confirmationDescriptionViewHeight2;
@property (nonatomic, weak) IBOutlet UITextView *confirmationDescriptionTxt;
@property (nonatomic, weak) IBOutlet UITableView *confirmationTable;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *confirmationTableHeight;
@property (nonatomic, strong) NSMutableArray *confirmationRowHeight;
@property (nonatomic, strong) NSArray *confirmations;

@property (nonatomic, weak) IBOutlet UIView *offlineView;

@property (nonatomic, strong) NSString * currentOrderID;

@end

@implementation ActiveOrdersViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    firstLayout = YES;
    
    // Initiate GMSMapView
    self.mapView.settings.myLocationButton = YES;
    self.mapView.myLocationEnabled = YES;
    
    self.mapView.mapType = [self.delegate setMapType];
    self.mapView.delegate = self;
    
    largeFontSize = [[DatabaseHelper shared] getDisplayLargeFontSize];
    
    self.ordersCollection.pagingEnabled = YES;
    UITapGestureRecognizer *orderTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(orderTap:)];
    orderTap.numberOfTapsRequired = 1;
    orderTap.numberOfTouchesRequired = 1;
    [self.ordersCollection addGestureRecognizer:orderTap];
    
    // Confirmation List
    UITapGestureRecognizer *confirmationTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeConfirmations:)];
    confirmationTap.numberOfTapsRequired = 1;
    confirmationTap.numberOfTouchesRequired = 1;
    [self.bgConfirmationView addGestureRecognizer:confirmationTap];
    self.confirmationView.hidden = YES;
    self.bgConfirmationView.hidden = YES;
    
    UITapGestureRecognizer *hideKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    hideKeyboardGesture.numberOfTapsRequired = 1;
    hideKeyboardGesture.numberOfTouchesRequired = 1;
    [self.confirmationView addGestureRecognizer:hideKeyboardGesture];
    
    [self initArrays];
    self.orderSelected = [NSIndexPath indexPathForRow:0 inSection:0];
    
    toolbar = [[CustomKeyboardToolbar alloc] initWithView:self.confirmationView];
    [[NSNotificationCenter defaultCenter] addObserver:toolbar selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:toolbar selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
    
    //Check when your pop up is going to be hidden
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(confirmationDone:) name:@NotificationsConfirmationDone object:nil];

    
    //Check when your pop up is going to be hidden
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(confirmationDoneImgUpload:) name:@NotificationsConfirmationDoneImageUpload object:nil];
    
    
    
    
    
    self.offlineView.hidden = YES;
}



- (void) confirmationDone:(NSNotification *) notification
{
    // update orders when returning from order details
    BOOL hasInternet = [json hasInternetConnection:YES];
    if (hasInternet)
    {
        [self.delegate getOrderList];
        
        [self layoutConfirmationList:self.TaskOpenedBeforeConfirmation];
        
    }
    
}



- (void) confirmationDoneImgUpload:(NSNotification *) notification
{
    // update orders when returning from order details
    BOOL hasInternet = [json hasInternetConnection:YES];
    if (hasInternet)
    {

       [ self.bottomSheetDropOff dismissViewControllerAnimated:YES completion:nil];

        [self.delegate getOrderList];

        [self layoutConfirmationList:self.TaskOpenedBeforeConfirmation];

    }
    
}
//
//- (void) openConfirmDialog:(NSNotification *) notification
//{
//    [checklistButton setHighlighted:YES];
//    [checklistButton sendActionsForControlEvents:UIControlEventTouchUpInside];
//    [checklistButton setHighlighted:NO];
//}


-(void)viewWillAppear:(BOOL)animated
{
    int route = [[DatabaseHelper shared] getMapRouteData];
    mapRouteSetting = (route == 0) ? TravelModeDriving : TravelModeBicycling;
    
    // update orders when returning from order details
    BOOL hasInternet = [json hasInternetConnection:YES];
    if (hasInternet && !currentConfirmation)
    {
        [self.delegate getOrderList];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (firstLayout)
    {
        self.ordersCollectionHeight.constant = self.view.frame.size.height - CLOSED_TASK_HEIGHT;
        self.ordersCollectionY.constant = self.view.frame.size.height - CLOSED_TASK_HEIGHT;
        [self.ordersCollection reloadData];
        
        firstLayout = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [readInTimer invalidate];
    [alertHelper dismissAlert];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:toolbar];
    
    [readInTimer invalidate];
    
    [toolbar hideKeyboard];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    self.confirmationTable.delegate = nil;
    self.confirmationTable.dataSource = nil;
}

-(void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - layout

-(void)initArrays
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.polylines = [NSMutableArray array];
        self.cellHeight = [NSMutableArray array];
        self.cellReadyInTxt = [NSMutableArray array];
        self.currentTaskArray = [NSMutableArray array];
        for (NSInteger i = 0; i < [self.orders count]; ++i)
        {
            [self.polylines addObject:[NSNull null]];
            [self.cellHeight addObject:[NSNull null]];
            [self.cellReadyInTxt addObject:[NSNull null]];
            [self.currentTaskArray addObject:[NSNull null]];
        }
    });
}

-(void)updateCurrentTaskArray:(NSUInteger)index task:(Task *)task
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (task && ![task isEqual:[NSNull null]] && index < self.currentTaskArray.count)
        {
            [self.currentTaskArray replaceObjectAtIndex:index withObject:task];
        }
    });
}

-(void)updateOrders:(NSMutableArray *)orders
{
    [self reloadData:orders];
}

-(void)reloadData:(NSMutableArray *)ordersAccepted
{
    // If has one order accepted, go to AcceptOrder
    if (ordersAccepted.count > 0 && self.orderSelected)
    {
        [[DatabaseHelper shared] insertOrders:ordersAccepted];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Assert collection and self.orders.count
            if (!self.orders)
            {
                self.orders = ordersAccepted;
                [self initArrays];
            }
            else if (self.orders.count > ordersAccepted.count)
            {
                NSMutableArray *indexPaths = [NSMutableArray array];
                [indexPaths addObject:self.orderSelected];
                NSInteger count = self.orders.count - 1;
                NSInteger last = self.orders.count - 1;
                while (count > ordersAccepted.count)
                {
                    if (last == self.orderSelected.row) last--;
                    [indexPaths addObject:[NSIndexPath indexPathForRow:last inSection:0]];
                    count--; last--;
                }
                
                self.orders = ordersAccepted;
                [self initArrays];
                [self.ordersCollection deleteItemsAtIndexPaths:indexPaths];
            }
            else if (self.orders.count < ordersAccepted.count)
            {
                NSIndexPath *firstItem = [NSIndexPath indexPathForRow:0 inSection:0];
                NSMutableArray *indexPaths = [NSMutableArray array];
                [indexPaths addObject:firstItem];
                NSInteger count = self.orders.count + 1;
                while (count < ordersAccepted.count)
                {
                    [indexPaths addObject:firstItem];
                    count++;
                }
                
                self.orders = ordersAccepted;
                [self initArrays];
                [self.ordersCollection insertItemsAtIndexPaths:indexPaths];
            }
            else
            {
                BOOL isSameOrders = YES;
                for (NSInteger i = 0; i < self.orders.count; i++)
                {
                    Order *oldOrder = [self.orders objectAtIndex:i];
                    Order *newOrder = [ordersAccepted objectAtIndex:i];
                    if ([oldOrder.orderId doubleValue] != [newOrder.orderId doubleValue])
                    {
                        isSameOrders = NO;
                        break;
                    }
                }
                
                if (!isSameOrders)
                {
                    [self initArrays];
                }
                
                self.orders = ordersAccepted;
                
            }
            
            NSInteger nextOrder = self.orderSelected.row;
            if (self.orderSelected.row >= self.orders.count)
            {
                nextOrder = self.orders.count-1;
            }
            
            [self selectOrder:nextOrder];
            //            NSMutableArray *visibleItems = [NSMutableArray arrayWithArray:[self.ordersCollection indexPathsForVisibleItems]];
            //            [visibleItems addObject:self.orderSelected];
            //            [self.ordersCollection reloadItemsAtIndexPaths:visibleItems];
            [self.ordersCollection reloadData];
            [self performSelector:@selector(openPaneAnimation) withObject:nil afterDelay:0.3];
        });
    }
    else
    {
        [readInTimer invalidate];
        self.orders = ordersAccepted;
        [self.ordersCollection reloadData];
        [self initArrays];
        [self createTabs];
        [self.mapView clear];
        self.ordersCollectionY.constant = self.view.frame.size.height + 30;
    }
    
    [self.delegate stopRefreshAnimation];
}

-(void)updateReadyInTime
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.orderSelected.row >= self.currentTaskArray.count || self.orderSelected.row >= self.cellReadyInTxt.count)
        {
            return;
        }
        
        Task *currentTask = [self.currentTaskArray objectAtIndex:self.orderSelected.row];
        if (!currentTask || [currentTask isEqual:[NSNull null]])
        {
            return;
        }
        
        if (currentTask.dueTime <= 0)
        {
            self.confirmationReadyInTxt.hidden = YES;
            return;
        }
        
        UILabel *readyInTxt = [self.cellReadyInTxt objectAtIndex:self.orderSelected.row];
        if (!readyInTxt || [readyInTxt isEqual:[NSNull null]])
        {
            return;
        }
        
        NSInteger countDown = [currentTask.dueTime doubleValue] - [[NSDate date] timeIntervalSince1970];
        self.confirmationReadyInTxt.hidden = NO;
        
        if (countDown > 0)
        {
            readyInTxt.text = [NSString stringWithFormat:NSLocalizedString(@"Ready in %@", nil), [NSDateHelper stringFromCountDown:countDown]];
            readyInTxt.textColor = [UIColor colorWithRed:0.83 green:0.4 blue:0.15 alpha:1.0];
            self.confirmationReadyInTxt.text = [NSDateHelper stringFromCountDown:countDown];
        }
        else
        {
            readyInTxt.text = NSLocalizedString(@"Ready to pick up", nil);
            readyInTxt.textColor = [UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0];
            self.confirmationReadyInTxt.text = NSLocalizedString(@"Ready to pick up", nil);
            self.confirmationReadyInTxt.textColor = [UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0];
            [readInTimer invalidate];
        }
        
        if ([currentTask.type isEqualToString:@TASK_DROP_OFF])
        {
            self.confirmationReadyInTxt.hidden = YES;
            readyInTxt.hidden = YES;
        }
    });
}

-(void)layoutConfirmationList:(Task *)task
{
    if (!task)
    {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([task.type isEqualToString:@TASK_PICKUP])
        {
            self.confirmationTypeTxt.text = NSLocalizedString(@"Pick up", nil);
            
        }
        else if ([task.type isEqualToString:@TASK_DROP_OFF])
        {
            self.confirmationTypeTxt.text = NSLocalizedString(@"Drop off", nil);
            self.confirmationReadyInTxt.hidden = YES;
        }
        else
        {
            self.confirmationTypeTxt.text = @"";
        }
        
        if (task.contact.phone && ![task.contact.phone isEqualToString:@""] && [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel:+11111"]])
        {
            self.confirmationPhoneButton.hidden = NO;
            self.confirmationPhoneButton.buttonData = task.contact.phone;
            self.confirmationPhoneButtonHeight.constant = 30;
            [self.confirmationPhoneButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"Call %@", nil), task.contact.name] forState:UIControlStateNormal];
        }
        else
        {
            self.confirmationPhoneButton.hidden = YES;
            self.confirmationPhoneButtonHeight.constant = 0;
        }
        
        if (task.taskDescription && ![task.taskDescription isEqualToString:@""])
        {
            self.confirmationDescriptionTxt.text = [task.taskDescription hasPrefix:@"\n"] ? [task.taskDescription substringFromIndex:1] : task.taskDescription;
            self.confirmationDescriptionView.hidden = NO;
            self.confirmationDescriptionViewHeight2.constant = [self.confirmationDescriptionTxt sizeThatFits:CGSizeMake(self.confirmationDescriptionTxt.frame.size.width,300)].height;
            self.confirmationDescriptionViewHeight.constant = self.confirmationDescriptionViewHeight2.constant + 17;
        }
        else
        {
            self.confirmationDescriptionView.hidden = YES;
            self.confirmationDescriptionViewHeight.constant = 0;
        }
        
        toolbar.textFields = [NSArray array];
        self.confirmations = task.confirmations;
        self.confirmationRowHeight = [NSMutableArray array];
        for (NSInteger i = 0; i < [self.confirmations count]; ++i)
        {
            [self.confirmationRowHeight addObject:[NSNumber numberWithInt:110]];
        }
        [self.confirmationTable reloadData];
        self.confirmationTableHeight.constant = self.confirmationTable.contentSize.height;
        CGFloat popupHeight = self.confirmationPhoneButtonHeight.constant + self.confirmationDescriptionViewHeight.constant + self.confirmationTableHeight.constant + 98;
        if (popupHeight > self.view.frame.size.height)
        {
            popupHeight = self.view.frame.size.height;
        }
        self.confirmationViewHeight.constant = popupHeight;
        
        [self.confirmationView setContentOffset: CGPointMake(0,0) animated:NO];
        self.confirmationView.hidden = NO;
        self.bgConfirmationView.hidden = NO;
    });
}

-(void)closeConfirmations:(id)sender
{
    if (self.confirmationView == sender)
    {
        return;
    }
    
    [self hideKeyboard];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.confirmationView.hidden = YES;
        self.bgConfirmationView.hidden = YES;
    });
}

-(void)connectivityDidChange:(BOOL)hasInternet
{
    self.offlineView.hidden = hasInternet;
}

#pragma mark - Orders Cards

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.orders.count;
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = collectionView.frame.size;
    size.height = self.ordersCollectionHeight.constant;
    return size;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"OrderCollectionCell";
    OrderCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    __weak OrderCollectionCell *weakCell = cell;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (indexPath.row < self.orders.count)
        {
            Order *order = [self.orders objectAtIndex:indexPath.row];
            Task *currentTask = [order getNextTask];
            [self updateCurrentTaskArray:indexPath.row task:currentTask];
            
            weakCell.orderNumberTxt.text = (order.tip && ![order.tip isEqualToString:@""] && ![order.tip isEqualToString:@"0.00"] && ![order.tip isEqualToString:@"0.0"]) ? [NSString stringWithFormat:@"%@ (+ $%.2f tip!)", order.num, [order.tip floatValue]] : order.num;
          
            //Commented on 25th May 2019
            
//            if ([order.distance longValue] > 0)
//            {
                weakCell.travelDistanceTxt.text = [MapHelper stringFromDistance:[order.distance doubleValue]];
//            }
            
            
            // commented on 25th may 2019
            //weakCell.travelTimeTxt.text = [self convertTime:[order.eta intValue]];
            
            weakCell.travelTimeTxt.text =[NSString stringWithFormat:@"%@",order.remaintime];

            
            if (indexPath.row < self.cellReadyInTxt.count)
            {
                [self.cellReadyInTxt replaceObjectAtIndex:indexPath.row withObject:weakCell.readyInTxt];
            }
            
            [self createTasksTable:order.tasks nextTask:currentTask scrollView:weakCell.scrollContentView];
            weakCell.contentViewHeight.constant = weakCell.scrollContentView.frame.size.height;
            [weakCell.scrollView setNeedsLayout];
            
            if (indexPath.row < self.cellHeight.count)
            {
                [self.cellHeight replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithFloat:weakCell.scrollContentView.frame.size.height]];
            }
        }
    });
    
    return weakCell;
}

-(NSString *)convertTime:(int)time
{
    if (time < 1) return @"";
    float hours = floor(time / 60);
    float minutes = (time % 60);
    if (hours == 0) return [NSString stringWithFormat:@"%.0f min", minutes];
    return [NSString stringWithFormat:@"%.0f hr %.0f min", hours, minutes];
}

#pragma mark- Orders Tabs

-(void)createTabs
{
    for (UIView *subview in self.ordersTabs.subviews)
    {
        [subview removeFromSuperview];
    }
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    int tabWidth = floor(screenRect.size.width/self.orders.count);
    if(tabWidth < 70) {
        tabWidth = 70;
    }
    
    int posX = 0;
    int tabHeight = 30;
    UIFont *fontBold = [UIFont boldSystemFontOfSize:12.0];
    
    for (int i = 0; i < self.orders.count; i++)
    {
        Order *order = [self.orders objectAtIndex:i];
        
        UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(posX, 0, tabWidth, tabHeight)];
        containerView.tag = i;
        UILabel *labelType = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, tabWidth-12, tabHeight)];
        labelType.text = order.num;
        [labelType setBackgroundColor:[UIColor clearColor]];
        [labelType setFont:fontBold];
        [containerView addSubview:labelType];
        
        if (i == self.orderSelected.row)
        {
            [containerView setBackgroundColor:[UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1.0f]];
            [labelType setTextColor:[UIColor whiteColor]];
        }
        else
        {
            [containerView setBackgroundColor:[UIColor whiteColor]];
            [labelType setTextColor:[UIColor blackColor]];
        }
        
        UILabel *separator = [[UILabel alloc] initWithFrame:CGRectMake(tabWidth-1, 0, 1, tabHeight)];
        [separator setBackgroundColor:[UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1.0f]];
        [containerView addSubview:separator];
        
        [self.ordersTabs addSubview:containerView];
        posX += tabWidth;
        
        // Tap
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectTab:)];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        [containerView addGestureRecognizer:singleTap];
    }
    
    self.ordersTabs.contentSize = CGSizeMake(posX, tabHeight);
    [self.ordersTabs setNeedsUpdateConstraints];
}

-(void)selectTab:(id)sender
{
    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *)sender;
    
    if (gesture.view.tag < 0 || gesture.view.tag >= self.orders.count) {
        return;
    }
    
    [self.ordersCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:gesture.view.tag inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
}

#pragma mark- Order Tasks

-(void)createTasksTable:(NSMutableArray *)tasks nextTask:(Task *)currentTask scrollView:(UIView *)contentView
{
    // Only called inside dispatch_get_main_queue
    int yPosition = 37;
    int pageWidth = self.ordersCollection.frame.size.width;
    int pageHeight = 80;
    double i = 0;
    
    for (UIView *subview in contentView.subviews)
    {
        if (subview.tag >= 500)
        {
            [subview removeFromSuperview];
        }
    }
    
    for (Task *task in tasks)
    {
        UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, yPosition, pageWidth, pageHeight)];
        containerView.tag = 500 + i;
        
        [self loadTask:task currentTask:currentTask view:containerView];
        
        
        //add the container view to the scroll view
        [contentView addSubview:containerView];
        yPosition += containerView.frame.size.height + 8;
        
        if (![task isEqual:[tasks lastObject]])
        {
            UILabel *separator = [[UILabel alloc] initWithFrame:CGRectMake(4, yPosition-4, pageWidth-8, 1)];
            [separator setBackgroundColor:[UIColor lightGrayColor]];
            separator.tag = 500 + i;
            [contentView addSubview:separator];
        }
    }
    
    CGRect frame = contentView.frame;
    frame.size.height = yPosition;
    contentView.frame = frame;
    [contentView setNeedsUpdateConstraints];
}

-(void)loadTask:(Task *)task currentTask:(Task *)currentTask view:(UIView *)view
{
    // Only called inside dispatch_get_main_queue
    CGFloat posX = 8;
    CGFloat posY = 4;
    CGFloat labelWidth = view.frame.size.width - 90;
    
    CGFloat largeTextViewWidth = view.frame.size.width - 10;

    CGFloat labelHeight = 15;
    CGFloat fontSize = (largeFontSize) ? 16.0 : 12.0;
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    UIFont *fontBold = [UIFont boldSystemFontOfSize:fontSize];
    

    UILabel *labelType = [[UILabel alloc] init];
    [labelType setBackgroundColor:[UIColor clearColor]];
    [labelType setFont:fontBold];
    labelType.text = [task.type isEqualToString:@TASK_PICKUP] ? NSLocalizedString(@"Pick up", nil) : NSLocalizedString(@"Drop off", nil);
    labelType.text = [NSString stringWithFormat:@"%@ (%@)",labelType.text,task.num];
    
   //labelType.text = task.num;
    
    labelType.frame = CGRectMake(posX, posY, labelWidth, labelHeight);
    [view addSubview:labelType];
    
 
    
    /*
           
           Delivery Window -title
           */
      
      if ([ActiveOrdersViewController isEmpty: task.start_time] == NO){
         
      
        CGFloat rightSideView = view.frame.size.width - 106;
      
            UILabel *labelTimeSlot = [[UILabel alloc] init];
            [labelTimeSlot setBackgroundColor:[UIColor clearColor]];
            [labelTimeSlot setFont:fontBold];
            labelTimeSlot.text = [NSString stringWithFormat:@"Delivery Window"];
                                                                              
             CGFloat TimeSlotHeight = [labelTimeSlot sizeThatFits:CGSizeMake(labelWidth,300)].height;
             labelTimeSlot.frame = CGRectMake(rightSideView, posY, labelWidth+5, TimeSlotHeight -1);
             [view addSubview:labelTimeSlot];
            
      
      
          CGFloat heightForTimeSlotValue = posY + TimeSlotHeight ;
          
          /*

           Time slot -value
           */
      
          CGFloat rightSideViewValue = view.frame.size.width - 88;

            UILabel *labelTimeSlotvalue = [[UILabel alloc] init];
            [labelTimeSlotvalue setFont:font];

             [labelTimeSlotvalue setBackgroundColor:[UIColor clearColor]];
             labelTimeSlotvalue.text = [NSString stringWithFormat:@"(%@-%@)",task.start_time,task.end_time];

             CGFloat TimeSlotHeightvalue = [labelTimeSlotvalue sizeThatFits:CGSizeMake(labelWidth,300)].height - 5;
             labelTimeSlotvalue.frame = CGRectMake(rightSideViewValue - 32  ,  heightForTimeSlotValue + 8, labelWidth+5, TimeSlotHeightvalue );
             [view addSubview:labelTimeSlotvalue];

      }
      
      
    
        /**
                Increasing height for other labels (Client order # , Name )
         */
       posY += labelHeight;
    
    
    
    /*
         
         Client Order #
         */
    
    CGFloat clientOrderNumSlotHeight = 0;
    
    if ([ActiveOrdersViewController isEmpty: task.merchant_order_num] == NO){
        
    
        UILabel *labelClientOrderNum = [[UILabel alloc] init];
        
           [labelClientOrderNum setBackgroundColor:[UIColor clearColor]];
           [labelClientOrderNum setFont:fontBold];
    
          labelClientOrderNum.text =  [NSString stringWithFormat: NSLocalizedString(@"Client Order # : %@", nil), task.merchant_order_num];
                                                                            
           CGFloat clientOrderNumSlotHeight = [labelClientOrderNum sizeThatFits:CGSizeMake(labelWidth,300)].height - 5;
           labelClientOrderNum.frame = CGRectMake(posX, posY + 5 , labelWidth+5, clientOrderNumSlotHeight);
           [view addSubview:labelClientOrderNum];
    }
    

      
  
    
    /*
         
        Name
         */
    
      //Added posY value for name
    
    if (clientOrderNumSlotHeight != 0) {
          posY += clientOrderNumSlotHeight;

    }
             
    
    UITextView *labelName = [[UITextView alloc] init];
    [labelName setEditable:NO];
    [labelName setSelectable:NO];
    [labelName setScrollEnabled:NO];
    [labelName setBackgroundColor:[UIColor clearColor]];
    [labelName setFont:fontBold];
    labelName.text = [NSString stringWithFormat:NSLocalizedString(@"Name : %@", nil), task.contact.name];
  
    CGFloat nameHeight = [labelName sizeThatFits:CGSizeMake(labelWidth,300)].height - 5;
    labelName.frame = CGRectMake(posX-5, posY + 13, labelWidth+5, nameHeight);
    [view addSubview:labelName];
    posY += nameHeight;
    
    
    
    
    
    /**
     
     CheckList button Code -STARTS
     
     */
    
    CGFloat buttonX = posX ;
    CGFloat buttonY = labelName.frame.size.height + 37;
    
    if ([task isAllConfirmed])
    {
        UILabel *completedLabel = [[UILabel alloc] initWithFrame:CGRectMake(buttonX, buttonY, 70, 20)];
        [completedLabel setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
        [completedLabel setFont:font];
        completedLabel.text = NSLocalizedString(@"Completed", nil);
        completedLabel.textColor = [UIColor whiteColor];
        completedLabel.textAlignment = NSTextAlignmentCenter;
        [view addSubview:completedLabel];
        
        
        /*
         Adding button height for padding above address
         */
        CGFloat padding = 25;
        posY = posY + completedLabel.frame.size.height + padding;
    }
    else if ([currentTask isEqual:task])
    {
        if (posY < 75)
        {
            posY = 75;
        }
        
        buttonY = buttonY + 8 ;
        checklistButton = [[RoundedButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, 80, 35)];
        [checklistButton setTitle:NSLocalizedString(@"Checklist", nil) forState:UIControlStateNormal];
        UIFont *fontButton = [UIFont systemFontOfSize:10.0];
        [checklistButton.titleLabel setFont:fontButton];
        //Make round corner buttons
        
        checklistButton.layer.cornerRadius = 6;
        checklistButton.layer.masksToBounds = YES;//Important
        [checklistButton setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
        checklistButton.buttonData = task;
        [checklistButton addTarget:self action:@selector(openConfirmationListAction:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:checklistButton];
        
        
        buttonX = buttonX + checklistButton.frame.size.width + 10;
        RoundedButton *wazeButton = [[RoundedButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, 65, 35)];
//        [wazeButton setTitle:@"Waze" forState:UIControlStateNormal];
        [wazeButton setTitle:@"Map" forState:UIControlStateNormal];
        //Make round corner buttons
        
        wazeButton.layer.cornerRadius = 6;
        wazeButton.layer.masksToBounds = YES;//Important
        [wazeButton.titleLabel setFont:fontButton];
        [wazeButton setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
        wazeButton.buttonData = task;
        [wazeButton addTarget:self action:@selector(openBottomSheetOption:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:wazeButton];
        
        
         if ([task.type isEqualToString:@TASK_PICKUP])
         {
             buttonX = buttonX + wazeButton.frame.size.width + 10;
//
//             RoundedButton *btn = [[RoundedButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, 100, 35)];
//             [btn setTitle:NSLocalizedString(@"Report Delay", nil) forState:UIControlStateNormal];
//             [btn.titleLabel setFont:fontButton];
//             [btn setBackgroundColor:[UIColor colorWithRed:0.83 green:0.4 blue:0.15 alpha:1.0]];
//             btn.buttonData = task;
//             [btn addTarget:self action:@selector(openDelayDialog:) forControlEvents:UIControlEventTouchUpInside];
//             [view addSubview:btn];
             
             RoundedButton *btn = [[RoundedButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, 100, 35)];
             [btn setTitle:NSLocalizedString(@"At PickUp", nil) forState:UIControlStateNormal];
             [btn.titleLabel setFont:fontButton];
             [btn setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
             btn.buttonData = task;
             //Make round corner buttons
             
             btn.layer.cornerRadius = 6;
             btn.layer.masksToBounds = YES;//Important
             [btn addTarget:self action:@selector(markPickup:) forControlEvents:UIControlEventTouchUpInside];
             [view addSubview:btn];
             
       
             
       
             
        }
        else
        {
            buttonX = buttonX + wazeButton.frame.size.width + 5;
            
            RoundedButton *btn = [[RoundedButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, 100, 35)];
            [btn setTitle:NSLocalizedString(@"At DropOff", nil) forState:UIControlStateNormal];
            [btn.titleLabel setFont:fontButton];
            [btn setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
            btn.buttonData = task;
            //Make round corner buttons
            
            btn.layer.cornerRadius = 6;
            btn.layer.masksToBounds = YES;//Important
            [btn addTarget:self action:@selector(markDropoff:) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:btn];
            
            
//            buttonY += buttonY;
//
//            RoundedButton *returnBtn = [[RoundedButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, 65, 35)];
//            [returnBtn setTitle:NSLocalizedString(@"Return", nil) forState:UIControlStateNormal];
//            [returnBtn.titleLabel setFont:fontButton];
//            [returnBtn setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
//            returnBtn.buttonData = task;
//            [returnBtn addTarget:self action:@selector(openReturnDialog:) forControlEvents:UIControlEventTouchUpInside];
//            [view addSubview:returnBtn];
//
//
//            buttonX = buttonX + returnBtn.frame.size.width + 5;
//            RoundedButton *delay_btn = [[RoundedButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, 90, 35)];
//            [delay_btn setTitle:NSLocalizedString(@"Report Delay", nil) forState:UIControlStateNormal];
//            [delay_btn.titleLabel setFont:fontButton];
//            [delay_btn setBackgroundColor:[UIColor colorWithRed:0.83 green:0.4 blue:0.15 alpha:1.0]];
//            delay_btn.buttonData = task;
//            [delay_btn addTarget:self action:@selector(openDelayDialog:) forControlEvents:UIControlEventTouchUpInside];
//            [view addSubview:delay_btn];
        }
        
       
        
        /*
         Adding button height for padding above address
         */
        posY = posY + 35;
    }
    else
    {
        /*
                Adding button height for padding above address
                */
               posY = posY + 35;
    }
    
    
    /**
     
     CheckList button Code -ENDS
     
     */
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(posX, posY, labelWidth, 40)];

    CustomButton *phoneButton ;
    if (task.contact.phone && ![task.contact.phone isEqualToString:@""])
    {
      //  UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(posX, posY, labelWidth, 15)];
      
        
        [containerView setBackgroundColor:[UIColor clearColor]];
        
        UILabel *labelPhone = [[UILabel alloc] init];
        [labelPhone setBackgroundColor:[UIColor clearColor]];
        [labelPhone setFont:font];
        labelPhone.text = NSLocalizedString(@"Phone number :", nil);
        CGSize newSize = [labelPhone sizeThatFits:CGSizeMake(labelWidth, 15)];
       // labelPhone.frame = CGRectMake(0, 0, newSize.width, 15);
        
       // labelPhone.frame = CGRectMake(0, 0, 200, 150);

        //labelPhone.frame = CGRectMake(0, 0, 0, 0);
        //  [containerView addSubview:labelPhone];
        
        NSString *text = task.contact.phone;
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text];
        NSRange phoneRange = NSMakeRange(0, task.contact.phone.length);
        // [attributedText setAttributes:@{NSFontAttributeName:font, NSUnderlineStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle]} range:phoneRange];
        
        phoneButton =  [CustomButton buttonWithType:UIButtonTypeCustom];
        [phoneButton setTitle:text forState:UIControlStateNormal];
        
        
        
        //*********************************
        //TODO AFTER 12th Dec2018 build
        //UNCOMMENT BELOW CODE
        //*********************************
        //Hide Phone Num
        [phoneButton setTitle:NSLocalizedString(@"Call", nil) forState:UIControlStateNormal];
        
        
        
        
        //*********************************
        //TODO AFTER 12th Dec2018 build
        //COMMENT BELOW CODE
        //*********************************
        
        [phoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [phoneButton setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
        [phoneButton.titleLabel setFont:font];
        CGSize newSize2 = [phoneButton sizeThatFits:CGSizeMake(labelWidth, 15)];
        // phoneButton.frame = CGRectMake(newSize.width+5, 0, newSize2.width+10, 10+15);
       // phoneButton.frame = CGRectMake(2, 10, newSize2.width+15, 10+15);
        
//        if ([currentTask isEqual:task])
//       {
          if ([task.type isEqualToString:@TASK_PICKUP]){
               phoneButton.frame = CGRectMake(2, 15, newSize2.width+35, 33);
               
           }
           else
           {
               phoneButton.frame = CGRectMake(2, 5, newSize2.width+35, 33);
           }
   //    }
        // phoneButton.frame = CGRectMake(2, 10, newSize2.width+15, 100);
        
        //Make round corner buttons
        
        phoneButton.layer.cornerRadius = 6;
        phoneButton.layer.masksToBounds = YES;//Important
        
        
        phoneButton.buttonData = task.contact.phone;
        
        //Setting Current Order ID
        self.currentOrderID = [task.orderId stringValue];
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel:+11111"]])
        {
            // [phoneButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            [phoneButton.titleLabel setAttributedText:attributedText];
            
            
            //*********************************
            //TODO AFTER 12th Dec2018 build
            //UNCOMMENT BELOW CODE
            //*********************************
            
            //Hide Phone Num
            phoneButton.titleLabel.text = NSLocalizedString(@"Call", nil);
            phoneButton.titleLabel.textColor = [UIColor whiteColor];
            [phoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            
            
            //*********************************
            //TODO AFTER 12th Dec2018 build
            //COMMENT BELOW CODE
            //*********************************
            
            // [phoneButton.titleLabel setAttributedText:attributedText];
            
        }
        
        // Right to Left languages 'arabic'
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0 && [UIView userInterfaceLayoutDirectionForSemanticContentAttribute:self.view.semanticContentAttribute] == UIUserInterfaceLayoutDirectionRightToLeft) {
            labelPhone.frame = CGRectMake(labelWidth-newSize.width, 0, newSize.width, 15);
            phoneButton.frame = CGRectMake(labelWidth-newSize.width-newSize.width-5, 0, newSize.width, 15);
        }
        
        //*********************************
        //TODO AFTER 12th Dec2018 build
        //COMMENT BELOW CODE
        //*********************************
        
        // [phoneButton addTarget:self action:@selector(phoneText:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        //*********************************
        //TODO AFTER 12th Dec2018 build
        //unCOMMENT BELOW CODE
        //*********************************
        
        
          [phoneButton addTarget:self action:@selector(phoneText:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        [containerView addSubview:phoneButton];
       [view addSubview:containerView];
        posY += 35;
    }
    
    
    
    UIFont *fontButton = [UIFont systemFontOfSize:10.0];
    CGRect PhoneBtnPosition = phoneButton.frame ;
    CGSize PhoneBtnSize = phoneButton.frame.size ;
    CGFloat phoneBtnYPosition = containerView.frame.origin.y +5;
    CGFloat BtnXPosition = PhoneBtnPosition.origin.x  + PhoneBtnSize.width  + 15;
    
    
    
    if ([currentTask isEqual:task])
   {
    if ([task.type isEqualToString:@TASK_PICKUP])
    {
    
        RoundedButton *btn = [[RoundedButton alloc] initWithFrame:CGRectMake(BtnXPosition, phoneBtnYPosition + 10, 100, 33)];
                     [btn setTitle:NSLocalizedString(@"Report Delay", nil) forState:UIControlStateNormal];
                     [btn.titleLabel setFont:fontButton];
        //Make round corner buttons
        
        btn.layer.cornerRadius = 6;
        btn.layer.masksToBounds = YES;//Important
        
                     [btn setBackgroundColor:[UIColor colorWithRed:0.83 green:0.4 blue:0.15 alpha:1.0]];
                     btn.buttonData = task;
                     [btn addTarget:self action:@selector(openDelayDialog:) forControlEvents:UIControlEventTouchUpInside];
        [containerView addSubview:btn];
        
        [view addSubview:btn];
        
        CGFloat roundBtnSize = btn.frame.size.height;
        posY += roundBtnSize;
                     
    
   }
    else
    {
        RoundedButton *returnBtn = [[RoundedButton alloc] initWithFrame:CGRectMake(BtnXPosition, phoneBtnYPosition, 65, 35)];
        [returnBtn setTitle:NSLocalizedString(@"Return", nil) forState:UIControlStateNormal];
        [returnBtn.titleLabel setFont:fontButton];
        [returnBtn setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
        returnBtn.buttonData = task;
        //Make round corner buttons
        
        returnBtn.layer.cornerRadius = 6;
        returnBtn.layer.masksToBounds = YES;//Important
        [returnBtn addTarget:self action:@selector(openReturnDialog:) forControlEvents:UIControlEventTouchUpInside];
       
        [view addSubview:returnBtn];
        
        
        buttonX = BtnXPosition + returnBtn.frame.size.width + 15;
        RoundedButton *btn = [[RoundedButton alloc] initWithFrame:CGRectMake(buttonX, phoneBtnYPosition, 90, 35)];
        [btn setTitle:NSLocalizedString(@"Report Delay", nil) forState:UIControlStateNormal];
        [btn.titleLabel setFont:fontButton];
        [btn setBackgroundColor:[UIColor colorWithRed:0.83 green:0.4 blue:0.15 alpha:1.0]];
        btn.buttonData = task;
        //Make round corner buttons
        
        btn.layer.cornerRadius = 6;
        btn.layer.masksToBounds = YES;//Important
        [btn addTarget:self action:@selector(openDelayDialog:) forControlEvents:UIControlEventTouchUpInside];
       
//        [containerView addSubview:returnBtn];
//
//        [containerView addSubview:btn];
//        [view addSubview:containerView];
      [view addSubview:btn];
        
        CGFloat roundBtnSize = btn.frame.size.height;
        posY += roundBtnSize -10;
    }
   // [view addSubview:containerView];
   }
    else
    {
        posY += 10;
    }
    
    
    NSString *taskInfo = [NSString stringWithFormat:NSLocalizedString(@"Address 1 : %@", nil), task.location.address.name];
    
    
   // task.address_line2 = @" 111-c Jami commercial Khayaban e ittedhad";
    
    NSString *addressLine2 = [NSString stringWithFormat:NSLocalizedString(@"Address 2 : %@", nil), task.address_line2];
    
    
    //First checking if order is SD or not. If Yes then add type (Business/Residential)
    if ([task.order.num rangeOfString:@"SD"].location != NSNotFound) {
        
        if (currentTask.type != nil) {
            NSLog(@"*****Adddress TYPE :%@ ",task.location.AddressType);
        }
    }
    
    taskInfo = (![task.location.suite.name isEqualToString:@""]) ? [NSString stringWithFormat:NSLocalizedString(@"%@\nSuite : %@", nil), taskInfo, task.location.suite.name] : taskInfo;
    taskInfo = (![task.location.buzzer.name isEqualToString:@""]) ? [NSString stringWithFormat:NSLocalizedString(@"%@\nBuzzer : %@", nil), taskInfo, task.location.buzzer.name] : taskInfo;
    taskInfo = ([task.weightValue doubleValue] > 0) ? [NSString stringWithFormat:NSLocalizedString(@"%@\nWeight : %@ %@", nil), taskInfo, task.weightValue, task.weightUnit] : taskInfo;
    
    //Setting Addres line 1
    if (taskInfo && ![taskInfo isEqualToString:@""])
    {
        UILabel *labelInfo = [[UILabel alloc] init];
        [labelInfo setBackgroundColor:[UIColor clearColor]];
        [labelInfo setFont:font];
        labelInfo.text = taskInfo;
        labelInfo.numberOfLines = 0;
        CGSize newSize = [labelInfo sizeThatFits:CGSizeMake(labelWidth, 500)];
        labelInfo.frame = CGRectMake(posX, posY, largeTextViewWidth, newSize.height);
        [view addSubview:labelInfo];
        posY += newSize.height;
    }
    
    //Setting Addres line 2
       if ([ActiveOrdersViewController isEmpty:task.address_line2] == NO)
       {
           UILabel *labelInfo = [[UILabel alloc] init];
           [labelInfo setBackgroundColor:[UIColor clearColor]];
           [labelInfo setFont:font];
           labelInfo.text = addressLine2;
           labelInfo.numberOfLines = 0;
           CGSize newSize = [labelInfo sizeThatFits:CGSizeMake(labelWidth, 500)];
           labelInfo.frame = CGRectMake(posX, posY + 5, largeTextViewWidth, newSize.height);
           [view addSubview:labelInfo];
           posY += newSize.height;
       }
//
//    if (task.contact.phone && ![task.contact.phone isEqualToString:@""])
//    {
//      //  UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(posX, posY, labelWidth, 15)];
//        UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(posX, posY, labelWidth, 40)];
//
//        [containerView setBackgroundColor:[UIColor clearColor]];
//
//        UILabel *labelPhone = [[UILabel alloc] init];
//        [labelPhone setBackgroundColor:[UIColor clearColor]];
//        [labelPhone setFont:font];
//        labelPhone.text = NSLocalizedString(@"Phone number :", nil);
//        CGSize newSize = [labelPhone sizeThatFits:CGSizeMake(labelWidth, 15)];
//       // labelPhone.frame = CGRectMake(0, 0, newSize.width, 15);
//
//       // labelPhone.frame = CGRectMake(0, 0, 200, 150);
//
//        //labelPhone.frame = CGRectMake(0, 0, 0, 0);
//        //  [containerView addSubview:labelPhone];
//
//        NSString *text = task.contact.phone;
//        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text];
//        NSRange phoneRange = NSMakeRange(0, task.contact.phone.length);
//        // [attributedText setAttributes:@{NSFontAttributeName:font, NSUnderlineStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle]} range:phoneRange];
//
//        CustomButton *phoneButton =  [CustomButton buttonWithType:UIButtonTypeCustom];
//        [phoneButton setTitle:text forState:UIControlStateNormal];
//
//
//
//        //*********************************
//        //TODO AFTER 12th Dec2018 build
//        //UNCOMMENT BELOW CODE
//        //*********************************
//        //Hide Phone Num
//        [phoneButton setTitle:NSLocalizedString(@"Call", nil) forState:UIControlStateNormal];
//
//
//
//
//        //*********************************
//        //TODO AFTER 12th Dec2018 build
//        //COMMENT BELOW CODE
//        //*********************************
//
//        [phoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [phoneButton setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
//        [phoneButton.titleLabel setFont:font];
//        CGSize newSize2 = [phoneButton sizeThatFits:CGSizeMake(labelWidth, 15)];
//        // phoneButton.frame = CGRectMake(newSize.width+5, 0, newSize2.width+10, 10+15);
//        phoneButton.frame = CGRectMake(2, 10, newSize2.width+15, 10+15);
//        // phoneButton.frame = CGRectMake(2, 10, newSize2.width+15, 100);
//
//
//
//        phoneButton.buttonData = task.contact.phone;
//
//        //Setting Current Order ID
//        self.currentOrderID = [task.orderId stringValue];
//
//        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel:+11111"]])
//        {
//            // [phoneButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//            [phoneButton.titleLabel setAttributedText:attributedText];
//
//
//            //*********************************
//            //TODO AFTER 12th Dec2018 build
//            //UNCOMMENT BELOW CODE
//            //*********************************
//
//            //Hide Phone Num
//            phoneButton.titleLabel.text = NSLocalizedString(@"Call", nil);
//            phoneButton.titleLabel.textColor = [UIColor whiteColor];
//            [phoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//
//
//
//            //*********************************
//            //TODO AFTER 12th Dec2018 build
//            //COMMENT BELOW CODE
//            //*********************************
//
//            // [phoneButton.titleLabel setAttributedText:attributedText];
//
//        }
//
//        // Right to Left languages 'arabic'
//        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0 && [UIView userInterfaceLayoutDirectionForSemanticContentAttribute:self.view.semanticContentAttribute] == UIUserInterfaceLayoutDirectionRightToLeft) {
//            labelPhone.frame = CGRectMake(labelWidth-newSize.width, 0, newSize.width, 15);
//            phoneButton.frame = CGRectMake(labelWidth-newSize.width-newSize.width-5, 0, newSize.width, 15);
//        }
//
//        //*********************************
//        //TODO AFTER 12th Dec2018 build
//        //COMMENT BELOW CODE
//        //*********************************
//
//        // [phoneButton addTarget:self action:@selector(phoneText:) forControlEvents:UIControlEventTouchUpInside];
//
//
//
//
//        //*********************************
//        //TODO AFTER 12th Dec2018 build
//        //unCOMMENT BELOW CODE
//        //*********************************
//
//
//          [phoneButton addTarget:self action:@selector(phoneText:) forControlEvents:UIControlEventTouchUpInside];
//
//
//
//        [containerView addSubview:phoneButton];
//        [view addSubview:containerView];
//        posY += 35;
//    }
//
    if (task.taskDescription && ![task.taskDescription isEqualToString:@""])
    {
        UILabel *labelDescription = [[UILabel alloc] init];
        [labelDescription setBackgroundColor:[UIColor clearColor]];
        [labelDescription setFont:font];
        labelDescription.text = [NSString stringWithFormat:NSLocalizedString(@"Description : %@", nil), task.taskDescription];
        labelDescription.numberOfLines = 0;
        CGSize newSize = [labelDescription sizeThatFits:CGSizeMake(labelWidth, 500)];
        labelDescription.frame = CGRectMake(posX, posY, labelWidth, newSize.height);
        [view addSubview:labelDescription];
        posY += newSize.height;
    }
    
    posY += 10;
    
    
    
    /*
     CGFloat buttonX = posX + labelWidth + 4;
     CGFloat buttonY = posY/2 - 17;
     
     if ([task isAllConfirmed])
     {
     UILabel *completedLabel = [[UILabel alloc] initWithFrame:CGRectMake(buttonX, buttonY, 70, 35)];
     [completedLabel setBackgroundColor:[UIColor clearColor]];
     [completedLabel setFont:font];
     completedLabel.text = NSLocalizedString(@"Completed", nil);
     [view addSubview:completedLabel];
     }
     else if ([currentTask isEqual:task])
     {
     if (posY < 75)
     {
     posY = 75;
     }
     
     buttonY = posY/2 - 33;
     RoundedButton *checklistButton = [[RoundedButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, 70, 35)];
     [checklistButton setTitle:NSLocalizedString(@"Checklist", nil) forState:UIControlStateNormal];
     UIFont *fontButton = [UIFont systemFontOfSize:13.0];
     [checklistButton.titleLabel setFont:fontButton];
     [checklistButton setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
     checklistButton.buttonData = task;
     [checklistButton addTarget:self action:@selector(openConfirmationListAction:) forControlEvents:UIControlEventTouchUpInside];
     [view addSubview:checklistButton];
     
     
     buttonY += 40;
     RoundedButton *returnButton = [[RoundedButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, 70, 35)];
     [returnButton setTitle:@"Return" forState:UIControlStateNormal];
     [returnButton.titleLabel setFont:fontButton];
     [returnButton setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
     returnButton.buttonData = task;
     [returnButton addTarget:self action:@selector(openDelayDialog:) forControlEvents:UIControlEventTouchUpInside];
     [view addSubview:returnButton];
     
     
     /*
     buttonY += 40;
     RoundedButton *wazeButton = [[RoundedButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, 70, 35)];
     [wazeButton setTitle:@"Waze" forState:UIControlStateNormal];
     [wazeButton.titleLabel setFont:fontButton];
     [wazeButton setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
     wazeButton.buttonData = task;
     [wazeButton addTarget:self action:@selector(openWaze:) forControlEvents:UIControlEventTouchUpInside];
     [view addSubview:wazeButton];
     */
    /*
     buttonY += 40;
     RoundedButton *returnButton = [[RoundedButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, 70, 35)];
     [returnButton setTitle:@"Return" forState:UIControlStateNormal];
     [returnButton.titleLabel setFont:fontButton];
     [returnButton setBackgroundColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
     returnButton.buttonData = task;
     [returnButton addTarget:self action:@selector(OpenOrderStatusBottomSheet:) forControlEvents:UIControlEventTouchUpInside];
     [view addSubview:returnButton];
     
     
     }
     
     */
    
    
    
    CGRect viewFrame = view.frame;
    viewFrame.size.height = posY;
    view.frame = viewFrame;
}

-(IBAction)phoneText:(id)sender
{
    CustomButton *button = (CustomButton *)sender;
    NSString *phoneString = button.buttonData;
    
    
    if ([[self checkPhoneNumberValidity:phoneString] length]>0) {
        [self placeCall:[self checkPhoneNumberValidity:phoneString] ];

    }
    else{
        [self.view makeToast:NSLocalizedString(@"Invalid Phone number", nil)];
        
    }
}

-(void)placeCall:(NSString *)phone
{
//    NSString *digitsOnly = @"0123456789";
//    NSString *phoneString = [phone filterString:digitsOnly];
//    NSRange range = [phoneString rangeOfString:@"^0*" options:NSRegularExpressionSearch];
//    phoneString = [phoneString stringByReplacingCharactersInRange:range withString:@""];
//
//    NSString *phoneCall = [NSString stringWithFormat:@"tel://%@", phoneString];
//
    
    
    //*********************************
    //TODO AFTER 12th Dec2018 build
    //COMMENT BELOW CODE
    //*********************************
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCall]];
    
    
    
    
    //*********************************
    //TODO AFTER 12th Dec2018 build
    //UN-COMMENT BELOW CODE
    //*********************************
    
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
//    CallNow *controller = [storyboard instantiateViewControllerWithIdentifier:@"CallNow"];
    CallNow *controller = [CallNow shared];
    controller.Joey_ID = [[UrlInfo getJoeyId] stringValue] ;
    controller.Order_ID = self.currentOrderID;
    controller.PhoneNum = phone;
    
    //only below line was commented to check JCdialpad
   // [self presentViewController:controller animated:YES completion:nil];
    
    if (@available(iOS 13.0, *)) {
              [controller setModalPresentationStyle: UIModalPresentationFullScreen];
          } else {
              // Fallback on earlier versions
          }
    
    
    AVAudioSessionRecordPermission permissionStatus = [[AVAudioSession sharedInstance] recordPermission];

    switch (permissionStatus) {
         case AVAudioSessionRecordPermissionUndetermined:{
              [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
              // CALL YOUR METHOD HERE - as this assumes being called only once from user interacting with permission alert!
                  if (granted) {
                         dispatch_async( dispatch_get_main_queue(), ^{

                   // Microphone enabled code
                  [self.navigationController presentViewController:controller animated:YES completion:nil];
                    });
                      
                  }
                  else {
                      // Microphone disabled code
                      [self callPermissionNotGiven];

                      
                  }
               }];
              break;
              }
         case AVAudioSessionRecordPermissionDenied:
              // direct to settings...
            [self callPermissionNotGiven];
              break;
         case AVAudioSessionRecordPermissionGranted:
              // mic access ok...
        {
            [self.navigationController presentViewController:controller animated:YES completion:nil];

        }
              break;
         default:
              // this should not happen.. maybe throw an exception.
              break;
    }
    
}


-(void)callPermissionNotGiven{
    
    [alertHelper showAlertWithButtons:@"Permission Required" message:@"JoeyCo would like to access the microphone" from:self withOkHandler:^{
         
               [[UIApplication sharedApplication] openURL:
               [NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
//    [alertHelper show:@"Permission Required" message:@"JoeyCo would like to access the microphone" from:self buttonTitle:@"Ok" withHandler:^{
//
//       // UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
//
//
//        [[UIApplication sharedApplication] openURL:
//        [NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//    }];
//
}

//
//+ (void)checkMicrophonePermissions:(void (^)(BOOL allowed))completion {
//    AVAudioSessionRecordPermission status = [[AVAudioSession sharedInstance] recordPermission];
//    switch (status) {
//        case AVAudioSessionRecordPermissionGranted:
//            if (completion) {
//                completion(YES);
//            }
//            break;
//        case AVAudioSessionRecordPermissionDenied:
//        {
//            // Optionally show alert with option to go to Settings
//
//            if (completion) {
//                completion(NO);
//            }
//        }
//            break;
//        case AVAudioSessionRecordPermissionUndetermined:
//            [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
//                if (granted && completion) {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        completion(granted);
//                    });
//                }
//            }];
//            break;
//    }
//
//}


-(void)openConfirmationListAction:(id)sender
{
    //Checking location permission.
    //If location is not ON then you cannot proceed with checklist confirmation
    if(  [MapHelper verifyLocationAuthorizationWithBooleanStatus])
    {
    
    
    
    RoundedButton *button = (RoundedButton *)sender;
    Task *task = (Task *)button.buttonData;
    
    int statusId = [task.statusId intValue];
    
    NSLog(@"status_ID :%d",statusId);
    
    [[LocationManager shared] startUpdatingLocation];
    
    //Getting last known location
     lastJoeyLocation = [[LocationManager shared] lastKnownLocation];
     
    //Location of Pickup/DropOff
    CLLocation *locationFromServer =[[CLLocation alloc] initWithLatitude:[task.location.locationLatitude doubleValue] longitude:[task.location.locationLongitude doubleValue]] ;


        int MinDistance = 0;
        if ([[[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys] containsObject:@"LOCATION_RADIUS"]) {
            MinDistance = [[[NSUserDefaults standardUserDefaults] objectForKey:@"LOCATION_RADIUS"] intValue]; // meters
        }
        else {
            MinDistance = 100; // meters
        }
    CLLocationDistance distance = [lastJoeyLocation distanceFromLocation:locationFromServer];
    
    
    
    if( distance< MinDistance) {
        // I'm close enough to the venue!
        

          
        BOOL checkIfStatusWasUpdated =   [self comparingIDOfDropOffStatus:statusId confirmDropOffArray:task.order.dropoff_status];
          
          
          if ( checkIfStatusWasUpdated == false && [task.type isEqualToString:@TASK_DROP_OFF])
          {
              
              //*********************************
              //TODO AFTER 12th Dec2018 build
              //UNCOMMENT BELOW CODE
              //*********************************
              
              //Open Order Status : Bottom Sheet
              [self openConfirmDropOffDialog:sender];
              
              
              //task opened before confirmation
              self.TaskOpenedBeforeConfirmation = task;
          }
          else
          {
              [self layoutConfirmationList:task];

          }
    }
    else
    {
        // basic usage
        [self.view makeToast:@"You are too far from the required location"];
    }
    
}
    
    
    
    
}

-(BOOL)comparingIDOfDropOffStatus:(int) StatusId confirmDropOffArray:(NSMutableArray *)arr{
    
    
    BOOL matchFound = false;

    for (id i in arr) {
        
        
        NSDictionary *data = [[NSDictionary alloc]initWithDictionary:i];
        NSLog(@"status of order : %@",[data objectForKey:@"id"]);
        
        
        int id_value =[[NSString stringWithFormat:@"%@",[data objectForKey:@"id"]] intValue];
      

        NSLog(@"id_value *****: %d",id_value);

        
        if (id_value == StatusId) {
            
            NSLog(@"MATCH FOUND *****");
            matchFound = true;
            return matchFound;
        }
        
    }
    return matchFound;
    
}



-(void)openBottomSheetOption:(id)sender
{
    
    
    RoundedButton *button = (RoundedButton *)sender;
    Task *task = (Task *)button.buttonData;
    
    
      // View controller the bottom sheet will hold
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
        
        OpenLocationBottomSheet * vc = [storyboard instantiateViewControllerWithIdentifier:@"OpenLocationBottomSheet"];
    
            vc.is_itinerary= NO;
            vc.latitude= [task.location.locationLatitude doubleValue];
            vc.longitude= [task.location.locationLongitude doubleValue];
        

        // Initialize the bottom sheet with the view controller just created
        self.bottomSheetDropOff = [[MDCBottomSheetController alloc] initWithContentViewController:vc];
      
         [self presentViewController:self.bottomSheetDropOff animated:true completion:nil];
    
}




/**
 Open Confirm Drop off Dialog
 */
- (void)openConfirmDropOffDialog:(id)sender {
    
    RoundedButton *button = (RoundedButton *)sender;
    Task *task = (Task *)button.buttonData;
    
    //Selected Order
    Order * selectedOrder = [self.orders objectAtIndex:self.orderSelected.row];
    
    
    // View controller the bottom sheet will hold
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
    
    ConfirmDropOffStatusSheet * viewControllerDropOffSheet = [storyboard instantiateViewControllerWithIdentifier:@"ConfirmDropOffStatusSheet"];
    
    viewControllerDropOffSheet.delayStatusObj = [[NSMutableArray alloc]init];
    viewControllerDropOffSheet.delayStatusObj =[selectedOrder arr_dropoff_status:self ];
    viewControllerDropOffSheet.order = selectedOrder;
    viewControllerDropOffSheet.order_task = task;
    
    
    // Initialize the bottom sheet with the view controller just created
    self.bottomSheetDropOff = [[MDCBottomSheetController alloc] initWithContentViewController:viewControllerDropOffSheet];
    // Present the bottom sheet
    //  [self.navigationController pushViewController:bottomSheet animated:YES];
    //[self presentViewController:self.bottomSheetDropOff animated:true completion:nil];
    
//    if (@available(iOS 13.0, *)) {
//              [self.bottomSheetDropOff setModalPresentationStyle: UIModalPresentationFullScreen];
//          } else {
//              // Fallback on earlier versions
//
//          }
     [self presentViewController:self.bottomSheetDropOff animated:true completion:nil];
}

/**
 Open Delay Dialog
 */
- (void)openDelayDialog:(id)sender {
    
    RoundedButton *button = (RoundedButton *)sender;
    Task *task = (Task *)button.buttonData;
    
    
    @try {
    
        //Selected Order
        Order * selectedOrder = [self.orders objectAtIndex:self.orderSelected.row];
        
        
        // View controller the bottom sheet will hold
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
        
        DelayReportViewController *viewController_delay = [storyboard instantiateViewControllerWithIdentifier:@"DelayReportViewController"];
        viewController_delay.delayStatusObj = [[NSMutableArray alloc]init];
        viewController_delay.delayStatusObj =[selectedOrder arr_delay_status:self ];
        viewController_delay.order = selectedOrder;
        viewController_delay.task = task;
        
        
        
        // Initialize the bottom sheet with the view controller just created
        MDCBottomSheetController *bottomSheet = [[MDCBottomSheetController alloc] initWithContentViewController:viewController_delay];
        
        
        // Present the bottom sheet
        //  [self.navigationController pushViewController:bottomSheet animated:YES];
      //  [self presentViewController:bottomSheet animated:true completion:nil];
        
//        if (@available(iOS 13.0, *)) {
//                  [bottomSheet setModalPresentationStyle: UIModalPresentationFullScreen];
//              } else {
//                  // Fallback on earlier versions
//              }
        [self.navigationController presentViewController:bottomSheet animated:YES completion:nil];
        
    } @catch (NSException *exception) {
        
        // basic usage
        [self.view makeToast:@"Not Available"];
        
    }
    
    
}


/**
 Open RETURN Dialog
 */
- (void)openReturnDialog:(id)sender {
    
    RoundedButton *button = (RoundedButton *)sender;
    Task *task = (Task *)button.buttonData;
    
    //Selected Order
    Order * selectedOrder = [self.orders objectAtIndex:self.orderSelected.row];
    
    // View controller the bottom sheet will hold
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
    
    ReturnOrderVC * CancelVC = [storyboard instantiateViewControllerWithIdentifier:@"CancelOrderVCViewController"];
    CancelVC.cancelStatusObj = [[NSMutableArray alloc]init];
    CancelVC.cancelStatusObj =[selectedOrder arr_cancellation_status:self ];
    CancelVC.order = selectedOrder;
    CancelVC.task = task;

    
    // Initialize the bottom sheet with the view controller just created
    MDCBottomSheetController *bottomSheet = [[MDCBottomSheetController alloc] initWithContentViewController:CancelVC];
    // Present the bottom sheet
    //  [self.navigationController pushViewController:bottomSheet animated:YES];
   // [self presentViewController:bottomSheet animated:true completion:nil];
//    if (@available(iOS 13.0, *)) {
//              [bottomSheet setModalPresentationStyle: UIModalPresentationFullScreen];
//          } else {
//              // Fallback on earlier versions
//          }

     [self.navigationController presentViewController:bottomSheet animated:YES completion:nil];
}


-(void)OpenOrderStatusBottomSheet :(id)sender {
    
    RoundedButton *button = (RoundedButton *)sender;
    Task *task = (Task *)button.buttonData;
    
    // View controller the bottom sheet will hold
    OrderStatusUpdates *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderStatusUpdates"];
    viewController.order = [self.orders objectAtIndex:self.orderSelected.row];
    viewController.task = task;
    
    
    // Initialize the bottom sheet with the view controller just created
    MDCBottomSheetController *bottomSheet = [[MDCBottomSheetController alloc] initWithContentViewController:viewController];
    // Present the bottom sheet
  //  [self presentViewController:bottomSheet animated:true completion:nil];
//    if (@available(iOS 13.0, *)) {
//              [bottomSheet setModalPresentationStyle: UIModalPresentationFullScreen];
//          } else {
//              // Fallback on earlier versions
//          }

     [self.navigationController presentViewController:bottomSheet animated:YES completion:nil];
}



#pragma mark- Horizontal animation

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
    if (!self.orderSelected || self.orderSelected.row != page)
    {
        [self selectOrder:page];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            (paneStatus == PaneStatusOpened) ? [self openPaneAnimation] : [self closePaneAnimation];
        });
    }
}

-(void)selectOrder:(NSInteger)index
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (index < 0 || index >= self.orders.count)
        {
            return;
        }
        
        self.orderSelected = [NSIndexPath indexPathForRow:index inSection:0];
        Order *order = [self.orders objectAtIndex:self.orderSelected.row];
        [self updateCurrentTaskArray:self.orderSelected.row task:[order getNextTask]];
        
        currentConfirmation = nil;
        [self loadMapMarkers];
        
        [self createTabs];
    });
    
    [readInTimer invalidate];
    readInTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateReadyInTime) userInfo:nil repeats:YES];
}

#pragma mark - Vertical animation

- (void)orderTap:(UIPanGestureRecognizer *)recognizer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.orderSelected.row >= self.cellHeight.count)
        {
            return;
        }
        
        CGRect collectionFrame = self.ordersCollection.frame;
        NSNumber *numberHeight = [self.cellHeight objectAtIndex:self.orderSelected.row];
        CGFloat totalHeight = [numberHeight floatValue];
        if (totalHeight > self.ordersCollectionHeight.constant)
        {
            totalHeight = self.ordersCollectionHeight.constant;
        }
        
        if (collectionFrame.origin.y > self.view.frame.size.height - totalHeight)
        {
            [self openPaneAnimation];
        }
        else
        {
            [self closePaneAnimation];
        }
        
        panInitialPositionX = -1;
        panInitialPositionY = -1;
    });
}

-(void)openPaneAnimation
{
    if (self.orderSelected.row >= self.cellHeight.count)
    {
        return;
    }
    
    NSNumber *numberHeight = [self.cellHeight objectAtIndex:self.orderSelected.row];
    CGFloat totalHeight = [numberHeight floatValue];
    if (totalHeight <= 0)
    {
        return;
    }
    paneStatus = PaneStatusAnimating;
    if (totalHeight > self.ordersCollectionHeight.constant)
    {
        totalHeight = self.ordersCollectionHeight.constant;
    }
    
    int minY = self.view.frame.size.height - totalHeight;
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.ordersCollectionY.constant = minY;
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         paneStatus = PaneStatusOpened;
                     }];
}

-(void)closePaneAnimation
{
    paneStatus = PaneStatusAnimating;
    
    int minY = self.view.frame.size.height - CLOSED_TASK_HEIGHT;
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.ordersCollectionY.constant = minY;
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         paneStatus = PaneStatusClosed;
                     }];
}

#pragma mark - Confirmation Table

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.confirmations count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row >= self.confirmationRowHeight.count)
    {
        return 0;
    }
    NSNumber *height = [self.confirmationRowHeight objectAtIndex:indexPath.row];
    return [height floatValue];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"ConfirmationCell";
    ConfirmationTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (!cell) cell = [[ConfirmationTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    __weak ConfirmationTableCell *weakCell = cell;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (indexPath.row >= self.confirmations.count || indexPath.row >= self.confirmationRowHeight.count)
        {
            return;
        }
        
        Confirmation *confirmation = [self.confirmations objectAtIndex:indexPath.row];
        weakCell.confirmation = confirmation;
        
        
        /**
         *
         * Author : Fz
         * Date : 28th aug 2019
         *
         * Why we have done this?
         *
         * When Drop off status sheet were created .
         *
         * An image upload popup becomes visible again
         *
         * THAT CREATES A SERIOUS confusion to customers .
         *
         * To eliminate that we are writing an extra text with "Image upload" text
         * */
        if ( [confirmation.task.type isEqualToString:@TASK_DROP_OFF ] && [confirmation.title isEqualToString:@"Upload Image" ]) {
            
            
           // confirmationTitle.setText(confirmation.getTitle() + "(Required by merchant)" );
            weakCell.titleTxt.text = [NSString stringWithFormat:@"%@(Required by merchant)",confirmation.title];
            
        }
        else
        {
           // confirmationTitle.setText(confirmation.getTitle());
            weakCell.titleTxt.text = [NSString stringWithFormat:@"%@",confirmation.title];

        }
        
        //weakCell.titleTxt.text = confirmation.title;
    
        NSString *ConfirmationDesc = confirmation.confirmationDescription;
        
        weakCell.descriptionTxt.text = ConfirmationDesc;
        
        
        
        // weakCell.descriptionTxt.text = @"JOEYCO_HTMLTEXT";
        
        if ([confirmation.name isEqualToString:@CONFIRMATION_CART_ITEMS] || [confirmation.name isEqualToString:@CONFIRMATION_CONFIRMATION_LIST_ITEMS] )
        {
            if (confirmation.confirmationDescription)
            {
                NSArray *testArray = [[NSArray alloc] init];
                
                testArray = [confirmation.confirmationDescription componentsSeparatedByString:@"<li>"];
                
                NSMutableString *cartItems = [NSMutableString new];
                int indent = -1;
                for (NSString *sub in testArray)
                {
                    if ([sub isEqualToString:@""]) {
                        continue;
                    } else if ([sub isEqualToString:@"<ul>"]) {
                        indent++;
                        continue;
                    }
                    
                    for (int i = 0; i < indent; i++) {
                        [cartItems appendString:@"    "];
                    }
                    
                    NSString *clearSub = [sub stringByReplacingOccurrencesOfString:@"<ul>" withString:@""];
                    clearSub = [clearSub stringByReplacingOccurrencesOfString:@"</ul>" withString:@""];
                    clearSub = [clearSub stringByReplacingOccurrencesOfString:@"</li>" withString:@""];
                    clearSub = [clearSub stringByReplacingOccurrencesOfString:@"</ul>" withString:@""];
                    
                    [cartItems appendString:[NSString stringWithFormat:@"\u2022 %@\n", clearSub]];
                    
                    int ulOccurrences = [sub occurrencesOfString:@"<ul>"];
                    int ulOccurrences2 = [sub occurrencesOfString:@"</ul>"];
                    
                    if (ulOccurrences > 0)
                    {
                        indent += ulOccurrences;
                    } else if (ulOccurrences2 > 0)
                    {
                        indent -= ulOccurrences2;
                    }
                }
                
                weakCell.descriptionTxt.text = cartItems;
                
                CGSize newSize = [weakCell.descriptionTxt sizeThatFits:CGSizeMake(weakCell.descriptionTxt.frame.size.width,500)];
                CGFloat labelHeight = newSize.height + 22;
                CGFloat cellHeight = [[self.confirmationRowHeight objectAtIndex:indexPath.row] floatValue];
                
                if (labelHeight > cellHeight)
                {
                    NSNumber *labelNumber = [NSNumber numberWithFloat:ceil(labelHeight)];
                    [self.confirmationRowHeight replaceObjectAtIndex:indexPath.row withObject:labelNumber];
                    [tableView reloadData];
                    self.confirmationTableHeight.constant = self.confirmationTable.contentSize.height;
                }
            }
        }
        else if ([confirmation.name isEqualToString:@CONFIRMATION_AMOUNT])
        {
            if ([confirmation.task.paymentType isEqualToString:@PAYMENT_COLLECT])
            {
                weakCell.descriptionTxt.text = [NSString stringWithFormat:NSLocalizedString(@"Collect Cash $%.2f", nil), [confirmation.task.paymentAmount floatValue]];
            }
            else if ([confirmation.task.paymentType isEqualToString:@PAYMENT_MAKE])
            {
                weakCell.descriptionTxt.text = [NSString stringWithFormat:NSLocalizedString(@"Make payment $%.2f", nil), [confirmation.task.paymentAmount floatValue]];
            }
        }
        
        if ([confirmation.inputType isEqualToString:@CONFIRMATION_INPUT_TYPE_TEXT] && ![confirmation.isConfirmed boolValue])
        {
            weakCell.editField.hidden = NO;
            weakCell.editField.keyboardType = UIKeyboardTypeNumberPad;
            weakCell.editField.delegate = self;
            weakCell.editField.inputAccessoryView = toolbar;
            [weakCell.editField.undoManager disableUndoRegistration];
            NSMutableArray *fields = [NSMutableArray arrayWithArray:toolbar.textFields];
            [fields addObject:weakCell.editField];
            toolbar.textFields = fields;
            
            if ([confirmation.name isEqualToString:@CONFIRMATION_AMOUNT])
            {
                weakCell.editField.fieldType = @FIELD_MONEY;
            }
            else if ([confirmation.name isEqualToString:@CONFIRMATION_PIN])
            {
                weakCell.editField.fieldType = @FIELD_PIN;
            }
            else
            {
                weakCell.editField.fieldType = @FIELD_TEXT;
            }
        }
        else
        {
            weakCell.editField.hidden = YES;
        }
        
        UIColor *greenColor = [UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0];
        UIColor *grayColor = [UIColor colorWithRed:0.70 green:0.70 blue:0.70 alpha:1.0];
        
        if ([confirmation.isConfirmed boolValue])
        {
            weakCell.confirmationButton.backgroundColor = greenColor;
            [weakCell.confirmationButton setTitle:NSLocalizedString(@"Ok", nil) forState:UIControlStateNormal];
        }
        else
        {
            weakCell.confirmationButton.backgroundColor = grayColor;
            [weakCell.confirmationButton setTitle:@"-" forState:UIControlStateNormal];
            
            weakCell.confirmationButton.buttonData = weakCell;
            [weakCell.confirmationButton addTarget:self action:@selector(confirmationAction:) forControlEvents:UIControlEventTouchUpInside];
        }
    });
    
    return weakCell;
}

-(void)confirmationAction:(id)sender
{
    RoundedButton *button = (RoundedButton *)sender;
    ConfirmationTableCell *cell = (ConfirmationTableCell *)button.buttonData;
    
    if ([cell.confirmation.isConfirmed boolValue])
    {
        return;
    }
    
    if ([cell.confirmation.inputType isEqualToString:@CONFIRMATION_INPUT_TYPE_IMAGE])
    {
        if ([cell.confirmation.name isEqualToString:@CONFIRMATION_SIGNATURE])
        {
            [self confirmationSignature:cell.confirmation];
        }
        else if ([cell.confirmation.name isEqualToString:@CONFIRMATION_IMAGE])
        {
            [self confirmationImage:cell.confirmation];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *digitsOnly = @"0123456789.";
            NSString *inputText = [cell.editField.text filterString:digitsOnly];
            cell.editField.text = @"";
            [self hideKeyboard];
            
            [self confirmationAction:cell.confirmation.task confirmation:cell.confirmation inputText:inputText];
        });
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.confirmationView.hidden = YES;
        self.bgConfirmationView.hidden = YES;
    });
    
}

-(void)confirmationAction:(Task *)task confirmation:(Confirmation *)confirmation inputText:(NSString *)inputText
{
    if (currentConfirmation)
    {
        return;
    }
    
    if ([confirmation.inputType isEqualToString:@CONFIRMATION_INPUT_TYPE_TEXT])
    {
        if ([confirmation.name isEqualToString:@CONFIRMATION_PIN])
        {
            NSString *correctPin = task.pin;
            if (![correctPin isEqualToString:inputText])
            {
                [alertHelper showAlertNoButtons:NSLocalizedString(@"Error", nil) type:AlertTypeNone message:NSLocalizedString(@"Invalid PIN", nil) dismissAfter:3.0 from:self];
                return;
            }
        }
        else if ([confirmation.name isEqualToString:@CONFIRMATION_AMOUNT])
        {
            CGFloat paymentInputed = [inputText floatValue];
            if (paymentInputed <= 0)
            {
                [alertHelper showAlertNoButtons:NSLocalizedString(@"Error", nil) type:AlertTypeNone message:NSLocalizedString(@"Please enter amount of payment", nil) dismissAfter:3.0 from:self];
                return;
            }
        }
    }
    
    BOOL hasInternet = [json hasInternetConnection:NO];
    if (hasInternet)
    {
        MetaStatus *status = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_PICK_UP_SUCCESS];
        if (!status || !status.value || ![UrlInfo getJoeyId]) {
            return;
        } else if (!confirmation || !confirmation.name) {
            [alertHelper showAlertWithOneButton:NSLocalizedString(@"Confirmation Error", nil) message:NSLocalizedString(@"Problem with confirmation parameters", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
            return;
        }
        
        currentConfirmation = confirmation;
        
        NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":[UrlInfo getJoeyId], @"name":confirmation.name, @"confirmed":[NSNumber numberWithBool:YES], @"status":status.value}];
        if ([confirmation.inputType isEqualToString:@CONFIRMATION_INPUT_TYPE_TEXT])
        {
            [jsonDictionary setObject:inputText forKey:confirmation.name];
        }
        
        
        //MM - SUPERMARKET WORK
        //Set By Faiz
        self.currentOrderID = [confirmation.orderId stringValue];
        
        [json confirmation:confirmation.url method:confirmation.method confirmationInfo:jsonDictionary action:@ACTION_CONFIRMATION];
    }
    else
    {
        [[DatabaseHelper shared] updateOfflineConfirmation:confirmation.confirmationId inputType:inputText];
        confirmation.isConfirmed = [NSNumber numberWithBool:YES];
        
        [self.confirmationTable reloadData];
        [self checkAllConfirmation];
    }
}

-(void)confirmationImage:(Confirmation *)confirmation
{
    CameraViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CameraViewController"];
    controller.pictureSelectedCompletion = ^(UIImage *image) {
        if (image)
        {
            BOOL hasInternet = [json hasInternetConnection:NO];
            if (hasInternet)
            {
                if (!confirmation || !confirmation.name) {
                    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Confirmation Error", nil) message:NSLocalizedString(@"Problem with confirmation parameters", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
                    return;
                }
                
                currentConfirmation = confirmation;
                NSString *encodedImage = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":[UrlInfo getJoeyId], confirmation.name:encodedImage}];
                [json confirmation:confirmation.url method:confirmation.method confirmationInfo:jsonDictionary action:@ACTION_CONFIRMATION];
            }
            else
            {
                [[DatabaseHelper shared] updateConfirmation:confirmation.confirmationId image:image];
                confirmation.isConfirmed = [NSNumber numberWithBool:YES];
                
                [self.confirmationTable reloadData];
                [self checkAllConfirmation];
            }
        }
        else
        {
            [alertHelper showAlertWithOneButton:NSLocalizedString(@"Picture not sent", nil) message:NSLocalizedString(@"Picture confirmation not sent. Please try again.", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        }
    };
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:controller animated:YES];
    });
}

-(void)confirmationSignature:(Confirmation *)confirmation
{
    SignatureViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SignatureViewController"];
    controller.signatureCompletionBlock = ^(UIImage *image) {
        if (image)
        {
            BOOL hasInternet = [json hasInternetConnection:NO];
            if (hasInternet)
            {
                if (!confirmation || !confirmation.name) {
                    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Confirmation Error", nil) message:NSLocalizedString(@"Problem with confirmation parameters", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
                    return;
                }
                
                currentConfirmation = confirmation;
                NSString *encodedImage = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":[UrlInfo getJoeyId], confirmation.name:encodedImage}];
                [json confirmation:confirmation.url method:confirmation.method confirmationInfo:jsonDictionary action:@ACTION_CONFIRMATION];
            }
            else
            {
                [[DatabaseHelper shared] updateConfirmation:confirmation.confirmationId image:image];
                confirmation.isConfirmed = [NSNumber numberWithBool:YES];
                
                [self.confirmationTable reloadData];
                [self checkAllConfirmation];
            }
        }
        else
        {
            [alertHelper showAlertWithOneButton:NSLocalizedString(@"Picture not sent", nil) message:NSLocalizedString(@"Picture confirmation not sent. Please try again.", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        }
    };
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:controller animated:YES];
    });
}

-(void)checkAllConfirmation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.orderSelected.row >= self.currentTaskArray.count)
        {
            [self closeConfirmations:nil];
            return;
        }
        
        Task *currentTask = [self.currentTaskArray objectAtIndex:self.orderSelected.row];
        
        
        currentTask.confirmations =  [[DatabaseHelper shared]getConfirmationsFromTaskId:currentTask.taskId];


        
        if ([currentTask isAllConfirmed])
        {
            [self closeConfirmations:nil];
            [self updateNextTask];
        }
        else
        {
            [self.confirmationTable reloadData];
//
//
//            // update orders when returning from order details
//            BOOL hasInternet = [self->json hasInternetConnection:YES];
//            if (hasInternet && !self->currentConfirmation)
//            {
//                [self.delegate getOrderList];
//
//              //  [self closeConfirmations:nil];
//
//            }
            

        }
    });
}

-(void)updateNextTask
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.orderSelected.row >= self.orders.count || self.orderSelected.row >= self.polylines.count)
        {
            return;
        }
        
        Order *order = [self.orders objectAtIndex:self.orderSelected.row];
        Task *currentTask = [order getNextTask];
        
        if (currentTask)
        {
            [self updateCurrentTaskArray:self.orderSelected.row task:currentTask];
            [self.ordersCollection reloadItemsAtIndexPaths:[self.ordersCollection indexPathsForVisibleItems]];
            
            [self.polylines replaceObjectAtIndex:self.orderSelected.row withObject:[NSNull null]];
            
            [self loadMapMarkers];
        }
        else // Order completed
        {
            BOOL hasInternet = [json hasInternetConnection:NO];
            if (hasInternet)
            {
                __unsafe_unretained typeof(self) weakSelf = self;
                [alertHelper showAlertWithOneButton:NSLocalizedString(@"Success", nil) message:NSLocalizedString(@"Order completed", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:^{
                    CLS_LOG(@"ActiveOrdersViewController updateNextTask Order completed");
                    if (weakSelf && [weakSelf isKindOfClass:[ActiveOrdersViewController class]])
                    {
                        [weakSelf.delegate performSelector:@selector(getOrderList) withObject:nil];
                    }
                }];
            }
            else
            {
                NSInteger count = self.orders.count - 1;
                __unsafe_unretained typeof(self) weakSelf = self;
                [alertHelper showAlertWithOneButton:NSLocalizedString(@"Success", nil) message:NSLocalizedString(@"Order completed", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:^{
                    if (weakSelf && [weakSelf isKindOfClass:[ActiveOrdersViewController class]])
                    {
                        if (count > 0)
                        {
                            [weakSelf performSelector:@selector(nextOfflineOrder) withObject:nil afterDelay:.2];
                        }
                        else
                        {
                            [self reloadData:nil];
                        }
                    }
                }];
            }
        }
    });
}

-(void)nextOfflineOrder
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.orderSelected.row >= self.orders.count)
        {
            return;
        }
        
        [self.orders removeObjectAtIndex:self.orderSelected.row];
        [self.ordersCollection deleteItemsAtIndexPaths:@[self.orderSelected]];
        
        NSInteger nextOrder = self.orderSelected.row;
        if (self.orderSelected.row >= self.orders.count)
        {
            nextOrder = self.orders.count-1;
        }
        
        [self selectOrder:nextOrder];
        [self openPaneAnimation];
        
        [self.ordersCollection reloadItemsAtIndexPaths:[self.ordersCollection indexPathsForVisibleItems]];
    });
}

#pragma mark - UITextField

-(BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
    return YES;
}

-(void)textFieldDidChange:(NSNotification *)notification
{
    if (![notification.object isKindOfClass:[CustomTextField class]])
    {
        return;
    }
    
    NSString *digitsOnly = @"0123456789";
    
    CustomTextField *customField = (CustomTextField *)notification.object;
    if ([customField.fieldType isEqualToString:@FIELD_MONEY])
    {
        NSString *paymentString = [customField.text filterString:digitsOnly];
        NSRange range = [paymentString rangeOfString:@"^0*" options:NSRegularExpressionSearch];
        paymentString = [paymentString stringByReplacingCharactersInRange:range withString:@""];
        
        if (paymentString.length == 0) paymentString = @"$0.00";
        else if (paymentString.length == 1) paymentString = [NSString stringWithFormat:@"$0.0%@", paymentString];
        else if (paymentString.length == 2) paymentString = [NSString stringWithFormat:@"$0.%@", paymentString];
        else
        {
            NSUInteger pointLocation = paymentString.length - 2;
            paymentString = [NSString stringWithFormat:@"$%@.%@", [paymentString substringToIndex:pointLocation], [paymentString substringFromIndex:pointLocation]];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            customField.text = paymentString;
        });
    }
    else if ([customField.fieldType isEqualToString:@FIELD_PIN])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            customField.text = [customField.text filterString:digitsOnly];
        });
    }
}


#pragma mark - Location

-(void)updateLastLocation:(CLLocation *)newLocation
{
    lastJoeyLocation = newLocation;
    [self showJoeyMarker];
    [self getGoogleMapRouteInfo];
}

-(void)showJoeyMarker
{
    
    if (!lastJoeyLocation || [lastJoeyLocation isEqual:[NSNull null]])
    {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (joeyMarker)
        {
            joeyMarker.map = nil;
            joeyMarker = nil;
        }
        
        Joey *joey = [[DatabaseHelper shared] getJoey];
        
        joeyMarker = [[GMSMarker alloc] init];
        joeyMarker.title = joey.firstName;
        joeyMarker.position = CLLocationCoordinate2DMake(lastJoeyLocation.coordinate.latitude, lastJoeyLocation.coordinate.longitude);
        joeyMarker.map = self.mapView;
        joeyMarker.icon = [UIImage imageNamed:@"joey_run.png"];
    });
}

-(void)getGoogleMapRouteInfo
{
    if (self.orderSelected.row < 0 || self.orderSelected.row >= self.orders.count)
    {
        return;
    }
    
    Order *order = [self.orders objectAtIndex:self.orderSelected.row];
    [self getGoogleMapRouteInfo:order];
}

-(void)getGoogleMapRouteInfo:(Order *)order
{
    if (!lastJoeyLocation) {
        return;
    }
    NSMutableArray *locations = [order tasksLocationList];
    [locations insertObject:lastJoeyLocation atIndex:0];
    
    [MapHelper getGoogleMapRouteInfo:locations travelMode:mapRouteSetting succeeded:^(GoogleMapRouteInfo *info) {
        dispatch_async(dispatch_get_main_queue(), ^{
            order.totalDistance = info.totalDistance;
            order.totalDuration = info.totalDuration;
            
            if (routePolyline)
            {
                routePolyline.map = nil;
                routePolyline = nil;
            }
            routePolyline = info.polyline;
            routePolyline.strokeWidth = 3;
            routePolyline.strokeColor = [UIColor colorWithRed:0 green:137/255 blue:255/255 alpha:1.0];
            routePolyline.map = self.mapView;
            
            if (self.orderSelected.row < self.polylines.count)
            {
                [self.polylines replaceObjectAtIndex:self.orderSelected.row withObject:routePolyline];
            }
            
            NSArray<NSIndexPath *> *visibleItems = [self.ordersCollection indexPathsForVisibleItems];
            BOOL verified = YES;
            for (NSIndexPath *indexPath in visibleItems)
            {
                if (indexPath.row >= self.orders.count) {
                    verified = NO;
                    break;
                }
            }
            
            if (verified)
            {
                [self.ordersCollection reloadItemsAtIndexPaths:visibleItems];
            }
        });
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@", errorMsg);
    }];
}

-(void)loadMapMarkers
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.orderSelected.row >= self.polylines.count || self.orderSelected.row >= self.orders.count)
        {
            return;
        }
        
        [self.mapView clear];
        
        NSMutableArray *markers = [NSMutableArray new];
        
        if (lastJoeyLocation)
        {
            if (joeyMarker)
            {
                joeyMarker.map = nil;
                joeyMarker = nil;
            }
            
            Joey *joey = [[DatabaseHelper shared] getJoey];
            
            joeyMarker = [[GMSMarker alloc] init];
            joeyMarker.title = joey.firstName;
            joeyMarker.position = CLLocationCoordinate2DMake(lastJoeyLocation.coordinate.latitude, lastJoeyLocation.coordinate.longitude);
            joeyMarker.map = self.mapView;
            joeyMarker.icon = [UIImage imageNamed:@"joey_run.png"];
            
            [markers addObject:joeyMarker];
            
            id obj = [self.polylines objectAtIndex:self.orderSelected.row];
            if ([obj isEqual:[NSNull null]])
            {
                Order *order = [self.orders objectAtIndex:self.orderSelected.row];
                [self getGoogleMapRouteInfo:order];
            }
            else
            {
                routePolyline = (GMSPolyline *)obj;
                routePolyline.map = self.mapView;
            }
        }
        
        Order *order = [self.orders objectAtIndex:self.orderSelected.row];
        for (Task *task in order.tasks)
        {
            GMSMarker *vendorMarker = [[GMSMarker alloc] init];
            vendorMarker.title = task.contact.name;
            vendorMarker.position = CLLocationCoordinate2DMake([task.location.locationLatitude doubleValue], [task.location.locationLongitude doubleValue]);
            vendorMarker.map = self.mapView;
            vendorMarker.userData = task;
            [markers addObject:vendorMarker];
            
            NSNumber *statusId = task.statusId;
            MetaStatus *status = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_TASK_FAILURE];
            
            if ([task.type isEqualToString:@TASK_PICKUP])
            {
                if ([statusId intValue] == [status.statusId intValue])
                {
                    vendorMarker.icon = [UIImage imageNamed:@"error.png"];
                }
                else if ([task isAllConfirmed])
                {
                    vendorMarker.icon = [UIImage imageNamed:@"success.png"];
                }
                else
                {
                    vendorMarker.icon = [UIImage imageNamed:@"restaurants.png"];
                }
            }
            else if ([task.type isEqualToString:@TASK_DROP_OFF])
            {
                if ([statusId intValue] == [status.statusId intValue])
                {
                    vendorMarker.icon = [UIImage imageNamed:@"error.png"];
                }
                else if ([task isAllConfirmed])
                {
                    vendorMarker.icon = [UIImage imageNamed:@"success.png"];
                }
                else
                {
                    vendorMarker.icon = [UIImage imageNamed:@"customer.png"];
                }
            }
            else if ([task.type isEqualToString:@TASK_RETURN])
            {
                vendorMarker.icon = [UIImage imageNamed:@"default_restaurants.png"];
            }
        }
        
        GMSCoordinateBounds *bounds = [MapHelper mapFitAllMarkers:markers];
        [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30]];
    });
}

-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.orderSelected.row >= self.currentTaskArray.count)
        {
            return;
        }
        
        Task *markerTask = marker.userData;
        Task *currentTask = [self.currentTaskArray objectAtIndex:self.orderSelected.row];
        if (markerTask == currentTask)
        {
            MetaStatus *status = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_TASK_FAILURE];
            if (![markerTask isAllConfirmed] && ([markerTask.statusId intValue] != [status.statusId intValue]))
            {
                [self layoutConfirmationList:markerTask];
            }
        }
    });
}

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView.selectedMarker = marker;
    });
    
    return YES;
}

#pragma mark - JSONHelperDelegate

-(void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    
    if ([action isEqualToString:@ACTION_SYSTEM_STATUS_API])
    {
       
        

         ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                             iconImg:[UIImage imageNamed:@"send"]
                                                     backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                               title:@"Success"
                                                             content:@"Status Updated"
                                                        buttonTitles:@[@"Ok"]
                                                 confirmBtnTextColor:nil otherBtnTextColor:nil
                                                  buttonPressedBlock:^(NSInteger btnIdx) {


             //Dismissing this sheet and calling delegate in itinerary orders to refresh data
             [self dismissViewControllerAnimated:YES completion:nil];

          


                                                  }];

         popupView.contentTextAlignment  = NSTextAlignmentCenter;

         [popupView present];
         
    }
    else if ([action isEqualToString:@ACTION_CONFIRMATION])
    {
        [alertHelper dismissAlert];
        Confirmation *confirmation = [list firstObject];
        
        if ([confirmation.isConfirmed boolValue])
        {
            [[DatabaseHelper shared] updateConfirmation:confirmation.confirmationId isConfirmed:ConfirmationStatusSent];
            currentConfirmation.isConfirmed = [NSNumber numberWithBool:YES];
            
            
            /**
             
             MM Store - SUPER MARKET
             */
            // sendUpdateToMM(confirmation.task.sprintId,confirmation.title);
            
            
            
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *myNumber = [f numberFromString:self.currentOrderID];
            
           // [self sendUpdateToMM: myNumber :confirmation.title];
            
            
            
        }
        else
        {
            [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"There was an error with the confirmation.", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        }
        
        currentConfirmation = nil;
        [self checkAllConfirmation];
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{
    currentConfirmation = nil;
    [self.delegate stopRefreshAnimation];
    
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

-(void)uploadData:(int64_t)totalSent totalToSend:(int64_t)totalToSend
{
    if (totalSent < totalToSend)
    {
        [alertHelper showProgressView:self message:[NSString stringWithFormat:NSLocalizedString(@"Sending picture %.1f%%", nil), ((CGFloat)totalSent/totalToSend)*100] totalSent:totalSent totalToSend:totalToSend];
    }
    else
    {
        [alertHelper dismissAlert];
    }
}



/**
 MM STORE MARKET Update
 */

-(void)sendUpdateToMM:(NSNumber *) orderSprintID : (NSString *) orderStatus
{
    
    
    //    Sprint sprint = orderSprint;
    //
    //    int sprintID = sprint.getId();
    //
    //   Log.d("CHECK_SPRINT","get Sprint ID : " + sprint.getId());
    
    NSLog(@"SPRINT_ID : %@",orderSprintID);
    
    
    /**
     * Call to webservice
     * */
    MMCallAPI * apiCall = [[MMCallAPI alloc ]init];
    
    apiCall.sprintID = orderSprintID;
    apiCall.JSON_response = orderStatus;
    
    //[apiCall updateMMAboutStatusUpdate];
    
    // MMCallAPI(orderSprintID, orderStatus).execute();
    
}

+(BOOL)isEmpty:(NSString *)str
{
    if(str.length==0 || [str isKindOfClass:[NSNull class]] || [str isEqualToString:@""]||[str  isEqualToString:NULL]||[str isEqualToString:@"(null)"]||str==nil || [str isEqualToString:@"<null>"]){
        return YES;
    }
    return NO;
}

#pragma mark - call now Phone number verification

-(NSString *)checkPhoneNumberValidity:(NSString *)phone{
    
    NSString *digitsOnly = @"0123456789";
      NSString *phoneString = [phone filterString:digitsOnly];
      NSRange range = [phoneString rangeOfString:@"^0*" options:NSRegularExpressionSearch];
      phoneString = [phoneString stringByReplacingCharactersInRange:range withString:@""];

    
    BOOL byDefaultValidation = NO;
    
    if ([phoneString length] == 10 || [phoneString length] == 11 ) {
        
        if ([phoneString length] == 10) {
           
            if([phoneString hasPrefix:@"1"] ) {
                NSLog(@"INVALID phone");
               // byDefaultValidation =  NO;
                phoneString = @"";
                   }
            else
            {
                NSLog(@"PHONE__ +1%@",phoneString);
                byDefaultValidation =  YES;
                phoneString = [NSString stringWithFormat:@"+1%@",phoneString];

            }
            
        }
        else   if ([phoneString length] == 11) {
 
            if([phoneString hasPrefix:@"1"] ) {
                  NSLog(@"PHONE__ +%@",phoneString);
              //  byDefaultValidation =  YES;
                phoneString =  [NSString stringWithFormat:@"+%@",phoneString];


        }
            else
               {
                   NSLog(@"INVALID phone");
                 //   byDefaultValidation =  NO;
                   phoneString = @"";


               }
        }
}
    else
    {
        NSLog(@"INVALID phone");
       //  byDefaultValidation = NO;
        phoneString = @"";


    }
        
    return phoneString;
        
}


/*
 Updating at pickup and drop off sstatus
 */



-(void)markPickup:(id)sender
{
    RoundedButton *button = (RoundedButton *)sender;
    Task *task = (Task *)button.buttonData;
  
  
    [self updateAtPickupAtDropOffStatus:@"pickup" orderId:[task.sprintId stringValue]  TaskID:[task.taskId stringValue] statusCode:@"67" ];
    
}

-(void)markDropoff:(id)sender
{
    RoundedButton *button = (RoundedButton *)sender;
    Task *task = (Task *)button.buttonData;
  
    [self updateAtPickupAtDropOffStatus:@"dropoff" orderId:[task.sprintId stringValue]  TaskID:[task.taskId stringValue] statusCode:@"68" ];
   
}

-(void)updateAtPickupAtDropOffStatus :(NSString *)type orderId:(NSString *)orderID TaskID:(NSString *)taskID statusCode:(NSString *)statusCode{
    
    [json updateSystemStatus:orderID statusCode:statusCode TaskId:taskID type:type];

}



@end
