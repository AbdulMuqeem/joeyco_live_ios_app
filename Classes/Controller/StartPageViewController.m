//
//  StartPageViewController.m
//  Joey
//
//  Created by Katia Maeda on 2016-06-02.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface StartPageViewController ()

@end

@implementation StartPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[LocationManager shared] startUpdatingLocation];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    if (![sharedDefaults stringForKey:@"ShouldShowTutorial"])
    {
        [sharedDefaults setObject:@"AlreadyShowed" forKey:@"ShouldShowTutorial"];
        [sharedDefaults synchronize];
        
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"TutorialScrollViewController"];
//        [self.navigationController presentViewController:controller animated:YES completion:nil];


        if (@available(iOS 13.0, *)) {
                  [controller setModalPresentationStyle: UIModalPresentationFullScreen];
              } else {
                  // Fallback on earlier versions
                 
              }
         [self.navigationController presentViewController:controller animated:YES completion:nil];
        
        return;
    }
    
    Joey *joey = [[DatabaseHelper shared] getJoey];
    if (joey)
    {
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"OffDutyViewController"];
        [self.navigationController setViewControllers:@[controller]];
    }
    else
    {
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        
        if (@available(iOS 13.0, *)) {
            [controller setModalPresentationStyle: UIModalPresentationFullScreen];
        } else {
            // Fallback on earlier versions
           
        }
         [self.navigationController presentViewController:controller animated:YES completion:nil];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
