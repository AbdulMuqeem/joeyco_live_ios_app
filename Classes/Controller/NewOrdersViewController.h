//
//  NewOrdersViewController.h
//  Joey
//
//  Created by Katia Maeda on 2015-11-06.
//  Copyright © 2015 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewOrdersViewController : UIViewController

@property(nonatomic, weak) OrderViewController *delegate;

-(void)updateLastLocation:(CLLocation *)newLocation;
-(void)updateOrders:(NSArray *)orders;

@end
