//
//  StartPageViewController.h
//  Joey
//
//  Created by Katia Maeda on 2016-06-02.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "NavigationContainerViewController.h"

@interface StartPageViewController : NavigationContainerViewController

@end
