//
//  SignatureViewController.m
//  Joey
//
//  Created by Katia Maeda on 2016-02-24.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface SignatureViewController ()

@property (nonatomic, weak) IBOutlet SignatureView *signatureView;

@end

@implementation SignatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view removeGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

-(IBAction)clearAction:(id)sender
{
    [self.signatureView clearSignature];
}

-(IBAction)saveAction:(id)sender
{
//    UIImageWriteToSavedPhotosAlbum([self.signatureView getSignatureImage], nil, nil, nil);
    self.signatureCompletionBlock([self.signatureView getSignatureImage]);
    [self.navigationController popViewControllerAnimated:YES];
}

@end
