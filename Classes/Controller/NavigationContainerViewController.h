//
//  NavigationContainerViewController.h
//  Customer
//
//  Created by Katia Maeda on 2014-10-10.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationContainerViewController : UIViewController

@property (nonatomic) NSInteger sendingConfirmations;

@property(nonatomic, strong) JSONHelper *json;


@property(nonatomic, strong) Reachability *reachability;

-(GMSMapViewType)setMapType;

-(void)superDealloc;
-(void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list;

-(void)showMessageBoard:(NSString *)text;
-(void)hideMessageBoard;

-(void)setupShiftTimers:(Shift *)shift;
-(void)getNextShift;

-(void)startRefreshAnimation;
-(void)stopRefreshAnimation;

-(void)logout;

@end
