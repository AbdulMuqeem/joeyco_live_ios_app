//
//  ItinearyOrdersViewController.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 06/05/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import "itineraryTableViewCell.h"
#import "Prefix.pch"
#import "Urlinfo.h"
#import "ItineraryOrder_Listing_ObjectDetail.h"
#import "Contact.h"
#import "Task.h"
#import "UIView+Toast.h"
#import "OpenLocation_itinerary.h"
#import "OrderStatus.h"
#import "MDCBottomSheetController.h"
#import "OpenLocationBottomSheet.h"
#import "ConfirmDropOffStatusSheet.h"
#import "Checklist_bottomSheetVC.h"
//@interface ItinearyOrdersViewController ()<JSONHelperDelegate,scanChecklistDelegate,ConfirmDropOffDelegate,SCANNERDelegate,google_SCANNERDelegate>
@interface ItinearyOrdersViewController ()<JSONHelperDelegate,scanChecklistDelegate,ConfirmDropOffDelegate,SCANNERDelegate>

{
        
    JSONHelper *json;
    AlertHelper *alertHelper;
    UIRefreshControl *refreshControl;

    
}
@property(nonatomic,strong) MDCBottomSheetController *bottomSheetDropOff;
@property(nonatomic,strong)NSIndexPath* cellIndexOptedToMarkReturnOfOrder;
@property(nonatomic,assign)BOOL isReturn;

@end

@implementation ItinearyOrdersViewController



#pragma mark - Scan Checklist button delegate methods


-(void)dropStatusCompleted: (ItineraryOrder_Listing_ObjectDetail *)obj{
    
    //NSLog(@"TRACKING_ID_DROPOFF %@",obj.modelData.tracking_id);

    
    [self callWebservice_OrderConfirm:obj.tracking_id];
}

#pragma mark - Scan Checklist button delegate methods


-(void)optionSelected:(int)selectedRow checklistObj:(Checklist_bottomSheetVC *)obj{

    NSMutableArray *datasource = [[NSMutableArray alloc]init];

    
    
    if (selectedRow == 0) {
        
        //Is this order being returned
        self.isReturn = NO;
        
        // basic usage
             [self.view makeToast:NSLocalizedString(@"opening Update Status sheet...", nil)];
        
        //Getting DROP OFF Status
        datasource = self.itineraryData.dropoff_status;
        
        //Open bottom sheet
    [self openBottomSheetDropOffStatus:datasource modelData:obj.modelData  bottomsheet_title:NSLocalizedString(@"Update Status", nil) bottomsheet_subtitle:NSLocalizedString(@"Describe how this order will be delivered", nil)];
        
    }
    else{
        
        //Is this order being returned
        self.isReturn = YES;
        
        //Getting RETURN Status
        datasource = self.itineraryData.dropoff_return_status;
        
        
        
        //Open bottom sheet
        [self openBottomSheet:datasource modelData:obj.modelData  bottomsheet_title:NSLocalizedString(@"Return Order", nil) bottomsheet_subtitle:NSLocalizedString(@"Please tell reason for returning this order", nil)];
        
    }
    
}



#pragma mark - RETURN Status Delegate when updating status

-(void)returnStatusUpdate:(NSIndexPath *)indexPath{
    //[self  updateWebServiceData];
    
    //CHANING BACKGROUND WHEN ITEM IS BEING RETURnED
    
    //Changing data of specific cell to set cell to grey
    ItineraryOrder_Listing_ObjectDetail * obj = [ self.tableViewData objectAtIndex: indexPath.row];
     obj.returned = YES;
    [self.tableViewData replaceObjectAtIndex:indexPath.row withObject:obj];
    
    
    NSArray *indexPathToReload = @[indexPath];
    [self.tableView reloadRowsAtIndexPaths:indexPathToReload withRowAnimation:UITableViewRowAnimationFade];

}

#pragma mark - ScanViewController Delegate

-(void)openSheetOnDismissOfScanScreen:(ItineraryOrder_Listing_ObjectDetail *)modelData{
    //open Bottom SHeet with two items
    //-Delivery
    //-Return order
    
     //NSLog(@"delegate_called ");
   // dispatch_sync(dispatch_get_main_queue(), ^{
               
    [self openBottomSheetForDropOffScan:modelData];
         //  });
}

#pragma mark - ScanViewController Delegate

-(void)scanViewScreenBeingDismissed{
   
    [self  updateWebServiceData];
   
}
-(void)updateWebServiceData{
    
    //Initial Parsing
          self.itineraryData = [[ItineraryOrder alloc]init];


       // basic usage
         [self.view makeToast:NSLocalizedString(@"Updating...", nil)];

          //Call webservice
          [self callWebservice];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
//    //Check when your pop up is going to be hidden
//       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationStatusUpdate:) name:@"GetTappedData_checklist" object:nil];

    
    //init JSON
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    //Initial Parsing
    self.itineraryData = [[ItineraryOrder alloc]init];
    
    //Pull to refresh
   refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];

    if (@available(iOS 10.0, *)) {
        self.tableView.refreshControl = refreshControl;
    } else {
        [self.tableView addSubview:refreshControl];
    }

    
    //Call webservice to get itinerary orders
    [self callWebservice];
}

- (void)refreshTable {
    //TODO: refresh your data
    [refreshControl endRefreshing];
    
    //Call webservice to get itinerary orders
    [self callWebservice];
    
   
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"itineraryCell";
    
    itineraryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (!cell) cell = [[itineraryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    
    ItineraryOrder_Listing_ObjectDetail * modelData = [ self.tableViewData objectAtIndex:indexPath.row];
          
    NSMutableDictionary *contactInfo = [[NSMutableDictionary alloc]initWithDictionary:modelData.contact];
    NSMutableDictionary *locationInfo = [[NSMutableDictionary alloc]initWithDictionary:modelData.location];

    cell.name.text = [NSString stringWithFormat:NSLocalizedString(@"Name : %@", nil),[contactInfo objForKey:@"name"]];
    cell.address_line1.text = [NSString stringWithFormat:NSLocalizedString(@"Address 1 : %@", nil),[locationInfo objForKey:@"address"]];
    
    if([locationInfo objForKey:@"address_line2"] != NULL){
    cell.address_line2.text = [NSString stringWithFormat:NSLocalizedString(@"Address 2 : %@", nil),[locationInfo objForKey:@"address_line2"]];
    }
    
    cell.DeliveryWindow.text = [NSString stringWithFormat:NSLocalizedString(@"Delivery Window : %@-%@", nil),modelData.start_time,modelData.end_time];
    
    //Initially we were using merhchant order # but now replaced with tracking ID
    cell.merchantOrder_num.text = [NSString stringWithFormat:NSLocalizedString(@"Tracking ID: %@", nil) ,modelData.tracking_id];
    cell.route_num.text = [NSString stringWithFormat:@"%@",modelData.num];

    cell.type_item.text = [NSString stringWithFormat:@"%@",modelData.type];
    
     
    
    //setting color
    
    cell.type_item.textColor =[self setColorForPickupsAndDrops:modelData.type];
    
    cell.view_item_vertical_bar.backgroundColor =[self setColorForPickupsAndDrops:modelData.type];
    
    
   
    
    //Set button actions
    
//    UIButton *Btn_return = (UIButton *)[cell viewWithTag:410];
//    [Btn_return addTarget:self action:@selector(ReturnButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
  
    UIButton *Btn_markstatus = (UIButton *)[cell viewWithTag:411];
     [Btn_markstatus addTarget:self action:@selector(MarkStatusButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    UIButton *Btn_openLocation = (UIButton *)[cell viewWithTag:412];
     [Btn_openLocation addTarget:self action:@selector(OpenLocationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    UIButton *Btn_bottomSheetMapChose = (UIButton *)[cell viewWithTag:413];
     [Btn_bottomSheetMapChose addTarget:self action:@selector(openBottomSheetOption:) forControlEvents:UIControlEventTouchUpInside];
   
    UIButton *Btn_checklist = (UIButton *)[cell viewWithTag:414];
    [Btn_checklist addTarget:self action:@selector(ScanButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    UIButton *Btn_callnow = (UIButton *)[cell viewWithTag:415];
    [Btn_callnow addTarget:self action:@selector(CallNowButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *Btn_CustomerInfo = (UIButton *)[cell viewWithTag:416];
    [Btn_CustomerInfo addTarget:self action:@selector(customerInfoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
       


    //Has_picked icon
    if (modelData.has_picked ) {
        //Item has been picked . so SHOW that icon
        cell.has_pickedView.hidden = NO;

    }
    else
    {
        //Item has not been  picked . so HIDE  that icon

        cell.has_pickedView.hidden = YES;

    }
    
    
    
    //If returned then set grey background
    if (modelData.returned ) {
        //Item has been picked . so SHOW that icon
        cell.backgroundColor = [UIColor lightGrayColor];

    }
    else
    {
        //Item has not been  picked . so HIDE  that icon

        cell.backgroundColor = [UIColor whiteColor];

    }

    
    return cell;
    
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ self.itineraryData.getDataForTableView count];
    
   
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    //Saving selected index
//    self.selectedIndex =(int) indexPath.row;
//
//    [self.tblView reloadData];
//
//    /**
//     Sending tapped data*/
//    [NSNotificationCenter.defaultCenter postNotificationName:@"getTappedData" object:[self.cancelStatusObj objectAtIndex:indexPath.row]];
//
//
//
//    [self updateToServer:indexPath.row];
//
    
}



/*
 Call webservice Order Confirm
 */

-(void)callWebservice_OrderConfirm:(NSString *)trackingID{
      
    
    [json SingleDropOff:trackingID];
}


/*
 Call webservice
-Fetch Itinerary API data
 */

-(void)callWebservice{
      
    [json itineraryApi];
}


/*
 On failure response
 */
-(void)errorMessage:(NSString *)action message:(NSString *)message{
    
    if([action isEqualToString:@ACTION_informationWindow])
    {
        
    [self.view makeToast:NSLocalizedString(@"No Information available", nil)];
    }
}

/**
 On response of  success
 */
- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)response {
    
    //Checking Information Window Webservice data
    if([action isEqualToString: @ACTION_informationWindow]){
        
        [self openInformationWindowBottomSheet:response];
        
    }
    
    //If Drop off was done using
    //Order/confirm api
    //then perform this
    else if([action isEqualToString: @ACTION_SINGLE_DROP_OFF]){

            ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                                iconImg:[UIImage imageNamed:@"send"]
                                                        backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                                  title:NSLocalizedString(@"Success", nil)
                                                                content:NSLocalizedString(@"Confirmed Successfully", nil)
                                                           buttonTitles:@[NSLocalizedString(@"Ok", nil)]
                                                    confirmBtnTextColor:nil otherBtnTextColor:nil
                                                     buttonPressedBlock:^(NSInteger btnIdx) {
        
        
                [self dismissViewControllerAnimated:YES completion:^{
                   
                    //Call webservice to refresh data
                    [self callWebservice];
                }];
        
        
                                                     }];
        
            popupView.contentTextAlignment  = NSTextAlignmentCenter;
        
            [popupView present];
            
    }
    else
    {
        //Gettting all data after parsing
        
        self.itineraryData = [response objectAtIndex:0];
        
        
        //Getting data for Bird view
        self.birdViewLocationData = [self.itineraryData getLocationInformation] ;
        
        //Getting data for table view
        self.tableViewData = [self.itineraryData getDataForTableView];
        
        //Setting counters
        //-total items
        //Total Drop offs Left
        int count1 =(int)[ self.itineraryData.getDataForTableView count];
        self.label_leftCounter.text = [NSString stringWithFormat:@"%d",count1];
        
        int count2 = self.itineraryData.getTotalNumberOfItemsPicked ;
        self.label_rightCounter.text = [NSString stringWithFormat:@"%d",count2];
//
        
        
        if ([self.tableViewData count] == 0) {
         
            //Hide no orders label
            self.noOrdersExistsLabel.hidden = NO;
            self.tableView.hidden = YES;
            self.bulkScanButtonView.hidden = YES;
        }
        else
        {
            //Show no orders label
            self.noOrdersExistsLabel.hidden = YES;
            self.tableView.hidden = NO;
            self.bulkScanButtonView.hidden = NO;


            
            //Refresh tablew view
              [self.tableView reloadData];
              
              NSMutableArray *trackingIds = [self.itineraryData getTrackingIds];
              
              
             // NSLog(@"tracking_ids*** %@",[self.itineraryData getTrackingIds]);
        }
    }
    
    
    
//
//    NSMutableArray * totalArray = [self.itineraryData getDataForTableView];
//
//    for(int i =0 ;i< [totalArray count]; i++)
//    {
//        ItineraryOrder_Listing_ObjectDetail * modelData = [totalArray objectAtIndex:i];
//
//        NSMutableDictionary *contactInfo = [[NSMutableDictionary alloc]initWithDictionary:modelData.contact];
//
//        NSLog(@"data_name : %@",[contactInfo objForKey:@"name"]);
//    }
//

   // cell.name.text = modelData.contact.name;
    
  
    
//    ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
//                                                        iconImg:[UIImage imageNamed:@"send"]
//                                                backgroundStyle:ZHPopupViewBackgroundType_Blur
//                                                          title:@"Success"
//                                                        content:responseString
//                                                   buttonTitles:@[@"Ok"]
//                                            confirmBtnTextColor:nil otherBtnTextColor:nil
//                                             buttonPressedBlock:^(NSInteger btnIdx) {
//
//
//
//
//
//                                             }];
//
//    popupView.contentTextAlignment  = NSTextAlignmentCenter;
//
//    [popupView present];
    
}

/*
 Set helper methods for pickup and drop offs
 */
-(UIColor * )setColorForPickupsAndDrops:(NSString *)itemType{
    
    UIColor *color;
    
    if ([itemType isEqualToString:@"pickup"]) {
        color =[UIColor colorWithRed: 0.20 green: 0.78 blue: 0.35 alpha: 1.00];
    }
    else
    {
          color =[UIColor colorWithRed: 1.00 green: 0.58 blue: 0.00 alpha: 1.00];
    }
    return color;
}
 

-(void)ReturnButtonClicked:(UIButton*)sender {
    
     CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
     NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
  

    
    //Getting array data from the list of table view data source
    ItineraryOrder_Listing_ObjectDetail * modelData = [ self.tableViewData objectAtIndex:indexPath.row];
    
    //Setting datasource after checking its pickup /dropoff
      NSMutableArray *datasource = [[NSMutableArray alloc]init];
      if ([modelData.type isEqualToString:@"pickup"]) {
          datasource = self.itineraryData.pickup_return_status;
          
      }
    else
    {
        datasource = self.itineraryData.dropoff_return_status;
        
        
    }
    
    //Cell index row which opted to return of order
    self.cellIndexOptedToMarkReturnOfOrder =indexPath;
    
    NSLog(@"TRACKING_ID_INDEX::: %ld", self.cellIndexOptedToMarkReturnOfOrder.row);

    
        //Open bottom sheet
       [self openBottomSheet:datasource modelData:modelData  bottomsheet_title:NSLocalizedString(@"Return Order", nil) bottomsheet_subtitle:NSLocalizedString(@"Please tell reason for returning this order", nil)];
     
}


-(void)MarkStatusButtonClicked:(UIButton*)sender {
    
     CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
     NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    
    
    //Getting array data from the list of table view data source
    ItineraryOrder_Listing_ObjectDetail * modelData = [ self.tableViewData objectAtIndex:indexPath.row];
    
    //Setting datasource after checking its pickup /dropoff
      NSMutableArray *datasource = [[NSMutableArray alloc]init];
      if ([modelData.type isEqualToString:@"pickup"]) {
          datasource = self.itineraryData.pickup_delay_status;
          
      }
    else
    {
        datasource = self.itineraryData.dropoff_delay_status;
    }
    
    //Cell index row which opted to return of order
       self.cellIndexOptedToMarkReturnOfOrder =indexPath;
    NSLog(@"TRACKING_ID_INDEX::: %ld", self.cellIndexOptedToMarkReturnOfOrder.row);

    //Open bottom sheet
    [self openBottomSheet:datasource modelData:modelData  bottomsheet_title:NSLocalizedString(@"Mark Status", nil) bottomsheet_subtitle:NSLocalizedString(@"Select one the option to update status", nil)];
}



-(void)OpenLocationButtonClicked:(UIButton*)sender {
    
     CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
     NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];

    //Getting array data from the list of table view data source
    ItineraryOrder_Listing_ObjectDetail * modelData = [ self.tableViewData objectAtIndex:indexPath.row];
    
    
    NSMutableDictionary *locationInfo = [[NSMutableDictionary alloc]initWithDictionary:modelData.location];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];

      
   
 
        
        OpenLocation_itinerary *controller = [storyboard instantiateViewControllerWithIdentifier:@"OpenLocation_itinerary"];
               controller.is_BirdView = NO;
                controller.markerTitle_singleMarker = modelData.num;
               controller.singleMarkerLatitude = [[locationInfo objForKey:@"latitude"]doubleValue ];
               controller.singleMarkerLongitude = [[locationInfo objForKey:@"longitude"]doubleValue ];

    if(controller.singleMarkerLatitude > 0.0){
         [self.navigationController pushViewController:controller animated:YES];
    }
       else
       {
           [self.view makeToast:NSLocalizedString(@"No location information was provided by provider", nil)];

       }
        
   

                                    
            
}


-(void)openBottomSheetOption:(UIButton*)sender
{
    
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
      
       //Getting array data from the list of table view data source
         ItineraryOrder_Listing_ObjectDetail * modelData = [ self.tableViewData objectAtIndex:indexPath.row];
    
      // View controller the bottom sheet will hold
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
        
        OpenLocationBottomSheet * vc = [storyboard instantiateViewControllerWithIdentifier:@"OpenLocationBottomSheet"];
    
       
    
    if ([modelData.location valueForKey:@"latitude"] != nil) {
        
         NSMutableDictionary *locationInfo = [[NSMutableDictionary alloc]initWithDictionary:modelData.location];
        
        vc.is_itinerary= YES;
        vc.latitude= [[locationInfo objForKey:@"latitude"]doubleValue ];
        vc.longitude= [[locationInfo objForKey:@"longitude"]doubleValue ];
                    

            // Initialize the bottom sheet with the view controller just created
            self.bottomSheetDropOff = [[MDCBottomSheetController alloc] initWithContentViewController:vc];
          
             [self presentViewController:self.bottomSheetDropOff animated:true completion:nil];
        
    }
    else{
        [self.view makeToast:NSLocalizedString(@"Location not available", nil)];

    }
    
        
}

//
//-(void)WazeButtonClicked:(UIButton*)sender {
//
//     CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
//     NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
//
//    //Getting array data from the list of table view data source
//      ItineraryOrder_Listing_ObjectDetail * modelData = [ self.tableViewData objectAtIndex:indexPath.row];
//
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"waze://"]]) // Waze is installed. Launch Waze and start navigation
//       {
//
//           NSMutableDictionary *locationInfo = [[NSMutableDictionary alloc]initWithDictionary:modelData.location];
//
//
//           NSString *urlStr = [NSString stringWithFormat:@"waze://?ll=%@,%@&navigate=yes",[locationInfo objForKey:@"latitude"], [locationInfo objForKey:@"longitude"]];
//           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
//       }
//       else // Waze is not installed. Launch AppStore to install Waze app
//       {
//           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
//       }
//}



-(void)ScanButtonClicked:(UIButton*)sender {
    
     CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
     NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];

    //Setting index path (would used later when refreshing data for return status response)
    self.cellIndexOptedToMarkReturnOfOrder = indexPath;
    NSLog(@"TRACKING_ID_INDEX::: %ld",self.cellIndexOptedToMarkReturnOfOrder.row);

    
    //Getting array data from the list of table view data source
    ItineraryOrder_Listing_ObjectDetail * modelData = [ self.tableViewData objectAtIndex:indexPath.row];
    
    //Setting datasource after checking its pickup /dropoff
         NSMutableArray *datasource = [[NSMutableArray alloc]init];
         if ([modelData.type isEqualToString:@"dropoff"]) {
            
           datasource = self.itineraryData.dropoff_status;
             
             
             if (modelData.has_picked ) {
                 
                 [self moveToScanViewScreenWithSingleTrackingID:modelData];
                 
//                 //Open bottom sheet
//                 [self openBottomSheetDropOffStatus:datasource modelData:modelData  bottomsheet_title:@"Update Status" bottomsheet_subtitle:@"Describe how this order will be delivered"];
             }
             else{
                 
                 [self.view makeToast:NSLocalizedString(@"You need to pick this order first", nil)];

             }
             
            
       }
         else{
           
             //Move to Bulk scan Pickup screen
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
                                            
                 //GoogleMLKITBarcodeController *vc = [storyboard instantiateViewControllerWithIdentifier:@"GoogleMLKITBarcodeController"];
                  ScanditScanViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ScanditScanViewController"];
             
             
             
                       vc.delegate = self;
                       vc.is_singleScan = YES;
                        vc.is_pickup = YES;
                       // Pass any objects to the view controller here, like...
                       vc.tracking_ids =[self.itineraryData getTrackingIds];
             
                [self.navigationController pushViewController:vc animated:YES];

                    /// [self moveToScanViewScreenWithSingleTrackingID:modelData];

//
//             OrderStatus *checklistObj = [[OrderStatus alloc]init];
//             checklistObj.orderDescription = @"Scan QR/Barcode to confirm";
//
//             [datasource addObject:checklistObj];
//
//             //Open bottom sheet
//            [self openBottomSheetChecklist:datasource modelData:modelData  bottomsheet_title:@"Checklist" bottomsheet_subtitle:@""];
//
             
         }
       

}

-(void)customerInfoButtonClicked:(UIButton*)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
          NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        
    //Getting array data from the list of table view data source
    ItineraryOrder_Listing_ObjectDetail * modelData = [ self.tableViewData objectAtIndex:indexPath.row];
    
    //Call Webservice
    [self callWebserviceInformationWindow:modelData.sprintId];

}



/*
 Open Information Window Bottom Sheet
 */

-(void)openInformationWindowBottomSheet:(NSMutableArray *)responseData{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
    
    OpenLocationBottomSheet * vc = [storyboard instantiateViewControllerWithIdentifier:@"InformationWindow"];
    vc.responseData= responseData;
    vc.is_itinerary= YES;
        
    // Initialize the bottom sheet with the view controller just created
    self.bottomSheetDropOff = [[MDCBottomSheetController alloc] initWithContentViewController:vc];
    
    CGFloat height = self.view.frame.size.height/2;
    
    self.bottomSheetDropOff.preferredContentSize = CGSizeMake( self.view.frame.size.width,  height+200);

     [self presentViewController:self.bottomSheetDropOff animated:true completion:nil];

}

/*
 Call webservice
-Information Window data
 */

-(void)callWebserviceInformationWindow :(NSNumber *)sprintID{
      
    [json informationWindow:sprintID];
}





-(void)CallNowButtonClicked:(UIButton*)sender {
    
     CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
     NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
     
    //Getting array data from the list of table view data source
       ItineraryOrder_Listing_ObjectDetail * modelData = [ self.tableViewData objectAtIndex:indexPath.row];
    
    NSMutableDictionary *contactInfo = [[NSMutableDictionary alloc]initWithDictionary:modelData.contact];

    //Check if phone number exists or not
    if ( [contactInfo objectForKey:@"phone"] == ( NSString *) [ NSNull null ]  ||
       [contactInfo  valueForKey:@"phone"] == nil
        ) {
        
//        [alertHelper showAlertWithButtons:@"Error" message:@"No phone number was provided" from:self withOkHandler:nil];
        // basic usage
               [self.view makeToast:NSLocalizedString(@"No phone number was provided", nil)];
       
    }
    else
    {
        
        if ([[self checkPhoneNumberValidity: [contactInfo  valueForKey:@"phone"] ] length]>0) {
                [self initiateCall: modelData];


           }
           else{
               [self.view makeToast:NSLocalizedString(@"Invalid Phone number", nil)];
               
           }
        
    }
    
   
    }

-(void)initiateCall :(ItineraryOrder_Listing_ObjectDetail*)modelData  {
   
    NSMutableDictionary *contactInfo = [[NSMutableDictionary alloc]initWithDictionary:modelData.contact];

    
       CallNow *controller = [CallNow shared];
       controller.Joey_ID = [[UrlInfo getJoeyId] stringValue] ;
       controller.Order_ID = [modelData.taskId stringValue];
       controller.PhoneNum = [self checkPhoneNumberValidity: [contactInfo  valueForKey:@"phone"] ];
           
           //only below line was commented to check JCdialpad
          // [self presentViewController:controller animated:YES completion:nil];
           
           if (@available(iOS 13.0, *)) {
                     [controller setModalPresentationStyle: UIModalPresentationFullScreen];
                 } else {
                     // Fallback on earlier versions
                 }
           
           
           AVAudioSessionRecordPermission permissionStatus = [[AVAudioSession sharedInstance] recordPermission];

           switch (permissionStatus) {
                case AVAudioSessionRecordPermissionUndetermined:{
                     [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
                     // CALL YOUR METHOD HERE - as this assumes being called only once from user interacting with permission alert!
                         if (granted) {
                             
                             dispatch_async( dispatch_get_main_queue(), ^{

                             // Microphone enabled code
                             [self.navigationController presentViewController:controller animated:YES completion:nil];
                                 });

                             
                         }
                         else {
                             // Microphone disabled code
                             [self callPermissionNotGiven];

                             
                         }
                      }];
                     break;
                     }
                case AVAudioSessionRecordPermissionDenied:
                     // direct to settings...
                   [self callPermissionNotGiven];
                     break;
                case AVAudioSessionRecordPermissionGranted:
                     // mic access ok...
               {
                   
                   dispatch_async( dispatch_get_main_queue(), ^{

                   [self.navigationController presentViewController:controller animated:YES completion:nil];
                       });


               }
                     break;
                default:
                     // this should not happen.. maybe throw an exception.
                     break;
           }
           
}


    -(void)callPermissionNotGiven{
        
         dispatch_async( dispatch_get_main_queue(), ^{
        
        [alertHelper showAlertWithButtons:NSLocalizedString(@"Permission Required", nil) message:NSLocalizedString(@"JoeyCo would like to access the microphone", nil) from:self withOkHandler:^{
             
                   [[UIApplication sharedApplication] openURL:
                   [NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
       });
  
    }



/*
 Open bottom sheet
 
 -Mark status
 -Return Order
 
 */

-(void)openBottomSheet :(NSMutableArray *)datasource
              modelData:(ItineraryOrder_Listing_ObjectDetail *)modelData
      bottomsheet_title:(NSString *)title
   bottomsheet_subtitle:(NSString *)subtitle  {
    //Setting task object
           Task *task = [[Task alloc]init];
           task.taskId =   modelData.taskId ;
           task.sprintId = modelData.sprintId;
    
            //Get indexpath of selected row
            
       
           
           // View controller the bottom sheet will hold
           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
           
           ReturnOrderVC * CancelVC = [storyboard instantiateViewControllerWithIdentifier:@"CancelOrderVCViewController"];
           CancelVC.cancelStatusObj = [[NSMutableArray alloc]init];
           CancelVC.cancelStatusObj = datasource;
           CancelVC.taskType = modelData.type;
           CancelVC.task = task;
           CancelVC.delegate = self;
           CancelVC.indexPathOfTableViewOfItneraryOrders = self.cellIndexOptedToMarkReturnOfOrder ;
           CancelVC.isReturn = self.isReturn ;
           CancelVC.modelData = modelData;

            CancelVC.text_bottomSheet_title=title;
            CancelVC.text_bottomSheet_subtitle=subtitle;
            
    

           
           // Initialize the bottom sheet with the view controller just created
           MDCBottomSheetController *bottomSheet = [[MDCBottomSheetController alloc] initWithContentViewController:CancelVC];
           [self presentViewController:bottomSheet animated:true completion:nil];
}




/*
 Open bottom sheet
 
 -Confirm Drop off
 
 */

-(void)openBottomSheetDropOffStatus :(NSMutableArray *)datasource modelData:(ItineraryOrder_Listing_ObjectDetail *)modelData bottomsheet_title:(NSString *)title bottomsheet_subtitle:(NSString *)subtitle  {

        //Setting task object
           Task *task = [[Task alloc]init];
           task.taskId =   modelData.taskId ;
           task.sprintId = modelData.sprintId;
   
    // View controller the bottom sheet will hold
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
        
        ConfirmDropOffStatusSheet * CancelVC = [storyboard instantiateViewControllerWithIdentifier:@"ConfirmDropOffStatusSheet"];
       

                CancelVC.delayStatusObj = [[NSMutableArray alloc]init];
               CancelVC.delayStatusObj = datasource;
               CancelVC.taskType = modelData.type;
               CancelVC.order_task = task;
                CancelVC.bottomSheet_title.text=title;
                CancelVC.bottomSheet_subtitle.text=subtitle;
                CancelVC.delegate= self;
                CancelVC.modelData = modelData;
                CancelVC.is_itinerary= YES;
               
               // Initialize the bottom sheet with the view controller just created
               MDCBottomSheetController *bottomSheet = [[MDCBottomSheetController alloc] initWithContentViewController:CancelVC];
               [self presentViewController:bottomSheet animated:true completion:nil];
}


/*
 Open bottom sheet
 
 -Checklist
 
 */

-(void)openBottomSheetChecklist :(NSMutableArray *)datasource modelData:(ItineraryOrder_Listing_ObjectDetail *)modelData bottomsheet_title:(NSString *)title bottomsheet_subtitle:(NSString *)subtitle  {

        //Setting task object
           Task *task = [[Task alloc]init];
           task.taskId =   modelData.taskId ;
           task.sprintId = modelData.sprintId;
   
    // View controller the bottom sheet will hold
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
        
        Checklist_bottomSheetVC * vc = [storyboard instantiateViewControllerWithIdentifier:@"Checklist_bottomSheetVC"];
       

               vc.itemsArray = [[NSMutableArray alloc]init];
               vc.itemsArray = datasource;
               vc.taskType = modelData.type;
               vc.task = task;
                vc.text_bottomSheet_title=title;
                vc.text_bottomSheet_subtitle=subtitle;
                vc.modelData = modelData;
                vc.delegate =self;
        

               
               // Initialize the bottom sheet with the view controller just created
               MDCBottomSheetController *bottomSheet = [[MDCBottomSheetController alloc] initWithContentViewController:vc];
               [self presentViewController:bottomSheet animated:true completion:nil];
}


/*
 Passing data to scanning controller
 
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"push_scanVC"])
       {
           // Get reference to the destination view controller
         //  GoogleMLKITBarcodeController *vc = [segue destinationViewController];
           
           ScanditScanViewController *vc = [segue destinationViewController];
           
           vc.delegate = self;
           vc.is_singleScan = NO;
           vc.is_pickup = NO;

           // Pass any objects to the view controller here, like...
           vc.tracking_ids =[self.itineraryData getTrackingIds];
       }
    
}




- (void) notificationStatusUpdate:(NSNotification *) notification
{
    // update orders when returning from order details
    BOOL hasInternet = [json hasInternetConnection:YES];
    if (hasInternet)
    {
//        [self.delegate getOrderList];
//        
//        [self layoutConfirmationList:self.TaskOpenedBeforeConfirmation];
//
        dispatch_async(dispatch_get_main_queue(), ^{
              
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
                    
            //GoogleMLKITBarcodeController *vc = [storyboard instantiateViewControllerWithIdentifier:@"GoogleMLKITBarcodeController"];
            ScanditScanViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ScanditScanViewController"];

            
            // Pass any objects to the view controller here, like...
            vc.tracking_ids =[self.itineraryData getTrackingIds];
            vc.delegate = self;
            vc.is_singleScan = NO;
            vc.is_pickup = NO;
             [self.navigationController presentViewController:vc animated:YES completion:nil];
                  
              });
    }
    
}


/*
 
 Move to scannig screen
 */
-(void)moveToScanViewScreenWithSingleTrackingID:( ItineraryOrder_Listing_ObjectDetail * )modelData{

    NSMutableArray *arr_trackingID = [[NSMutableArray alloc]init];
    [arr_trackingID addObject:modelData.tracking_id];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
//
//      GoogleMLKITBarcodeController *vc = [storyboard instantiateViewControllerWithIdentifier:@"GoogleMLKITBarcodeController"];
    //GoogleMLKITBarcodeController *vc = [storyboard instantiateViewControllerWithIdentifier:@"GoogleMLKITBarcodeController"];
         ScanditScanViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ScanditScanViewController"];
    
                      
              // Pass any objects to the view controller here, like...
              vc.tracking_ids =arr_trackingID;
              vc.delegate = self;
              vc.is_singleScan = YES;
              vc.modelData = modelData;
              vc.is_pickup = NO;

               [self.navigationController pushViewController:vc animated:YES];
    
}




/*
 
 Open bottom sheet on dismiss of scan screen
 */
-(void)openBottomSheetForDropOffScan:(ItineraryOrder_Listing_ObjectDetail *)modelData
{
            NSMutableArray *datasource = [[NSMutableArray alloc]init];

              OrderStatus *checklistObj1 = [[OrderStatus alloc]init];
              checklistObj1.orderDescription = NSLocalizedString(@"Deliver Order", nil);
    
             OrderStatus *checklistObj2 = [[OrderStatus alloc]init];
            checklistObj2.orderDescription = NSLocalizedString(@"Return Order", nil);

              [datasource addObject:checklistObj1];
              [datasource addObject:checklistObj2];

              //Open bottom sheet
             [self openBottomSheetChecklist:datasource modelData:modelData  bottomsheet_title:NSLocalizedString(@"Order Delivery Status", nil) bottomsheet_subtitle:NSLocalizedString(@"Confirm /Return the order", nil)];
              
    
}

- (IBAction)openBirdView:(id)sender {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
           OpenLocation_itinerary *controller = [storyboard instantiateViewControllerWithIdentifier:@"OpenLocation_itinerary"];
                  controller.is_BirdView = YES ;
//            [controller initallMarkers];
//            controller.arr_allMarkers= self.birdViewLocationData;
    controller.arr_allMarkers=  [self.itineraryData getLocationInformation] ;
    
       
            [self.navigationController pushViewController:controller animated:YES];
       
}



#pragma mark - call now Phone number verification

-(NSString *)checkPhoneNumberValidity:(NSString *)phone{
    
    NSString *digitsOnly = @"0123456789";
      NSString *phoneString = [phone filterString:digitsOnly];
      NSRange range = [phoneString rangeOfString:@"^0*" options:NSRegularExpressionSearch];
      phoneString = [phoneString stringByReplacingCharactersInRange:range withString:@""];

    
    BOOL byDefaultValidation = NO;
    
    if ([phoneString length] == 10 || [phoneString length] == 11 ) {
        
        if ([phoneString length] == 10) {
           
            if([phoneString hasPrefix:@"1"] ) {
                NSLog(@"INVALID phone");
               // byDefaultValidation =  NO;
                phoneString = @"";
                   }
            else
            {
                NSLog(@"PHONE__ +1%@",phoneString);
                byDefaultValidation =  YES;
                phoneString = [NSString stringWithFormat:@"+1%@",phoneString];

            }
            
        }
        else   if ([phoneString length] == 11) {
 
            if([phoneString hasPrefix:@"1"] ) {
                  NSLog(@"PHONE__ +%@",phoneString);
              //  byDefaultValidation =  YES;
                phoneString =  [NSString stringWithFormat:@"+%@",phoneString];


        }
            else
               {
                   NSLog(@"INVALID phone");
                 //   byDefaultValidation =  NO;
                   phoneString = @"";


               }
        }
}
    else
    {
        NSLog(@"INVALID phone");
       //  byDefaultValidation = NO;
        phoneString = @"";


    }
        
    return phoneString;
        
}

@end
