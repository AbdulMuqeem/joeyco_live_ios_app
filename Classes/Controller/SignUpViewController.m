//
//  SignUpViewController.m
//  Joey
//
//  Created by Katia Maeda on 2016-06-10.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface SignUpViewController () <JSONHelperDelegate, UITextFieldDelegate, AutocompleteDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
    CLLocation *lastJoeyLocation;
    
    CustomKeyboardToolbar *toolbar;
    NSString *postalCode;
    NSString *address;
}

@property (nonatomic, weak) IBOutlet CustomScrollView *scrollView;
@property (nonatomic, strong) NSMutableDictionary *mappingFields;

@property (nonatomic, weak) IBOutlet CustomTextField *firstNameField;
@property (nonatomic, weak) IBOutlet CustomTextField *lastNameField;
@property (nonatomic, weak) IBOutlet CustomTextField *emailField;
@property (nonatomic, weak) IBOutlet CustomTextField *phoneField;
@property (nonatomic, weak) IBOutlet CustomTextField *passwordField;
@property (nonatomic, weak) IBOutlet CustomTextField *confirmPasswordField;
@property (nonatomic, weak) IBOutlet CustomTextField *addressField;

@property (nonatomic, weak) IBOutlet UITextView *passwordText;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *passwordTextHeight;

@property (nonatomic, weak) IBOutlet UIView *pickerView;
@property (nonatomic, weak) IBOutlet UIPickerView *picker;
@property (nonatomic, strong) UIButton *buttonSelected;

@property (nonatomic, strong) NSArray *vehicleList;
@property (nonatomic, weak) IBOutlet UIButton *vehicleButton;
@property (nonatomic, strong) Vehicle *vehicleSelected;

@property (nonatomic, strong) NSArray *zoneList;
@property (nonatomic, weak) IBOutlet UIButton *zoneButton;
@property (nonatomic, strong) Zone *zoneSelected;

@property (nonatomic, strong) NSArray *workList;
@property (nonatomic, weak) IBOutlet UIButton *workButton;
@property (nonatomic, strong) Item *workSelected;

@property (nonatomic, strong) NSArray *timeList;
@property (nonatomic, weak) IBOutlet UIButton *timeButton;
@property (nonatomic, strong) Item *timeSelected;

@property (nonatomic, weak) IBOutlet UIButton *termsAgreeButton;
@property (nonatomic, weak) IBOutlet UIButton *privacyAgreeButton;
@property (nonatomic) BOOL isTermsAgree;
@property (nonatomic) BOOL isPrivacyAgree;

@end

@implementation SignUpViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    lastJoeyLocation = [[LocationManager shared] lastKnownLocation];
    
    // Register for TextField
    toolbar = [[CustomKeyboardToolbar alloc] initWithView:self.scrollView];
    [[NSNotificationCenter defaultCenter] addObserver:toolbar selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:toolbar selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    toolbar.textFields = @[self.firstNameField, self.lastNameField, self.emailField, self.phoneField, self.passwordField, self.confirmPasswordField, self.addressField];
    self.firstNameField.inputAccessoryView = toolbar;
    self.lastNameField.inputAccessoryView = toolbar;
    self.emailField.inputAccessoryView = toolbar;
    self.phoneField.inputAccessoryView = toolbar;
    self.passwordField.inputAccessoryView = toolbar;
    self.confirmPasswordField.inputAccessoryView = toolbar;
    self.addressField.inputAccessoryView = toolbar;
    
    self.mappingFields = [NSMutableDictionary dictionary];
    [self.mappingFields setValue:self.firstNameField forKey:[NSString stringWithFormat:@"%@", @"first_name"]];
    [self.mappingFields setValue:self.lastNameField forKey:[NSString stringWithFormat:@"%@", @"last_name"]];
    [self.mappingFields setValue:self.emailField forKey:[NSString stringWithFormat:@"%@", @"email"]];
    [self.mappingFields setValue:self.phoneField forKey:[NSString stringWithFormat:@"%@", @"phone"]];
    [self.mappingFields setValue:self.passwordField forKey:[NSString stringWithFormat:@"%@", @"password"]];
    [self.mappingFields setValue:self.confirmPasswordField forKey:[NSString stringWithFormat:@"%@", @"password_confirmation"]];
    [self.mappingFields setValue:self.addressField forKey:[NSString stringWithFormat:@"%@", @"address"]];
    [self.mappingFields setValue:self.addressField forKey:[NSString stringWithFormat:@"%@", @"postal_code"]];

    self.passwordTextHeight.constant = 0;
    
    UITapGestureRecognizer *hideKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    hideKeyboardGesture.numberOfTapsRequired = 1;
    hideKeyboardGesture.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:hideKeyboardGesture];
    
    MetaData *metaData = [[DatabaseHelper shared] getFullMetaData];
    if (metaData && ![NSDateHelper isLater:metaData.lastUpdated plusHours:24] && metaData.zones.count > 0)
    {
        self.zoneList = metaData.zones;
        self.zoneSelected = [self.zoneList firstObject];
        [self.zoneButton setTitle:self.zoneSelected.name forState:UIControlStateNormal];
        
        self.vehicleList = metaData.vehicles;
        self.vehicleSelected = [self.vehicleList firstObject];
        [self.vehicleButton setTitle:self.vehicleSelected.name forState:UIControlStateNormal];
    }
    else
    {
        if (!lastJoeyLocation || [lastJoeyLocation isEqual:[NSNull null]])
        {
            [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Could not get user location", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        }
        
        [json metaData:lastJoeyLocation.coordinate.latitude longitude:lastJoeyLocation.coordinate.longitude];
    }
    
    self.workList = @[[[Item alloc] init:NSLocalizedString(@"Full Time", nil) value:@"full_time"], [[Item alloc] init:NSLocalizedString(@"Part Time", nil) value:@"part_time"], [[Item alloc] init:NSLocalizedString(@"Seasonal", nil) value:@"seasonal"], [[Item alloc] init:NSLocalizedString(@"Casual", nil) value:@"casual"]];
    self.workSelected = [self.workList firstObject];
    [self.workButton setTitle:self.workSelected.title forState:UIControlStateNormal];
    
    self.timeList = @[[[Item alloc] init:NSLocalizedString(@"8 AM - 12 PM", nil) value:@"8_12"], [[Item alloc] init:NSLocalizedString(@"12 PM - 4 PM", nil) value:@"12_16"], [[Item alloc] init:NSLocalizedString(@"4 PM - 8 PM", nil) value:@"16_20"]];
    self.timeSelected = [self.timeList firstObject];
    [self.timeButton setTitle:self.timeSelected.title forState:UIControlStateNormal];
    
    self.isTermsAgree = NO;
    self.isPrivacyAgree = NO;
    
    [self.termsAgreeButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.privacyAgreeButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setupCheckboxes];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Register for UITextField notifications.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Checkboxes

-(void)setupCheckboxes
{
    [self.termsAgreeButton setImage:[UIImage imageNamed:(self.isTermsAgree ? @"checkbox-checked.png" : @"checkbox-unchecked.png")] forState:UIControlStateNormal];
    [self.privacyAgreeButton setImage:[UIImage imageNamed:(self.isPrivacyAgree ? @"checkbox-checked.png" : @"checkbox-unchecked.png")] forState:UIControlStateNormal];
}

-(void)toggleButton:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if (button == self.termsAgreeButton)
    {
        self.isTermsAgree = !self.isTermsAgree;
    }
    else if (button == self.privacyAgreeButton)
    {
        self.isPrivacyAgree = !self.isPrivacyAgree;
    }
    
    [self setupCheckboxes];
}

#pragma mark - UITextViewDelegate

-(void)hideKeyboard
{
    [self.view endEditing:YES];
    [self.view setNeedsLayout];
    [self.view setNeedsDisplay];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.addressField)
    {
        AutocompleteViewController *acController = [[AutocompleteViewController alloc] init];
        acController.delegate = self;
        acController.modalPresentationStyle = UIModalPresentationPopover;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:acController];
        //[self.navigationController presentViewController:navigationController animated:YES completion:nil];
        if (@available(iOS 13.0, *)) {
                  [navigationController setModalPresentationStyle: UIModalPresentationFullScreen];
              } else {
                  // Fallback on earlier versions
                   
              }
         [self.navigationController presentViewController:navigationController animated:YES completion:nil];
    }
}

-(void)textFieldDidChange:(NSNotification *)notification
{
    NSString *digitsOnly = @"0123456789";
    if (notification.object == self.phoneField)
    {
        self.phoneField.text = [self.phoneField.text filterString:digitsOnly];
        self.phoneField.text = (self.phoneField.text.length < 10) ? self.phoneField.text : [self.phoneField.text substringToIndex:10];
        
        if ([self.phoneField.text validatePhoneNumber])
        {
            [self.phoneField setBackground:[self.phoneField defaultBackground]];
        }
        else
        {
            [self.phoneField setRedBackground];
        }
    }
    else if (notification.object == self.emailField)
    {
        if ([self.emailField.text validateEmail])
        {
            [self.emailField setBackground:[self.emailField defaultBackground]];
        }
        else
        {
            [self.emailField setRedBackground];
        }
    }
    else
    {
        CustomTextField *field = notification.object;
        [field setBackground:[field defaultBackground]];
    }
}

#pragma mark - Filter Picker

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.vehicleButton == self.buttonSelected)
    {
        return self.vehicleList.count;
    }
    else if (self.zoneButton == self.buttonSelected)
    {
        return self.zoneList.count;
    }
    else if (self.workButton == self.buttonSelected)
    {
        return self.workList.count;
    }
    
    return self.timeList.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = nil;
    
    if (self.vehicleButton == self.buttonSelected)
    {
        if (row < 0 || row >= self.vehicleList.count)
        {
            return @"";
        }
        
        Vehicle *vehicle = [self.vehicleList objectAtIndex:row];
        title = vehicle.name;
    }
    else if (self.zoneButton == self.buttonSelected)
    {
        if (row < 0 || row >= self.zoneList.count)
        {
            return @"";
        }
        
        Zone *zone = [self.zoneList objectAtIndex:row];
        title = zone.name;
    }
    else if (self.workButton == self.buttonSelected)
    {
        if (row < 0 || row >= self.workList.count)
        {
            return @"";
        }
        
        Item *item = [self.workList objectAtIndex:row];
        title = item.title;
    }
    else if (self.timeButton == self.buttonSelected)
    {
        if (row < 0 || row >= self.timeList.count)
        {
            return @"";
        }
        
        Item *item = [self.timeList objectAtIndex:row];
        title = item.title;
    }
    
    return title;
}

-(IBAction)selectButton:(id)sender
{
    if (self.pickerView.hidden)
    {
        [self openSelectedStatusAnimation:self.pickerView];
    }
    else if (self.buttonSelected == sender)
    {
        [self closeSelectedStatusAnimation:self.pickerView];
    }
    
    self.buttonSelected = sender;
    [self.picker reloadAllComponents];
}

-(IBAction)selectPickerValue:(id)sender
{
    NSInteger row = [self.picker selectedRowInComponent:0];
    
    NSString *title = nil;
    if (self.vehicleButton == self.buttonSelected)
    {
        if (row < 0 || row >= self.vehicleList.count)
        {
            return;
        }
        
        self.vehicleSelected = [self.vehicleList objectAtIndex:row];
        title = self.vehicleSelected.name;
    }
    else if (self.zoneButton == self.buttonSelected)
    {
        if (row < 0 || row >= self.zoneList.count)
        {
            return;
        }
        
        self.zoneSelected = [self.zoneList objectAtIndex:row];
        title = self.zoneSelected.name;
    }
    else if (self.workButton == self.buttonSelected)
    {
        if (row < 0 || row >= self.workList.count)
        {
            return;
        }
        
        self.workSelected = [self.workList objectAtIndex:row];
        title = self.workSelected.title;
    }
    else if (self.timeButton == self.buttonSelected)
    {
        if (row < 0 || row >= self.timeList.count)
        {
            return;
        }
        
        self.timeSelected = [self.timeList objectAtIndex:row];
        title = self.timeSelected.title;
    }
    
    [self.buttonSelected setTitle:title forState:UIControlStateNormal];
    [self closeSelectedStatusAnimation:self.pickerView];
}

#pragma mark - Animations

-(void)openSelectedStatusAnimation:(UIView *)view
{
    if (!view.hidden)
    {
        return;
    }
    
    view.hidden = NO;
    
    CGRect frame = view.frame;
    frame.origin.y -= frame.size.height;
    
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.frame = frame;
                     }
                     completion:nil];
}

-(void)closeSelectedStatusAnimation:(UIView *)view
{
    if (view.hidden)
    {
        return;
    }
    
    view.hidden = NO;
    
    CGRect frame = view.frame;
    frame.origin.y += frame.size.height;
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         view.hidden = YES;
                     }];
}

#pragma mark - AutocompleteDelegate

-(void)didAutocompleteWithPlace:(GMSPlace *)place
{
    address = place.name;
    postalCode = @"";
    
    for (GMSAddressComponent *component in place.addressComponents)
    {
        if ([component.type isEqualToString:@"postal_code"])
        {
            postalCode = component.name;
            break;
        }
    }
    
    self.addressField.text = [NSString stringWithFormat:@"%@, %@", address, postalCode];
}

#pragma mark - JSONHelperDelegate

- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    if ([action isEqualToString:@ACTION_GET_META_DATA])
    {
        if (list)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                MetaData *metaData = [list firstObject];
                self.zoneList = metaData.zones;
                self.zoneSelected = [self.zoneList firstObject];
                [self.zoneButton setTitle:self.zoneSelected.name forState:UIControlStateNormal];
                
                self.vehicleList = metaData.vehicles;
                self.vehicleSelected = [self.vehicleList firstObject];
                [self.vehicleButton setTitle:self.vehicleSelected.name forState:UIControlStateNormal];
            });
        }
    }
    else if ([action isEqualToString:@ACTION_SIGN_UP])
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"One More Step", nil) message:NSLocalizedString(@"Please click on the activation link in your email", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:^{
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }
}

-(void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

-(void)errorMessage:(NSString *)action message:(NSString *)message fields:(NSArray *)fieldsArray
{
    for (NSString *key in fieldsArray)
    {
        CustomTextField *field = [self.mappingFields objectForKey:key];
        if (field)
        {
            [field setRedBackground];
        }

        //self.passwordTextHeight.constant = 0;
    }
    
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

#pragma mark - Actions

-(IBAction)navigateBack
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)openTermsOfUse:(id)sender
{
    [self hideKeyboard];
    
    BOOL hasInternet = [json hasInternetConnection:YES];
    if (hasInternet)
    {
        MetaData *metaData = [[DatabaseHelper shared] getMetaData];
        if (metaData)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:metaData.termsLink]];
        }
    }
}

-(IBAction)openPrivacyPolicy:(id)sender
{
    [self hideKeyboard];
    
    BOOL hasInternet = [json hasInternetConnection:YES];
    if (hasInternet)
    {
        MetaData *metaData = [[DatabaseHelper shared] getMetaData];
        if (metaData)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:metaData.privacyLink]];
        }
    }
}

-(IBAction)signUpAction
{
    [self hideKeyboard];
    
    if ([self.firstNameField.text isEqualToString:@""] || [self.lastNameField.text isEqualToString:@""] || [self.emailField.text isEqualToString:@""] || [self.phoneField.text isEqualToString:@""] || [self.passwordField.text isEqualToString:@""] || [self.confirmPasswordField.text isEqualToString:@""] || [self.addressField.text isEqualToString:@""] || [postalCode isEqualToString:@""])
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Required", nil) message:NSLocalizedString(@"All fields are required.", nil)  from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    if (![self.passwordField.text isEqualToString:self.confirmPasswordField.text])
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Password", nil) message:NSLocalizedString(@"Password does not match the confirm password.", nil)  from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    if (!self.vehicleSelected)
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"You must select a vehicle.", nil)  from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    if (!self.zoneSelected)
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"You must select a zone.", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    if (!self.isTermsAgree)
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"You must accept the Terms of Use", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    if (!self.isPrivacyAgree)
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"You must agree to the Privacy Policy", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }

    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"first_name":self.firstNameField.text, @"last_name":self.lastNameField.text, @"email":self.emailField.text, @"phone":self.phoneField.text, @"password":self.passwordField.text, @"password_confirmation":self.confirmPasswordField.text, @"address":address, @"postal_code":postalCode, @"vehicle":self.vehicleSelected.vehicleId, @"preferred_zone":self.zoneSelected.zoneId, @"work_type":self.workSelected.value, @"contact_time":self.timeSelected.value, @"terms_agree":[NSNumber numberWithBool:YES], @"privacy_agree":[NSNumber numberWithBool:YES]}];

    [json signup:jsonDictionary];
}

@end

