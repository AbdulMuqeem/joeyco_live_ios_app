//
//  SummaryShiftsViewController.m
//  Joey
//
//  Created by Katia Maeda on 2016-05-20.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface SummaryShiftsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *ordersTable;

@end

@implementation SummaryShiftsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setSummary:(Summary *)summary
{
    _summary = summary;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.ordersTable reloadData];
    });
}

#pragma mark UITableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.summary.shifts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"SummaryOrderTableViewCell";
    TwoLabelsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (!cell) cell = [[TwoLabelsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    __weak TwoLabelsTableCell *weakCell = cell;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        Shift *shift = [self.summary.shifts objectAtIndex:indexPath.row];
        
        weakCell.label1.text = [NSDateHelper stringDateFromDate:shift.startTime];
        weakCell.label2.text = [NSString stringWithFormat:@"%@ | $%.2f", shift.num, [shift.wage floatValue]];
    });
    
    return weakCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    Shift *shift = [self.summary.shifts objectAtIndex:indexPath.row];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        SummaryDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SummaryDetailViewController"];
        viewController.shift = shift;
        [self.navigationController pushViewController:viewController animated:YES];
    });
}

@end
