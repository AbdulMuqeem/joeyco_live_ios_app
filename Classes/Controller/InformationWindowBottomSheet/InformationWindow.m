//
//  InformationWindow.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 03/10/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import "InformationWindow.h"
#import "AppDelegate.h"
#import "CallNow.h"
#import <CoreLocation/CoreLocation.h>
#import "UIView+Toast.h"
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>

@interface InformationWindow () <CallNowDelegate>

@end

@implementation InformationWindow 

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self parseWebserviceData];
    
    //Bind label to uilabel
    self.infoWindow_phoneNumber.text = self.phone;
    
    NSString *heading1 = @"Address Line 1:";
    NSString *heading2 = @"Address Line 2:";
    NSString *heading3 = @"Address Line 3:";
    NSString *heading4 = @"Postal Code:";
    NSString *heading5 = @"City:";



    NSString *finalText = [NSString stringWithFormat:@"%@%@%@%@%@",
                           [self appendDataWithHeading:heading1 dataToAppend:self.addressLine1],
                           [self appendDataWithHeading:heading2 dataToAppend:self.addressLine2],
                           [self appendDataWithHeading:heading3 dataToAppend:self.addressLine3],
                           [self appendDataWithHeading:heading4 dataToAppend:self.zipCode],
                           [self appendDataWithHeading:heading5 dataToAppend:self.city]];

    
    
    // Define general attributes for the entire text
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor grayColor],
                            
                              NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:12]
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:finalText attributes:attribs];

    
    
    [self setHeadingColorBold:attributedText finalText:finalText  heading:heading1];
    [self setHeadingColorBold:attributedText finalText:finalText  heading:heading2];
    [self setHeadingColorBold:attributedText finalText:finalText  heading:heading3];
    [self setHeadingColorBold:attributedText finalText:finalText  heading:heading4];
    [self setHeadingColorBold:attributedText finalText:finalText  heading:heading5];


    
    self.AddressLines.attributedText =attributedText;
}

-(void)setHeadingColorBold:(NSMutableAttributedString *)attributedText finalText:(NSString *)finalText  heading:(NSString *)heading{
   
    UIFont *boldFont = [UIFont fontWithName:@"Helvetica-Bold" size:12.0];
    
    NSRange range = [finalText rangeOfString:heading];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                    NSFontAttributeName:boldFont}
                                    range:range];
}

-(NSString *)appendDataWithHeading:(NSString *) heading dataToAppend:(NSString *)dataValue{
    
    if([InformationWindow isEmpty:dataValue])
        return @"";
    else{
        return  [NSString stringWithFormat:@"%@\n%@\n\n",heading,dataValue];
    }
    
}
+(BOOL)isEmpty:(NSString *)str
{
    if(str.length==0 || [str isKindOfClass:[NSNull class]] || [str isEqualToString:@""]||[str  isEqualToString:NULL]||[str isEqualToString:@"(null)"]||str==nil || [str isEqualToString:@"<null>"]){
        return YES;
    }
    return NO;
}


-(void)parseWebserviceData{
    NSDictionary *data =[self.responseData objectAtIndex:0];
    
//    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
//    [geocoder geocodeAddressString:[data objectForKey:@"informationWindow_zip"] completionHandler:^(NSArray *placemarks, NSError *error) {
//
//        if(error != nil)
//        {
//            NSLog(@"error from geocoder is %@", [error localizedDescription]);
//        } else {
//            for(CLPlacemark *placemark in placemarks){
//
//                //Get lat lng
//                self.latitude =placemark.location.coordinate.latitude;
//                self.longitude =placemark.location.coordinate.longitude;
//
//
//            }
//        }
//    }];
//
    
    
    //Get phone
    self.phone =[data objectForKey:@"informationWindow_phone"] ;
    
    //Get Zip code
    self.zipCode =[data objectForKey:@"informationWindow_zip"] ;

    //Get City
    self.city =[data objectForKey:@"informationWindow_city"] ;

    //Get address line 1,address line 2,address line 3,
    self.addressLine1 =[data objectForKey:@"informationWindow_address1"] ;
    self.addressLine2 =[data objectForKey:@"informationWindow_address2"] ;
    self.addressLine3 =[data objectForKey:@"informationWindow_address3"] ;


    
   

    
}

- (IBAction)call:(id)sender {
    CallNow *callController = [CallNow shared];
//     If already on call then show tap to return button
    if (callController.call) {
        [self.view makeToast:@"You are on another call"];
    }else{
        NSString *phoneNum = [self.phone stringByReplacingOccurrencesOfString:@" " withString:@""];

        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",phoneNum]];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                 NSLog(@"Opened url");
            }
        }];
    }
   
    
}
- (IBAction)closeBtnAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)openWaze:(id)sender {
    
    
    NSDictionary *data =[self.responseData objectAtIndex:0];
       
       CLGeocoder *geocoder = [[CLGeocoder alloc] init];
       [geocoder geocodeAddressString:[data objectForKey:@"informationWindow_zip"] completionHandler:^(NSArray *placemarks, NSError *error) {

           if(error != nil)
           {
               NSLog(@"error from geocoder is %@", [error localizedDescription]);
           } else {
               for(CLPlacemark *placemark in placemarks){
                   
                   //Get lat lng
                   self.latitude =placemark.location.coordinate.latitude;
                   self.longitude =placemark.location.coordinate.longitude;
                   
                   if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"waze://"]]) // Waze is installed. Launch Waze and start navigation
                   {
                       NSString *urlStr = [NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes", self.latitude, self.longitude];
                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
                   }
                   else // Waze is not installed. Launch AppStore to install Waze app
                   {
                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
                   }

               }
           }
       }];
       
    

}

- (IBAction)openGoogleMaps:(id)sender {
    
    
    NSDictionary *data =[self.responseData objectAtIndex:0];
       
       CLGeocoder *geocoder = [[CLGeocoder alloc] init];
       [geocoder geocodeAddressString:[data objectForKey:@"informationWindow_zip"] completionHandler:^(NSArray *placemarks, NSError *error) {

           if(error != nil)
           {
               NSLog(@"error from geocoder is %@", [error localizedDescription]);
           } else {
               for(CLPlacemark *placemark in placemarks){
                   
                   //Get lat lng
                   self.latitude =placemark.location.coordinate.latitude;
                   self.longitude =placemark.location.coordinate.longitude;
                   
                    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps-x-callback://"]]) // Waze is installed. Launch Waze and start navigation
                        {
                            NSString *urlStr = [NSString stringWithFormat:@"comgooglemaps://?saddr=&daddr=%f,%f&directionsmode=driving&x-success=sourceapp://?resume=true&x-source=AirApp", self.latitude, self.longitude];



                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
                        }
                        else // Waze is not installed. Launch AppStore to install Waze app
                        {
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://apps.apple.com/us/app/google-maps-transit-food/id585027354"]];
                        }

               }
           }
       }];
    
  
}
//-(bool)isOnPhoneCall {
    /*

     Returns TRUE/YES if the user is currently on a phone call

     */

//    CTCallCenter *callCenter = [[[CTCallCenter alloc] init] autorelease];
//    for (CTCall *call in callCenter.currentCalls)  {
//        if (call.callState == CTCallStateConnected) {
//            return YES;
//        }
//    }
//    return NO;
//}
@end
