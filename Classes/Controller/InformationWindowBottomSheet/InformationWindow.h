//
//  InformationWindow.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 03/10/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InformationWindow : UIViewController

@property(strong, nonatomic)NSMutableArray * responseData;

@property(assign)double latitude;
@property(assign)double longitude;

@property(strong,nonatomic)NSString * addressLine1;
@property(strong,nonatomic)NSString * addressLine2;
@property(strong,nonatomic)NSString * addressLine3;

@property(strong,nonatomic)NSString * phone;
@property(strong,nonatomic)NSString * zipCode;
@property(strong,nonatomic)NSString * city;


@property (weak, nonatomic) IBOutlet UILabel *AddressLines;

@property (weak, nonatomic) IBOutlet UILabel *infoWindow_phoneNumber;
- (IBAction)call:(id)sender;
- (IBAction)openGoogleMaps:(id)sender;
- (IBAction)openWaze:(id)sender;
- (IBAction)closeBtnAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
