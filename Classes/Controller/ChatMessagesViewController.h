//
//  ChatMessagesViewController.h
//  Joey
//
//  Created by Katia Maeda on 2016-08-09.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Channel.h"

@interface ChatMessagesViewController : JSQMessagesViewController

@property(nonatomic, strong) Channel *channel;

@end
