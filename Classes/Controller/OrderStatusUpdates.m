//
//  OrderStatusUpdates.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 24/10/2018.
//  Copyright © 2018 JoeyCo. All rights reserved.
//

#import "OrderStatusUpdates.h"
#import "OrderStatus.h"

@interface OrderStatusUpdates ()

@end

@implementation OrderStatusUpdates

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    {
        if ([self.task.type isEqualToString:@TASK_PICKUP]){
            //Setting data source
            self.Arr_StatusObj = [self.order arr_pickup_status:self] ;
            
        }
        else{
            //Setting data source
            self.Arr_StatusObj = [self.order arr_dropoff_status:self] ;
            
        }

    }
   

}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"StatusUpdateCell";
    
    UpdateStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (!cell) cell = [[UpdateStatusCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    __weak UpdateStatusCell *weakCell = cell;
    
    
    
    OrderStatus * orderStatus = [self.Arr_StatusObj objectAtIndex:indexPath.row];
    cell.Cell_status_description.text = orderStatus.orderDescription;
    
    
   OrderStatus * Status =  [self.Arr_StatusObj objectAtIndex:indexPath.row];
    
    if ([Status.orderActive intValue]== 0) {
        cell.cell_status_image.image = [UIImage imageNamed:@"Status_grey"];
        cell.Cell_statusUpdate_button.hidden = NO;
    }
    else{
        
        cell.cell_status_image.image = [UIImage imageNamed:@"Status_green"];
        cell.Cell_statusUpdate_button.hidden = YES;
    }

    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        Info *info = [self.namesList objectAtIndex:indexPath.row];
//
//        [weakCell.button setImage:[UIImage imageNamed:([info.value2 isEqualToString:@"1"]) ? @"radio-checked.png" : @"radio-unchecked.png"] forState:UIControlStateNormal];
//        weakCell.button.buttonData = info;
//        [weakCell.button addTarget:self action:@selector(itemSelected:) forControlEvents:UIControlEventTouchUpInside];
//
//        weakCell.label1.text = info.title;
//    });
    
    return weakCell;
}




- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.order arr_pickup_status:self] count];
}



- (IBAction)ProceedToCancelScreen:(id)sender {


    
    // View controller the bottom sheet will hold
       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
       ReturnOrderVC * CancelVC = [storyboard instantiateViewControllerWithIdentifier:@"CancelOrderVCViewController"];
      CancelVC.cancelStatusObj =[self.order arr_cancellation_status:self ];
    
    // Initialize the bottom sheet with the view controller just created
    MDCBottomSheetController *bottomSheet = [[MDCBottomSheetController alloc] initWithContentViewController:CancelVC];
    // Present the bottom sheet
  //  [self.navigationController pushViewController:bottomSheet animated:YES];
    //[self presentViewController:bottomSheet animated:true completion:nil];
    
//    if (@available(iOS 13.0, *)) {
//                 [bottomSheet setModalPresentationStyle: UIModalPresentationFullScreen];
//             } else {
//                 // Fallback on earlier versions
//             }
    [self.navigationController presentViewController:bottomSheet animated:YES completion:nil];

    
    
}

- (IBAction)ReportDelay:(id)sender {
    
    
    // View controller the bottom sheet will hold
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
    DelayReportViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"DelayReportViewController"];
    viewController.delayStatusObj = [self.order arr_delay_status:self];
    
    // Initialize the bottom sheet with the view controller just created
    MDCBottomSheetController *bottomSheet = [[MDCBottomSheetController alloc] initWithContentViewController:viewController];
    // Present the bottom sheet
    //  [self.navigationController pushViewController:bottomSheet animated:YES];
   // [self presentViewController:bottomSheet animated:true completion:nil];
    
//    if (@available(iOS 13.0, *)) {
//                   [bottomSheet setModalPresentationStyle: UIModalPresentationFullScreen];
//               } else {
//                   // Fallback on earlier versions
//               }
      [self.navigationController presentViewController:bottomSheet animated:YES completion:nil];
}

@end
