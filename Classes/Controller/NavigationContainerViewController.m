//
//  NavigationContainerViewController.m
//  Customer
//
//  Created by Katia Maeda on 2014-10-10.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "Prefix.pch"
#import "AppDelegate.h"
#import "ZHPopupView.h"

@interface NavigationContainerViewController () <JSONHelperDelegate,CallNowDelegate>
{
    NSInteger messageBoardHeight;
    UIView *messageBoardView;
    UILabel *messageBoardText;
    NSTimer *messageBoardTimer;
}

@property (nonatomic, strong) UIImageView *refreshImage;

@end;

@implementation NavigationContainerViewController

static NSTimer *startShiftTimer;
static NSTimer *endShiftTimer;

#pragma mark - lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
    CLS_LOG(@"ViewController viewDidLoad %@", self);
    
   
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(callStatus:)
                                                 name:@"Notification_call_status"
                                               object:nil];

    self.json = [[JSONHelper alloc] init:self andDelegate:self];
    
    // Left Menu
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -10;
    
    UIButton *menuButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
    menuButton.frame = CGRectMake(0, 0, 32, 32);
    
    [menuButton.widthAnchor constraintEqualToConstant:32].active = YES;
    [menuButton.heightAnchor constraintEqualToConstant:32].active = YES;
    [[menuButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    NSMutableArray *menuButtons = [NSMutableArray array];

    // Back button
    if(self.navigationController.topViewController == self && self.navigationController.viewControllers.count > 1)
    {
        NSString *title = @"<";
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0 &&
            [UIView userInterfaceLayoutDirectionForSemanticContentAttribute:self.view.semanticContentAttribute] == UIUserInterfaceLayoutDirectionRightToLeft) {
            title = @">";
        }
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:self action:@selector(navigateBack)];
        [backButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:30.0f]} forState: UIControlStateNormal];
        backButton.tintColor = [UIColor colorWithRed:0.87 green:1.0 blue:0 alpha:1.0];
        backButton.width = 70;
        
        [menuButtons addObjectsFromArray:@[backButton, menuBarButton]];
    }
    else
    {
        [menuButtons addObjectsFromArray:@[negativeSpacer, menuBarButton]];
    }
    
    
    
    
        // refresh button
        if ([self isKindOfClass:[OrderViewController class]])
        {
            self.refreshImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
            self.refreshImage.image = [UIImage imageNamed:@"refresh.png"];
  
    
            UIButton *refreshButton =  [UIButton buttonWithType:UIButtonTypeCustom];
            refreshButton.frame = CGRectMake(0, 0, 32, 32);
            [refreshButton.widthAnchor constraintEqualToConstant:32].active = YES;
            [refreshButton.heightAnchor constraintEqualToConstant:32].active = YES;
    
            [refreshButton setBackgroundImage:[UIImage imageNamed:@"menu-bg.png"] forState:UIControlStateNormal];
         
      
    
    
            [refreshButton addTarget:self action:@selector(refreshOrderList) forControlEvents:UIControlEventTouchUpInside];
            [refreshButton addSubview:self.refreshImage];
    
            UIBarButtonItem *menuRefreshButton = [[UIBarButtonItem alloc] initWithCustomView:refreshButton];
            [menuButtons addObject:menuRefreshButton];
        }
    
        self.navigationItem.leftBarButtonItems = menuButtons;
    
    

    
    

    
    
    // Set the gesture
    if (self.revealViewController)
    {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
    
    // Notification area
    messageBoardHeight = 100;
    messageBoardView = [[UIView alloc] initWithFrame:CGRectMake(0, -messageBoardHeight, self.view.frame.size.width, messageBoardHeight)];
    messageBoardView.backgroundColor = [UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1.0];
    
    UIFont *font = [UIFont systemFontOfSize:15.0f];
    messageBoardText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, messageBoardHeight)];
    [messageBoardText setBackgroundColor:[UIColor clearColor]];
    [messageBoardText setTextColor:[UIColor whiteColor]];
    [messageBoardText setFont:font];
    messageBoardText.textAlignment = NSTextAlignmentCenter;

    messageBoardView.hidden = YES;
    [messageBoardView addSubview:messageBoardText];
    [self.view addSubview:messageBoardView];
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(hideMessageBoard)];
    [messageBoardView addGestureRecognizer:panGestureRecognizer];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMessageBoard)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [messageBoardView addGestureRecognizer:tapGesture];

    // Internet connection
    // Monitor internet connection
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];

    // Notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerForPushNotifications:) name:@NotificationsWithDeviceToken object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shiftUpdatedNotification:) name:@NotificationsReceivedShiftUpdated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shiftNotifyNotification:) name:@NotificationsReceivedShiftNotify object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [endShiftTimer invalidate];
    [startShiftTimer invalidate];
    [messageBoardTimer invalidate];
}

-(void)superDealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    CLS_LOG(@"ViewController superDealloc %@", self);
    [self.reachability stopNotifier];
    
    [messageBoardTimer invalidate];
    [startShiftTimer invalidate];
    [endShiftTimer invalidate];
}

-(void)navigateBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)startRefreshAnimation
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * 2 * .2 ];
    rotationAnimation.duration = .2;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = HUGE_VALF;
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    [self.refreshImage.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

-(void)stopRefreshAnimation
{
    [self.refreshImage.layer removeAllAnimations];
}

-(void)goToHome
{
    dispatch_async(dispatch_get_main_queue(), ^{
        OrderViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderViewController"];
        [self.navigationController setViewControllers:@[viewController] animated:NO];
    });
}

-(void)logout
{
    NSString *deviceToken = [[DatabaseHelper shared] getSetting:@"deviceToken"];
    [self.json logout:deviceToken];
}

#pragma mark - Map

-(GMSMapViewType)setMapType
{
    MapTypeSetting mapType = [[DatabaseHelper shared] getMapTypeSetting];
    if (mapType == MapTypeSettingHybrid)
    {
        return kGMSTypeHybrid;
    }
    else if (mapType == MapTypeSettingNormal)
    {
        return kGMSTypeNormal;
    }
    return kGMSTypeNormal;
}

#pragma mark - Push notifications

-(void)registerForPushNotifications:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@NotificationsWithDeviceToken])
    {
        NSDictionary *deviceInfo = notification.userInfo;
        NSData *deviceToken = [deviceInfo objectForKey:@"deviceToken"];
        
        // Save on DB to send after login
        [[DatabaseHelper shared] insertSetting:@"deviceToken" value:[NSString stringWithFormat:@"%@", deviceToken]];
        
        Joey *joey = [[DatabaseHelper shared] getJoey];
        NSString *deviceTokeString = [NSString stringWithFormat:@"%@", deviceToken];
        if (joey && deviceTokeString && ![deviceTokeString isEqualToString:@""])
        {
            [self.json registerDevice:deviceTokeString];
        }
    }
}

-(void)shiftUpdatedNotification:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@NotificationsReceivedShiftUpdated])
    {
        [self showMessageBoard:NSLocalizedString(@"Your schedule has been updated", nil)];
        [self getNextShift];
    }
}

-(void)shiftNotifyNotification:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@NotificationsReceivedShiftNotify])
    {
        [self showMessageBoard:NSLocalizedString(@"Your shift is about to start", nil)];
    }
}

#pragma mark - Shift

-(void)getNextShift
{
    BOOL hasInternet = [self.json hasInternetConnection:NO];
    if (hasInternet)
    {
        [self.json nextShift];
    }
}

-(void)setupShiftTimers:(Shift *)shift
{
    if (!shift)
    {
        return;
    }
    
    [endShiftTimer invalidate];
    [startShiftTimer invalidate];
    
    long diffEndTime = [shift.endTime timeIntervalSince1970] - [[NSDate date] timeIntervalSince1970];
    if (diffEndTime > 0)
    {
        endShiftTimer = [NSTimer scheduledTimerWithTimeInterval:diffEndTime target:self selector:@selector(showEndShiftMessage) userInfo:nil repeats:NO];
    }
    
    long diffStartTime = [shift.startTime timeIntervalSince1970] - [[NSDate date] timeIntervalSince1970];
    if (diffStartTime > 0)
    {
        startShiftTimer = [NSTimer scheduledTimerWithTimeInterval:diffStartTime target:self selector:@selector(showStartShiftMessage) userInfo:nil repeats:NO];
    }
}

-(void)showEndShiftMessage
{
    [endShiftTimer invalidate];
    [self showMessageBoard:NSLocalizedString(@"Your shift has finished", nil)];
}

-(void)showStartShiftMessage
{
    [startShiftTimer invalidate];
    [self showMessageBoard:NSLocalizedString(@"Starting Shift", nil)];
}

#pragma mark - JSONHelperDelegate

-(void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    if ([action isEqualToString:@ACTION_GET_SHIFTS])
    {
        if (list)
        {
            Shift *shift = [list firstObject];
            [[DatabaseHelper shared] insertShift:shift];
            [self setupShiftTimers:shift];
        }
    }
    else if ([action isEqualToString:@ACTION_LOGOUT])
    {        
        [self goToHome];
    }
}

-(void)refreshOrderList
{
    
}

#pragma mark - Message Board

- (void)showMessageBoard:(NSString *)text
{
    if (!messageBoardView.hidden)
    {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        messageBoardView.hidden = NO;
        messageBoardText.text = text;
        [messageBoardTimer invalidate];
        
        [UIView animateWithDuration:.5 animations:^{
            messageBoardView.frame = CGRectMake(0, 0, messageBoardView.frame.size.width, messageBoardHeight);
        } completion:^(BOOL finished) {
            messageBoardTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(hideMessageBoard) userInfo:nil repeats:NO];
        }];
    });
}

- (void)hideMessageBoard
{
    if (messageBoardView.hidden)
    {
        [messageBoardTimer invalidate];
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:1.0 animations:^{
            messageBoardView.frame = CGRectMake(0, -messageBoardView.frame.size.width, messageBoardView.frame.size.width, messageBoardHeight);
        } completion:^(BOOL finished) {
            messageBoardView.hidden = YES;
            messageBoardText.text = @"";
            [messageBoardTimer invalidate];
        }];
    });
}





    /**
     To make Button blink
     */

     -(void)blinkView_infinite:(UIView *)viewToAnimate{
    
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation setFromValue:[NSNumber numberWithFloat:1.0]];
    [animation setToValue:[NSNumber numberWithFloat:0.0]];
    [animation setDuration:0.9f];
    [animation setTimingFunction:[CAMediaTimingFunction
                                  functionWithName:kCAMediaTimingFunctionLinear]];
    [animation setAutoreverses:YES];
    [animation setRepeatCount:20000];
    [[viewToAnimate layer] addAnimation:animation forKey:@"opacity"];
    
    
     }

     
     
- (void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(callStatus:) name:@"Notification_call_status" object:nil];
    
    /**
     Tap to return to call
     
     */
    
    UIButton * TapToReturnCall=[UIButton buttonWithType:UIButtonTypeCustom];
    [TapToReturnCall setFrame:CGRectMake(0, 0, 95, 32)];
    TapToReturnCall.contentEdgeInsets =  UIEdgeInsetsMake(2, 6, 2, 6);
    [TapToReturnCall addTarget:self action:@selector(openCallScreen:) forControlEvents:UIControlEventTouchUpInside];
    
    [TapToReturnCall setTitle:@"Tap to return to calls" forState:UIControlStateNormal];
    [TapToReturnCall.heightAnchor constraintEqualToConstant:32].active = YES;
    
    [TapToReturnCall setBackgroundColor:[UIColor colorWithRed:0.79 green:0.86 blue:0.00 alpha:1.0]];
    [TapToReturnCall setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    [self blinkView_infinite:TapToReturnCall];
    
    
    UIBarButtonItem *tapToReturnToCall = [[UIBarButtonItem alloc] initWithCustomView:TapToReturnCall];
    
    
    
    CallNow *callController = [CallNow shared];

    // If already on call then show tap to return button
    if (callController.call) {
        
        self.navigationItem.rightBarButtonItem = tapToReturnToCall;
        
    }
    else
    {
        [self removeTapToReturnToCallButton];

        
    }
    
}


     
    /**
     Open call screen
     */
     
-(void)openCallScreen :(UIButton*)sender{
    
    CallNow *controller = [CallNow shared];
//    controller.Joey_ID = [[UrlInfo getJoeyId] stringValue] ;
//    controller.Order_ID = self.currentOrderID;
//    controller.PhoneNum = phoneString;
//
   // [self presentViewController:controller animated:YES completion:nil];
    
    if (@available(iOS 13.0, *)) {
              [controller setModalPresentationStyle: UIModalPresentationFullScreen];
          } else {
              // Fallback on earlier versions
            
          }
      [self.navigationController presentViewController:controller animated:YES completion:nil];
}




/**
 
 Twilio call delegates
 
 */


#pragma mark - Notification
-(void) callStatus:(NSNotification *) notification
{
    
    [self removeTapToReturnToCallButton];
    
//    CallNow *callController = [CallNow shared];

   

        //Call has been ended
      //  [self showAlertCallEnded];

//
//    NSDictionary *dict = notification.userInfo;
//    NSString *callStatus = [dict valueForKey:@"callConnected"];
//
//
//
//    if ([callStatus isEqualToString:@"NO"]) {
//        // do stuff here with your message data
//
//    }
}




/**
 
 Remove Tap to call button
 */
-(void)removeTapToReturnToCallButton{
    self.navigationItem.rightBarButtonItems = nil;
}


/**
 Inform user that call has been ended by customer
 
 */

-(void)showAlertCallEnded{
    
    
    
    
    ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                        iconImg:[UIImage imageNamed:@"call_end"]
                                                backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                          title:@"Call Ended"
                                                        content:@"Call has been disconnected"
                                                   buttonTitles:@[@"Ok"]
                                            confirmBtnTextColor:nil otherBtnTextColor:nil
                                             buttonPressedBlock:^(NSInteger btnIdx) {
                                                 
                                               
                                                 
                                                 
                                             }];
    
    popupView.contentTextAlignment  = NSTextAlignmentCenter;
    
    [popupView present];
}


@end
