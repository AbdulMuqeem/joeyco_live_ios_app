//
//  OpenLocation_itinerary.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 23/06/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenLocation_itinerary : UIViewController
{
    CLLocation *lastJoeyLocation;

}
@property (nonatomic,strong) NSMutableArray * arr_allMarkers;

@property (nonatomic,strong) NSString * markerTitle_singleMarker;

@property (nonatomic,assign) BOOL is_BirdView;
@property (nonatomic,assign) double singleMarkerLatitude;
@property (nonatomic,assign) double singleMarkerLongitude;


@property (weak, nonatomic) IBOutlet UIView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
- (IBAction)closeMap:(id)sender;
- (void) initallMarkers;
@end

NS_ASSUME_NONNULL_END
