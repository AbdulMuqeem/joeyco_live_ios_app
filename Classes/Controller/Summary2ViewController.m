//
//  Summary2ViewController.m
//  Joey
//
//  Created by Katia Maeda on 2016-05-20.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface Summary2ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *summaryTable;
@property (nonatomic, strong) NSMutableArray *summaryList;

@end

@implementation Summary2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setSummary:(Summary *)summary
{
    _summary = summary;
    
    self.summaryList = [NSMutableArray array];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Avg. Time:", nil) value:self.summary.averageDuration defaultValue:@"-"]];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Avg. Distance:", nil) value:[NSString stringWithFormat:@"%.2f km", [self.summary.averageDistance floatValue]/1000] defaultValue:@"-"]];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Total Distance:", nil) value:[NSString stringWithFormat:@"%.2f km", [self.summary.totalDistance floatValue]/1000] defaultValue:@"-"]];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Furthest Distance:", nil) value:[NSString stringWithFormat:@"%.2f km", [self.summary.maxDistance floatValue]/1000] defaultValue:@"-"]];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Total Orders:", nil) value:[NSString stringWithFormat:@"%d", [self.summary.numTotalOrders intValue]] defaultValue:@"-"]];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Total Hours Logged:", nil) value:self.summary.totalHours defaultValue:@"-"]];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Total Earnings:", nil) value:[NSString stringWithFormat:@"$%.2f", [self.summary.earnings floatValue]] defaultValue:@"-"]];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Total Cash Collected:", nil) value:[NSString stringWithFormat:@"$%.2f", [self.summary.cashCollected floatValue]] defaultValue:@"-"]];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Total Payments Made:", nil) value:[NSString stringWithFormat:@"$%.2f", [self.summary.paymentsMade floatValue]] defaultValue:@"-"]];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Total Wages:", nil) value:[NSString stringWithFormat:@"$%.2f", [self.summary.wages floatValue]] defaultValue:@"-"]];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Total Wages Owed:", nil) value:[NSString stringWithFormat:@"$%.2f", [self.summary.wagesOwed floatValue]] defaultValue:@"-"]];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Total Tips:", nil) value:[NSString stringWithFormat:@"$%.2f", [self.summary.tips floatValue]] defaultValue:@"-"]];
    [self.summaryList addObject:[[Item alloc] init:NSLocalizedString(@"Total Transfers Made:", nil) value:[NSString stringWithFormat:@"$%.2f", [self.summary.transfers floatValue]] defaultValue:@"-"]];

    [self reloadData];
}

-(void)reloadData
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.summaryTable reloadData];
    });
}

#pragma mark UITableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.summaryList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"SummaryTableViewCell";
    TwoLabelsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (!cell) cell = [[TwoLabelsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    __weak TwoLabelsTableCell *weakCell = cell;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        Item *item = [self.summaryList objectAtIndex:indexPath.row];
        weakCell.label1.text = item.title;
        weakCell.label2.text = item.value;
        weakCell.label2Width.constant = [weakCell.label2 sizeThatFits:CGSizeMake(1000, weakCell.label2.frame.size.height)].width;
    });
    
    return weakCell;
}

@end

@implementation Item

-(instancetype)init:(NSString *)title value:(NSString *)value
{
    _title = title;
    _value = value;
    return self;
}

-(instancetype)init:(NSString *)title value:(NSString *)value defaultValue:(NSString *)defaultValue
{
    _title = title;
    _value = (!value || [value isEqualToString:@""]) ? defaultValue : value;
    return self;
}

@end