//
//  ScheduleEdit2ViewController.m
//  Joey
//
//  Created by Katia Maeda on 2015-12-02.
//  Copyright © 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface ScheduleEdit2ViewController () <JTCalendarDataSource, JSONHelperDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
}

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet RoundedButton *calendarButton;

@property (nonatomic, weak) IBOutlet JTCalendarMenuView *calendarTitleView;
@property (nonatomic, weak) IBOutlet JTCalendarContentView *calendarContentView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *calendarContentViewHeight;

@property (nonatomic, weak) IBOutlet UITableView *shiftsTable;
@property (nonatomic, strong) NSMutableArray *shiftsList;

@property (nonatomic, strong) JTCalendar *calendar;
@property (nonatomic, strong) NSDate *dateSelected;

@end

@implementation ScheduleEdit2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    self.titleLabel.text = [NSString stringWithFormat:@"%@ - %@", self.shift.zone.name, self.shift.vehicle.name];
    
    self.calendar = [JTCalendar new];
    self.calendar.calendarAppearance.calendar.firstWeekday = 1; // Sunday == 1, Saturday == 7
    self.calendar.calendarAppearance.dayCircleRatio = 9. / 10.;
    self.calendar.calendarAppearance.ratioContentMenu = 1;
    self.calendar.calendarAppearance.weekDayFormat = JTCalendarWeekDayFormatShort;
    
    [self.calendar setMenuMonthsView:self.calendarTitleView];
    [self.calendar setContentView:self.calendarContentView];
    [self.calendar setDataSource:self];
    
    self.dateSelected = [NSDate date];
    [self getSchedules:self.dateSelected];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.calendar showViews];
    [self.calendar reloadData]; // Must be call in viewDidAppear
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [alertHelper dismissAlert];
    [self.calendar hideViews];
}

-(void)dealloc
{
    [self superDealloc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reloadData
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.shiftsTable reloadData];
    });
}

#pragma mark - Actions

- (IBAction)ToggleCalendarMode
{
    self.calendar.calendarAppearance.isWeekMode = !self.calendar.calendarAppearance.isWeekMode;
    [self.calendarButton setTitle:(self.calendar.calendarAppearance.isWeekMode ? NSLocalizedString(@"Month View", nil) : NSLocalizedString(@"Week View", nil)) forState:UIControlStateNormal];
    
    [self transitionExample];
}

- (void)transitionExample
{
    CGFloat newHeight = 250;
    if(self.calendar.calendarAppearance.isWeekMode){
        newHeight = 75.;
    }
    
    [UIView animateWithDuration:.5
                     animations:^{
                         self.calendarContentViewHeight.constant = newHeight;
                         [self.view layoutIfNeeded];
                     }];
    
    [UIView animateWithDuration:.25
                     animations:^{
                         self.calendarContentView.layer.opacity = 0;
                     }
                     completion:^(BOOL finished) {
                         [self.calendar reloadAppearance];
                         
                         [UIView animateWithDuration:.25
                                          animations:^{
                                              self.calendarContentView.layer.opacity = 1;
                                          }];
                     }];
}

#pragma mark - JTCalendarDataSource

-(BOOL)calendarHaveEvent:(JTCalendar *)calendar date:(NSDate *)date
{
    return NO;
}

-(void)calendarDidDateSelected:(JTCalendar *)calendar date:(NSDate *)date
{
    self.dateSelected = date;
    [self getSchedules:self.dateSelected];
}

-(void)calendarDidChangedDates:(NSDate *)date
{
    
}

-(void)getSchedules:(NSDate *)date
{
    BOOL hasInternet = [json hasInternetConnection:NO];
    if (hasInternet)
    {
        NSInteger startDate = [NSDateHelper getFirstHour:date];
        NSInteger endDate = [NSDateHelper getLastHour:date];
 
        [json getSchedules:self.shift.zone.zoneId vehicle:self.shift.vehicle.vehicleId start:(int)startDate end:(int)endDate];
    }
}

#pragma mark - UITableView methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.shiftsList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SlotCell";
    
    TwoLabelsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[TwoLabelsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    __weak TwoLabelsTableCell *weakCell = cell;

    dispatch_async(dispatch_get_main_queue(), ^{
        if (indexPath.row < 0 || indexPath.row >= self.shiftsList.count)
        {
            return;
        }
        
        Shift *shift = [self.shiftsList objectAtIndex:indexPath.row];
        
        weakCell.label1.text = [NSString stringWithFormat:@"%@ - %@", [NSDateHelper stringTimeFromDate:shift.startTime], [NSDateHelper stringTimeFromDate:shift.endTime]];
        
        int available = [shift.capacity intValue] - [shift.occupancy intValue];
        weakCell.label2.text = (available == 1) ? [NSString stringWithFormat:NSLocalizedString(@"1 slot remaining", nil)] : [NSString stringWithFormat:NSLocalizedString(@"%d slots remaining", nil), available];
        
        BOOL isScheduled = [self isJoeyScheduled:shift];

        weakCell.button.buttonData = shift;
        [weakCell.button setImage:[UIImage imageNamed:(isScheduled ? @"checkbox-checked.png" : @"checkbox-unchecked.png")] forState:UIControlStateNormal];
        [weakCell.button addTarget:self action:@selector(toggleShift:) forControlEvents:UIControlEventTouchUpInside];
    });
    
    return weakCell;
}

-(BOOL)isJoeyScheduled:(Shift *)shift
{
    BOOL isScheduled = NO;
    for (NSNumber *joeyId in shift.joeys)
    {
        if ([joeyId intValue] == [[UrlInfo getJoeyId] intValue])
        {
            isScheduled = YES;
            break;
        }
    }
    return isScheduled;
}

-(void)toggleShift:(id)sender
{
    CustomButton *button = (CustomButton *)sender;
    Shift *shift = (Shift *)button.buttonData;
    BOOL isScheduled = [self isJoeyScheduled:shift];
    
    if (isScheduled)
    {
        
        
        if ([shift.can_cancel integerValue] == 1){
            [alertHelper showAlertWithButtons:NSLocalizedString(@"Remove schedule", nil) message:NSLocalizedString(@"Are you sure you want to remove this schedule?", nil) from:self withOkHandler:^(void) {
                [json removeJoeySchedule:shift.shiftId];
            }];
        }
        else{
            
            [alertHelper showAlertWithOneButton:NSLocalizedString(@"Cannot Remove", nil) message:NSLocalizedString(@"This schedule cannot be removed,because either the shift is active or about to start", nil) from:self buttonTitle:@"Ok" withHandler:^{
                
            }];
                     
        }
        
        
      
    }
    else
    {
        [alertHelper showAlertWithButtons:NSLocalizedString(@"Add schedule", nil) message:NSLocalizedString(@"Are you sure you want to add this schedule?", nil) from:self withOkHandler:^(void) {
            [json addJoeySchedule:shift.shiftId];
        }];
    }
}

#pragma mark - JSONHelperDelegate

- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    [super parsedObjects:action objects:list];
    
    if ([action isEqualToString:@ACTION_GET_SCHEDULES])
    {
        if (list)
        {
            self.shiftsList = list;
            [self reloadData];
        }
    }
    else if ([action isEqualToString:@ACTION_ADD_JOEY_SCHEDULE])
    {
        [self getSchedules:self.dateSelected];
    }
    else if ([action isEqualToString:@ACTION_REMOVE_JOEY_SCHEDULE])
    {
        [self getSchedules:self.dateSelected];
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

@end
