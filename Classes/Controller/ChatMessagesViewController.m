//
//  ChatMessagesViewController.m
//  Joey
//
//  Created by Katia Maeda on 2016-08-09.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface ChatMessagesViewController () <JSONHelperDelegate, UIGestureRecognizerDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
    BOOL isProcessing;
    
    JSQMessagesBubbleImage *incomingBubble;
    JSQMessagesBubbleImage *incomingBubbleTailless;
    JSQMessagesBubbleImage *outgoingBubble;
    JSQMessagesBubbleImage *outgoingBubbleTailless;
    
    NSInteger lastRowDisplayed;
}

@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) UIImageView *snapshot;

@end

@implementation ChatMessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Title
    CGRect frame = self.navigationController.navigationBar.frame;
    frame.origin.y = 0;
    frame.size.width -= 150;
    UILabel *label1 = [[UILabel alloc] initWithFrame:frame];
    label1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    label1.font = [UIFont fontWithName:@"CenturyGothic-Bold" size:16];
    label1.textAlignment = NSTextAlignmentCenter;
    label1.textColor = [UIColor whiteColor];
    label1.text = self.channel.name;

    UIView *customTitleView = [[UIView alloc] initWithFrame:frame];
    [customTitleView addSubview:label1];
    self.navigationItem.titleView = customTitleView;
    
    
    /*
     //COMMENTED ON 23RD JULY 2018
    self.senderId = [NSString stringWithFormat:@"%@", [UrlInfo getJoeyId]];
     
     */
    Joey *joey = [[DatabaseHelper shared] getJoey];
    self.automaticallyScrollsToMostRecentMessage = YES;
    self.showLoadEarlierMessagesHeader = NO;
   
    /*
    //COMMENTED ON 23RD JULY 2018
    self.senderDisplayName = joey.firstName;
    
     */
    //Color of the keyboard (Dark to match everything else)
    self.inputToolbar.contentView.textView.keyboardAppearance = UIKeyboardAppearanceDark;
    self.inputToolbar.contentView.textView.placeHolder = @"";
    UIButton *cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [cameraButton setImage:[UIImage imageNamed:@"camera.png"] forState:UIControlStateNormal];
    self.inputToolbar.contentView.leftBarButtonItem = cameraButton;
    UIButton *sendButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [sendButton setImage:[UIImage imageNamed:@"send.png"] forState:UIControlStateNormal];
    self.inputToolbar.contentView.rightBarButtonItem = sendButton;
    
    //Color the inputview background
    self.inputToolbar.backgroundColor = [UIColor colorWithWhite:0 alpha:1.0];
    
    //Delete the avatars appearing next to the messages
    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    self.collectionView.collectionViewLayout.messageBubbleFont = [UIFont systemFontOfSize:14];
    self.collectionView.typingIndicatorDisplaysOnLeft = NO;
    
    
    /*
     //COMMENTED ON 23RD JULY 2018
    */
//     JSQMessagesBubbleImageFactory *imageFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRegularImage] capInsets:UIEdgeInsetsZero];
    
    JSQMessagesBubbleImageFactory *imageFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRegularImage]  capInsets:UIEdgeInsetsZero layoutDirection:UIUserInterfaceLayoutDirectionLeftToRight];

    
    
    incomingBubble = [imageFactory outgoingMessagesBubbleImageWithColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
    outgoingBubble = [imageFactory incomingMessagesBubbleImageWithColor:[UIColor colorWithWhite:.9 alpha:1.0]];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0 &&
        [UIView userInterfaceLayoutDirectionForSemanticContentAttribute:self.view.semanticContentAttribute] == UIUserInterfaceLayoutDirectionRightToLeft) {
        incomingBubble = [imageFactory incomingMessagesBubbleImageWithColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
        outgoingBubble = [imageFactory outgoingMessagesBubbleImageWithColor:[UIColor colorWithWhite:.9 alpha:1.0]];
    }
    
    /*
     //COMMENTED ON 23RD JULY 2018*/
//    JSQMessagesBubbleImageFactory *imageFactoryTailless = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRegularTaillessImage] capInsets:UIEdgeInsetsZero];
     
    JSQMessagesBubbleImageFactory *imageFactoryTailless = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRegularTaillessImage] capInsets:UIEdgeInsetsZero layoutDirection:UIUserInterfaceLayoutDirectionLeftToRight];

    
    incomingBubbleTailless = [imageFactoryTailless outgoingMessagesBubbleImageWithColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
    outgoingBubbleTailless = [imageFactoryTailless incomingMessagesBubbleImageWithColor:[UIColor colorWithWhite:.9 alpha:1.0]];

    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    UITapGestureRecognizer *hideKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    hideKeyboardGesture.numberOfTapsRequired = 1;
    hideKeyboardGesture.numberOfTouchesRequired = 1;
    hideKeyboardGesture.delegate = self;
    [self.collectionView addGestureRecognizer:hideKeyboardGesture];
    
    self.snapshot = [[UIImageView alloc] initWithFrame:self.collectionView.frame];
    [self.view addSubview:self.snapshot];
    self.snapshot.hidden = YES;
    
    isProcessing = NO;
    self.messages = [NSMutableArray array];
    [self getMessages:0 endDate:[NSDate date]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotificationWithMessage:) name:@NotificationsReceivedNewMessage object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.snapshot.frame = self.collectionView.frame;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@NotificationsReceivedNewMessage object:nil];
    [alertHelper dismissAlert];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getMessages:(NSDate *)startDate endDate:(NSDate *)endDate
{
    if (isProcessing)
    {
        return;
    }
    isProcessing = YES;
    [json chatChannelMessages:[self.channel.channelId intValue] startDate:[startDate timeIntervalSince1970] endDate:[endDate timeIntervalSince1970]];
}

-(void)loadMoreMessages
{
    if (isProcessing)
    {
        return;
    }

    if (lastRowDisplayed <= 10)
    {
        Message *message = [self.messages objectAtIndex:0];
        [self getMessages:0 endDate:message.date];
    }
}

-(void)reloadData:(NSArray *)newArray
{
    if (newArray.count < 25)
    {
        self.showLoadEarlierMessagesHeader = NO;
    }
    
    CGFloat bottomOffset = self.collectionView.contentSize.height - self.collectionView.contentOffset.y;
    if (self.messages.count <= 0)
    {
        bottomOffset = -1;
    }
    
    NSMutableArray *insertIndexPaths = [NSMutableArray array];
    NSMutableArray *insertMessagesBefore = [NSMutableArray array];
    NSMutableArray *insertMessagesAfter = [NSMutableArray array];
    Message *lastMessage = [self.messages lastObject];
    for (NSInteger i = 0; i < newArray.count; i++)
    {
        Message *msg = [newArray objectAtIndex:i];
        JSQMessage *message = nil;
        if ([msg.type isEqualToString:@"image"])
        {
            JSQPhotoMediaItem *media = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@""]];
            message = [[JSQMessage alloc] initWithSenderId:[NSString stringWithFormat:@"%@", msg.contact.contactId] senderDisplayName:msg.contact.name date:msg.date media:media];

            [ImageHelper loadImage:msg.message authorization:[UrlInfo apiAuthentication] withCompletionHandler:^(UIImage *image) {
                JSQPhotoMediaItem *media = [[JSQPhotoMediaItem alloc] initWithImage:image];
              
                /*
                 //COMMENTED ON 23RD JULY 2018
                // message.media = media;
                 */
                [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]]];
            }];
        }
        else
        {
            message = [[JSQMessage alloc] initWithSenderId:[NSString stringWithFormat:@"%@", msg.contact.contactId] senderDisplayName:msg.contact.name date:msg.date text:msg.message];
            
        }
        
        NSComparisonResult result = [lastMessage.date compare:msg.date];
        if (lastMessage.date == nil || result == NSOrderedDescending)
        {
            [insertMessagesBefore addObject:message];
            [insertIndexPaths addObject: [NSIndexPath indexPathForRow:i inSection:0]];
        }
        else if (result == NSOrderedAscending)
        {
            [insertMessagesAfter addObject:message];
        }
    }
    
    if (insertMessagesAfter.count > 0)
    {
        [self.messages addObjectsFromArray:insertMessagesAfter];
        [self finishSendingMessageAnimated:YES];
    }
    else
    {
        [UIView performWithoutAnimation:^{
            UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, 0);
            [self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];
            UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            self.snapshot.image = snapshotImage;
            self.snapshot.hidden = NO;
            
            [self.collectionView performBatchUpdates:^{
                [self.collectionView insertItemsAtIndexPaths:insertIndexPaths];
                [self.messages insertObjects:insertMessagesBefore atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, insertMessagesBefore.count)]];
            } completion:^(BOOL finished) {
                if (bottomOffset < 0)
                {
                    [self scrollToBottomAnimated:NO];
                    self.snapshot.hidden = YES;
                    self.snapshot.image = nil;
                }
                else
                {
                    self.collectionView.contentOffset = CGPointMake(0, self.collectionView.contentSize.height - bottomOffset);
                    [self.collectionView.collectionViewLayout invalidateLayout];
                    
                    self.snapshot.hidden = YES;
                    self.snapshot.image = nil;
                }
            }];
        }];
    }
}

#pragma mark - Notification

-(void)receivedNotificationWithMessage:(NSNotification *)notification
{
    Message *lastMessage = [self.messages lastObject];
    [self getMessages:[NSDateHelper date:lastMessage.date plusSeconds:1] endDate:[NSDate date]];
}

#pragma mark - JSONHelperDelegate

- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    if ([action isEqualToString:@ACTION_CHAT_MESSAGES])
    {
        isProcessing = NO;
        [self reloadData:list];
    }
    else if ([action isEqualToString:@ACTION_CHAT_NEW_MESSAGE])
    {
        [alertHelper dismissAlert];
    }
}

-(void)uploadData:(int64_t)totalSent totalToSend:(int64_t)totalToSend
{
    if (totalSent < totalToSend)
    {
        [alertHelper showProgressView:self message:[NSString stringWithFormat:NSLocalizedString(@"Sending picture %.1f%%", nil), ((CGFloat)totalSent/totalToSend)*100] totalSent:totalSent totalToSend:totalToSend];
    }
    else
    {
        [alertHelper dismissAlert];
    }
}

-(void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
    isProcessing = NO;
}

-(void)errorMessage:(NSString *)action message:(NSString *)message fields:(NSArray *)fieldsArray
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
    isProcessing = NO;
}

#pragma mark - Action Button

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

-(void)hideKeyboard
{
    [self.inputToolbar.contentView.textView resignFirstResponder];
}

-(void)didPressAccessoryButton:(UIButton *)sender
{
    [self hideKeyboard];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
    CameraViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"CameraViewController"];
    controller.pictureSelectedCompletion = ^(UIImage *image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (image)
            {
                JSQPhotoMediaItem *media = [[JSQPhotoMediaItem alloc] initWithImage:image];
                JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId senderDisplayName:self.senderDisplayName date:[NSDate date] media:media];
 
                [self.messages addObject:message];
                [self finishSendingMessageAnimated:YES];
                
                NSString *messageType = @"image";
                NSString *encodedImage = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                
                if (!encodedImage)
                {
                    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Problem getting the image", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
                    return;
                }
                
                NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"user_type":@"joey", @"user_id":[UrlInfo getJoeyId], @"message":encodedImage, @"type":messageType}];

                [json chatNewMessage:jsonDictionary channelId:[self.channel.channelId intValue]];
            }
        });
    };
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:controller animated:YES];
    });
}

- (void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date {
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId senderDisplayName:senderDisplayName date:date text:text];
    
    [self.messages addObject:message];
    [self finishSendingMessageAnimated:YES];

    NSString *inputMessage = text;
    NSString *messageType = @"text";
    
    inputMessage = [inputMessage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    inputMessage = [inputMessage stringByReplacingOccurrencesOfString:@"%0A" withString:@"\n"];
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"user_type":@"joey", @"user_id":[UrlInfo getJoeyId], @"message":inputMessage, @"type":messageType}];
    
    [json chatNewMessage:jsonDictionary channelId:[self.channel.channelId intValue]];
}

-(void)imageSelected:(id)sender
{
    /*
    //COMMENTED ON 23RD JULY 2018
    JSQMessagesCellButton *button = (JSQMessagesCellButton *)sender;
    JSQMessage *message = (JSQMessage *)button.buttonData;
    
    Message *lastMessage = [self.messages lastObject];
    [self getMessages:[NSDateHelper date:lastMessage.date plusSeconds:1] endDate:[NSDate date]];
    */
     
     
     
//    UIImageView *imageView = (UIImageView *)[message.media mediaView];
//    if ([imageView class] == [UIImageView class] && imageView.image) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
//            ImageViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ImageViewController"];
//            viewController.image = imageView.image;
//            [self.navigationController pushViewController:viewController animated:YES];
//        });
//    }
}

#pragma mark - JSQMessages Data Source methods

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.messages objectAtIndex:indexPath.row];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    if (indexPath.row < self.messages.count-1) {
        JSQMessage *nextMessage = [self.messages objectAtIndex:indexPath.row+1];
        NSString *newDate = [NSDateHelper stringDateFromDate:message.date];
        NSString *nextDate = [NSDateHelper stringDateFromDate:nextMessage.date];

        if ([message.senderId isEqualToString:nextMessage.senderId] && [newDate isEqualToString:nextDate])
        {
            return ([message.senderId isEqualToString:self.senderId]) ? incomingBubbleTailless : outgoingBubbleTailless;
        }
    }

    return ([message.senderId isEqualToString:self.senderId]) ? incomingBubble : outgoingBubble;
}

-(CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

-(CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.messages.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.row];
    
    if (indexPath.row > 0) {
        JSQMessage *previousMessage = [self.messages objectAtIndex:indexPath.row-1];
        NSString *newDate = [NSDateHelper stringDateFromDate:message.date];
        NSString *previousDate = [NSDateHelper stringDateFromDate:previousMessage.date];
        
        if ([newDate isEqualToString:previousDate])
        {
            return nil;
        }
        
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
}

-(CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.row];

    if (indexPath.row > 0) {
        JSQMessage *previousMessage = [self.messages objectAtIndex:indexPath.row-1];
        NSString *newDate = [NSDateHelper stringDateFromDate:message.date];
        NSString *previousDate = [NSDateHelper stringDateFromDate:previousMessage.date];
        
        if ([newDate isEqualToString:previousDate])
        {
            return 0.0;
        }
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];

    JSQMessage *message = [self.messages objectAtIndex:indexPath.row];
    
    if (cell.textView)
    {
        cell.textView.text = message.text;
        if ([message.senderId isEqualToString:self.senderId])
        {
            cell.textView.textColor = [UIColor blackColor];
        }
        else
        {
            cell.textView.textColor = [UIColor blackColor];
        }
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    cell.messageBubbleTopLabel.text = message.senderDisplayName;
    cell.messageBubbleTopLabel.font = [UIFont systemFontOfSize:10];
    cell.cellBottomLabel.text = [NSDateHelper stringTimeFromDate:message.date];
    cell.cellBottomLabel.font = [UIFont systemFontOfSize:10];

    
    /*
     //COMMENTED ON 23RD JULY 2018
  ///  cell.mediaImageView.buttonData = message;
   // [cell.mediaImageView addTarget:self action:@selector(imageSelected:) forControlEvents:UIControlEventTouchUpInside];
     
     */

    return cell;
}

-(void)collectionView:(JSQMessagesCollectionView *)collectionView header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    [self loadMoreMessages];
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    lastRowDisplayed = indexPath.row;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self loadMoreMessages];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadMoreMessages];
}

@end
