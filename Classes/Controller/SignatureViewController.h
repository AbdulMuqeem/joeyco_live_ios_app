//
//  SignatureViewController.h
//  Joey
//
//  Created by Katia Maeda on 2016-02-24.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "NavigationContainerViewController.h"

typedef void (^SignatureCompletionBlock)(UIImage *);

@interface SignatureViewController : NavigationContainerViewController

@property (nonatomic, strong) SignatureCompletionBlock signatureCompletionBlock;

@end
