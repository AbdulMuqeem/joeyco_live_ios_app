//
//  TutorialScrollViewController.m
//  Customer
//
//  Created by Katia Maeda on 2015-01-07.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface TutorialScrollViewController () <JSONHelperDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
    CLLocation *lastJoeyLocation;
}

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *page1BackgroundWidth;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *page2BackgroundWidth;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *page3BackgroundWidth;

@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;

@end

@implementation TutorialScrollViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
    
    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    lastJoeyLocation = [[LocationManager shared] lastKnownLocation];
    
    [json metaData:lastJoeyLocation.coordinate.latitude longitude:lastJoeyLocation.coordinate.longitude];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reloadPages];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)dealloc
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

-(void)reloadPages
{
    self.page1BackgroundWidth.constant = self.scrollView.frame.size.width;
    self.page2BackgroundWidth.constant = self.scrollView.frame.size.width;
    self.page3BackgroundWidth.constant = self.scrollView.frame.size.width;
}

- (void) parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    if ([action isEqualToString:@ACTION_GET_META_DATA])
    {
        if (list)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                MetaData *metaData = [list firstObject];
                [[DatabaseHelper shared] insertMetaData:metaData];
                [[DatabaseHelper shared] insertVehicles:metaData.vehicles];
                [[DatabaseHelper shared] insertZones:metaData.zones];
            });
        }
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertNoButtons:NSLocalizedString(@"Login fail", nil) type:AlertTypeNone message:message dismissAfter:3.0 from:self];
}

-(IBAction)startBouncingAction:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)applyNowAction:(id)sender
{
    BOOL hasInternet = [json hasInternetConnection:YES];
    if (hasInternet)
    {
        MetaData *metaData = [[DatabaseHelper shared] getMetaData];
        if (metaData)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:metaData.signupLink]];
        }
    }
}

-(IBAction)pageControlSelected:(id)sender
{
    NSInteger page = self.pageControl.currentPage;
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:YES];
}

-(IBAction)buttonSelect:(id)sender
{
    NSInteger page = 1;
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:YES];
}

-(IBAction)buttonSelect2:(id)sender
{
    NSInteger page = 2;
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:YES];
}

#pragma mark UIScrollView delegate

-(void)scrollViewDidScroll:(UIScrollView *)scroll
{
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = self.scrollView.contentOffset.x / pageWidth;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.pageControl.currentPage = page;
    });
}

@end

