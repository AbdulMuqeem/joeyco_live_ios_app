//
//  ScanQR.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 12/06/2019.
//  Copyright © 2019 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface ScanQR ()<ScannerViewControllerDelegate>

@end

@implementation ScanQR

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)scanNow:(id)sender {
    
    
    NSMutableArray *ordersList = [[DatabaseHelper shared] getListOfAllOrders];
    
    if ([ordersList count] >0 ) {
        
        [self openQRCode];
    }
    else{
        [self showConfirmationDialog];
    }
    
    
}




-(void)showConfirmationDialog{
    
    NSString * titleString = @"No orders exists!";
    NSString * contentString = @"There are no orders. Do you still want to open QR scanner?";
    
    ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                        iconImg:[UIImage imageNamed:@"qr_code"]
                                                backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                          title:titleString
                                                        content:contentString
                                                   buttonTitles:@[@"No", @"Yes"]
                                            confirmBtnTextColor:nil otherBtnTextColor:nil
                                             buttonPressedBlock:^(NSInteger btnIdx) {
                                                 
                                                 
                                                 if (btnIdx == 1) {
                                                     [self openQRCode];
                                                 }
                                                 else{
                                                     [popupView disappear];
                                                 }
                                                 
                                                 
                                                 
                                                 
                                             }];
    
    popupView.contentTextAlignment  = NSTextAlignmentCenter;
    
    [popupView present];
    
}

-(void)openQRCode{
    ScannerViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ScannerViewController"];
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
}


@end
