//
//  ScanditScanViewController.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 19/05/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SCANNERDelegate;

#import <ScanditBarcodeScanner/ScanditBarcodeScanner.h>
#import "Prefix.pch"
#import "ItineraryOrder_Listing_ObjectDetail.h"

NS_ASSUME_NONNULL_BEGIN


@interface ScanditScanViewController : UIViewController
{
    id<SCANNERDelegate> delegate;
}

@property (nonatomic) BOOL is_singleScan;
@property (nonatomic) BOOL is_pickup;

@property (weak, nonatomic) id<SCANNERDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *pickerParentView;
@property (weak, nonatomic) IBOutlet UIButton *upload_btn;
@property (weak, nonatomic) IBOutlet UILabel *items_count;
@property (weak, nonatomic) IBOutlet UIButton *close_btn;
@property(strong, nonatomic)NSMutableArray *tracking_ids;
@property(strong, nonatomic)NSMutableArray *valid_scanned_tracking_ids;
@property(strong, nonatomic)NSMutableArray *trackingIdWithTimeStamp;

@property(nonatomic,strong) ItineraryOrder_Listing_ObjectDetail * modelData;

- (IBAction)close_scanner:(id)sender;

@end


@protocol SCANNERDelegate
@optional
-(void)scanViewScreenBeingDismissed;

-(void)openSheetOnDismissOfScanScreen :(ItineraryOrder_Listing_ObjectDetail * )modelData;
@end

NS_ASSUME_NONNULL_END
