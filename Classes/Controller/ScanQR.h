//
//  ScanQR.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 12/06/2019.
//  Copyright © 2019 JoeyCo. All rights reserved.
//

#import "NavigationContainerViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScanQR : NavigationContainerViewController

@end

NS_ASSUME_NONNULL_END
