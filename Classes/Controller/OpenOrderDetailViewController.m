//
//  OpenOrderDetailViewController.m
//  Joey
//
//  Created by Katia Maeda on 2015-02-18.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface OpenOrderDetailViewController () <UITableViewDataSource, UITableViewDelegate, JSONHelperDelegate>
{
    GMSMarker *joeyMarker;
    Joey *joey;
    CLLocation *lastJoeyLocation;
    GMSPolyline *routePolyline;
    TravelMode mapRouteSetting;
    
    AlertHelper *alertHelper;
    JSONHelper *json;
    NSTimer *readInTimer;
}

@property (nonatomic, weak) IBOutlet GMSMapView *mapView;

@property (nonatomic, weak) IBOutlet UILabel *orderNumberTxt;
@property (nonatomic, weak) IBOutlet UILabel *readyInTxt;
@property (nonatomic, weak) IBOutlet UILabel *travelDistanceTxt;
@property (nonatomic, weak) IBOutlet UILabel *travelTimeTxt;

@property (nonatomic, weak) IBOutlet UITableView *pickupTable;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *pickupTableHeight;
@property (nonatomic, strong) NSMutableArray *pickupList;
@property (nonatomic, strong) NSMutableArray *pickupCellHeight;

@property (nonatomic, weak) IBOutlet UITableView *dropoffTable;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *dropoffTableHeight;
@property (nonatomic, strong) NSMutableArray *dropoffList;
@property (nonatomic, strong) NSMutableArray *dropoffCellHeight;

@end

@implementation OpenOrderDetailViewController

#pragma mark - lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    // Initiate GMSMapView
    self.mapView.settings.myLocationButton = YES;
    self.mapView.myLocationEnabled = YES;
    self.mapView.mapType = [self setMapType];
    lastJoeyLocation = [[LocationManager shared] lastKnownLocation];
    
    int route = [[DatabaseHelper shared] getMapRouteData];
    mapRouteSetting = (route == 0) ? TravelModeDriving : TravelModeBicycling;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationChanged:) name:@LocationManagerUpdateLocation object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];

    self.orderNumberTxt.text = self.order.num;
    self.pickupList = [self.order pickupList];
    self.dropoffList = [self.order dropOffList];
    
    CGFloat rowHeight = 40.0f;
    self.pickupCellHeight = [NSMutableArray array];
    for (NSInteger i = 0; i < [self.pickupList count]; ++i)
    {
        [self.pickupCellHeight addObject:[NSNumber numberWithInt:rowHeight]];
    }
    self.dropoffCellHeight = [NSMutableArray array];
    for (NSInteger i = 0; i < [self.dropoffList count]; ++i)
    {
        [self.dropoffCellHeight addObject:[NSNumber numberWithInt:rowHeight]];
    }
    
    [self.pickupTable reloadData];
    self.pickupTableHeight.constant = self.pickupTable.contentSize.height;
    [self.dropoffTable reloadData];
    self.dropoffTableHeight.constant = self.dropoffTable.contentSize.height;

    joey = [[DatabaseHelper shared] getJoey];
}

-(void)dealloc
{
    [self superDealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    [readInTimer invalidate];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
 
    self.pickupTable.delegate = nil;
    self.pickupTable.dataSource = nil;
    self.dropoffTable.delegate = nil;
    self.dropoffTable.dataSource = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self loadMapMarkers];
    
    [readInTimer invalidate];
    readInTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateReadyInTime) userInfo:nil repeats:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [readInTimer invalidate];
    [alertHelper dismissAlert];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)applicationWillEnterForeground:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationChanged:) name:@LocationManagerUpdateLocation object:nil];
    [readInTimer invalidate];
    readInTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateReadyInTime) userInfo:nil repeats:YES];
}

- (void)applicationDidEnterBackground:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@LocationManagerUpdateLocation object:nil];
    [readInTimer invalidate];
}

#pragma mark - Layout

-(void)updateReadyInTime
{
    dispatch_async(dispatch_get_main_queue(), ^{
        Task *firsTask = [self.pickupList firstObject];
        NSInteger countDown = [firsTask.dueTime doubleValue] - [[NSDate date] timeIntervalSince1970];
        if (countDown > 0)
        {
            self.readyInTxt.text = [NSString stringWithFormat:NSLocalizedString(@"Ready in %@", nil), [NSDateHelper stringFromCountDown:countDown]];
            self.readyInTxt.textColor = [UIColor colorWithRed:0.83 green:0.4 blue:0.15 alpha:1.0];
        }
        else
        {
            self.readyInTxt.text = NSLocalizedString(@"Ready to pick up", nil);
            self.readyInTxt.textColor = [UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0];
            [readInTimer invalidate];
        }
    });
}

#pragma mark - Map

-(void)locationChanged:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@LocationManagerUpdateLocation])
    {
        CLLocation *newLocation = [[LocationManager shared] lastKnownLocation];
        if (newLocation && ![newLocation isEqual:[NSNull null]])
        {
            double distance = [MapHelper getDistanceBetweenLocations:lastJoeyLocation.coordinate and:newLocation.coordinate];
            lastJoeyLocation = newLocation;
            if (distance > 50)
            {
                [self getGoogleMapRouteInfo];
            }
            
            [self loadMapMarkers];
        }
    }
}

-(void)showJoeyMarker
{
    // Called inside dispatch_get_main_queue
    if (joeyMarker)
    {
        joeyMarker.map = nil;
        joeyMarker = nil;
    }
    
    joeyMarker = [[GMSMarker alloc] init];
    joeyMarker.title = joey.firstName;
    joeyMarker.position = CLLocationCoordinate2DMake(lastJoeyLocation.coordinate.latitude, lastJoeyLocation.coordinate.longitude);
    joeyMarker.map = self.mapView;
    joeyMarker.icon = [UIImage imageNamed:@"joey_run.png"];
}

-(void)getGoogleMapRouteInfo
{
    NSMutableArray *locations = [self.order tasksLocationList];
    [locations insertObject:lastJoeyLocation atIndex:0];

    [MapHelper getGoogleMapRouteInfo:locations travelMode:mapRouteSetting succeeded:^(GoogleMapRouteInfo *info) {
        self.order.totalDistance = info.totalDistance;
        self.order.totalDuration = info.totalDuration;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.travelDistanceTxt.text = [MapHelper stringFromDistance:[self.order.distance doubleValue]];
           
            self.travelTimeTxt.text = [self convertTime:[self.order.eta intValue]];

            if (routePolyline)
            {
                routePolyline.map = nil;
                routePolyline = nil;
            }
            routePolyline = info.polyline;
            routePolyline.strokeWidth = 3;
            routePolyline.strokeColor = [UIColor colorWithRed:0 green:137/255 blue:255/255 alpha:1.0];
            routePolyline.map = self.mapView;
        });
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@", errorMsg);
    }];
}

-(NSString *)convertTime:(int)time
{
    if (time < 1) return @"";
    float hours = floor(time / 60);
    float minutes = (time % 60);
    if (hours == 0) return [NSString stringWithFormat:@"%.0f min", minutes];
    return [NSString stringWithFormat:@"%.0f hr %.0f min", hours, minutes];
}

-(void)loadMapMarkers
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mapView clear];

        NSMutableArray *markers = [NSMutableArray new];
        if (lastJoeyLocation)
        {
            [self showJoeyMarker];
            [self getGoogleMapRouteInfo];
            [markers addObject:joeyMarker];
        }

        for (Task *task in self.order.tasks)
        {
            GMSMarker *vendorMarker = [[GMSMarker alloc] init];
            vendorMarker.title = task.contact.name;
            vendorMarker.position = CLLocationCoordinate2DMake([task.location.locationLatitude doubleValue], [task.location.locationLongitude doubleValue]);
            vendorMarker.map = self.mapView;
            vendorMarker.userData = task;
            [markers addObject:vendorMarker];
            
            NSNumber *statusId = task.statusId;
            MetaStatus *status = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_TASK_FAILURE];
            
            if ([task.type isEqualToString:@TASK_PICKUP])
            {
                if ([statusId intValue] == [status.statusId intValue])
                {
                    vendorMarker.icon = [UIImage imageNamed:@"error.png"];
                }
                else if ([task isAllConfirmed])
                {
                    vendorMarker.icon = [UIImage imageNamed:@"success.png"];
                }
                else
                {
                    vendorMarker.icon = [UIImage imageNamed:@"restaurants.png"];
                }
            }
            else if ([task.type isEqualToString:@TASK_DROP_OFF])
            {
                if ([statusId intValue] == [status.statusId intValue])
                {
                    vendorMarker.icon = [UIImage imageNamed:@"error.png"];
                }
                else if ([task isAllConfirmed])
                {
                    vendorMarker.icon = [UIImage imageNamed:@"success.png"];
                }
                else
                {
                    vendorMarker.icon = [UIImage imageNamed:@"customer.png"];
                }
            }
            else if ([task.type isEqualToString:@TASK_RETURN])
            {
                vendorMarker.icon = [UIImage imageNamed:@"default_restaurants.png"];
            }
        }
        
        GMSCoordinateBounds *bounds = [MapHelper mapFitAllMarkers:markers];
        [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30]];
    });
}

#pragma mark - JSONHelperDelegate

- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    [super parsedObjects:action objects:list];
    
    if ([action isEqualToString:@ACTION_ACCEPT_ORDER])
    {
        [self performSelector:@selector(goBack) withObject:nil afterDelay:.3];
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{
    if ([action isEqualToString:@ACTION_ACCEPT_ORDER])
    {
        if (!message || [message isEqualToString:@""])
        {
            message = NSLocalizedString(@"This order was accepted by other joey", nil);
        }
        
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"The order is not available", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
    }
    else
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
    }
}

#pragma mark - Navigation

-(void)goBack
{
    [readInTimer invalidate];
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)showConfirmAcceptOrderAlert:(id)sender
{
    __unsafe_unretained typeof(self) weakSelf = self;
    [alertHelper showAlertWithButtons:NSLocalizedString(@"Do you want to accept this order?", nil) message:NSLocalizedString(@"Click yes to accept!", nil) from:self withOkHandler:^(void) {
        if (weakSelf && [weakSelf isKindOfClass:[OpenOrderDetailViewController class]])
        {
            [weakSelf performSelector:@selector(acceptOrder) withObject:nil afterDelay:.1];
        }
    }];
}

-(void)acceptOrder
{
    if (!lastJoeyLocation || [lastJoeyLocation isEqual:[NSNull null]])
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Order not accepted", nil) message:NSLocalizedString(@"Could not get user location", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    if (!self.order || !self.order.orderId)
    {
        CLS_LOG(@"Problem accepting order:%@", self.order.orderId);
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Order not accepted", nil) message:NSLocalizedString(@"Could not get order data", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    if (![json hasInternetConnection:YES]) return;
    
    NSMutableArray *locations = [self.order pickupLocationList];
    [locations insertObject:lastJoeyLocation atIndex:0];
    
    __unsafe_unretained typeof(self) weakSelf = self;
    [MapHelper getGoogleMapRouteInfo:locations travelMode:mapRouteSetting succeeded:^(GoogleMapRouteInfo *info) {
        if (![info isKindOfClass:[GoogleMapRouteInfo class]]) {
            return;
        }
        
        NSMutableArray *ordersArray = [NSMutableArray array];
        int eta = 0;
        
        NSMutableArray *pickupList = [self.order pickupList];
        for (int i = 0; i < pickupList.count; i++)
        {
            Task *task = [pickupList objectAtIndex:i];
            
            int duration = 0;
            if (i < info.durations.count)
            {
                duration = [[info.durations objectAtIndex:i] intValue];
            }

            eta += duration;
            
            if (!task || !task.taskId)
            {
                CLS_LOG(@"Problem accepting order, task:%@/ order:%@", task.taskId, task.orderId);
                return;
            }
            
            NSMutableDictionary *pickup = [NSMutableDictionary dictionaryWithDictionary:@{@"id":task.taskId, @"eta":[NSNumber numberWithInt:eta]}];
            [ordersArray addObject:pickup];
        }
        
        NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":[UrlInfo getJoeyId], @"orders":ordersArray}];
        NSMutableDictionary *orderDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"dictionary":jsonDictionary, @"orderId":self.order.orderId}];
        
        if (weakSelf && [weakSelf isKindOfClass:[OpenOrderDetailViewController class]])
        {
            [weakSelf performSelector:@selector(jsonAcceptOrder:) withObject:orderDictionary afterDelay:0.1];
        }
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@", errorMsg);
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Order not accepted", nil) message:errorMsg from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
    }];
}

-(void)jsonAcceptOrder:(NSMutableDictionary *)dictionary
{
    NSMutableDictionary *jsonDictionary = [dictionary objectForKey:@"dictionary"];
    NSNumber *orderId = [dictionary objectForKey:@"orderId"];
    [json acceptOrder:[orderId intValue] pickupInfo:jsonDictionary];
}

#pragma mark - UITableView methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.pickupTable == tableView)
    {
        return self.pickupList.count;
    }
    
    return self.dropoffList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.pickupTable == tableView)
    {
        if (indexPath.row < 0 || indexPath.row >= self.pickupCellHeight.count)
        {
            return 0;
        }
        
        NSNumber *height = [self.pickupCellHeight objectAtIndex:indexPath.row];
        return [height floatValue];
    }

    if (indexPath.row < 0 || indexPath.row >= self.dropoffCellHeight.count)
    {
        return 0;
    }
    
    NSNumber *height = [self.dropoffCellHeight objectAtIndex:indexPath.row];
    return [height floatValue];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"orderSimpleCell";
    
    TwoLabelsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[TwoLabelsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    __weak TwoLabelsTableCell *weakCell = cell;

    dispatch_async(dispatch_get_main_queue(), ^{
        if (indexPath.row < 0 || (self.pickupTable == tableView  && indexPath.row >= self.pickupList.count) || (self.dropoffTable == tableView  && indexPath.row >= self.dropoffList.count))
        {
            return;
        }
        
        Task *task = nil;
        if (self.pickupTable == tableView)
        {
            task = [self.pickupList objectAtIndex:indexPath.row];
        }
        else
        {
            task = [self.dropoffList objectAtIndex:indexPath.row];
        }
        
        weakCell.textView1.text = [NSString stringWithFormat:NSLocalizedString(@"Name : %@", nil), task.contact.name];
        weakCell.textView1Height.constant = [weakCell.textView1 sizeThatFits:CGSizeMake(weakCell.textView1.frame.size.width,300)].height;
        weakCell.label2.text = [NSString stringWithFormat:NSLocalizedString(@"Address : %@", nil), task.location.address.name];
        
        if (self.pickupTable == tableView)
        {
            CGFloat height = weakCell.textView1Height.constant + 20;
            if (indexPath.row < self.pickupCellHeight.count)
            {
                CGFloat cellHeight = [[self.pickupCellHeight objectAtIndex:indexPath.row] floatValue];
                if (height > cellHeight)
                {
                    NSNumber *labelNumber = [NSNumber numberWithFloat:ceil(height)];
                    [self.pickupCellHeight replaceObjectAtIndex:indexPath.row withObject:labelNumber];
                    [tableView reloadData];
                    self.pickupTableHeight.constant = self.pickupTable.contentSize.height;
                }
            }
        }
        else
        {
            CGFloat height = weakCell.textView1Height.constant + 20;
            if (indexPath.row < self.dropoffCellHeight.count)
            {
                CGFloat cellHeight = [[self.dropoffCellHeight objectAtIndex:indexPath.row] floatValue];
                if (height > cellHeight)
                {
                    NSNumber *labelNumber = [NSNumber numberWithFloat:ceil(height)];
                    [self.dropoffCellHeight replaceObjectAtIndex:indexPath.row withObject:labelNumber];
                    [tableView reloadData];
                    self.dropoffTableHeight.constant = self.dropoffTable.contentSize.height;
                }
            }
        }
    });
    
    return weakCell;
}

@end

