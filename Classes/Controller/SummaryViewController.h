//
//  SummaryViewController.h
//  Joey
//
//  Created by Katia Maeda on 2016-05-20.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "NavigationContainerViewController.h"

@interface SummaryViewController : NavigationContainerViewController

@end
