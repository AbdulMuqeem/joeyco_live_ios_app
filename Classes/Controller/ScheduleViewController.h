//
//  ScheduleViewController.h
//  Joey
//
//  Created by Katia Maeda on 2015-02-24.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "NavigationContainerViewController.h"

@interface ScheduleViewController : NavigationContainerViewController

@end
