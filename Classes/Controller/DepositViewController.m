//
//  DepositViewController.m
//  Joey
//
//  Created by Katia Maeda on 2015-02-23.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"
//@class QuizViewController;
@interface DepositViewController () <UITableViewDataSource, UITableViewDelegate, JSONHelperDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
    
    UIImage *imageSelected;
    
    CustomKeyboardToolbar *toolbar;
}

@property (nonatomic, weak) IBOutlet CustomScrollView *scrollView;

@property (nonatomic, weak) IBOutlet UITableView *depositTable;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *depositTableHeight;
@property (nonatomic, strong) NSMutableArray *depositList;

@property (nonatomic, weak) IBOutlet CustomTextField *amountField;
@property (nonatomic, weak) IBOutlet RoundedButton *takePictureBtn;
@property (nonatomic, weak) IBOutlet UIDatePicker *datePicker;

@end

@implementation DepositViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    [json cashOnHand];
    
    self.datePicker.maximumDate = [NSDate date];
    if (@available(iOS 13.4, *)) {
        _datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [self.scrollView flashScrollIndicators];
    
    // Register for TextField
    toolbar = [[CustomKeyboardToolbar alloc] initWithView:self.scrollView];
    [[NSNotificationCenter defaultCenter] addObserver:toolbar selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:toolbar selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
    
    self.amountField.fieldType = @FIELD_MONEY;
    self.amountField.inputAccessoryView = toolbar;
    [self.amountField.undoManager disableUndoRegistration];
    toolbar.textFields = @[self.amountField];
    
    UITapGestureRecognizer *hideKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    hideKeyboardGesture.numberOfTapsRequired = 1;
    hideKeyboardGesture.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:hideKeyboardGesture];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [toolbar updateParameters:self.view.frame.size.height y:self.view.frame.origin.y];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [alertHelper dismissAlert];
}

-(void)dealloc
{
    [self superDealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:toolbar];
    
    self.depositTable.delegate = nil;
    self.depositTable.dataSource = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)dateChanged:(id)sender
{
    self.datePicker.maximumDate = [NSDate date];
}

#pragma mark - TextField

-(void)textFieldDidChange:(NSNotification *)notification
{
    NSString *digitsOnly = @"0123456789";
    
    CustomTextField *customField = (CustomTextField *)notification.object;
    if ([customField.fieldType isEqualToString:@FIELD_MONEY])
    {
        NSString *paymentString = [customField.text filterString:digitsOnly];
        NSRange range = [paymentString rangeOfString:@"^0*" options:NSRegularExpressionSearch];
        paymentString = [paymentString stringByReplacingCharactersInRange:range withString:@""];
        
        if (paymentString.length == 0) paymentString = @"$0.00";
        else if (paymentString.length == 1) paymentString = [NSString stringWithFormat:@"$0.0%@", paymentString];
        else if (paymentString.length == 2) paymentString = [NSString stringWithFormat:@"$0.%@", paymentString];
        else
        {
            NSUInteger pointLocation = paymentString.length - 2;
            paymentString = [NSString stringWithFormat:@"$%@.%@", [paymentString substringToIndex:pointLocation], [paymentString substringFromIndex:pointLocation]];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            customField.text = paymentString;
        });
    }
}

-(void)hideKeyboard
{
    [self.view endEditing:YES];
}

#pragma mark - JSONHelperDelegate

- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    [super parsedObjects:action objects:list];
    
    if ([action isEqualToString:@ACTION_CASH_ON_HAND])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.depositList = list;
            [self.depositTable reloadData];
            self.depositTableHeight.constant = self.depositTable.contentSize.height;
        });
    }
    else if ([action isEqualToString:@ACTION_ADD_DEPOSIT])
    {
        NSString *message = [list firstObject];
        __unsafe_unretained typeof(self) weakSelf = self;
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Deposit", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:^{
            if (weakSelf && [weakSelf isKindOfClass:[DepositViewController class]])
            {
                [weakSelf cashOnHand];
            }
        }];
    }
}

-(void)cashOnHand
{
    [json cashOnHand];
}

-(void)uploadData:(int64_t)totalSent totalToSend:(int64_t)totalToSend
{
    if (totalSent < totalToSend)
    {
        [alertHelper showProgressView:self message:[NSString stringWithFormat:NSLocalizedString(@"Sending picture %.1f%%", nil), ((CGFloat)totalSent/totalToSend)*100] totalSent:totalSent totalToSend:totalToSend];
    }
    else
    {
        [alertHelper dismissAlert];
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

- (void) errorMessage:(NSString *)action message:(NSString *)message fields:(NSArray *)fieldsArray
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

#pragma mark - UITableView methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.depositList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DepositCell";
    
    TwoLabelsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[TwoLabelsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    __weak TwoLabelsTableCell *weakCell = cell;

    dispatch_async(dispatch_get_main_queue(), ^{
        if (indexPath.row >= self.depositList.count)
        {
            return;
        }
        
        Info *info = [self.depositList objectAtIndex:indexPath.row];

        weakCell.label1.text = info.title;
        weakCell.label2.text = info.value;
        weakCell.label2Width.constant = [weakCell.label2 sizeThatFits:CGSizeMake(1000, weakCell.label2.frame.size.height)].width;

        UIFont *currentFont = weakCell.label1.font;
        if ([info.title isEqualToString:@"Amount to deposit"])
        {
            UIFont *boldFont = [UIFont boldSystemFontOfSize:currentFont.pointSize];
            [weakCell.label1 setFont:boldFont];
        }
        else
        {
            UIFont *normalFont = [UIFont systemFontOfSize:currentFont.pointSize];
            [weakCell.label1 setFont:normalFont];
        }
    });

    return weakCell;
}

#pragma mark - Navigation

-(IBAction)takePictureAction
{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_OnBoarding" bundle:nil];
//<<<<<<< HEAD
//
//=======
//
//>>>>>>> 84057137e09637348258e63fae8c0385be30f1c1
//      QuizViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"QuizViewController"];
//
//           [self presentViewController:vc animated:true completion:nil];
    CameraViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CameraViewController"];
    controller.pictureSelectedCompletion = ^(UIImage *image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (image)
            {
                imageSelected = image;

                UIColor *greenColor = [UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0];
                self.takePictureBtn.backgroundColor = greenColor;
                [self.takePictureBtn setTitle:NSLocalizedString(@"Ok", nil) forState:UIControlStateNormal];
            }
            else
            {
                [alertHelper showAlertNoButtons:NSLocalizedString(@"Error", nil) type:AlertTypeNone message:NSLocalizedString(@"Picture not taken.", nil) dismissAfter:3.0 from:self];

                UIColor *grayColor = [UIColor colorWithRed:0.70 green:0.70 blue:0.70 alpha:1.0];
                self.takePictureBtn.backgroundColor = grayColor;
                [self.takePictureBtn setTitle:NSLocalizedString(@"Take Picture", nil) forState:UIControlStateNormal];
            }
        });
    };

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:controller animated:YES];
    });
}

-(IBAction)submitDepositAction
{
    NSString *digitsOnly = @"0123456789.";
    NSString *amountText = [self.amountField.text filterString:digitsOnly];
    
    // Verify required fields
    if ([amountText isEqualToString:@""])
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Required", nil) message:NSLocalizedString(@"Amount is a required field.", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    if (!imageSelected)
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Required", nil) message:NSLocalizedString(@"Please take a picture.", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    [self hideKeyboard];
    
    NSInteger depositeDate = [self.datePicker.date timeIntervalSince1970];
    depositeDate += [NSDateHelper getTime:[NSDate date]];
    
    NSString *encodedImage = [UIImagePNGRepresentation(imageSelected) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    if (!encodedImage) {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Deposit Error", nil) message:NSLocalizedString(@"Problem getting the image", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"amount":amountText, @"date":[NSNumber numberWithInteger:depositeDate], @"image":encodedImage}];
    
    [self clearFields];

    [json addDeposit:jsonDictionary];
}

-(void)clearFields
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.amountField.text = @"";
        imageSelected = nil;
        
        UIColor *grayColor = [UIColor colorWithRed:0.70 green:0.70 blue:0.70 alpha:1.0];
        self.takePictureBtn.backgroundColor = grayColor;
        [self.takePictureBtn setTitle:NSLocalizedString(@"Take Picture", nil) forState:UIControlStateNormal];
    });
}

@end
