//
//  OrderViewController.m
//  Customer
//
//  Created by Katia Maeda on 2015-06-17.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface OrderViewController () <SlidingPagesDataSource, SlidingPagesDelegate, JSONHelperDelegate>
{
    JSONHelper *json;
    AlertHelper *alertHelper;

    BOOL isSyncing;
    
    CLLocation *lastJoeyLocation;
}

@property (nonatomic, weak) IBOutlet UIView *contentView;

@property (nonatomic, weak) IBOutlet GraySegmentedControl *segmentedControl;
@property (nonatomic, weak) IBOutlet UIImageView *ordersIcon;
@property (nonatomic, weak) IBOutlet UIImageView *activeOrdersIcon;

@property (nonatomic, strong) SlidingPagesViewController *slider;

@property (nonatomic, strong) NewOrdersViewController *openView;
@property (nonatomic, strong) ActiveOrdersViewController *activeView;

@end

@implementation OrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];

    // Setup SlidingPages
    self.slider = [[SlidingPagesViewController alloc] init];
    self.slider.dataSource = self;
    self.slider.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
    self.slider.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.contentView addSubview:self.slider.view];
    [self addChildViewController:self.slider];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
    self.openView = [storyboard instantiateViewControllerWithIdentifier:@"NewOrdersViewController"];
    self.openView.delegate = self;
    self.activeView = [storyboard instantiateViewControllerWithIdentifier:@"ActiveOrdersViewController"];
    self.activeView.delegate = self;
    
    json = [[JSONHelper alloc] init:self andDelegate:self];
    alertHelper = [[AlertHelper alloc] init];

    Joey *joey = [[DatabaseHelper shared] getJoey];
    if (!joey)
    {
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        //[self.navigationController presentViewController:controller animated:YES completion:nil];
        
        if (@available(iOS 13.0, *)) {
                  [controller setModalPresentationStyle: UIModalPresentationFullScreen];
              } else {
                  // Fallback on earlier versions
                
              }
        [self.navigationController presentViewController:controller animated:YES completion:nil];
        
        return;
    }

    isSyncing = NO;

    
    [self sendPendingConfirmations];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.segmentedControl.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatelastJoeyLocationOnServer:) name:@LocationManagerUpdateLocation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectivityDidChange:) name:kReachabilityChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNewOrderNotification) name:@NotificationsReceivedNewOrder object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedAcceptedOrderNotification) name:@NotificationsReceivedAcceptedOrder object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedTransferedOrderNotification) name:@NotificationsReceivedOrderTransferred object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedOrderNotificationWithMessage:) name:@NotificationsOrderMessage object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [alertHelper dismissAlert];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

-(void)dealloc
{
    [self superDealloc];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)applicationWillEnterForeground:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatelastJoeyLocationOnServer:) name:@LocationManagerUpdateLocation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectivityDidChange:) name:kReachabilityChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNewOrderNotification) name:@NotificationsReceivedNewOrder object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedAcceptedOrderNotification) name:@NotificationsReceivedAcceptedOrder object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedTransferedOrderNotification) name:@NotificationsReceivedOrderTransferred object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedOrderNotificationWithMessage:) name:@NotificationsOrderMessage object:nil];

    [self getOrderList];
}

- (void)applicationDidEnterBackground:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadTabsIcons:(NSArray *)newOrders active:(NSArray *)activeOrders
{
    self.ordersIcon.image = [self tabsIconImage:(int)newOrders.count];
    self.activeOrdersIcon.image = [self tabsIconImage:(int)activeOrders.count];
}

-(UIImage *)tabsIconImage:(int)number
{
    int sizeTotal = 20;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, sizeTotal, sizeTotal)];
    [view setBackgroundColor:[UIColor redColor]];
    
    view.layer.cornerRadius = sizeTotal/2;
    view.layer.masksToBounds = YES;
    
    int size = 18;
    CGRect frame = CGRectMake((sizeTotal-size)/2, (sizeTotal-size)/2, size, size);
    UILabel *labelNumber = [[UILabel alloc]initWithFrame:frame];
    [labelNumber setBackgroundColor:[UIColor clearColor]];
    [labelNumber setTextColor:[UIColor whiteColor]];
    labelNumber.text = [NSString stringWithFormat:@"%d", number];
    labelNumber.textAlignment = NSTextAlignmentCenter;
    UIFont *font = [UIFont boldSystemFontOfSize:14];
    [labelNumber setFont:font];
//    labelNumber.adjustsFontSizeToFitWidth = YES;
    [view addSubview:labelNumber];
    
    // Create image
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - Map

-(void)updatelastJoeyLocationOnServer:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@LocationManagerUpdateLocation])
    {
        CLLocation *newLocation = [[LocationManager shared] lastKnownLocation];
        if (newLocation && ![newLocation isEqual:[NSNull null]])
        {
            double distance = [MapHelper getDistanceBetweenLocations:lastJoeyLocation.coordinate and:newLocation.coordinate];
            if (distance > 50)
            {
                [self updateViewsLocation:newLocation];
            }
        }
    }
}

-(void)updateViewsLocation:(CLLocation *)newLocation
{
    lastJoeyLocation = newLocation;
    [self.openView updateLastLocation:newLocation];
    [self.activeView updateLastLocation:newLocation];
}

#pragma mark - Internet

-(void)connectivityDidChange:(NSNotification *)notification
{
    NetworkStatus internetStatus = [self.reachability currentReachabilityStatus];
    
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
        [self.activeView connectivityDidChange:NO];
    }
    else
    {
        [self.activeView connectivityDidChange:YES];
        [self performSelector:@selector(sendPendingConfirmations) withObject:nil afterDelay:2.0];
    }
}

-(void)sendPendingConfirmations
{
    if (self.sendingConfirmations > 0)
    {
        return;
    }
    
    MetaStatus *status = [[DatabaseHelper shared] getMetaStatusByKey:@JCO_ORDER_PICK_UP_SUCCESS];
    Joey *joey = [[DatabaseHelper shared] getJoey];
    
    self.sendingConfirmations = 0;
    
    NSMutableArray *confirmations = [[DatabaseHelper shared] getConfirmationsWith:ConfirmationStatusNotSent];
    
    for (Confirmation *confirmation in confirmations)
    {
        if (!status || !status.value || ![UrlInfo getJoeyId] || !confirmation || !confirmation.name)
        {
            CLS_LOG(@"Problem sending pending confirmations, status:%@, status.value:%@, joeyId:%@, confirmation:%@, confirmation.name:%@", status, status.value, [UrlInfo getJoeyId], confirmation, confirmation.name);
        }
        
        NSMutableDictionary *jsonDictionary = nil;
        if (confirmation.savedImage.length > 0)
        {
            NSString *encodedImage = [confirmation.savedImage base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":joey.joeyId, confirmation.name:encodedImage}];
        }
        else
        {
            jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":joey.joeyId, @"name":confirmation.name, @"confirmed":[NSNumber numberWithBool:YES], @"status":status.value}];
            
            if ([confirmation.inputType isEqualToString:@CONFIRMATION_INPUT_TYPE_TEXT])
            {
                [jsonDictionary setObject:confirmation.savedInputField forKey:confirmation.name];
            }
        }
        
        self.sendingConfirmations++;
        [self.json confirmation:confirmation.url method:confirmation.method confirmationInfo:jsonDictionary action:@ACTION_OFFLINE_CONFIRMATION];
    }
}

#pragma mark - actions

-(void)refreshOrderList
{
    BOOL hasInternet = [json hasInternetConnection:YES];
    if (hasInternet)
    {
        [self startRefreshAnimation];
        [self getOrderList];
    }
}

-(void)getOrderList
{
    if (isSyncing)
    {
        return;
    }
    
    NSString *type = @"new";
    if (self.segmentedControl.selectedSegmentIndex == 1)
    {
        type = @"active";
    }

    isSyncing = YES;
    [json orderList:type];
}

#pragma mark - Notifications

-(void)receivedNewOrderNotification
{
    [self refreshOrderList];
    [alertHelper showAlertNoButtons:NSLocalizedString(@"Refresh", nil) type:AlertTypeNone message:NSLocalizedString(@"Order list has been updated", nil) dismissAfter:3.0 from:self];
}

-(void)receivedTransferedOrderNotification
{
    __unsafe_unretained typeof(self) weakSelf = self;
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Refresh", nil) message:NSLocalizedString(@"Your order has been transferred", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:^{
        CLS_LOG(@"ActiveOrdersViewController receivedTransferedOrderNotification");
        if (weakSelf && [weakSelf isKindOfClass:[OrderViewController class]])
        {
            [weakSelf performSelector:@selector(refreshOrderList) withObject:nil];
        }
    }];
}

-(void)receivedAcceptedOrderNotification
{
    __unsafe_unretained typeof(self) weakSelf = self;
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Refresh", nil) message:NSLocalizedString(@"Order accepted", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:^{
        CLS_LOG(@"OrderViewController receivedAcceptedOrderNotification");
        if (weakSelf && [weakSelf isKindOfClass:[OrderViewController class]])
        {
            [weakSelf performSelector:@selector(refreshOrderList) withObject:nil];
        }
    }];
}

-(void)receivedOrderNotificationWithMessage:(NSNotification *)notification
{
    NSDictionary *userData = [notification userInfo];
    NSString *message = [userData objectForKey:@"message"];
    
    __unsafe_unretained typeof(self) weakSelf = self;
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Order", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:^{
        CLS_LOG(@"OrderViewController receivedAcceptedOrderNotification");
        if (weakSelf && [weakSelf isKindOfClass:[OrderViewController class]])
        {
            [weakSelf performSelector:@selector(refreshOrderList) withObject:nil];
        }
    }];
}

#pragma mark - JSONHelperDelegate

-(void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    [super parsedObjects:action objects:list];
    
    if ([action isEqualToString:@ACTION_GET_ORDER_LIST])
    {
        NSMutableArray *newOrders = [NSMutableArray array];
        NSMutableArray *activeOrders = [NSMutableArray array];

        for (Order *order in list)
        {
            if (order.joey)
            {
                if ([order.joey.joeyId intValue] == [[UrlInfo getJoeyId] intValue])
                {
                    [activeOrders addObject:order];
                }
            }
            else
            {
                [newOrders addObject:order];
            }
        }
        
        [[DatabaseHelper shared] deleteOrderIfPossible];
        [[DatabaseHelper shared] insertOrders:activeOrders];
        [self loadTabsIcons:newOrders active:activeOrders];
        
        [self.openView updateOrders:newOrders];
        [self.activeView updateOrders:activeOrders];
        
        if (activeOrders.count <= 0)
        {
            [self.slider scrollToPage:0 animated:NO];
            [self.segmentedControl setSelectedSegmentIndex:0];
        }
        
        if (!list || list.count <= 0)
        {
            [alertHelper showAlertNoButtons:@"" type:AlertTypeNone message:NSLocalizedString(@"No new orders", nil) dismissAfter:3.0 from:self];
        }
        
        [self stopRefreshAnimation];
        isSyncing = NO;
    }
    else if ([action isEqualToString:@ACTION_OFFLINE_CONFIRMATION])
    {
        Confirmation *confirmation = [list firstObject];
        if ([confirmation.isConfirmed boolValue])
        {
            [[DatabaseHelper shared] updateConfirmation:confirmation.confirmationId isConfirmed:ConfirmationStatusSent];
        }
        
        self.sendingConfirmations--;

        if (self.sendingConfirmations <= 0)
        {
            [self getOrderList];
        }
    }
}

-(void)errorMessage:(NSString *)action message:(NSString *)message
{
    [self stopRefreshAnimation];
    isSyncing = NO;
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

#pragma mark - Segmented Control methods

- (IBAction)changeSlidingPage:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 0 || sender.selectedSegmentIndex == 1)
    {
        [self.slider scrollToPage:sender.selectedSegmentIndex animated:YES];
        return;
    }
    
    [self.slider scrollToPage:0 animated:NO];
    return;
}

#pragma mark -
#pragma mark SlidingPagesDataSource methods

-(int)numberOfPagesInSlidingPages
{
    return 2;
}

-(NSString *)titleForSlidingPagesAtIndex:(int)index
{
    switch (index) {
        case 0:
            return NSLocalizedString(@"New Orders", nil);
            break;
        case 1:
            return NSLocalizedString(@"Active Orders", nil);
            break;
    }
    
    return NSLocalizedString(@"Order", nil);
}

-(UIViewController *)pageForSlidingPagesAtIndex:(int)index
{
    if (index == 0)
    {
        return self.openView;
    }
    return self.activeView;
}

-(void)contentViewHeight:(CGFloat)height
{

}

@end
