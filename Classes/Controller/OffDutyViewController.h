//
//  OffDutyViewController.h
//  Joey
//
//  Created by Katia Maeda on 2015-11-05.
//  Copyright © 2015 JoeyCo. All rights reserved.
//

#import "NavigationContainerViewController.h"

@interface OffDutyViewController : NavigationContainerViewController

@end
