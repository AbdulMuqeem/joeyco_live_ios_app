//
//  ScheduleFullViewController.m
//  Joey
//
//  Created by Katia Maeda on 2015-12-03.
//  Copyright © 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface ScheduleFullViewController () <JTCalendarDataSource, JSONHelperDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
    
    //used for user selected date
    NSDate *selectedDate;
    BOOL didUserSelectDate;
}

@property (nonatomic, weak) IBOutlet RoundedButton *calendarButton;

@property (nonatomic, weak) IBOutlet JTCalendarMenuView *calendarTitleView;
@property (nonatomic, weak) IBOutlet JTCalendarContentView *calendarContentView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *calendarContentViewHeight;

@property (nonatomic, weak) IBOutlet UITableView *shiftsTable;
@property (nonatomic, strong) NSMutableArray *shiftsList;

@property (nonatomic, strong) JTCalendar *calendar;

@end

@implementation ScheduleFullViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    self.calendar = [JTCalendar new];
    self.calendar.calendarAppearance.calendar.firstWeekday = 1; // Sunday == 1, Saturday == 7
    self.calendar.calendarAppearance.dayCircleRatio = 9. / 10.;
    self.calendar.calendarAppearance.ratioContentMenu = 1;
    self.calendar.calendarAppearance.weekDayFormat = JTCalendarWeekDayFormatShort;
    self.calendar.calendarAppearance.isSelectable = YES;
    
    [self.calendar setMenuMonthsView:self.calendarTitleView];
    [self.calendar setContentView:self.calendarContentView];
    [self.calendar setDataSource:self];
    
    selectedDate = nil;
    didUserSelectDate = NO;
    
    [self getFullSchedule];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.calendar reloadData]; // Must be call in viewDidAppear
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [alertHelper dismissAlert];
    [self.calendar hideViews];
}

-(void)dealloc
{
    [self superDealloc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getFullSchedule
{
    NSDate *date;
    if (didUserSelectDate) {
        didUserSelectDate = NO;
        date = selectedDate;
    }
    else {
        date = self.calendar.currentDate;
    }
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = nil;
    NSDate *startDate, *endDate;
    
    if (self.calendar.calendarAppearance.isWeekMode)
    {
        components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday|NSCalendarUnitWeekOfMonth fromDate:date];
        switch ([components weekday]) {
            case 1: break;
            case 2: [components setDay:[components day]-1]; break;
            case 3: [components setDay:[components day]-2]; break;
            case 4: [components setDay:[components day]-3]; break;
            case 5: [components setDay:[components day]-4]; break;
            case 6: [components setDay:[components day]-5]; break;
            case 7: [components setDay:[components day]-6]; break;
        }
        
        startDate = [calendar dateFromComponents:components];
        [components setDay:[components day]+6];
        endDate = [calendar dateFromComponents:components];
    }
    else
    {
        components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
        [components setDay:1];
        startDate = [calendar dateFromComponents:components];
        
        [components setMonth:[components month]+1];
        [components setDay:0];
        endDate = [calendar dateFromComponents:components];
    }
    
    [json getFullSchedule:[startDate timeIntervalSince1970] endDate:[endDate timeIntervalSince1970]];
}

#pragma mark - Actions

- (IBAction)ToggleCalendarMode
{
    self.calendar.calendarAppearance.isWeekMode = !self.calendar.calendarAppearance.isWeekMode;
    [self.calendarButton setTitle:(self.calendar.calendarAppearance.isWeekMode ? NSLocalizedString(@"Month View", nil) : NSLocalizedString(@"Week View", nil)) forState:UIControlStateNormal];
    [self getFullSchedule];
    
    [self transitionExample];
}

- (void)transitionExample
{
    CGFloat newHeight = 250;
    if(self.calendar.calendarAppearance.isWeekMode){
        newHeight = 75.;
    }
    
    [UIView animateWithDuration:.5
                     animations:^{
                         self.calendarContentViewHeight.constant = newHeight;
                         [self.view layoutIfNeeded];
                     }];
    
    [UIView animateWithDuration:.25
                     animations:^{
                         self.calendarContentView.layer.opacity = 0;
                     }
                     completion:^(BOOL finished) {
                         [self.calendar reloadAppearance];
                         
                         [UIView animateWithDuration:.25
                                          animations:^{
                                              self.calendarContentView.layer.opacity = 1;
                                          }];
                     }];
}

#pragma mark - JTCalendarDataSource

-(BOOL)calendarHaveEvent:(JTCalendar *)calendar date:(NSDate *)date
{
    return NO;
}

-(void)calendarDidDateSelected:(JTCalendar *)calendar date:(NSDate *)date
{
    didUserSelectDate = YES;
    selectedDate = date;
    
    [self getFullSchedule];

}

- (void)calendarDidChangedDates:(NSDate *)date
{
    [self getFullSchedule];
}

#pragma mark - UITableView methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.shiftsList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SlotCell";
    
    TwoLabelsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[TwoLabelsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
//    TwoLabelsTableCell *cell = [[TwoLabelsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    __weak TwoLabelsTableCell *weakCell = cell;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (indexPath.row < 0 || indexPath.row >= self.shiftsList.count)
        {
            return;
        }
        
        Shift *shift = [self.shiftsList objectAtIndex:indexPath.row];
        
        weakCell.label1.text = [NSString stringWithFormat:@"%@ - %@ - %@", [NSDateHelper stringDateFromDate2:shift.startTime], [NSDateHelper stringTimeFromDate:shift.startTime], [NSDateHelper stringTimeFromDate:shift.endTime]];
        
        weakCell.label2.text = (shift.vehicle.name) ?[NSString stringWithFormat:@"%@ - %@", shift.zone.name, shift.vehicle.name] : shift.zone.name  ;

        weakCell.button.buttonData = shift;
        [weakCell.button setImage:[UIImage imageNamed:@"checkbox-checked.png"] forState:UIControlStateNormal];
        [weakCell.button addTarget:self action:@selector(toggleShift:) forControlEvents:UIControlEventTouchUpInside];
    });

    return weakCell;
}

-(void)toggleShift:(id)sender
{
    CustomButton *button = (CustomButton *)sender;
    Shift *shift = (Shift *)button.buttonData;
    

    
    if ([shift.can_cancel integerValue] == 1){

        [alertHelper showAlertWithButtons:NSLocalizedString(@"Remove schedule", nil) message:NSLocalizedString(@"Are you sure you want to remove this schedule?", nil) from:self withOkHandler:^(void) {
            [json removeJoeySchedule:shift.shiftId];
        }];
    }
    else{
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Cannot Remove", nil) message:NSLocalizedString(@"This schedule cannot be removed,because either the shift is active or about to start", nil) from:self buttonTitle:@"Ok" withHandler:^{
            
        }];
        
        
    }
    
 
    
    
}

#pragma mark - JSONHelperDelegate

- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    [super parsedObjects:action objects:list];
    
    if ([action isEqualToString:@ACTION_GET_FULL_SCHEDULE])
    {
        if (list)
        {
            self.shiftsList = list;
            [self.shiftsTable reloadData];
        }
    }
    else if ([action isEqualToString:@ACTION_REMOVE_JOEY_SCHEDULE])
    {
        [self getFullSchedule];
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

@end
