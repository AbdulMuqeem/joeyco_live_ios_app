//
//  ScheduleEdit2ViewController.h
//  Joey
//
//  Created by Katia Maeda on 2015-12-02.
//  Copyright © 2015 JoeyCo. All rights reserved.
//

#import "NavigationContainerViewController.h"
#import "NewShift.h"

@interface ScheduleEdit2ViewController : NavigationContainerViewController

@property(nonatomic, strong) NewShift *shift;

@end
