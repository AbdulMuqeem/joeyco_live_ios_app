//
//  MMCallAPI.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 03/01/2019.
//  Copyright © 2019 JoeyCo. All rights reserved.
//

#import "MMCallAPI.h"
#import <JoeyCoUtilities/JoeyCoUtilities.h>

#import "JSONHelper.h"

@interface MMCallAPI ()<JSONHelperDelegate>
{

JSONHelper *json;
}
@end

@implementation MMCallAPI

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
}




/***
 * Update MM about order status update
 * */

- (void) updateMMAboutStatusUpdate{
    
    NSString *mm_token =[self generateToken:[self.sprintID stringValue]];
    
    
    NSString *PARAMS  = [NSString stringWithFormat:@"%@&token=%@&status=%@",self.sprintID,mm_token,self.JSON_response];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@",MM_SprintStatusUpdate,PARAMS];

    
    json = [[JSONHelper alloc] init:self andDelegate:self];

    [json updateMMAboutStatusUpdate_API:URL];
    
}




/**
 *
 * Calling webservice
 * Check if sprint ID exists or not
 *
 * */

-(void)checkIfSprintExists:(NSString *)url  {
    
    [json checkIfSprintExists_API:url];
}





- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list {
    
}



/**
 Generate TOKEN
 * SHA-256 TOKEN

 */

-(NSString *) generateToken:(NSString *)sprintID {
    
    NSString * mm_salt = @"qS85efRf^#Hlb9RPZR0&y#$";
    
    NSString *payload =[NSString stringWithFormat:@"%@|%@|%@",mm_salt,sprintID,mm_salt];
    
    NSString * token = [CryptographyHelper sha256HashFor:payload];
    
    NSLog(@"mm_token : %@",token);
    
    return token;
    
}

@end
