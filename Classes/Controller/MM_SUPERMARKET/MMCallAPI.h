//
//  MMCallAPI.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 03/01/2019.
//  Copyright © 2019 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 MM SUPERMARKET API
 */
#define MM_SprintStatusUpdate @"http://dev-delivery-services.mmfoodmarket.com/api/joeyco/sprintstatusupdate?sprintId="



NS_ASSUME_NONNULL_BEGIN

@interface MMCallAPI : UIViewController

@property(nonatomic,assign)NSNumber *sprintID;
@property(nonatomic,strong)NSString * JSON_response;


-(void) updateMMAboutStatusUpdate;

@end

NS_ASSUME_NONNULL_END
