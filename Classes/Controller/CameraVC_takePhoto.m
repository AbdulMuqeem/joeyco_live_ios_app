//
//  CameraViewController.m
//  Customer
//
//  Created by Katia Maeda on 2015-01-30.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface CameraVC_takePhoto () <UIImagePickerControllerDelegate, UINavigationControllerDelegate,JSONHelperDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;

}

@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@property (nonatomic, weak) IBOutlet RoundedButton *takePictureButton;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *takePictureButtonWidth;

@property (nonatomic, strong) UIBarButtonItem *doneButton;

@end

@implementation CameraVC_takePhoto

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    alertHelper = [[AlertHelper alloc] init];
    
    
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        self.takePictureButton.hidden = YES;
        self.takePictureButtonWidth.constant = 0;
    }
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Upload", nil) style:UIBarButtonItemStylePlain target:self action:@selector(doneAction)];
    [self.doneButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15.0f]} forState: UIControlStateNormal];
    self.doneButton.tintColor = [UIColor colorWithRed:0.87 green:1.0 blue:0 alpha:1.0];
    self.navigationItem.rightBarButtonItem = nil;
    
    NSString *backTitle = @"<";
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0 &&
        [UIView userInterfaceLayoutDirectionForSemanticContentAttribute:self.view.semanticContentAttribute] == UIUserInterfaceLayoutDirectionRightToLeft) {
        backTitle = @">";
    }
    
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:backTitle style:UIBarButtonItemStylePlain target:self action:@selector(navigatingBack)];
//    [backButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:25.0f]} forState: UIControlStateNormal];
//    backButton.tintColor = [UIColor colorWithRed:0.87 green:1.0 blue:0 alpha:1.0];
//    
//    
    UIImage *backBtnGreen = [UIImage imageNamed:@"backBtnGreen.png"];
    UIButton *btnImg = [UIButton buttonWithType:UIButtonTypeCustom];
    btnImg.bounds = CGRectMake( 0, 0, backBtnGreen.size.width, backBtnGreen.size.height );
    [btnImg setImage:backBtnGreen forState:UIControlStateNormal];

    UIBarButtonItem *backBarBtn = [[UIBarButtonItem alloc] initWithImage:backBtnGreen style:UIBarButtonItemStylePlain target:self action:@selector(navigatingBack)];

    self.navigationItem.leftBarButtonItem = backBarBtn;
    
    self.imageView.image = self.image;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [alertHelper dismissAlert];
}

-(void)navigatingBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)doneAction
{
    self.pictureSelectedCompletion(self.imageView.image);
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)takePictureAction:(id)sender
{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera."
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else{
        //other action
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:picker animated:YES completion:NULL];
        });
    }
    
   
}

- (IBAction)selectPictureAction:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:picker animated:YES completion:NULL];
    });
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:NULL];

    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    UIImage *compressedImage = [ImageHelper compress:originalImage];

    dispatch_async(dispatch_get_main_queue(), ^{
        self.imageView.image = compressedImage;
        self.navigationItem.rightBarButtonItem = self.doneButton;
    });
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)closeDialogAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];

    
}

- (IBAction)uploadPictureToServer:(id)sender {
    
    
//    self.pictureSelectedCompletion = ^(UIImage *image) {

    if (self.imageView.image )
    {
        BOOL hasInternet = [json hasInternetConnection:NO];
        if (hasInternet)
        {
            //                if (!confirmation || !confirmation.name) {
            //                    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Confirmation Error", nil) message:NSLocalizedString(@"Problem with confirmation parameters", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
            //                    return;
            //                }
            
            //                currentConfirmation = confirmation;
            NSString *encodedImage = [UIImagePNGRepresentation(self.imageView.image ) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            //                NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":[UrlInfo getJoeyId], confirmation.name:encodedImage}];
            //                [json confirmation:confirmation.url method:confirmation.method confirmationInfo:jsonDictionary action:@ACTION_CONFIRMATION];
            //
            /**
             Upload image to server
             */
            [self uploadImageToServer:self.selectedIndexOnSheet img:encodedImage];
            
        }
        else
        {
            //                [[DatabaseHelper shared] updateConfirmation:confirmation.confirmationId image:image];
            //                confirmation.isConfirmed = [NSNumber numberWithBool:YES];
            //
            //                [self.confirmationTable reloadData];
            //                [self checkAllConfirmation];
        }
    }
    else
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Picture not sent", nil) message:NSLocalizedString(@"Picture confirmation not sent. Please try again.", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
    }
        
   // };
    
}


/*
 Update To server
 */
- (void)uploadImageToServer:(NSInteger)indexPathRow img:(NSString *)encodedImage{
    
    
    
    OrderStatus * dataModel = [self.StatusObj objectAtIndex:indexPathRow];
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":[UrlInfo getJoeyId],
                                                                                          @"image":encodedImage,
                                                                                          @"task_id":[self.order_task.taskId stringValue],
                                                                                          @"order_id":[self.order_task.sprintId stringValue],
                                                                                          @"status_id":dataModel.orderStatusID,
                                                                                          
                                                                                          }];
    
    
    //upload image
    [json updateSystemStatusImageUpload:jsonDictionary];
    
    
}



/**
 On response of complain section success
 */
- (void)parsedObjects:(NSString *)action objects:(NSMutableDictionary *)responseString {
    
    
    
    ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                        iconImg:[UIImage imageNamed:@"send"]
                                                backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                          title:@"Success"
                                                        content:[responseString objectForKey:@"message"]
                                                   buttonTitles:@[@"Ok"]
                                            confirmBtnTextColor:nil otherBtnTextColor:nil
                                             buttonPressedBlock:^(NSInteger btnIdx) {
                                                 
                                
        if(self.is_itinerary){
           
            [self dismissViewControllerAnimated:YES completion:nil];

            
            //Calling delegate method to update in itineranry screen
            [self.delegate dropStatusCompletedUsingImage:self.modelData];
                 
            
        }else
        {
            /**
            confirmationDone*/
            [NSNotificationCenter.defaultCenter postNotificationName:@NotificationsConfirmationDoneImageUpload object:self];
            
              [self dismissViewControllerAnimated:YES completion:nil];
        }
        
                                            
                                                 
                                               

                                                 
                                             }];
    
    popupView.contentTextAlignment  = NSTextAlignmentCenter;
    
    [popupView present];
    
}



@end
