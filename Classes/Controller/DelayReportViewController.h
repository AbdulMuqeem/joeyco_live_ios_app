//
//  DelayReportViewController.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 16/11/2018.
//  Copyright © 2018 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MaterialBottomSheet.h"
#import "DelayReportViewController.h"
#import "Order.h"
#import "Task.h"
#import "MetaData.h"
#import "JSONHelper.h"

NS_ASSUME_NONNULL_BEGIN

@interface DelayReportViewController : UIViewController

{
    
    
}

- (IBAction)closeButton:(id)sender;
- (IBAction)RadioButtonTapped:(id)sender;

@property(nonatomic,strong)NSMutableArray * delayStatusObj;

@property (weak, nonatomic) IBOutlet UITableView *tableVIew;
@property(nonatomic,assign)int selectedIndex;


//Order
@property(nonatomic,strong)Order * order;
@property(nonatomic,strong)Task *  task;
@property(nonatomic,strong)NSString * taskType;

@end

NS_ASSUME_NONNULL_END
