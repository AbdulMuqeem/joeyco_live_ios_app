//
//  Checklist_bottomSheetVC.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 02/06/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol scanChecklistDelegate;

#import "DelayReportViewController.h"
#import "Order.h"
#import "Task.h"
#import "MetaData.h"
#import "ItineraryOrder_Listing_ObjectDetail.h"

#import "JSONHelper.h"
NS_ASSUME_NONNULL_BEGIN

@interface Checklist_bottomSheetVC : UIViewController
{
    id<scanChecklistDelegate> delegate;
}
@property (weak, nonatomic) id<scanChecklistDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *bottomSheet_title;
@property (weak, nonatomic) IBOutlet UILabel *bottomSheet_subtitle;


@property (strong, nonatomic)  NSString *text_bottomSheet_title;
@property (strong, nonatomic)  NSString *text_bottomSheet_subtitle;

@property(nonatomic,strong)NSMutableArray *itemsArray;

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic,assign)int selectedIndex;

- (IBAction)closeButton:(id)sender;

//Order
@property(nonatomic,strong)Order * order;
@property(nonatomic,strong)Task *  task;
@property(nonatomic,strong)NSString * taskType;

//Model Data of clicked obj
@property(nonatomic,strong)ItineraryOrder_Listing_ObjectDetail * modelData;

@end


@protocol scanChecklistDelegate
-(void)optionSelected:(int)selectedRow checklistObj:(Checklist_bottomSheetVC *)obj;
@end


NS_ASSUME_NONNULL_END
