//
//  LoginViewController.m
//  Joey
//
//  Created by Katia Maeda on 2015-02-18.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

//@class QuizViewController;

@interface LoginViewController () <JSONHelperDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
    CLLocation *lastJoeyLocation;
}

@property (nonatomic, weak) IBOutlet UITextField *usernameField;
@property (nonatomic, weak) IBOutlet UITextField *passwordField;

@property (nonatomic, strong) CustomKeyboardToolbar *toolbar;

@end

@implementation LoginViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
  //  [self checkPhone:@"(905) 821-68450"];

//    [self checkPhone:@"+12345678911"];
//    [self checkPhone:@"+********12345678910"];
//    [self checkPhone:@"03452962859"];
//    [self checkPhone:@"+923452962859"];
//
//    [self checkPhone:@"1416419188"];
//    [self checkPhone:@"15143643320121"];
//    [self checkPhone:@"+15143643320121"];
//    [self checkPhone:@"+15143643320"];
//    [self checkPhone:@"+1514364332"];
//    [self checkPhone:@"4165284879"];
//    [self checkPhone:@"1-866-938-1119"];
//    [self checkPhone:@"866-938-1119"];




    

    
    
    
    self.toolbar = [[CustomKeyboardToolbar alloc] initWithView:self.view];
    self.usernameField.inputAccessoryView = self.toolbar;
    self.passwordField.inputAccessoryView = self.toolbar;
    self.toolbar.textFields = @[self.usernameField, self.passwordField];
    
    UITapGestureRecognizer *hideKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    hideKeyboardGesture.numberOfTapsRequired = 1;
    hideKeyboardGesture.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:hideKeyboardGesture];
    
    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    lastJoeyLocation = [[LocationManager shared] lastKnownLocation];
    
    [json metaData:lastJoeyLocation.coordinate.latitude longitude:lastJoeyLocation.coordinate.longitude];
    
 
    //Checking ip
   // [json CheckIp];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [alertHelper dismissAlert];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hideKeyboard
{
    [self.view endEditing:YES];
}

#pragma mark - JSONHelperDelegate

- (IBAction)loginAction
{
    // Verify required fields
    if ([self.usernameField.text isEqualToString:@""] || [self.passwordField.text isEqualToString:@""])
    {
        [alertHelper showAlertNoButtons:NSLocalizedString(@"Required field", nil) type:AlertTypeNone message:NSLocalizedString(@"Please fill all required fields", nil) dismissAfter:3.0 from:self];
        return;
    }
    
    [self.toolbar hideKeyboard];

    [json signIn:self.usernameField.text password:self.passwordField.text];
}

- (void) parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    if ([action isEqualToString:@ACTION_SIGN_IN])
    {
        Joey *joey = [list firstObject];
        
        // save on database
        if (joey)
        {
            [[DatabaseHelper shared] insertJoey:joey];
            [UrlInfo setJoeyId:[joey.joeyId integerValue]];
            
            NSString *deviceToken = [[DatabaseHelper shared] getSetting:@"deviceToken"];
            [json registerDevice:[NSString stringWithFormat:@"%@", deviceToken]];
        }
    }
    else if ([action isEqualToString:@ACTION_REGISTER_DEVICE])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if ([action isEqualToString:@ACTION_GET_META_DATA])
    {
        if (list)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                MetaData *metaData = [list firstObject];
                [[DatabaseHelper shared] insertMetaData:metaData];
                [[DatabaseHelper shared] insertVehicles:metaData.vehicles];
                [[DatabaseHelper shared] insertZones:metaData.zones];
            });
        }
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertNoButtons:NSLocalizedString(@"Login fail", nil) type:AlertTypeNone message:message dismissAfter:3.0 from:self];
}

#pragma mark - Navigation

-(IBAction)openTermsOfUse:(id)sender
{
         [self.toolbar hideKeyboard];

    BOOL hasInternet = [json hasInternetConnection:YES];
    if (hasInternet)
    {
        MetaData *metaData = [[DatabaseHelper shared] getMetaData];
        if (metaData)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:metaData.termsLink]];
        }
    }
}

-(IBAction)openPrivacyPolicy:(id)sender
{
    [self.toolbar hideKeyboard];
    
    BOOL hasInternet = [json hasInternetConnection:YES];
    if (hasInternet)
    {
        MetaData *metaData = [[DatabaseHelper shared] getMetaData];
        if (metaData)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:metaData.privacyLink]];
        }
    }
}

-(IBAction)openWebsite:(id)sender
{
    [self.toolbar hideKeyboard];
    
    BOOL hasInternet = [json hasInternetConnection:YES];
    if (hasInternet)
    {
        MetaData *metaData = [[DatabaseHelper shared] getMetaData];
        if (metaData)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:metaData.homeLink]];
        }
    }
}

-(IBAction)signupAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    
    if (@available(iOS 13.0, *)) {
              [controller setModalPresentationStyle: UIModalPresentationFullScreen];
          } else {
              // Fallback on earlier versions
             

          }
     [self.navigationController presentViewController:controller animated:YES completion:nil];
    
}

@end
