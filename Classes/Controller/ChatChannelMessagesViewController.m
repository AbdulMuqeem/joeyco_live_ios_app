//
//  ChatChannelMessagesViewController.m
//  Joey
//
//  Created by Katia Maeda on 2016-01-28.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface ChatChannelMessagesViewController () <UITableViewDataSource, UITableViewDelegate, JSONHelperDelegate, UITextViewDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
    CustomKeyboardToolbar *toolbar;

    UIImage *imageSelected;
    
    BOOL isProcessing;
}

@property (nonatomic, weak) IBOutlet CustomScrollView *scrollView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *subviewHeight;

@property (nonatomic, weak) IBOutlet CustomTableView *messageTable;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *messageTableHeight;
@property (nonatomic, strong) NSMutableArray *messageList;
@property (nonatomic, strong) NSMutableArray *preLoadedCell;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *messageBoxHeight;
@property (nonatomic, weak) IBOutlet UIView *messageInputBorder;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *messageInputBorderHeight;
@property (nonatomic, weak) IBOutlet UITextView *messageInput;
@property (nonatomic, weak) IBOutlet RoundedButton *messageCamera;
@property (nonatomic, weak) IBOutlet RoundedButton *messageSend;
@property (nonatomic, weak) IBOutlet UIImageView *messageImage;
@property (nonatomic, weak) IBOutlet RoundedButton *messageImageDelete;

@end

@implementation ChatChannelMessagesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    // Register for TextField
    toolbar = [[CustomKeyboardToolbar alloc] initWithView:self.scrollView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:toolbar selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    toolbar.textFields = @[self.messageInput];
    self.messageInput.inputAccessoryView = toolbar;
    
    UITapGestureRecognizer *hideKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    hideKeyboardGesture.numberOfTapsRequired = 1;
    hideKeyboardGesture.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:hideKeyboardGesture];
    
    [ImageHelper deleteCache];
    [self clearFields];

    isProcessing = NO;
    [self getMessages:[NSDate date]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotificationWithMessage:) name:@NotificationsReceivedNewMessage object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@NotificationsReceivedNewMessage object:nil];
    [alertHelper dismissAlert];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

-(void)dealloc
{
    [self superDealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:toolbar];
    
    self.messageTable.delegate = nil;
    self.messageTable.dataSource = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)applicationWillEnterForeground:(NSNotification *)notification
{
    [self getMessages:[NSDate date]];
}

-(void)keyboardWillShow:(NSNotification*)notification
{
    [toolbar performSelectorOnMainThread:@selector(keyboardWillShow:) withObject:notification waitUntilDone:YES];
    [self scrollToBottom];
}

-(void)hideKeyboard
{
    [self.view endEditing:YES];
}

-(void)clearFields
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.messageInput.text = @"";
        self.messageInput.hidden = NO;
        self.messageInputBorder.hidden = NO;

        imageSelected = nil;
        self.messageImage.hidden = YES;
        self.messageImageDelete.hidden = YES;
        
        [self setupBoxHeight];
    });
}

-(void)showImageField
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.messageInput.text = @"";
        self.messageInput.hidden = YES;
        self.messageInputBorder.hidden = YES;
        
        self.messageImage.hidden = NO;
        self.messageImageDelete.hidden = NO;
        
        self.messageBoxHeight.constant = 140;
    });
}

-(void)scrollToBottom
{
    [self scrollToPosition:self.messageList.count-1 scrollPosition:UITableViewScrollPositionBottom];
}

-(void)scrollToPosition:(NSInteger)position scrollPosition:(UITableViewScrollPosition)scrollPosition
{
    if (self.messageList.count <= 0) {
        return;
    }
    
    NSIndexPath *positionIndex = [NSIndexPath indexPathForRow:position inSection:0];
    [self scrollToRowAtIndexPathBottom:positionIndex];
    
    [self performSelector:@selector(scrollToRowAtIndexPathBottom:) withObject:positionIndex afterDelay:0.5];
}

-(void)scrollToRowAtIndexPathBottom:(NSIndexPath*)position
{
    [self.messageTable scrollToRowAtIndexPath:position atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

#pragma mark - Notification

-(void)receivedNotificationWithMessage:(NSNotification *)notification
{
//    NSDictionary *userData = [notification userInfo];
//    NSString *message = [userData objectForKey:@"message"];
    
    [self getMessages:[NSDate date]];
}

#pragma mark - UITextViewDelegate

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.scrollView scrollRectToVisible:textView.superview.frame animated:YES];
}

- (void)textViewDidChange:(UITextView *)textView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.messageInputBorderHeight.constant = [textView sizeThatFits:CGSizeMake(textView.frame.size.width,1000)].height + 5;
        self.subviewHeight.constant = self.messageInputBorderHeight.constant + 30 + [toolbar getHeyboardHeight];
        [toolbar updateContentHeight:self.subviewHeight.constant + self.messageTableHeight.constant + 5];
    });
}

-(void)setupBoxHeight
{
    self.messageBoxHeight.constant = self.messageInputBorderHeight.constant + 20;
    self.messageTableHeight.constant = self.view.frame.size.height - self.messageBoxHeight.constant - 5;
}

#pragma mark - JSONHelperDelegate

- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    [super parsedObjects:action objects:list];
    
    if ([action isEqualToString:@ACTION_CHAT_MESSAGES])
    {
        [self updateTable:list];
        isProcessing = NO;
    }
    else if ([action isEqualToString:@ACTION_CHAT_NEW_MESSAGE])
    {
        [alertHelper dismissAlert];
        [self getMessages:[NSDate date]];
    }
}

-(void)updateTable:(NSArray *)newArray
{
    if (!self.messageList)
    {
        self.messageList = [NSMutableArray arrayWithArray:newArray];
        self.preLoadedCell = [NSMutableArray array];
        for (NSInteger i = 0; i < [self.messageList count]; ++i)
        {
            [self.preLoadedCell addObject:[NSNull null]];
        }
        [self loadAllCells];
        [self.messageTable reloadData];
        [self scrollToBottom];
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{

        Message *firstPreviousMessage = [self.messageList firstObject];
        Message *firstNewMessage = [newArray firstObject];
        NSComparisonResult result = [firstPreviousMessage.date compare:firstNewMessage.date];
        if (result == NSOrderedSame && self.messageList.count == newArray.count)
        {
            return;
        }
        else if (result == NSOrderedDescending)
        {
            NSMutableArray *insertIndexPaths = [[NSMutableArray alloc] init];
            for (NSInteger i = 0; i < newArray.count; i++)
            {
                Message *message = [newArray objectAtIndex:i];
                if ([message.date timeIntervalSince1970] == [firstPreviousMessage.date timeIntervalSince1970] &&
                    [message.contact.name isEqualToString:firstPreviousMessage.contact.name] &&
                    [message.message isEqualToString:firstPreviousMessage.message]) {
                    for (NSInteger j = i-1; j >= 0; j--)
                    {
                        [insertIndexPaths addObject: [NSIndexPath indexPathForRow:j inSection:0]];
                        Message *message = [newArray objectAtIndex:j];
                        [self.messageList insertObject:message atIndex:0];
                        [self.preLoadedCell insertObject:[NSNull null] atIndex:0];
                    }
                    break;
                }
            }
            
            if (insertIndexPaths.count > 0)
            {
                CGSize beforeContentSize = self.messageTable.contentSize;
                [self loadAllCells];
                [self.messageTable reloadData];

                CGSize afterContentSize = self.messageTable.contentSize;
                CGPoint afterContentOffset = self.messageTable.contentOffset;
                CGPoint newContentOffset = CGPointMake(afterContentOffset.x, afterContentOffset.y + afterContentSize.height - beforeContentSize.height);
                [self.messageTable setContentOffset:CGPointMake(newContentOffset.x,newContentOffset.y) animated:NO];
            }
        }
        else if (result == NSOrderedAscending || result == NSOrderedSame)
        {
            Message *lastPreviousMessage = [self.messageList lastObject];
            NSMutableArray *insertIndexPaths = [[NSMutableArray alloc] init];
            for (NSInteger i = (newArray.count - 1); i > 0; i--)
            {
                Message *message = [newArray objectAtIndex:i];
                if ([message.date timeIntervalSince1970] == [lastPreviousMessage.date timeIntervalSince1970] &&
                    [message.contact.name isEqualToString:lastPreviousMessage.contact.name] &&
                    [message.message isEqualToString:lastPreviousMessage.message]) {
                    NSInteger initJ = self.messageList.count;
                    i++;
                    for (NSInteger j = initJ; j < initJ + (newArray.count - i); j++)
                    {
                        if (i < newArray.count)
                        {
                            [insertIndexPaths addObject: [NSIndexPath indexPathForRow:j inSection:0]];
                            Message *message = [newArray objectAtIndex:i++];
                            [self.messageList addObject:message];
                            [self.preLoadedCell addObject:[NSNull null]];
                        }
                    }
                    break;
                }
            }
            
            if (insertIndexPaths.count > 0)
            {
                [self loadAllCells];
                [self.messageTable insertRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationTop];
                NSInteger lastPosition = ((NSIndexPath *)[insertIndexPaths lastObject]).row;
                [self scrollToPosition:lastPosition scrollPosition:UITableViewScrollPositionBottom];
            }
            
            [self scrollToBottom];
        }
    });
}

-(void)getMessages:(NSDate *)date
{
    if (isProcessing)
    {
        return;
    }
    isProcessing = YES;
    [json chatChannelMessages:[self.channel.channelId intValue] startDate:0 endDate:[date timeIntervalSince1970]];
}

-(void)uploadData:(int64_t)totalSent totalToSend:(int64_t)totalToSend
{
    if (totalSent < totalToSend)
    {
        [alertHelper showProgressView:self message:[NSString stringWithFormat:NSLocalizedString(@"Sending picture %.1f%%", nil), ((CGFloat)totalSent/totalToSend)*100] totalSent:totalSent totalToSend:totalToSend];
    }
    else
    {
        [alertHelper dismissAlert];
    }
}

-(void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

-(void)errorMessage:(NSString *)action message:(NSString *)message fields:(NSArray *)fieldsArray
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

#pragma mark - UITableView methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messageList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row >= self.preLoadedCell.count)
    {
        return 0;
    }
    FourLabelsTableViewCell *cell = [self.preLoadedCell objectAtIndex:indexPath.row];
    if (!cell)
    {
        return 0;
    }
    return cell.frame.size.height;
}

-(void)loadAllCells
{
    for (NSInteger i = 0; i < [self.messageList count]; ++i)
    {
        if (i >= self.preLoadedCell.count)
        {
            [self loadCellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        }
        FourLabelsTableViewCell *cell = [self.preLoadedCell objectAtIndex:i];
        if ([cell isEqual:[NSNull null]])
        {
            [self loadCellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        }
    }
}

-(FourLabelsTableViewCell *)loadCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"ChatMessageTableViewCell";
    FourLabelsTableViewCell *cell = [[FourLabelsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    __weak FourLabelsTableViewCell *weakCell = cell;
    
    if (indexPath.row >= self.messageList.count)
    {
        return nil;
    }
    
    Message *message = [self.messageList objectAtIndex:indexPath.row];
    
    weakCell.backgroundColor = [UIColor clearColor];
    
    CGFloat padding = 25;
    CGFloat labelWidth = self.messageTable.frame.size.width-2*padding;
    CGFloat posY = 15;
    
    UILabel *headerLabel = [[UILabel alloc] init];
    headerLabel.font = [UIFont boldSystemFontOfSize:14.0];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1.0f];
    headerLabel.text = message.contact.name;
    headerLabel.numberOfLines = 0;
    CGSize headerLabelSize = [headerLabel sizeThatFits:CGSizeMake(labelWidth, 500)];
    headerLabel.frame = CGRectMake(padding, posY, labelWidth, headerLabelSize.height);
    [weakCell addSubview:headerLabel];
    posY += headerLabelSize.height;
    
    UILabel *timeLabel = [[UILabel alloc] init];
    
    if ([message.type isEqualToString:@"image"])
    {
        CGFloat imageSize = 80;
        CustomButton *imageButton = [[CustomButton alloc] initWithFrame:CGRectMake(padding, posY, imageSize, imageSize)];
        imageButton.backgroundColor = [UIColor clearColor];
        [imageButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
        [ImageHelper loadImage:message.message toButton:imageButton authorization:[UrlInfo apiAuthentication]];
        imageButton.buttonData = message;
        [imageButton addTarget:self action:@selector(imageSelected:) forControlEvents:UIControlEventTouchUpInside];
        [weakCell addSubview:imageButton];
        
        posY += imageButton.frame.size.height - 16;
        timeLabel.frame = CGRectMake(padding+imageSize+10, posY, labelWidth-imageSize+10, 16);
    }
    else
    {
        UILabel *textLabel = [[UILabel alloc] init];
        textLabel.font = [UIFont systemFontOfSize:14.0];
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.textColor = [UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1.0f];
        textLabel.text = message.message;
        textLabel.numberOfLines = 0;
        CGSize textLabelSize = [textLabel sizeThatFits:CGSizeMake(labelWidth, 500)];
        textLabel.frame = CGRectMake(padding, posY, labelWidth, textLabelSize.height);
        [weakCell addSubview:textLabel];
        
        posY += textLabelSize.height + 4;
        timeLabel.frame = CGRectMake(padding, posY, labelWidth, 16);
    }
    
    NSString *timeString = [NSDateHelper stringTimeFromDate:message.date];
    if (![NSDateHelper isToday:message.date])
    {
        timeString = [NSDateHelper stringDateTimeFromDate4:message.date];
    }
    
    timeLabel.font = [UIFont systemFontOfSize:12.0];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.textColor = [UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1.0f];
    timeLabel.textAlignment = NSTextAlignmentRight;
    timeLabel.text = timeString;
    [weakCell addSubview:timeLabel];
    posY += timeLabel.frame.size.height + 15;

    weakCell.frame = CGRectMake(weakCell.frame.origin.x, weakCell.frame.origin.y, weakCell.frame.size.width, posY);

    [self.preLoadedCell replaceObjectAtIndex:indexPath.row withObject:weakCell];
    
    return weakCell;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FourLabelsTableViewCell *cell = [self.preLoadedCell objectAtIndex:indexPath.row];
    if (!cell)
    {
        cell = [self loadCellForRowAtIndexPath:indexPath];
        [self.preLoadedCell replaceObjectAtIndex:indexPath.row withObject:cell];
    }
    return cell;
}

-(void)imageSelected:(id)sender
{
    RoundedButton *button = (RoundedButton *)sender;
    Message *message = (Message *)button.buttonData;
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        ImageViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageViewController"];
//        viewController.imageString = message.message;
//        [self.navigationController pushViewController:viewController animated:YES];
//    });
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self loadMoreMessages];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadMoreMessages];
}

-(void)loadMoreMessages
{
    NSArray *indexPathsForVisibleRows = [self.messageTable indexPathsForVisibleRows];
    
    if (isProcessing || indexPathsForVisibleRows.count <= 0)
    {
        return;
    }

    NSIndexPath *firstVisibleIndexPath = [indexPathsForVisibleRows objectAtIndex:0];

    if (firstVisibleIndexPath.row < 2)
    {
        Message *message = [self.messageList objectAtIndex:0];
        [self getMessages:message.date];
    }
}

#pragma mark - Actions

-(IBAction)takePictureAction
{
    [self hideKeyboard];
    
    CameraViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CameraViewController"];
    controller.pictureSelectedCompletion = ^(UIImage *image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (image)
            {
                [self showImageField];
                imageSelected = image;
                self.messageImage.image = image;
            }
        });
    };
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:controller animated:YES];
    });
}

-(IBAction)deleteAction
{
    [self clearFields];
}

-(IBAction)sendAction
{
    NSString *inputMessage = self.messageInput.text;
    NSString *messageType = @"text";
    if (imageSelected)
    {
        messageType = @"image";
        NSString *encodedImage = [UIImagePNGRepresentation(imageSelected) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        
        if (!encodedImage)
        {
            [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Problem getting the image", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
            return;
        }
        inputMessage = encodedImage;
    }
    else
    {
        // Verify required fields
        if ([self.messageInput.text isEqualToString:@""])
        {
            [alertHelper showAlertWithOneButton:NSLocalizedString(@"Required", nil) message:NSLocalizedString(@"Message is a required field.", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
            return;
        }

        inputMessage = [inputMessage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }

    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"user_type":@"joey", @"user_id":[UrlInfo getJoeyId], @"message":inputMessage, @"type":messageType}];
    
    [self clearFields];
    
    [json chatNewMessage:jsonDictionary channelId:[self.channel.channelId intValue]];
}

@end