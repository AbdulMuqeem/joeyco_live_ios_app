//
//  VendorItemMoreDetailViewController.m
//  Customer
//
//  Created by Katia Maeda on 2014-11-07.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface ImageViewController () <UIScrollViewDelegate>

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation ImageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.maximumZoomScale = 3.5;
    self.scrollView.zoomScale = 1.0;
    self.scrollView.delegate = self;

    [self setImage];
//    [self loadImage];
}

//-(void)loadImage
//{
//    if (!self.imageString)
//    {
//        return;
//    }
//    
//    ImageHelper *operation = [ImageHelper new];
//    operation.downloadUrl = self.imageString;
//    operation.authorization = [UrlInfo apiAuthentication];
//    operation.completionAction = ^(NSURL *imageUrl, BOOL success){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if (success)
//            {
//                [self setImage];
//            }
//        });
//    };
//    BOOL alreadyDownloaded = [operation startDownload];
//    
//    if (alreadyDownloaded)
//    {
//        [self setImage];
//    }
//}

-(void)setImage
{
    dispatch_async(dispatch_get_main_queue(), ^{
        CGRect rect = self.view.frame;
        int minSize = (rect.size.height < rect.size.width) ? rect.size.height : rect.size.width;
        
        UIImage *image = self.image;
        CGSize imageSize = image.size;
        CGRect imageViewFrame = CGRectMake(0, 0, minSize, minSize);
        CGRect viewFrame = self.view.frame;
        
        CGFloat viewRatio = viewFrame.size.width / viewFrame.size.height;
        CGFloat imageRatio = viewFrame.size.width / viewFrame.size.height;
        
        if (viewFrame.size.width > viewFrame.size.height)
        {
            if (imageSize.width > imageSize.height)
            {
                imageViewFrame = viewFrame;
                
                if (viewRatio < imageRatio)
                {
                    imageViewFrame.size.height = (imageViewFrame.size.width * imageSize.height) / imageSize.width;
                }
                else
                {
                    imageViewFrame.size.width = (imageViewFrame.size.height * imageSize.width) / imageSize.height;
                }
            }
            else
            {
                imageViewFrame.size.width = (imageViewFrame.size.height * imageSize.width) / imageSize.height;
            }
        }
        else
        {
            if (imageSize.width > imageSize.height)
            {
                imageViewFrame.size.height = (imageViewFrame.size.width * imageSize.height) / imageSize.width;
            }
            else
            {
                if (viewRatio < imageRatio)
                {
                    imageViewFrame.size.height = (imageViewFrame.size.width * imageSize.height) / imageSize.width;
                }
                else
                {
                    imageViewFrame.size.width = (imageViewFrame.size.height * imageSize.width) / imageSize.height;
                }
            }
        }
        
        self.imageView = [[UIImageView alloc] initWithImage:image];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.imageView.frame = imageViewFrame;
        [self centerScrollViewContents];
        
        [self.scrollView addSubview:self.imageView];
    });
}

- (void)centerScrollViewContents
{
    dispatch_async(dispatch_get_main_queue(), ^{
        CGSize boundsSize = self.view.frame.size;
        CGRect contentsFrame = self.imageView.frame;
        
        if (contentsFrame.size.width < boundsSize.width)
        {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
        }
        else
        {
            contentsFrame.origin.x = 0.0f;
        }
        
        if (contentsFrame.size.height < boundsSize.height)
        {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
        }
        else
        {
            contentsFrame.origin.y = 0.0f;
        }
        
        self.imageView.frame = contentsFrame;
    });
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self centerScrollViewContents];
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
