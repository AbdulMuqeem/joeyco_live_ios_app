//
//  SummaryViewController.m
//  Joey
//
//  Created by Katia Maeda on 2016-05-20.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface SummaryViewController () <SlidingPagesDataSource, SlidingPagesDelegate, JSONHelperDelegate, UITextFieldDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
}

@property (nonatomic, weak) IBOutlet CustomTextField *startDateField;
@property (nonatomic, weak) IBOutlet CustomTextField *endDateField;
@property (nonatomic, strong) CustomTextField *fieldSelected;
@property (nonatomic, weak) IBOutlet UIView *dateView;
@property (nonatomic, weak) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;

@property (nonatomic, weak) IBOutlet GraySegmentedControl *segmentedControl;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic, strong) SlidingPagesViewController *slider;
@property (nonatomic, strong) Summary2ViewController *summaryView;
@property (nonatomic, strong) SummaryOrdersViewController *ordersView;
@property (nonatomic, strong) SummaryShiftsViewController *shiftsView;

@property (nonatomic, strong) Summary *summary;

@end

@implementation SummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.startDate = [NSDate date];
    self.endDate = [NSDate date];
    self.startDateField.text = [NSDateHelper stringDateFromDate:self.startDate];
    self.endDateField.text = [NSDateHelper stringDateFromDate:self.endDate];
    self.datePicker.maximumDate = [NSDate date];
    if (@available(iOS 13.4, *)) {
        self.datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    
    [_startDateField addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    [_endDateField addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    
    // Setup SlidingPages
    self.slider = [[SlidingPagesViewController alloc] init];
    self.slider.dataSource = self;
    self.slider.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
    self.slider.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.contentView addSubview:self.slider.view];
    [self addChildViewController:self.slider];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
    self.summaryView = [storyboard instantiateViewControllerWithIdentifier:@"Summary2ViewController"];
    self.ordersView = [storyboard instantiateViewControllerWithIdentifier:@"SummaryOrdersViewController"];
    self.shiftsView = [storyboard instantiateViewControllerWithIdentifier:@"SummaryShiftsViewController"];

    json = [[JSONHelper alloc] init:self andDelegate:self];
    alertHelper = [[AlertHelper alloc] init];
    
    [self lookupAction:nil];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]} forState:UIControlStateSelected];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.segmentedControl.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)lookupAction:(id)sender
{
    [json summary:[NSDateHelper getFirstHour:self.startDate] endDate:[NSDateHelper getLastHour:self.endDate]];
}

#pragma mark - Date Picker

-(void)updateTextField:(id)sender
{
    if (self.fieldSelected)
    {
        NSString *date = [NSDateHelper stringDateFromDate:self.datePicker.date];
        self.fieldSelected.text = date;
        
        if (self.fieldSelected == self.startDateField)
        {
            self.startDateField.text = [self formatDate:self.datePicker.date];
            self.startDate = self.datePicker.date;
        }
        else
        {
            self.endDateField.text = [self formatDate:self.datePicker.date];
            self.endDate = self.datePicker.date;
        }
    }
//    [self closeSelectedStatusAnimation:self.dateView];
}

- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDateFormat:@"MMM' 'dd', 'yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.fieldSelected = (CustomTextField *)textField;
    
//    if (self.dateView.hidden)
//    {
        if (self.fieldSelected == self.startDateField)
        {
            self.datePicker.date = self.startDate;
        }
        else
        {
            self.datePicker.date = self.endDate;
        }
        
        [self openSelectedStatusAnimation:self.dateView];
//    }
//    else
//    {
//        [self closeSelectedStatusAnimation:self.dateView];
//    }
    
    return NO;
}


-(IBAction)selectDate:(id)sender
{
    if (self.fieldSelected)
    {
        NSString *date = [NSDateHelper stringDateFromDate:self.datePicker.date];
        self.fieldSelected.text = date;
        
        if (self.fieldSelected == self.startDateField)
        {
            self.startDate = self.datePicker.date;
        }
        else
        {
            self.endDate = self.datePicker.date;
        }
        [self lookupAction:nil];
    }
    [self closeSelectedStatusAnimation:self.dateView];
}

#pragma mark - Animations

-(void)openSelectedStatusAnimation:(UIView *)view
{
//    if (!view.hidden)
//    {
//        return;
//    }
    
    view.hidden = NO;

//    CGRect frame = view.frame;
////    frame.origin.y -= frame.size.height;
//    frame.origin.y = 200;
//    view.frame = frame;
    
    CGRect frame = view.frame;
//    frame.origin.y = 200;
    double screenSize = self.view.bounds.size.height;
    frame.origin.y = screenSize - (view.frame.size.height);
    
    [UIView animateWithDuration:.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.frame = frame;
        self.datePicker.hidden = NO;
                     }
                     completion: nil];
//                         view.hidden = NO;
//                     ];

//    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
//                     animations:^{
//                         view.frame = frame;
//                     }
//                     completion:nil];
}

-(void)closeSelectedStatusAnimation:(UIView *)view
{
    if (view.hidden)
    {
        return;
    }
    
    view.hidden = NO;
    
    CGRect frame = view.frame;
    frame.origin.y += frame.size.height;
    [UIView animateWithDuration:.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         view.hidden = YES;
                     }];
}

-(void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    if ([action isEqualToString:@ACTION_SUMMARY])
    {
        self.summary = [list firstObject];
        
        self.summaryView.summary = self.summary;
        self.ordersView.summary = self.summary;
        self.shiftsView.summary = self.summary;
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

#pragma mark - Segmented Control methods
- (IBAction)changeSlidingPage:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex >= 0 && sender.selectedSegmentIndex <= 2)
    {
        [self.slider scrollToPage:sender.selectedSegmentIndex animated:YES];
        return;
    }
    
    [self.slider scrollToPage:0 animated:NO];
    return;
}

#pragma mark -
#pragma mark SlidingPagesDataSource methods

-(int)numberOfPagesInSlidingPages
{
    return 3;
}

-(NSString *)titleForSlidingPagesAtIndex:(int)index
{
    switch (index) {
        case 0:
            return NSLocalizedString(@"Summary", nil);
            break;
        case 1:
            return NSLocalizedString(@"Orders", nil);
            break;
        case 2:
            return NSLocalizedString(@"Shifts", nil);
            break;
    }
    
    return NSLocalizedString(@"Summary", nil);
}

-(UIViewController *)pageForSlidingPagesAtIndex:(int)index
{
    if (index == 0)
    {
        return self.summaryView;
    }
    else if (index == 1)
    {
        return self.ordersView;
    }
    return self.shiftsView;
}

-(void)contentViewHeight:(CGFloat)height
{
    
}

@end
