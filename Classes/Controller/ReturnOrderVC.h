//
//  CancelOrderVCViewController.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 06/11/2018.
//  Copyright © 2018 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ReturnStatusUpdateDelegate;

//#import "MaterialBottomSheet.h"
#import "DelayReportViewController.h"
#import "Order.h"
#import "Task.h"
#import "MetaData.h"
#import "JSONHelper.h"

#import "ItineraryOrder_Listing_ObjectDetail.h"
#import "CameraVC_takePhoto.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReturnOrderVC : UIViewController <ImageConfirmDropOffDelegate>

{
    
    id<ReturnStatusUpdateDelegate> delegate;

}
@property (weak, nonatomic) id<ReturnStatusUpdateDelegate> delegate;

@property (strong, nonatomic)  NSString *text_bottomSheet_title;
@property (strong, nonatomic)  NSString *text_bottomSheet_subtitle;

@property (weak, nonatomic) IBOutlet UILabel *bottomSheet_title;
@property (weak, nonatomic) IBOutlet UILabel *bottomSheet_subtitle;

@property(nonatomic,strong)NSMutableArray * cancelStatusObj;

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic,assign)int selectedIndex;


//Order
@property(nonatomic,strong)Order * order;
@property(nonatomic,strong)Task *  task;
@property(nonatomic,strong)NSString * taskType;

//Index path of row
@property(nonatomic,strong)NSIndexPath * indexPathOfTableViewOfItneraryOrders;
@property(nonatomic,assign)BOOL  isReturn;


- (IBAction)closeButton:(id)sender;

//Model Data of clicked obj
@property(nonatomic,strong)ItineraryOrder_Listing_ObjectDetail * modelData;

@end



@protocol ReturnStatusUpdateDelegate

-(void)returnStatusUpdate:(NSIndexPath *)indexPath;

@end
NS_ASSUME_NONNULL_END
