//
//  ScheduleViewController.m
//  Joey
//
//  Created by Katia Maeda on 2015-02-24.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface ScheduleViewController () <JSONHelperDelegate>
{
    GMSMarker *joeyMarker;
    Joey *joey;
    CLLocation *lastJoeyLocation;
    
    AlertHelper *alertHelper;
    JSONHelper *json;
    
    Shift *nextShift;
    NSTimer *showStartButtonTimer;
    NSTimer *startShiftTimer;
    NSTimer *endShiftTimer;
}


@property (nonatomic, weak) IBOutlet GMSMapView *mapView;

@property (nonatomic, weak) IBOutlet UIView *noShiftBox;
@property (nonatomic, weak) IBOutlet UILabel *noShiftTxt;

@property (nonatomic, weak) IBOutlet UIView *shiftBox;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *shiftBoxHeight;
@property (nonatomic, weak) IBOutlet UILabel *shiftOffline;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *shiftOfflineHeight;
@property (nonatomic, weak) IBOutlet UILabel *shiftNum;
@property (nonatomic, weak) IBOutlet UILabel *shiftStartTxt;
@property (nonatomic, weak) IBOutlet UILabel *shiftEndTxt;
@property (nonatomic, weak) IBOutlet UILabel *zoneNum;
@property (nonatomic, weak) IBOutlet RoundedButton *shiftStartBtn;
@property (nonatomic, weak) IBOutlet RoundedButton *shiftEndBtn;
@property (nonatomic, weak) IBOutlet UILabel *shiftStartedTxt;
@property (nonatomic, weak) IBOutlet UILabel *shiftEndedTxt;

@end

@implementation ScheduleViewController

#pragma mark - lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    joey = [[DatabaseHelper shared] getJoey];
    
    // Initiate GMSMapView
    self.mapView.settings.myLocationButton = YES;
    self.mapView.myLocationEnabled = YES;
    self.mapView.mapType = [self setMapType];
    lastJoeyLocation = [[LocationManager shared] lastKnownLocation];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationChanged:) name:@LocationManagerUpdateLocation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectivityDidChange:) name:kReachabilityChangedNotification object:nil];

    [self getNextShift];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@LocationManagerUpdateLocation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [self clearTimers];
    [alertHelper dismissAlert];
}

-(void)dealloc
{
    [self superDealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self clearTimers];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)applicationWillEnterForeground:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationChanged:) name:@LocationManagerUpdateLocation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectivityDidChange:) name:kReachabilityChangedNotification object:nil];
    [self getNextShift];
}

- (void)applicationDidEnterBackground:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@LocationManagerUpdateLocation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [self clearTimers];
}

#pragma mark - Internet

- (void)connectivityDidChange:(NSNotification *)notification
{
    [self reloadData];
}

#pragma mark - Navigation

-(IBAction)openFullSchedule:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        ScheduleFullViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ScheduleFullViewController"];
        [self.navigationController pushViewController:viewController animated:YES];
    });
}

-(IBAction)openEditSchedule:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        ScheduleEdit1ViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ScheduleEdit1ViewController"];
        [self.navigationController pushViewController:viewController animated:YES];
    });
}

#pragma mark - Layout

-(void)getNextShift
{
    [self clearTimers];
    
    BOOL hasInternet = [json hasInternetConnection:NO];
    if (hasInternet)
    {
        [json nextShift];
    }
    else
    {
        nextShift = [[DatabaseHelper shared] getShift:0];

        if (nextShift)
        {
            [self reloadData];
            [self setupShiftTimers];
        }
        else
        {
            [alertHelper showAlertNoButtons:@"" type:AlertTypeNone message:NSLocalizedString(@"No Saved Shifts in Database", nil) dismissAfter:3.0 from:self];
        }
    }
}

-(void)reloadData
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mapView clear];
        [self showJoeyMarker];
        
        if (nextShift)
        {
            self.shiftBox.hidden = NO;
            self.noShiftBox.hidden = YES;
            
            self.shiftNum.text = [NSString stringWithFormat:NSLocalizedString(@"Shift: %@", nil), nextShift.num];
            self.shiftStartTxt.text = [NSDateHelper stringDateTimeFromDate2:nextShift.startTime];
            self.shiftEndTxt.text = [NSDateHelper stringDateTimeFromDate2:nextShift.endTime];
            
            if (nextShift.zone)
            {
                self.zoneNum.text = [NSString stringWithFormat:NSLocalizedString(@"Zone: %@", nil), nextShift.zone.num];
            }
            
            if (nextShift.realStartTime)
            {
                self.shiftStartedTxt.text = [NSDateHelper stringDateTimeFromDate2:nextShift.realStartTime];
            }
            if (nextShift.realEndTime)
            {
                self.shiftEndedTxt.text = [NSDateHelper stringDateTimeFromDate2:nextShift.realEndTime];
            }
            
            BOOL hasInternet = [json hasInternetConnection:NO];
            if (hasInternet)
            {
                self.shiftOffline.hidden = YES;
                self.shiftOfflineHeight.constant = 0;
                self.shiftBoxHeight.constant = 147;
                
                if ([NSDateHelper isLater:nextShift.startTime plusMinutes:0] && !nextShift.realStartTime)
                {
                    self.shiftStartBtn.hidden = NO;
                    self.shiftStartedTxt.hidden = YES;
                }
                else
                {
                    self.shiftStartBtn.hidden = YES;
                    self.shiftStartedTxt.hidden = (nextShift.realStartTime) ? NO : YES;
                }
                
                if (nextShift.realStartTime && !nextShift.realEndTime)
                {
                    self.shiftEndBtn.hidden = NO;
                    self.shiftEndedTxt.hidden = YES;
                }
                else
                {
                    self.shiftEndBtn.hidden = YES;
                    self.shiftEndedTxt.hidden = (nextShift.realEndTime) ? NO : YES;
                }
            }
            else
            {
                self.shiftOffline.hidden = NO;
                self.shiftOfflineHeight.constant = 18;
                self.shiftBoxHeight.constant = 165;
                
                self.shiftStartBtn.hidden = YES;
                self.shiftEndBtn.hidden = YES;
                
                self.shiftStartedTxt.hidden = (nextShift.realStartTime) ? NO : YES;
                self.shiftEndedTxt.hidden = (nextShift.realEndTime) ? NO : YES;
            }
            
            // Load Map
            if (nextShift.zone)
            {
                CLLocationCoordinate2D zonePosition = CLLocationCoordinate2DMake([nextShift.zone.centerLatitude doubleValue], [nextShift.zone.centerLongitude doubleValue]);
                
                GMSCircle *circle = [[GMSCircle alloc] init];
                circle.radius = [nextShift.zone.radius intValue]; // Meters
                circle.position = zonePosition;
                circle.fillColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.33];
                circle.strokeWidth = .8;
                circle.strokeColor = [UIColor blackColor];
                
                GMSMarker *marker = [[GMSMarker alloc] init];
                marker.position = zonePosition;

                circle.map = self.mapView;
                marker.map = self.mapView;
                self.mapView.camera = [GMSCameraPosition cameraWithTarget:zonePosition zoom:13];
            }
        }
        else
        {
            self.shiftBox.hidden = YES;
            self.noShiftBox.hidden = NO;
        }
    });
}

-(IBAction)startShiftAction:(id)sender
{
    if (!nextShift)
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Start Shift Error", nil) message:NSLocalizedString(@"Please reload the page", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }

    NSString *fieldHint = NSLocalizedString(@"Message", nil);
    BOOL isRequired = NO;
    if ([NSDateHelper isLater:nextShift.startTime plusMinutes:15])
    {
        fieldHint = NSLocalizedString(@"Required", nil);
        isRequired = YES;
    }
    
    __unsafe_unretained typeof(self) weakSelf = self;
    [alertHelper showAlertWithField:NSLocalizedString(@"Starting Shift", nil) message:@"" from:self fieldHint:fieldHint withOkHandler:^(UITextField *field) {
        if (isRequired && (!field.text || [field.text isEqualToString:@""]))
        {
            [alertHelper showAlertNoButtons:NSLocalizedString(@"Please", nil) type:AlertTypeNone message:NSLocalizedString(@"Message is a required field.", nil) dismissAfter:3.0 from:self];
            return;
        }

        NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"notes":field.text}];
        NSMutableDictionary *shiftDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"dictionary":jsonDictionary, @"shiftId":nextShift.shiftId, @"latitude":[NSNumber numberWithDouble:lastJoeyLocation.coordinate.latitude], @"longitude":[NSNumber numberWithDouble:lastJoeyLocation.coordinate.longitude]}];
        
        if (weakSelf && [weakSelf isKindOfClass:[ScheduleViewController class]])
        {
            [weakSelf performSelector:@selector(jsonStartShift:) withObject:shiftDictionary afterDelay:0.1];
        }
    }];
}

-(void)jsonStartShift:(NSMutableDictionary *)dictionary
{
    NSMutableDictionary *jsonDictionary = [dictionary objectForKey:@"dictionary"];
    NSNumber *shiftId = [dictionary objectForKey:@"shiftId"];
    [json startShift:[shiftId intValue] shiftInfo:jsonDictionary];
}

-(IBAction)endShiftAction:(id)sender
{
    if (!nextShift)
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"End Shift Error", nil) message:NSLocalizedString(@"Please reload the page", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    BOOL isRequired = NO;
    NSString *fieldHint = NSLocalizedString(@"Message", nil);
    NSString *message = @"";
    if ([nextShift.endTime laterDate:[NSDate date]])
    {
        isRequired = YES;
        fieldHint = NSLocalizedString(@"Required", nil);
        message = NSLocalizedString(@"WARNING: If you end your shift early, you will lose out on earning the hourly wages earned for this shift. Please enter a message to explain why your shift is ending early.", nil);
    }
    
    __unsafe_unretained typeof(self) weakSelf = self;
    [alertHelper showAlertWithField:NSLocalizedString(@"Ending Shift", nil) message:message from:self fieldHint:fieldHint withOkHandler:^(UITextField *field) {
        if (isRequired && (!field.text || [field.text isEqualToString:@""]))
        {
            [alertHelper showAlertNoButtons:NSLocalizedString(@"Please", nil) type:AlertTypeNone message:NSLocalizedString(@"Message is a required field.", nil) dismissAfter:3.0 from:self];
            return;
        }
        
        NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"notes":field.text}];
        NSMutableDictionary *shiftDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"dictionary":jsonDictionary, @"shiftId":nextShift.shiftId}];
        
        if (weakSelf && [weakSelf isKindOfClass:[ScheduleViewController class]])
        {
            [weakSelf performSelector:@selector(jsonEndShift:) withObject:shiftDictionary afterDelay:0.1];
        }
    }];
}

-(void)jsonEndShift:(NSMutableDictionary *)dictionary
{
    NSMutableDictionary *jsonDictionary = [dictionary objectForKey:@"dictionary"];
    NSNumber *shiftId = [dictionary objectForKey:@"shiftId"];
    [json endShift:[shiftId intValue] shiftInfo:jsonDictionary];
}

-(void)setupShiftTimers
{
    if (!nextShift)
    {
        return;
    }
    
    [self clearTimers];
    
    long diffTimestamp = [nextShift.startTime timeIntervalSince1970] - [[NSDate date] timeIntervalSince1970];
    if (diffTimestamp > 0)
    {
        [showStartButtonTimer invalidate];
        showStartButtonTimer = [NSTimer scheduledTimerWithTimeInterval:diffTimestamp+1 target:self selector:@selector(getNextShift) userInfo:nil repeats:NO];
    }
    
    long diffEndTime = [nextShift.endTime timeIntervalSince1970] - [[NSDate date] timeIntervalSince1970];
    if (diffEndTime > 0)
    {
        [endShiftTimer invalidate];
        endShiftTimer = [NSTimer scheduledTimerWithTimeInterval:diffEndTime target:self selector:@selector(showEndShiftMessage) userInfo:nil repeats:NO];
    }
    
    long diffStartTime = [nextShift.startTime timeIntervalSince1970] - [[NSDate date] timeIntervalSince1970];
    if (diffStartTime > 0)
    {
        [startShiftTimer invalidate];
        startShiftTimer = [NSTimer scheduledTimerWithTimeInterval:diffStartTime target:self selector:@selector(showStartShiftMessage) userInfo:nil repeats:NO];
    }
}

-(void)clearTimers
{
    [showStartButtonTimer invalidate];
    [endShiftTimer invalidate];
    [startShiftTimer invalidate];
}

-(void)showEndShiftMessage
{
    [self showMessageBoard:NSLocalizedString(@"Your shift has finished", nil)];
}

-(void)showStartShiftMessage
{
    [self showMessageBoard:NSLocalizedString(@"Starting Shift", nil)];
}

#pragma mark - Map

-(void)locationChanged:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@LocationManagerUpdateLocation])
    {
        CLLocation *newLocation = [[LocationManager shared] lastKnownLocation];
        if (newLocation && ![newLocation isEqual:[NSNull null]])
        {
            lastJoeyLocation = newLocation;
            [self showJoeyMarker];
        }
    }
}

-(void)showJoeyMarker
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (joeyMarker)
        {
            joeyMarker.map = nil;
            joeyMarker = nil;
        }

        joeyMarker = [[GMSMarker alloc] init];
        joeyMarker.title = joey.firstName;
        joeyMarker.position = CLLocationCoordinate2DMake(lastJoeyLocation.coordinate.latitude, lastJoeyLocation.coordinate.longitude);
        joeyMarker.map = self.mapView;
        joeyMarker.icon = [UIImage imageNamed:@"joey_run.png"];
    });
}

#pragma mark - JSONHelperDelegate

- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    [super parsedObjects:action objects:list];
    
    if ([action isEqualToString:@ACTION_GET_SHIFTS])
    {
        if (list)
        {
            nextShift = [list firstObject];
            [self reloadData];

            if (nextShift)
            {
                [self setupShiftTimers];
                [[DatabaseHelper shared] insertShift:nextShift];
            }
        }
        else
        {
            nextShift = nil;
            [self reloadData];
        }
    }
    else if ([action isEqualToString:@ACTION_START_SHIFT])
    {
        [alertHelper showAlertNoButtons:NSLocalizedString(@"Shift started", nil) type:AlertTypeNone message:@"" dismissAfter:3.0 from:self];
        [self getNextShift];
    }
    else if ([action isEqualToString:@ACTION_END_SHIFT])
    {
        [alertHelper showAlertNoButtons:NSLocalizedString(@"Shift ended", nil) type:AlertTypeNone message:@"" dismissAfter:3.0 from:self];
        [self getNextShift];
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

@end

