//
//  ViewController.h
//  Twilio Voice with CallKit Quickstart - Objective-C
//
//  Copyright © 2016 Twilio, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSONHelper.h"
#import <CallKit/CXCallObserver.h>
#import "ZHPopupView.h"
#import "MZTimerLabel.h"
#import "AppDelegate.h"
#import "Prefix.pch"

@import AVFoundation;
@import PushKit;
@import CallKit;
@import TwilioVoice;

@class CallNow;             //define class, so protocol can see MyClass

@protocol CallNowDelegate <NSObject>
@optional
- (void)callDisconnected: (CallNow *) sender;
- (void)digitsPressed: (CallNow *) sender; //not used yet

@end

@interface CallNow : UIViewController<PKPushRegistryDelegate, TVONotificationDelegate, TVOCallDelegate, CXProviderDelegate, UITextFieldDelegate,CXCallObserverDelegate ,JSONHelperDelegate,AVAudioPlayerDelegate>{
    
    JSONHelper *json;
    AlertHelper *alertHelper;


}

@property(nonatomic,strong)ZHPopupView *popupView;

@property (nonatomic) BOOL isComingFromDialpadScreen;

@property (weak, nonatomic) IBOutlet UILabel *chronometerLabel;

@property (strong, nonatomic) MZTimerLabel *chronometer;

@property (weak, nonatomic) IBOutlet UIView *dialButtonView;

@property (weak, nonatomic) IBOutlet UIButton *dialButton;
- (IBAction)dialButtonMethod:(id)sender;


@property (nonatomic, weak) id <CallNowDelegate> delegate;


@property(nonatomic,strong)NSString * Joey_ID;
@property(nonatomic,strong)NSString * Order_ID;

@property(nonatomic,strong)NSString * start_time;
@property(nonatomic,strong)NSString * end_time;

@property(nonatomic,strong)NSString * PhoneNum;
@property (weak, nonatomic) IBOutlet UILabel *CallStatus;

@property (weak, nonatomic) IBOutlet UIButton *mainCallButton;
@property (weak, nonatomic) IBOutlet UILabel *mainCallButtonText;




@property (nonatomic, strong) NSString *deviceTokenString;

@property (nonatomic, strong) PKPushRegistry *voipRegistry;
@property (nonatomic, strong) TVOCallInvite *callInvite;
@property (nonatomic, strong) TVOCall *call;


@property (nonatomic, strong) void(^incomingPushCompletionCallback)(void);
@property (nonatomic, strong) TVODefaultAudioDevice *audioDevice;
@property (nonatomic, strong) NSMutableDictionary *activeCallInvites;
@property (nonatomic, strong) NSMutableDictionary *activeCalls;

@property (nonatomic, strong) void(^callKitCompletionCallback)(BOOL);

@property (nonatomic, strong) CXProvider *callKitProvider;
@property (nonatomic, strong) CXCallController *callKitCallController;

@property (nonatomic, weak) IBOutlet UIImageView *iconView;
@property (nonatomic, assign, getter=isSpinning) BOOL spinning;

@property (nonatomic, weak) IBOutlet UIButton *placeCallButton;
@property (nonatomic, weak) IBOutlet UITextField *outgoingValue;
@property (weak, nonatomic) IBOutlet UIView *callControlView;
@property (weak, nonatomic) IBOutlet UISwitch *muteSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *speakerSwitch;
@property (nonatomic, strong) CXCallObserver* observer;

@property (nonatomic, assign) BOOL userInitiatedDisconnect;


- (IBAction)endCall:(id)sender;

-(void)InitCall;

-(void)ForceFulCallEnd;


//For creating shared instance
+(CallNow *)shared;


@end

