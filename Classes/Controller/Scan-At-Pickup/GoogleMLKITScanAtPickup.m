//
//  Copyright (c) 2018 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "GoogleMLKITScanAtPickup.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CoreVideo.h>
#import "UIUtilities.h"
#import "QuartzCore/QuartzCore.h"
#import "PickedUpItems.h"
@import MLKit;

NS_ASSUME_NONNULL_BEGIN

static NSString *const alertControllerTitle = @"Vision Detectors";
static NSString *const alertControllerMessage = @"Select a detector";
static NSString *const cancelActionTitleText = @"Cancel";
static NSString *const videoDataOutputQueueLabel =
    @"com.google.mlkit.visiondetector.VideoDataOutputQueue";
static NSString *const sessionQueueLabel = @"com.google.mlkit.visiondetector.SessionQueue";
static NSString *const noResultsMessage = @"No Results";
static NSString *const localModelFileName = @"bird";
static NSString *const localModelFileType = @"tflite";

static const CGFloat MLKSmallDotRadius = 4.0;
static const CGFloat MLKconstantScale = 1.0;

@interface GoogleMLKITScanAtPickup () <AVCaptureVideoDataOutputSampleBufferDelegate,JSONHelperDelegate>
{
    int validItemsCounted ;
       JSONHelper *json;
       AlertHelper *alertHelper;


}


typedef NS_ENUM(NSInteger, Detector) {
  DetectorOnDeviceBarcode,
  DetectorOnDeviceFace,
  DetectorOnDeviceText,
  DetectorOnDeviceObjectProminentNoClassifier,
  DetectorOnDeviceObjectProminentWithClassifier,
  DetectorOnDeviceObjectMultipleNoClassifier,
  DetectorOnDeviceObjectMultipleWithClassifier,
  DetectorOnDeviceObjectCustomProminentNoClassifier,
  DetectorOnDeviceObjectCustomProminentWithClassifier,
  DetectorOnDeviceObjectCustomMultipleNoClassifier,
  DetectorOnDeviceObjectCustomMultipleWithClassifier
};


@property(nonatomic) NSArray *detectors;
@property(nonatomic) Detector currentDetector;
@property(nonatomic) bool isUsingFrontCamera;
@property(nonatomic, nonnull) AVCaptureVideoPreviewLayer *previewLayer;
@property(nonatomic) AVCaptureSession *captureSession;
@property(nonatomic) dispatch_queue_t sessionQueue;
@property(nonatomic) UIView *annotationOverlayView;
@property(nonatomic) UIImageView *previewOverlayView;
//@property(weak, nonatomic) IBOutlet UIView *cameraView;
@property(nonatomic) CMSampleBufferRef lastFrame;
@end

@implementation GoogleMLKITScanAtPickup



-(void)dismissVC{
    
    //to notify root vc
    [self.delegate scanViewScreenBeingDismissed];
}



- (NSString *)stringForDetector:(Detector)detector {
//  switch (detector) {
//    case DetectorOnDeviceBarcode:
//      return @"On-Device Barcode Scanner";
//    case DetectorOnDeviceFace:
//      return @"On-Device Face Detection";
//    case DetectorOnDeviceText:
//      return @"On-Device Text Recognition";
//    case DetectorOnDeviceObjectProminentNoClassifier:
//      return @"ODT, single, no labeling";
//    case DetectorOnDeviceObjectProminentWithClassifier:
//      return @"ODT, single, labeling";
//    case DetectorOnDeviceObjectMultipleNoClassifier:
//      return @"ODT, multiple, no labeling";
//    case DetectorOnDeviceObjectMultipleWithClassifier:
//      return @"ODT, multiple, labeling";
//    case DetectorOnDeviceObjectCustomProminentNoClassifier:
//      return @"ODT, custom, single, no labeling";
//    case DetectorOnDeviceObjectCustomProminentWithClassifier:
//      return @"ODT, custom, single, labeling";
//    case DetectorOnDeviceObjectCustomMultipleNoClassifier:
//      return @"ODT, custom, multiple, no labeling";
//    case DetectorOnDeviceObjectCustomMultipleWithClassifier:
//      return @"ODT, custom, multiple, labeling";
//  }
    
    
     return @"On-Device Barcode Scanner";
}

-(void)viewWillAppear:(BOOL)animated{
    
}
- (void)viewDidLoad {
  [super viewDidLoad];
    
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
       
       
       //Alert view controller
        alertHelper = [[AlertHelper alloc] init];

       
       //init JSON
       json = [[JSONHelper alloc] init:self andDelegate:self];
       
       //Hide the count label initially
       self.items_count.hidden = YES;
       

//       //Init array for storing valid tracking id which matched parcel/scanned item
//       self.valid_scanned_tracking_ids = [[NSMutableArray alloc]init];
//
//
//       //Init array with tracking and time stamp for server upload
//       self.trackingIdWithTimeStamp = [[NSMutableArray alloc]init];

       //Valid items
       validItemsCounted =0;
       

       [self setupScanner];
    
}

- (void)setupScanner {

    //  _detectors = @[
    //    @(DetectorOnDeviceBarcode), @(DetectorOnDeviceFace), @(DetectorOnDeviceText),
    //    @(DetectorOnDeviceObjectProminentNoClassifier),
    //    @(DetectorOnDeviceObjectProminentWithClassifier), @(DetectorOnDeviceObjectMultipleNoClassifier),
    //    @(DetectorOnDeviceObjectMultipleWithClassifier),
    //    @(DetectorOnDeviceObjectCustomProminentNoClassifier),
    //    @(DetectorOnDeviceObjectCustomProminentWithClassifier),
    //    @(DetectorOnDeviceObjectCustomMultipleNoClassifier),
    //    @(DetectorOnDeviceObjectCustomMultipleWithClassifier)
    //  ];
    //
        _detectors = @[
           @(DetectorOnDeviceBarcode)
         ];
      _currentDetector = DetectorOnDeviceBarcode;
      _isUsingFrontCamera = NO;
      _captureSession = [[AVCaptureSession alloc] init];
      _sessionQueue = dispatch_queue_create(sessionQueueLabel.UTF8String, nil);
      _previewOverlayView = [[UIImageView alloc] initWithFrame:CGRectZero];
      _previewOverlayView.contentMode = UIViewContentModeScaleAspectFill;
      _previewOverlayView.translatesAutoresizingMaskIntoConstraints = NO;
      _annotationOverlayView = [[UIView alloc] initWithFrame:CGRectZero];
      _annotationOverlayView.translatesAutoresizingMaskIntoConstraints = NO;

      self.previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:_captureSession];
      [self setUpPreviewOverlayView];
      [self setUpAnnotationOverlayView];
      [self setUpCaptureSessionOutput];
      [self setUpCaptureSessionInput];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
    [self askingCameraPermission];

}

- (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];
  [self stopSession];
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  _previewLayer.frame = _pickerParentView.frame;
}

- (IBAction)selectDetector:(id)sender {
  [self presentDetectorsAlertController];
}

- (IBAction)switchCamera:(id)sender {
  self.isUsingFrontCamera = !_isUsingFrontCamera;
  [self removeDetectionAnnotations];
  [self setUpCaptureSessionInput];
}

#pragma mark - On-Device Detections
//
//- (void)detectFacesOnDeviceInImage:(MLKVisionImage *)image
//                             width:(CGFloat)width
//                            height:(CGFloat)height {
//  // When performing latency tests to determine ideal detection settings, run the app in 'release'
//  // mode to get accurate performance metrics.
//  MLKFaceDetectorOptions *options = [[MLKFaceDetectorOptions alloc] init];
//  options.performanceMode = MLKFaceDetectorPerformanceModeFast;
//  options.contourMode = MLKFaceDetectorContourModeAll;
//  options.landmarkMode = MLKFaceDetectorLandmarkModeNone;
//  options.classificationMode = MLKFaceDetectorClassificationModeNone;
//  MLKFaceDetector *faceDetector = [MLKFaceDetector faceDetectorWithOptions:options];
//  NSError *error;
//  NSArray<MLKFace *> *faces = [faceDetector resultsInImage:image error:&error];
//  dispatch_sync(dispatch_get_main_queue(), ^{
//    [self updatePreviewOverlayView];
//    [self removeDetectionAnnotations];
//    if (error != nil) {
//      NSLog(@"Failed to detect faces with error: %@", error.localizedDescription);
//      return;
//    }
//    if (faces.count == 0) {
//      NSLog(@"On-Device face detector returned no results.");
//      return;
//    }
//    for (MLKFace *face in faces) {
//      CGRect normalizedRect =
//          CGRectMake(face.frame.origin.x / width, face.frame.origin.y / height,
//                     face.frame.size.width / width, face.frame.size.height / height);
//      CGRect standardizedRect = CGRectStandardize(
//          [self->_previewLayer rectForMetadataOutputRectOfInterest:normalizedRect]);
//      [UIUtilities addRectangle:standardizedRect
//                         toView:self->_annotationOverlayView
//                          color:UIColor.greenColor];
//      [self addContoursForFace:face width:width height:height];
//    }
//  });
//}
//
//- (void)recognizeTextOnDeviceInImage:(MLKVisionImage *)image
//                               width:(CGFloat)width
//                              height:(CGFloat)height {
//  MLKTextRecognizer *textRecognizer = [MLKTextRecognizer textRecognizer];
//  NSError *error;
//  MLKText *text = [textRecognizer resultsInImage:image error:&error];
//  dispatch_sync(dispatch_get_main_queue(), ^{
//    [self removeDetectionAnnotations];
//    [self updatePreviewOverlayView];
//    if (error != nil) {
//      NSLog(@"Failed to recognize text with error: %@", error.localizedDescription);
//      return;
//    }
//    // Blocks.
//    for (MLKTextBlock *block in text.blocks) {
//      NSArray<NSValue *> *points = [self convertedPointsFromPoints:block.cornerPoints
//                                                             width:width
//                                                            height:height];
//      [UIUtilities addShapeWithPoints:points
//                               toView:self->_annotationOverlayView
//                                color:UIColor.purpleColor];
//
//      // Lines.
//      for (MLKTextLine *line in block.lines) {
//        NSArray<NSValue *> *points = [self convertedPointsFromPoints:line.cornerPoints
//                                                               width:width
//                                                              height:height];
//        [UIUtilities addShapeWithPoints:points
//                                 toView:self->_annotationOverlayView
//                                  color:UIColor.purpleColor];
//
//        // Elements.
//        for (MLKTextElement *element in line.elements) {
//          CGRect normalizedRect =
//              CGRectMake(element.frame.origin.x / width, element.frame.origin.y / height,
//                         element.frame.size.width / width, element.frame.size.height / height);
//          CGRect convertedRect =
//              [self->_previewLayer rectForMetadataOutputRectOfInterest:normalizedRect];
//          [UIUtilities addRectangle:convertedRect
//                             toView:self->_annotationOverlayView
//                              color:UIColor.greenColor];
//          UILabel *label = [[UILabel alloc] initWithFrame:convertedRect];
//          label.text = element.text;
//          label.adjustsFontSizeToFitWidth = YES;
//          [self.annotationOverlayView addSubview:label];
//        }
//      }
//    }
//  });
//}

- (void)scanBarcodesOnDeviceInImage:(MLKVisionImage *)image
                              width:(CGFloat)width
                             height:(CGFloat)height
                            options:(MLKBarcodeScannerOptions *)options {
  MLKBarcodeScanner *scanner = [MLKBarcodeScanner barcodeScannerWithOptions:options];
  NSError *error;
  NSArray<MLKBarcode *> *barcodes = [scanner resultsInImage:image error:&error];
  dispatch_sync(dispatch_get_main_queue(), ^{
    [self removeDetectionAnnotations];
    [self updatePreviewOverlayView];
      
      //Adding a red line
            
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, _pickerParentView.frame.size.height /2,  _pickerParentView.frame.size.width, 1)];
            lineView.backgroundColor = [UIColor redColor];
            lineView.layoutMargins = UIEdgeInsetsMake(0, 10, 0, 10);
            [self blinkView_infinite:lineView];
             [self.annotationOverlayView addSubview:lineView];
           
      
      
      
    if (error != nil) {
      NSLog(@"Failed to scan barcodes with error: %@", error.localizedDescription);
      return;
    }
    if (barcodes.count == 0) {
    //  NSLog(@"On-Device barcode scanner returned no results.");
      return;
    }
    for (MLKBarcode *barcode in barcodes) {

        
        CGRect normalizedRect = CGRectMake(_pickerParentView.frame.origin.x ,       // X
                                                 _pickerParentView.frame.origin.y ,      // Y
                                                 _pickerParentView.frame.size.width ,     // Width
                                                 _pickerParentView.frame.size.height );  //.rect
        
     CGRect standardizedRect =
                      CGRectStandardize([self.previewLayer rectForMetadataOutputRectOfInterest:normalizedRect]);
        
      UILabel *label = [[UILabel alloc] initWithFrame:standardizedRect];
      label.numberOfLines = 0;
      NSMutableString *description = [NSMutableString new];
      [description appendString:barcode.rawValue];
     // label.text = description;

      label.adjustsFontSizeToFitWidth = YES;
      [self.annotationOverlayView addSubview:label];
        
        
      
        //Stop the camera
        [self stopSession];
        
        //Play Beep sound
          [self playBeepSound];
        
        //Call Webservice
        [self callWebservice:description];

//        //Detecting barcode and putting RED and GREEN masking
//
//        if ([self searchItem:description]== NO){
//
//
//
//        [UIUtilities addRectangle:normalizedRect
//                           toView:self.annotationOverlayView
//                            color:UIColor.redColor];
//
//
//              }
//          else
//          {
//
//        [UIUtilities addRectangle:normalizedRect
//                        toView:self.annotationOverlayView
//                        color:UIColor.greenColor];
//
//          }
//
//
//         //Code below only adds valid barcode which match to your data
//        //Core logic to add valid codes
//        [self addingValidItems:description];
         
    }
  });
}
//
//- (void)detectObjectsOnDeviceInImage:(MLKVisionImage *)image
//                               width:(CGFloat)width
//                              height:(CGFloat)height
//                             options:(MLKCommonObjectDetectorOptions *)options {
//  MLKObjectDetector *detector = [MLKObjectDetector objectDetectorWithOptions:options];
//
//  NSError *error;
//  NSArray *objects = [detector resultsInImage:image error:&error];
//  dispatch_sync(dispatch_get_main_queue(), ^{
//    [self updatePreviewOverlayView];
//    [self removeDetectionAnnotations];
//    if (error != nil) {
//      NSLog(@"Failed to detect object with error: %@", error.localizedDescription);
//      return;
//    }
//    if (objects.count == 0) {
//      NSLog(@"On-Device object detector returned no results.");
//      return;
//    }
//    for (MLKObject *object in objects) {
//      NSMutableString *description = [[NSMutableString alloc] init];
//      CGRect normalizedRect =
//          CGRectMake(object.frame.origin.x / width, object.frame.origin.y / height,
//                     object.frame.size.width / width, object.frame.size.height / height);
//      CGRect standardizedRect =
//          CGRectStandardize([self.previewLayer rectForMetadataOutputRectOfInterest:normalizedRect]);
//      [UIUtilities addRectangle:standardizedRect
//                         toView:self.annotationOverlayView
//                          color:UIColor.greenColor];
//      UILabel *label = [[UILabel alloc] initWithFrame:standardizedRect];
//      if (object.trackingID != nil) {
//        [description appendFormat:@"Object ID: %@\n", object.trackingID];
//      }
//
//      [description appendString:@"Labels:\n"];
//      int i = 0;
//      for (MLKObjectLabel *l in object.labels) {
//        NSString *labelString = [NSString stringWithFormat:@"Label %d: %@, %f, %lu\n", i++, l.text,
//                                                           l.confidence, (unsigned long)l.index];
//        [description appendString:labelString];
//      }
//      label.text = description;
//      label.numberOfLines = 0;
//      label.adjustsFontSizeToFitWidth = YES;
//      [self.annotationOverlayView addSubview:label];
//    }
//  });
//}

#pragma mark - Private

- (void)setUpCaptureSessionOutput {
  dispatch_async(_sessionQueue, ^{
    [self->_captureSession beginConfiguration];
    // When performing latency tests to determine ideal capture settings,
    // run the app in 'release' mode to get accurate performance metrics
    self->_captureSession.sessionPreset = AVCaptureSessionPreset352x288;

    AVCaptureVideoDataOutput *output = [[AVCaptureVideoDataOutput alloc] init];
    output.videoSettings = @{
      (id)
      kCVPixelBufferPixelFormatTypeKey : [NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA]
    };
    output.alwaysDiscardsLateVideoFrames = YES;
    dispatch_queue_t outputQueue = dispatch_queue_create(videoDataOutputQueueLabel.UTF8String, nil);
    [output setSampleBufferDelegate:self queue:outputQueue];
    if ([self.captureSession canAddOutput:output]) {
      [self.captureSession addOutput:output];
      [self.captureSession commitConfiguration];
    } else {
      NSLog(@"%@", @"Failed to add capture session output.");
    }
  });
}

- (void)setUpCaptureSessionInput {
  dispatch_async(_sessionQueue, ^{
    AVCaptureDevicePosition cameraPosition =
        self.isUsingFrontCamera ? AVCaptureDevicePositionFront : AVCaptureDevicePositionBack;
    AVCaptureDevice *device = [self captureDeviceForPosition:cameraPosition];
    if (device) {
      [self->_captureSession beginConfiguration];
      NSArray<AVCaptureInput *> *currentInputs = self.captureSession.inputs;
      for (AVCaptureInput *input in currentInputs) {
        [self.captureSession removeInput:input];
      }
      NSError *error;
      AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device
                                                                          error:&error];
      if (error) {
        NSLog(@"Failed to create capture device input: %@", error.localizedDescription);
        return;
      } else {
        if ([self.captureSession canAddInput:input]) {
          [self.captureSession addInput:input];
        } else {
          NSLog(@"%@", @"Failed to add capture session input.");
        }
      }
      [self.captureSession commitConfiguration];
    } else {
      NSLog(@"Failed to get capture device for camera position: %ld", cameraPosition);
    }
  });
}

- (void)startSession {
  dispatch_async(_sessionQueue, ^{
    [self->_captureSession startRunning];
  });
}

- (void)stopSession {
  dispatch_async(_sessionQueue, ^{
      
      if(self->_captureSession.running == YES)
      {
    [self->_captureSession stopRunning];
 
      }
      });
}

- (void)setUpPreviewOverlayView {
    
    [_pickerParentView addSubview:_previewOverlayView];
  [NSLayoutConstraint activateConstraints:@[
    [_previewOverlayView.centerYAnchor constraintEqualToAnchor:_pickerParentView.centerYAnchor],
    [_previewOverlayView.centerXAnchor constraintEqualToAnchor:_pickerParentView.centerXAnchor],
    [_previewOverlayView.leadingAnchor constraintEqualToAnchor:_pickerParentView.leadingAnchor],
    [_previewOverlayView.trailingAnchor constraintEqualToAnchor:_pickerParentView.trailingAnchor]
  ]];
}
- (void)setUpAnnotationOverlayView {
  [_pickerParentView addSubview:_annotationOverlayView];
  [NSLayoutConstraint activateConstraints:@[
    [_annotationOverlayView.topAnchor constraintEqualToAnchor:_pickerParentView.topAnchor],
    [_annotationOverlayView.leadingAnchor constraintEqualToAnchor:_pickerParentView.leadingAnchor],
    [_annotationOverlayView.trailingAnchor constraintEqualToAnchor:_pickerParentView.trailingAnchor],
    [_annotationOverlayView.bottomAnchor constraintEqualToAnchor:_pickerParentView.bottomAnchor]
  ]];
}

- (AVCaptureDevice *)captureDeviceForPosition:(AVCaptureDevicePosition)position {
  if (@available(iOS 10, *)) {
    AVCaptureDeviceDiscoverySession *discoverySession = [AVCaptureDeviceDiscoverySession
        discoverySessionWithDeviceTypes:@[ AVCaptureDeviceTypeBuiltInWideAngleCamera ]
                              mediaType:AVMediaTypeVideo
                               position:AVCaptureDevicePositionUnspecified];
    for (AVCaptureDevice *device in discoverySession.devices) {
      if (device.position == position) {
        return device;
      }
    }
  }
  return nil;
}

- (void)presentDetectorsAlertController {
  UIAlertController *alertController =
      [UIAlertController alertControllerWithTitle:alertControllerTitle
                                          message:alertControllerMessage
                                   preferredStyle:UIAlertControllerStyleAlert];
  for (NSNumber *detectorType in _detectors) {
    NSInteger detector = detectorType.integerValue;
    UIAlertAction *action = [UIAlertAction actionWithTitle:[self stringForDetector:detector]
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *_Nonnull action) {
                                                     self.currentDetector = detector;
                                                     [self removeDetectionAnnotations];
                                                   }];
    if (detector == _currentDetector) {
      [action setEnabled:NO];
    }
    [alertController addAction:action];
  }
  [alertController addAction:[UIAlertAction actionWithTitle:cancelActionTitleText
                                                      style:UIAlertActionStyleCancel
                                                    handler:nil]];
  [self presentViewController:alertController animated:YES completion:nil];
}

- (void)removeDetectionAnnotations {
  for (UIView *annotationView in _annotationOverlayView.subviews) {
    [annotationView removeFromSuperview];
  }
}

- (void)updatePreviewOverlayView {
  CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(_lastFrame);
  if (imageBuffer == nil) {
    return;
  }
  CIImage *ciImage = [CIImage imageWithCVPixelBuffer:imageBuffer];
  CIContext *context = [[CIContext alloc] initWithOptions:nil];
  CGImageRef cgImage = [context createCGImage:ciImage fromRect:ciImage.extent];
  if (cgImage == nil) {
    return;
  }
  UIImage *rotatedImage = [UIImage imageWithCGImage:cgImage
                                              scale:MLKconstantScale
                                        orientation:UIImageOrientationRight];
  if (_isUsingFrontCamera) {
    CGImageRef rotatedCGImage = rotatedImage.CGImage;
    if (rotatedCGImage == nil) {
      return;
    }
    UIImage *mirroredImage = [UIImage imageWithCGImage:rotatedCGImage
                                                 scale:MLKconstantScale
                                           orientation:UIImageOrientationLeftMirrored];
    _previewOverlayView.image = mirroredImage;
  } else {
    _previewOverlayView.image = rotatedImage;
  }
  CGImageRelease(cgImage);
}

- (NSArray<NSValue *> *)convertedPointsFromPoints:(NSArray<NSValue *> *)points
                                            width:(CGFloat)width
                                           height:(CGFloat)height {
  NSMutableArray *result = [NSMutableArray arrayWithCapacity:points.count];
  for (NSValue *point in points) {
    CGPoint cgPointValue = point.CGPointValue;
    CGPoint normalizedPoint = CGPointMake(cgPointValue.x / width, cgPointValue.y / height);
    CGPoint cgPoint = [_previewLayer pointForCaptureDevicePointOfInterest:normalizedPoint];
    [result addObject:[NSValue valueWithCGPoint:cgPoint]];
  }
  return result;
}

- (CGPoint)normalizedPointFromVisionPoint:(MLKVisionPoint *)point
                                    width:(CGFloat)width
                                   height:(CGFloat)height {
  CGPoint cgPointValue = CGPointMake(point.x, point.y);
  CGPoint normalizedPoint = CGPointMake(cgPointValue.x / width, cgPointValue.y / height);
  CGPoint cgPoint = [_previewLayer pointForCaptureDevicePointOfInterest:normalizedPoint];
  return cgPoint;
}
//
//- (void)addContoursForFace:(MLKFace *)face width:(CGFloat)width height:(CGFloat)height {
//  // Face
//  MLKFaceContour *faceContour = [face contourOfType:MLKFaceContourTypeFace];
//  for (MLKVisionPoint *point in faceContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.blueColor
//                           radius:MLKSmallDotRadius];
//  }
//
//  // Eyebrows
//  MLKFaceContour *leftEyebrowTopContour = [face contourOfType:MLKFaceContourTypeLeftEyebrowTop];
//  for (MLKVisionPoint *point in leftEyebrowTopContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.orangeColor
//                           radius:MLKSmallDotRadius];
//  }
//  MLKFaceContour *leftEyebrowBottomContour =
//      [face contourOfType:MLKFaceContourTypeLeftEyebrowBottom];
//  for (MLKVisionPoint *point in leftEyebrowBottomContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.orangeColor
//                           radius:MLKSmallDotRadius];
//  }
//  MLKFaceContour *rightEyebrowTopContour = [face contourOfType:MLKFaceContourTypeRightEyebrowTop];
//  for (MLKVisionPoint *point in rightEyebrowTopContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.orangeColor
//                           radius:MLKSmallDotRadius];
//  }
//  MLKFaceContour *rightEyebrowBottomContour =
//      [face contourOfType:MLKFaceContourTypeRightEyebrowBottom];
//  for (MLKVisionPoint *point in rightEyebrowBottomContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.orangeColor
//                           radius:MLKSmallDotRadius];
//  }
//
//  // Eyes
//  MLKFaceContour *leftEyeContour = [face contourOfType:MLKFaceContourTypeLeftEye];
//  for (MLKVisionPoint *point in leftEyeContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.cyanColor
//                           radius:MLKSmallDotRadius];
//  }
//  MLKFaceContour *rightEyeContour = [face contourOfType:MLKFaceContourTypeRightEye];
//  for (MLKVisionPoint *point in rightEyeContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.cyanColor
//                           radius:MLKSmallDotRadius];
//  }
//
//  // Lips
//  MLKFaceContour *upperLipTopContour = [face contourOfType:MLKFaceContourTypeUpperLipTop];
//  for (MLKVisionPoint *point in upperLipTopContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.redColor
//                           radius:MLKSmallDotRadius];
//  }
//  MLKFaceContour *upperLipBottomContour = [face contourOfType:MLKFaceContourTypeUpperLipBottom];
//  for (MLKVisionPoint *point in upperLipBottomContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.redColor
//                           radius:MLKSmallDotRadius];
//  }
//  MLKFaceContour *lowerLipTopContour = [face contourOfType:MLKFaceContourTypeLowerLipTop];
//  for (MLKVisionPoint *point in lowerLipTopContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.redColor
//                           radius:MLKSmallDotRadius];
//  }
//  MLKFaceContour *lowerLipBottomContour = [face contourOfType:MLKFaceContourTypeLowerLipBottom];
//  for (MLKVisionPoint *point in lowerLipBottomContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.redColor
//                           radius:MLKSmallDotRadius];
//  }
//
//  // Nose
//  MLKFaceContour *noseBridgeContour = [face contourOfType:MLKFaceContourTypeNoseBridge];
//  for (MLKVisionPoint *point in noseBridgeContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.yellowColor
//                           radius:MLKSmallDotRadius];
//  }
//  MLKFaceContour *noseBottomContour = [face contourOfType:MLKFaceContourTypeNoseBottom];
//  for (MLKVisionPoint *point in noseBottomContour.points) {
//    CGPoint cgPoint = [self normalizedPointFromVisionPoint:point width:width height:height];
//    [UIUtilities addCircleAtPoint:cgPoint
//                           toView:self->_annotationOverlayView
//                            color:UIColor.yellowColor
//                           radius:MLKSmallDotRadius];
//  }
//}

#pragma mark - AVCaptureVideoDataOutputSampleBufferDelegate

- (void)captureOutput:(AVCaptureOutput *)output
    didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
           fromConnection:(AVCaptureConnection *)connection {
  CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
  if (imageBuffer) {
    _lastFrame = sampleBuffer;
    MLKVisionImage *visionImage = [[MLKVisionImage alloc] initWithBuffer:sampleBuffer];
    UIImageOrientation orientation = [UIUtilities
        imageOrientationFromDevicePosition:_isUsingFrontCamera ? AVCaptureDevicePositionFront
                                                               : AVCaptureDevicePositionBack];

    visionImage.orientation = orientation;
    CGFloat imageWidth = CVPixelBufferGetWidth(imageBuffer);
    CGFloat imageHeight = CVPixelBufferGetHeight(imageBuffer);
    BOOL shouldEnableClassification = NO;
    BOOL shouldEnableMultipleObjects = NO;
    switch (_currentDetector) {
      case DetectorOnDeviceObjectCustomMultipleWithClassifier:
      case DetectorOnDeviceObjectCustomProminentWithClassifier:
      case DetectorOnDeviceObjectMultipleWithClassifier:
      case DetectorOnDeviceObjectProminentWithClassifier:
        shouldEnableClassification = YES;
      default:
        break;
    }
    switch (_currentDetector) {
      case DetectorOnDeviceObjectCustomMultipleNoClassifier:
      case DetectorOnDeviceObjectCustomMultipleWithClassifier:
      case DetectorOnDeviceObjectMultipleNoClassifier:
      case DetectorOnDeviceObjectMultipleWithClassifier:
        shouldEnableMultipleObjects = YES;
      default:
        break;
    }

    switch (_currentDetector) {
      case DetectorOnDeviceBarcode: {
        MLKBarcodeScannerOptions *options = [[MLKBarcodeScannerOptions alloc] init];
        [self scanBarcodesOnDeviceInImage:visionImage
                                    width:imageWidth
                                   height:imageHeight
                                  options:options];
        break;
      }
//      case DetectorOnDeviceFace:
//        [self detectFacesOnDeviceInImage:visionImage width:imageWidth height:imageHeight];
//        break;
//      case DetectorOnDeviceText:
//        [self recognizeTextOnDeviceInImage:visionImage width:imageWidth height:imageHeight];
//        break;
//      case DetectorOnDeviceObjectProminentNoClassifier:
//      case DetectorOnDeviceObjectProminentWithClassifier:
//      case DetectorOnDeviceObjectMultipleNoClassifier:
//      case DetectorOnDeviceObjectMultipleWithClassifier: {
//        MLKObjectDetectorOptions *options = [MLKObjectDetectorOptions new];
//        options.shouldEnableClassification = shouldEnableClassification;
//        options.shouldEnableMultipleObjects = shouldEnableMultipleObjects;
//        options.detectorMode = MLKObjectDetectorModeStream;
//        [self detectObjectsOnDeviceInImage:visionImage
//                                     width:imageWidth
//                                    height:imageHeight
//                                   options:options];
//        break;
//      }
//      case DetectorOnDeviceObjectCustomProminentNoClassifier:
//      case DetectorOnDeviceObjectCustomProminentWithClassifier:
//      case DetectorOnDeviceObjectCustomMultipleNoClassifier:
//      case DetectorOnDeviceObjectCustomMultipleWithClassifier: {
//        NSString *localModelFilePath = [[NSBundle mainBundle] pathForResource:localModelFileName
//                                                                       ofType:localModelFileType];
//        if (localModelFilePath == nil) {
//          NSLog(@"Failed to find custom local model file: %@.%@", localModelFileName,
//                localModelFileType);
//          return;
//        }
//        MLKLocalModel *localModel = [[MLKLocalModel alloc] initWithPath:localModelFilePath];
//        MLKCustomObjectDetectorOptions *options =
//            [[MLKCustomObjectDetectorOptions alloc] initWithLocalModel:localModel];
//        options.shouldEnableClassification = shouldEnableClassification;
//        options.shouldEnableMultipleObjects = shouldEnableMultipleObjects;
//        options.detectorMode = MLKObjectDetectorModeStream;
//        [self detectObjectsOnDeviceInImage:visionImage
//                                     width:imageWidth
//                                    height:imageHeight
//                                   options:options];
//        break;
//      }
    }
  } else {
    NSLog(@"%@", @"Failed to get image buffer from sample buffer.");
  }
}




- (IBAction)close_scanner:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)updateTotalCount{
    dispatch_async( dispatch_get_main_queue(), ^{

    
    self.items_count.hidden = NO;
    
    [self updateItemCount];
    
        
        if(self->validItemsCounted == 1  ){
              self.items_count.text = [NSString stringWithFormat:@"%d item has been picked up",(int)self->validItemsCounted];
        }
        else
        {
             self.items_count.text = [NSString stringWithFormat:@"%d items have been picked",(int)self->validItemsCounted];
        }
      
        
    });
}

-(BOOL)searchItem:(NSString *)searchedItemName{
    
   
    BOOL itemExists = NO;

    for (int i = 0; i < [self.tracking_ids count];i++ )
    {
        NSString *item = [self.tracking_ids objectAtIndex:i];


        if ([searchedItemName isEqualToString:item])
        {
            
            itemExists =YES;
        }
    }

    
    return itemExists;
    
}


-(BOOL)itemScannedAlready:(NSString *)searchedItemName{
    
   
    BOOL itemExists = NO;

    for (int i = 0; i < [self.valid_scanned_tracking_ids count];i++ )
    {
        NSString *item = [self.valid_scanned_tracking_ids objectAtIndex:i];


        if ([searchedItemName isEqualToString:item])
        {
            
            itemExists =YES;
        }
    }

    
    return itemExists;
    
}

-(void)addItemInVerifiedTrackingCodeList :(NSString *)item{
    
    double timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSNumber *timeStampToString = [NSNumber numberWithDouble:timestamp];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:item forKey:@"tracking_id"];
    [dict setObject:[timeStampToString stringValue]forKey:@"time_stamp"];

    
    //Used for sending to server
    [self.trackingIdWithTimeStamp addObject:dict];
    
    //Used for scanning
    [self.valid_scanned_tracking_ids addObject:item];
}

-(void)updateItemCount{
    
    //Increase total count
      validItemsCounted ++;
}
- (IBAction)upload:(id)sender {

    //Move to Bulk scan Pickup screen
   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
                                             
      PickedUpItems *vc = [storyboard instantiateViewControllerWithIdentifier:@"PickedUpItems"];
//    vc.delegate = self;
//                        vc.is_singleScan = YES;
//                         vc.is_pickup = YES;
                        // Pass any objects to the view controller here, like...
//                        vc.tracking_ids =[self.itineraryData getTrackingIds];
              
                 [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)openDialog:(NSString *)responseString {
    
    
    ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                        iconImg:[UIImage imageNamed:@"send"]
                                                backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                          title:@"Response"
                                                        content:responseString
                                                   buttonTitles:@[@"Ok"]
                                            confirmBtnTextColor:nil otherBtnTextColor:nil
                                             buttonPressedBlock:^(NSInteger btnIdx) {
                                                 
                [self startSession];

    
//
//        if([self isBeingPresented]){
//
//
//        [self dismissViewControllerAnimated:YES completion:^{
//     //CALLING DELEGATE METHOD
//     //to refresh data with latest webservice data
//     [self.delegate scanViewScreenBeingDismissed];
//            }];
//        }
//        //else if([self isMovingToParentViewController]){
//            else {
//                [CATransaction begin];
//                [CATransaction setCompletionBlock:^{
//                    // handle completion here
//
//                     //CALLING DELEGATE METHOD
//                    //       //to refresh data with latest webservice data
//                            [self.delegate scanViewScreenBeingDismissed];
//
//                }];
//
//                [self.navigationController popViewControllerAnimated:YES];
//
//                [CATransaction commit];
//
//      }
    }];
      
    
    
    popupView.contentTextAlignment  = NSTextAlignmentCenter;
    
    [popupView present];
    
}


/*
 Adding valid items
 */

-(void)addingValidItems: (NSString *)trackedCode{
    
     //First search if the scanned item belong to your list of webservice  or not
          //Then, checking if this has been already marked and counted or not
                          
          if ([self searchItem:trackedCode] && [self itemScannedAlready:trackedCode] == NO) {
              
            
              //Update the counter
               [self updateTotalCount];
               [self addItemInVerifiedTrackingCodeList:trackedCode];
              
              //Play Beep sound
              [self playBeepSound];
             
              
              //checking if its BULK PICKUP scan or just single Drop off
                    
                    if (self.is_singleScan == YES) {
                       

                        //if drop off then open a bottom sheet
                        if(self.is_pickup){
                          
                            //Call Webservice and send that tracking id immeditaely to server
                           [json ItineraryBulkScanApi:self.trackingIdWithTimeStamp];
                        }
                        else
                        {
                            //Its Single Scan . And would be used for drop off
                           //so after perfect match just move back to itinery orders and open BOttom sheet
                              

                            
                            [self.navigationController popViewControllerAnimated:YES];
                         [self.delegate openSheetOnDismissOfScanScreen:self.modelData];
                     
                        }
                        
                    }
          }
}




#pragma mark - Adding overlay to overlayController
-(void)addOverlayToCameraView{
    
}


#pragma mark - Remove overlay frm overlayController
-(void)removeOverlayFromCameraView{
    
    
}



#pragma mark - Set background colors

-(void)setBackgroundColors_invalid:(UIView *)OverlayView{
    if (@available(iOS 11.0, *)) {
       OverlayView.backgroundColor = [UIColor redColor];
        
        //Animate View for few seconds
        [self blinkView:OverlayView];
        
        
    } else {
        // Fallback on earlier versions
    }

}

-(void)setBackgroundColors_valid :(UIView *)OverlayView{
    if (@available(iOS 11.0, *)) {
        OverlayView.backgroundColor = [UIColor greenColor];
        
        
        //Animate View for few seconds
        [self blinkView:OverlayView];
        
    } else {
        // Fallback on earlier versions
    }

}


#pragma mark - Blinking Animation

-(void)blinkView:(UIView *)viewToAnimate{
    
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation setFromValue:[NSNumber numberWithFloat:1.0]];
    [animation setToValue:[NSNumber numberWithFloat:0.0]];
    [animation setDuration:0.4f];
    [animation setTimingFunction:[CAMediaTimingFunction
                                  functionWithName:kCAMediaTimingFunctionLinear]];
    [animation setAutoreverses:YES];
    [animation setRepeatCount:3];

    
    
    //To remove blinking colors
    [CATransaction setAnimationDuration:0.4f];
    [CATransaction setCompletionBlock:^{
        viewToAnimate.backgroundColor = [UIColor clearColor];
        
    }];

    [CATransaction begin];
    [[viewToAnimate layer] addAnimation:animation forKey:@"opacity"];
    [CATransaction commit];

    
    
}

-(void)playBeepSound{
    NSString *path  = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"wav"];
    NSURL *pathURL = [NSURL fileURLWithPath : path];

    SystemSoundID audioEffect;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef) pathURL, &audioEffect);
    AudioServicesPlaySystemSound(audioEffect);
    // Using GCD, we can use a block to dispose of the audio effect without using a NSTimer or something else to figure out when it'll be finished playing.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        AudioServicesDisposeSystemSoundID(audioEffect);
    });
}


#pragma mark - Call Webservice

-(void)callWebservice:(NSString *)trackingID{
    
    [json storePickup:trackingID];
}


#pragma mark -Webservice Delegate methods
- (void)parsedObjects:(NSString *)action objects:(NSString *)responseString {
    
    //Open success pop up
   // [self openDialog:responseString];
    [self startSession];
 
    [self validItemBottomUI:responseString];
    
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{

    [alertHelper showAlertWithOneButton:@"Error" message:message from:self buttonTitle:@"Ok" withHandler:^{
        
        [self inValidItemBottomUI:message];
               
             
        
          [self startSession];
        
       

        
//        if([self isBeingPresented]){
//                       [self dismissViewControllerAnimated:YES completion:nil];
//                 }
//                 //else if([self isMovingToParentViewController]){
//                     else {
//
//                         [self.navigationController popViewControllerAnimated:YES];
//                 }
    }];
    
   
          
}


#pragma mark - Asking Camera permisssion

-(void)askingCameraPermission{
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
       if(authStatus == AVAuthorizationStatusAuthorized)
       {
          // [self popCamera];
           [self startSession];

       }
       else if(authStatus == AVAuthorizationStatusNotDetermined)
       {
           NSLog(@"%@", @"Camera access not determined. Ask for permission.");

           [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
           {
               if(granted)
               {
                   NSLog(@"Granted access to %@", AVMediaTypeVideo);
                   [self startSession];

                 //  [self popCamera];
               }
               else
               {
                   NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                   [self camDenied];
               }
           }];
       }
       else if (authStatus == AVAuthorizationStatusRestricted)
       {

           [alertHelper showAlertWithOneButton:@"Error" message:@"Please allow camera permissions from setting app" from:self buttonTitle:@"Ok" withHandler:nil];

       }
       else
       {
           [self camDenied];
       }
    
}

- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");

    NSString *alertText;
    NSString *alertButton;

    [alertHelper showAlertWithOneButton:@"Error" message:@"Please allow camera permissions from setting app" from:self buttonTitle:@"Ok" withHandler:^{
         [self openSettings];
    }];
    
    
//  [alertHelper showAlertWithOneButton:@"Error" message:@"Please allow camera permissions from setting app" from:self buttonTitle:@"Ok" withHandler:{
//
//      [self openSettings];
//  }];

}

- (void)openSettings
{
   
  
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
      
        
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];

}

-(void)validItemBottomUI:(NSString *)msg{
    
    //Update the counter
    [self updateTotalCount];
  
     
    
    self.bottomBar_mainImage.image =[UIImage imageNamed:@"Status_green.png"];
    self.bottomBar_title.text = @"Valid";
    self.bottomBar_subtitle.text=msg;

}


-(void)inValidItemBottomUI:(NSString *)msg{

           
           self.bottomBar_mainImage.image =[UIImage imageNamed:@"invalid.png"];

    self.bottomBar_title.text = @"Invalid";
    self.bottomBar_subtitle.text=msg;
}




-(void)blinkView_infinite:(UIView *)viewToAnimate{
    
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation setFromValue:[NSNumber numberWithFloat:1.0]];
    [animation setToValue:[NSNumber numberWithFloat:0.0]];
    [animation setDuration:0.5f];
    [animation setTimingFunction:[CAMediaTimingFunction
                                  functionWithName:kCAMediaTimingFunctionLinear]];
    [animation setAutoreverses:YES];
    [animation setRepeatCount:20000];
    [[viewToAnimate layer] addAnimation:animation forKey:@"opacity"];
    
    
}

@end

NS_ASSUME_NONNULL_END
