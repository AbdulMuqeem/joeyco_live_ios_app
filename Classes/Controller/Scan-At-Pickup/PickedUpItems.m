//
//  PickedUpItems.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 28/07/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import "PickedUpItems.h"
#import "PickedUpItemsTableViewCell.h"
#import "Prefix.pch"

@interface PickedUpItems ()<JSONHelperDelegate,UITableViewDelegate,UITableViewDataSource>
{
    JSONHelper *json;
       AlertHelper *alertHelper;
       UIRefreshControl *refreshControl;
    
}
@property (weak, nonatomic) IBOutlet UIButton *uploadBtn;
@property (weak, nonatomic) IBOutlet UILabel *noDataExistsLabel;

@property(nonatomic,strong)NSMutableArray *pickupListData;
@end

@implementation PickedUpItems

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //init JSON
    json = [[JSONHelper alloc] init:self andDelegate:self];

    //Alert view controller
    alertHelper = [[AlertHelper alloc] init];

    
    _pickupListData=  [[NSMutableArray alloc]init];
    
     //Pull to refresh
    refreshControl = [[UIRefreshControl alloc]init];
     [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];

     if (@available(iOS 10.0, *)) {
         self.tableView.refreshControl = refreshControl;
     } else {
         [self.tableView addSubview:refreshControl];
     }
    
      //Call webservice
       [self callWebservice_FetchAllPickedItems];
    
}

- (void)refreshTable {
        //TODO: refresh your data
        [refreshControl endRefreshing];
        
        //Call webservice to get itinerary orders
        [self callWebservice_FetchAllPickedItems];
        
       
    }

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"pickedUpItemCell";
    
    PickedUpItemsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (!cell) cell = [[PickedUpItemsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
  
    if([_pickupListData count]>0){
        
        
        
          NSMutableDictionary *obj = [[NSMutableDictionary alloc]initWithDictionary:[_pickupListData objectAtIndex:indexPath.row]];
        
         cell.label_title.text = [[obj objForKey:@"contact"] objForKey:@"name"];
        
        cell.label_tracking_ID.text = [obj objForKey:@"tracking_id"];
        
    }
   
    
    
    
    return cell;
    
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_pickupListData count];
    
   
}


- (IBAction)upload:(id)sender {
    [self callWebservice_HubDeliveryOfAllPickedItems];
}

-(void)callWebservice_FetchAllPickedItems{
    [json getStorePickupList];
}



-(void)callWebservice_HubDeliveryOfAllPickedItems{
     [json hubDeliverByJoey];
}




/**
 On response of  success
 */
- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)response {
    
    
    if([action isEqualToString: @ACTION_storePickupList]){
        _pickupListData= response;
        
        
        //If no data exists
        if([_pickupListData count]<=0){
            self.tableView.hidden = YES;
             self.uploadBtn.hidden = YES;
            self.noDataExistsLabel.hidden = NO;
            
        }
        else{
            
            self.tableView.hidden = NO;
             self.uploadBtn.hidden = NO;
            self.noDataExistsLabel.hidden = YES;
        }
        
    }
    else
    {
        [alertHelper showAlertWithOneButton:@"Success" message:@"All items have been marked as delivered at HUB" from:self buttonTitle:@"Ok" withHandler:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    
    //Reload data
     [self.tableView reloadData];
    
}


- (void)errorMessage:(NSString *)action message:(NSString *)message
{

    [alertHelper showAlertWithOneButton:@"Error" message:message from:self buttonTitle:@"Ok" withHandler:^{
                    
                 }];
    
    if([action isEqualToString: @ACTION_storePickupList]){
        
       
    }
    else
    {
           
    }
}


@end
