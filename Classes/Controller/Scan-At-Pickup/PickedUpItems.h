//
//  PickedUpItems.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 28/07/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PickedUpItems : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *main_heading_label;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)upload:(id)sender;

@end

NS_ASSUME_NONNULL_END
