//
//  Chat_tawkTo.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 15/05/2019.
//  Copyright © 2019 JoeyCo. All rights reserved.
//


#import "NavigationContainerViewController.h"
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Chat_tawkTo : NavigationContainerViewController
@property (strong, nonatomic) IBOutlet WKWebView *webView;

@end

NS_ASSUME_NONNULL_END
