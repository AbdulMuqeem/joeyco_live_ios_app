//
//  SummaryShiftsViewController.h
//  Joey
//
//  Created by Katia Maeda on 2016-05-20.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Summary.h"

@interface SummaryShiftsViewController : UIViewController

@property (nonatomic, strong) Summary *summary;

@end
