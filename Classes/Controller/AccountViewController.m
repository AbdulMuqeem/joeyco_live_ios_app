//
//  AccountViewController.m
//  Joey
//
//  Created by Katia Maeda on 2015-02-23.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface AccountViewController () <UITableViewDataSource, UITableViewDelegate, JSONHelperDelegate, UITextViewDelegate, UITextFieldDelegate>
{
    AlertHelper *alertHelper;
    JSONHelper *json;
    CustomKeyboardToolbar *toolbar;
    Joey *joey;
    
    UIImage *imageSelected;
}

@property (nonatomic, weak) IBOutlet CustomScrollView *scrollView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *subviewHeight;

@property (nonatomic, weak) IBOutlet UIButton *imageButton;
@property (nonatomic, weak) IBOutlet UILabel *nameField;
@property (nonatomic, weak) IBOutlet UILabel *emailField;

@property (nonatomic, weak) IBOutlet UITextField *nicknameField;
@property (nonatomic, weak) IBOutlet UITextField *firstNameField;
@property (nonatomic, weak) IBOutlet UITextField *lastNameField;
@property (nonatomic, weak) IBOutlet UITextField *phoneField;

@property (nonatomic, weak) IBOutlet UITableView *namesTable;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *namesTableHeight;
@property (nonatomic, strong) NSMutableArray *namesList;

@property (nonatomic, weak) IBOutlet UIView *messageInputBorder;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *messageInputBorderHeight;
@property (nonatomic, weak) IBOutlet UITextView *messageInput;
@property (nonatomic, weak) IBOutlet UILabel *messageInputPlaceholder;

@property (nonatomic, weak) IBOutlet UISwitch *mapType;
@property (nonatomic, weak) IBOutlet UISwitch *bicycleModeSwitch;
@property (nonatomic, weak) IBOutlet UISwitch *largeFontSwitch;

@property (nonatomic, weak) IBOutlet UILabel *versionLabel;

@end

@implementation AccountViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    alertHelper = [[AlertHelper alloc] init];
    json = [[JSONHelper alloc] init:self andDelegate:self];
    
    // Register for TextField
    toolbar = [[CustomKeyboardToolbar alloc] initWithView:self.scrollView];
    [[NSNotificationCenter defaultCenter] addObserver:toolbar selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:toolbar selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    toolbar.textFields = @[self.nicknameField, self.phoneField, self.messageInput];
    self.nicknameField.inputAccessoryView = toolbar;
    self.firstNameField.inputAccessoryView = toolbar;
    self.lastNameField.inputAccessoryView = toolbar;
    self.phoneField.inputAccessoryView = toolbar;
    self.messageInput.inputAccessoryView = toolbar;
    
    UITapGestureRecognizer *hideKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    hideKeyboardGesture.numberOfTapsRequired = 1;
    hideKeyboardGesture.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:hideKeyboardGesture];

    Joey *savedJoey = [[DatabaseHelper shared] getJoey];
    if (!savedJoey)
    {
        return;
    }
    
    [self getPersonalData];
    
    MapTypeSetting mapType = [[DatabaseHelper shared] getMapTypeSetting];
    if (mapType == MapTypeSettingHybrid)
    {
        [self.mapType setOn:YES];
    }
    else if (mapType == MapTypeSettingNormal)
    {
        [self.mapType setOn:NO];
    }
    
    int route = [[DatabaseHelper shared] getMapRouteData];
    [self.bicycleModeSwitch setOn:(route == 0) ? NO : YES];
    
    BOOL displayLargeFont = [[DatabaseHelper shared] getDisplayLargeFontSize];
    [self.largeFontSwitch setOn:displayLargeFont];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    self.versionLabel.text = [NSString stringWithFormat:@"v%@%@", majorVersion, [UrlInfo getEnvironment]];
}

-(void)dealloc
{
    [self superDealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:toolbar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getPersonalData
{
    [json personal];
}

- (IBAction)toggleMapType
{
    [[DatabaseHelper shared] updateSetting:joeyFieldSettingMap value:(self.mapType.isOn) ? 1 : 0];
}

- (IBAction)toggleByclicleMode
{
    [[DatabaseHelper shared] updateSetting:joeyFieldSettingRoute value:(self.bicycleModeSwitch.isOn) ? 1 : 0];
}

- (IBAction)toggleLargeFont
{
    [[DatabaseHelper shared] updateSetting:joeyFieldSettingLargeFont value:(self.largeFontSwitch.isOn) ? 1 : 0];
}

#pragma mark - UITextFieldDelegate
-(void)setupNameTable
{
    Info *fullInfo = nil;
    Info *abbrInfo = nil;
    Info *nicknameInfo = nil;
    if (![self.firstNameField.text isEqualToString:@""])
    {
        if (![self.lastNameField.text isEqualToString:@""])
        {
            fullInfo = [[Info alloc] init:[NSString stringWithFormat:@"%@ %@", self.firstNameField.text, self.lastNameField.text] value:@"full" value2:@"0"];
            abbrInfo = [[Info alloc] init:[NSString stringWithFormat:@"%@ %c.", self.firstNameField.text, [self.lastNameField.text characterAtIndex:0]] value:@"abbr" value2:@"0"];
        }
        else
        {
            fullInfo = [[Info alloc] init:[NSString stringWithFormat:@"%@", self.firstNameField.text] value:@"full" value2:@"0"];
        }
    }
    else
    {
        if (![self.lastNameField.text isEqualToString:@""])
        {
            fullInfo = [[Info alloc] init:[NSString stringWithFormat:@"%@", self.lastNameField.text] value:@"full" value2:@"0"];
            abbrInfo = [[Info alloc] init:[NSString stringWithFormat:@"%c.", [self.lastNameField.text characterAtIndex:0]] value:@"abbr" value2:@"0"];
        }
    }
    
    if (![self.nicknameField.text isEqualToString:@""])
    {
        nicknameInfo = [[Info alloc] init:self.nicknameField.text value:@"nickname" value2:@"0"];
    }
    
    if ([joey.displayName isEqualToString:@"full"])
    {
        fullInfo.value2 = @"1";
        self.nameField.text = fullInfo.title;
    }
    else if ([joey.displayName isEqualToString:@"abbr"])
    {
        abbrInfo.value2 = @"1";
        self.nameField.text = abbrInfo.title;
    }
    else if ([joey.displayName isEqualToString:@"nickname"])
    {
        nicknameInfo.value2 = @"1";
        self.nameField.text = nicknameInfo.title;
    }
    
    self.namesList = [NSMutableArray array];
    if (fullInfo) {
        [self.namesList addObject:fullInfo];
    }
    if (abbrInfo)
    {
        [self.namesList addObject:abbrInfo];
    }
    if (nicknameInfo)
    {
        [self.namesList addObject:nicknameInfo];
    }

    [self.namesTable reloadData];
    self.namesTableHeight.constant = self.namesTable.contentSize.height;
}

#pragma mark - UITextViewDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.nicknameField || textField == self.firstNameField || textField == self.lastNameField)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        textField.text = newString;
        [self setupNameTable];
        return NO;
    }
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.scrollView scrollRectToVisible:textView.superview.frame animated:YES];
}

- (void)textViewDidChange:(UITextView *)textView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.messageInputPlaceholder.hidden = ([textView.text isEqualToString:@""]) ? NO : YES;
        self.messageInputBorderHeight.constant = [textView sizeThatFits:CGSizeMake(textView.frame.size.width,1000)].height + 5;
        if (self.messageInputBorderHeight.constant < 55)
        {
            self.messageInputBorderHeight.constant = 55;
        }
        self.subviewHeight.constant = self.messageInputBorderHeight.constant + 202 + [toolbar getHeyboardHeight];
        [toolbar updateContentHeight:self.subviewHeight.constant + 380];
    });
}

-(void)hideKeyboard
{
    [self.view endEditing:YES];
    [self.view setNeedsLayout];
    [self.view setNeedsDisplay];
}

#pragma mark - JSONHelperDelegate

- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    [super parsedObjects:action objects:list];
    
    if ([action isEqualToString:@ACTION_PERSONAL_DATA])
    {
        joey = [list firstObject];
        
        // save on database
        if (joey)
        {
            [[DatabaseHelper shared] updateJoey:joey];
            self.firstNameField.text = joey.firstName;
            self.lastNameField.text = joey.lastName;
            self.nicknameField.text = joey.nickName;
            self.emailField.text = joey.email;
            self.phoneField.text = joey.phone;
            self.messageInput.text = joey.about;
            self.messageInputPlaceholder.hidden = ([joey.about isEqualToString:@""]) ? NO : YES;
            
            if (joey.image && ![joey.image isEqualToString:@""])
            {
                [self.imageButton setBackgroundColor:[UIColor clearColor]];
                [self.imageButton setTitle:@"" forState:UIControlStateNormal];
                [self.imageButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
                [ImageHelper loadImage:[NSString stringWithFormat:@"%@?width=150&height=150&crop=1&pin=1", joey.image] toButton:self.imageButton authorization:[UrlInfo apiAuthentication]];
            }
            else
            {
                [self.imageButton setBackgroundImage:[UIImage imageNamed:@"profile.png"] forState:UIControlStateNormal];
            }

            [self setupNameTable];
        }
    }
    else if ([action isEqualToString:@ACTION_SAVE_PERSONAL_DATA])
    {
        __unsafe_unretained typeof(self) weakSelf = self;
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Saved", nil) message:@"" from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:^{
            [weakSelf getPersonalData];
        }];
    }
}

-(void)uploadData:(int64_t)totalSent totalToSend:(int64_t)totalToSend
{
    if (totalSent < totalToSend)
    {
        [alertHelper showProgressView:self message:[NSString stringWithFormat:NSLocalizedString(@"Sending picture %.1f%%", nil), ((CGFloat)totalSent/totalToSend)*100] totalSent:totalSent totalToSend:totalToSend];
    }
    else
    {
        [alertHelper dismissAlert];
    }
}

-(void)errorMessage:(NSString *)action message:(NSString *)message
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

-(void)errorMessage:(NSString *)action message:(NSString *)message fields:(NSArray *)fieldsArray
{
    [alertHelper showAlertWithOneButton:NSLocalizedString(@"Error", nil) message:message from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
}

#pragma mark - UITableView methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.namesList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"AccountNamesCell";
    
    TwoLabelsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (!cell) cell = [[TwoLabelsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    __weak TwoLabelsTableCell *weakCell = cell;

    dispatch_async(dispatch_get_main_queue(), ^{
        Info *info = [self.namesList objectAtIndex:indexPath.row];

        [weakCell.button setImage:[UIImage imageNamed:([info.value2 isEqualToString:@"1"]) ? @"radio-checked.png" : @"radio-unchecked.png"] forState:UIControlStateNormal];
        weakCell.button.buttonData = info;
        [weakCell.button addTarget:self action:@selector(itemSelected:) forControlEvents:UIControlEventTouchUpInside];
        
        weakCell.label1.text = info.title;
    });
    
    return weakCell;
}

-(void)itemSelected:(id)sender
{
    for (Info *info in self.namesList)
    {
        info.value2 = @"0";
    }
    
    CustomButton *button = sender;
    Info *info = button.buttonData;
    info.value2 = @"1";
    self.nameField.text = info.title;
    joey.displayName = info.value;
    
    [self.namesTable reloadData];
    self.namesTableHeight.constant = self.namesTable.contentSize.height;
}

#pragma mark - Actions

-(IBAction)takePictureAction
{
    CameraViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CameraViewController"];
    controller.pictureSelectedCompletion = ^(UIImage *image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (image)
            {
                imageSelected = image;

                UIImage *imageConverted = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                [self.imageButton setImage:imageConverted forState:UIControlStateNormal];
                [self.imageButton setTitle:@"" forState:UIControlStateNormal];
            }
            else
            {
                [alertHelper showAlertNoButtons:NSLocalizedString(@"Error", nil) type:AlertTypeNone message:NSLocalizedString(@"Picture not taken.", nil) dismissAfter:3.0 from:self];
            }
        });
    };
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:controller animated:YES];
    });
}

-(IBAction)saveAction
{
    [self hideKeyboard];
    
    if ([self.phoneField.text isEqualToString:@""])
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Required", nil) message:NSLocalizedString(@"First name, last name and phone are required fields.", nil)  from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    if (self.messageInput.text.length > 256)
    {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"About field", nil) message:NSLocalizedString(@"Limited to 256 characters.", nil) from:self buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }

    NSString *encodedImage = [UIImagePNGRepresentation(imageSelected) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];

    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"phone_number":self.phoneField.text}];
    if (encodedImage && ![encodedImage isEqualToString:@""]) {
        [jsonDictionary setObject:encodedImage forKey:@"image"];
    }
    if (![self.nicknameField.text isEqualToString:@""]) {
        [jsonDictionary setObject:self.nicknameField.text forKey:@"nickname"];
    }
    if (joey.displayName && ![joey.displayName isEqualToString:@""]) {
        [jsonDictionary setObject:joey.displayName forKey:@"display_name"];
    }
    if (![self.messageInput.text isEqualToString:@""]) {
        [jsonDictionary setObject:self.messageInput.text forKey:@"about"];
    }
    
    [jsonDictionary setObject:joey.joeyId forKey:@"joey_id"];
    [jsonDictionary setObject:joey.address forKey:@"address"];

    [json savePersonal:jsonDictionary];
}

@end
