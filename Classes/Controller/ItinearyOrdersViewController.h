//
//  ItinearyOrdersViewController.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 06/05/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import "NavigationContainerViewController.h"
#import "ReturnOrderVC.h"

#import "ItineraryOrder.h"

NS_ASSUME_NONNULL_BEGIN

@interface ItinearyOrdersViewController : NavigationContainerViewController<UITableViewDelegate,UITableViewDataSource,ReturnStatusUpdateDelegate>



@property (weak, nonatomic) IBOutlet UILabel *label_rightCounter;
@property (weak, nonatomic) IBOutlet UILabel *label_leftCounter;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic)NSMutableArray *tableViewData;
@property(strong, nonatomic)NSMutableArray *birdViewLocationData;

@property (strong, nonatomic) ItineraryOrder *itineraryData;
@property (weak, nonatomic) IBOutlet UIView *return_order_view;
@property (weak, nonatomic) IBOutlet UIView *bulkScanButtonView;
@property (weak, nonatomic) IBOutlet UILabel *noOrdersExistsLabel;
- (IBAction)openBirdView:(id)sender;

@end

NS_ASSUME_NONNULL_END
