//
//  OpenOrderDetailViewController.h
//  Joey
//
//  Created by Katia Maeda on 2015-02-18.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

@interface OpenOrderDetailViewController : NavigationContainerViewController

@property(nonatomic, weak) Order *order;

@end
