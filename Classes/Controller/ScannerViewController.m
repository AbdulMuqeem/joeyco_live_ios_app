//
//  ScannerViewController.m
//  Joey
//
//  Created by Katia Maeda on 2016-02-24.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface ScannerViewController ()
{
    BOOL scanDetected;
}

@property (nonatomic, weak) IBOutlet UIView *highlightView;
@property (nonatomic, weak) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *ScannedStatus;
@property (weak, nonatomic) IBOutlet UIView *bottomView1;

@property (strong, nonatomic) AVCaptureSession* session;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer* preview;

@property(nonatomic,strong)NSMutableArray *ordersList;

@end

@implementation ScannerViewController


-(NSString *)removeCharsAndAlphabet:(NSString * )dataString{
    NSString * number = dataString;

    NSString * strippedNumber = [number stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [number length])];

    return strippedNumber;
}

-(BOOL)checkIfOrderExists:(NSString * )scannedOrderID{
    
    BOOL idFound = false;
    
    for (id data in self.ordersList) {
        
        Order *order = data;
        NSString *orderIDInList = [self removeCharsAndAlphabet:order.num];
        
        scannedOrderID = [self removeCharsAndAlphabet:scannedOrderID];
        
        if ([orderIDInList isEqualToString:scannedOrderID]) {
            idFound = true;
            break;
        }
        
    }
    
    
    return idFound;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    /*
     Get Orders
     */
    self.ordersList = [[DatabaseHelper shared] getListOfAllOrders];

   
    /**
     Set Blinking Red line
     */
    [self blinkView_infinite: self.redBlinkingLine];
    
    
    
//    UIView *mask = [[UIView alloc] initWithFrame:(CGRect){0,0,100,100}];
//    mask.backgroundColor = [UIColor clearColor];
//    UIView *areaToReveal = [[UIView alloc] initWithFrame:(CGRect){20,20,50,50}];
//    areaToReveal.backgroundColor = [UIColor whiteColor];
//    [mask addSubview:areaToReveal];
//    self.greyBackgroundView.maskView = mask;

    
    if([self isCameraAvailable])
    {
        [self setupScanner];
    }
    else
    {
        [self setupNoCameraView];
    }
    scanDetected = NO;
    
    
    //Reset screen
    [self resetScreen];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)isCameraAvailable;
{
    NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    return [videoDevices count] > 0;
}

- (void) setupScanner;
{
    self.highlightView.frame = CGRectMake(0, 0, 1, 1);
    self.highlightView.layer.borderColor = [UIColor greenColor].CGColor;
    self.highlightView.layer.borderWidth = 3;
    
   // self.textLabel.backgroundColor = [UIColor colorWithWhite:0.15 alpha:0.65];
    self.textLabel.text = NSLocalizedString(@"(none)", nil);

    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
    self.session = [[AVCaptureSession alloc] init];
    [self.session addInput:input];
    
    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc] init];
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [self.session addOutput:output];
    output.metadataObjectTypes = output.availableMetadataObjectTypes;
    
    self.preview = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
    self.preview.frame = self.view.bounds;
    
    self.preview.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer insertSublayer:self.preview atIndex:0];
    
    [self.session startRunning];
}

- (void) setupNoCameraView;
{
    UILabel *labelNoCam = [[UILabel alloc] init];
    labelNoCam.text = NSLocalizedString(@"No Camera available", nil);
    labelNoCam.textColor = [UIColor blackColor];
    [self.view addSubview:labelNoCam];
    [labelNoCam sizeToFit];
    labelNoCam.center = self.view.center;
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)evt
{
    UITouch *touch=[touches anyObject];
    CGPoint pt= [touch locationInView:self.view];
    [self focus:pt];
}

#pragma mark -
#pragma mark Helper Methods

- (void)startScanning;
{
    [self.session startRunning];
}

- (void) stopScanning;
{
    [self.session stopRunning];
}

-(IBAction)toggleFlash:(id)sender
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    [device lockForConfiguration:nil];
    if ([device hasTorch])
    {
        AVCaptureTorchMode flashMode = [device torchMode];
        [device setTorchMode:(flashMode == AVCaptureTorchModeOff) ? AVCaptureTorchModeOn : AVCaptureTorchModeOff];
    }
    [device unlockForConfiguration];
}

- (void)focus:(CGPoint) aPoint;
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if([device isFocusPointOfInterestSupported] &&
       [device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        double screenWidth = screenRect.size.width;
        double screenHeight = screenRect.size.height;
        double focus_x = aPoint.x/screenWidth;
        double focus_y = aPoint.y/screenHeight;
        if([device lockForConfiguration:nil]) {
            [device setFocusPointOfInterest:CGPointMake(focus_x,focus_y)];
            [device setFocusMode:AVCaptureFocusModeAutoFocus];
            if ([device isExposureModeSupported:AVCaptureExposureModeAutoExpose]){
                [device setExposureMode:AVCaptureExposureModeAutoExpose];
            }
            [device unlockForConfiguration];
        }
    }
}

#pragma mark -
#pragma mark AVCaptureMetadataOutputObjectsDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects
       fromConnection:(AVCaptureConnection *)connection
{
    if (scanDetected)
    {
        return;
    }
    scanDetected = YES;
    
    CGRect highlightViewRect = CGRectZero;
    NSString *detectionString = nil;
    NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];
    
    for (AVMetadataObject *metadata in metadataObjects) {
        for (NSString *type in barCodeTypes) {
            if ([metadata.type isEqualToString:type])
            {
                AVMetadataMachineReadableCodeObject *barCodeObject = (AVMetadataMachineReadableCodeObject *)[self.preview transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                highlightViewRect = barCodeObject.bounds;
                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
                break;
            }
        }
        
        if (detectionString && ![detectionString isEqualToString:@""])
        {
            break;
        }
        else
        {
            detectionString = NSLocalizedString(@"(none)", nil);
            
        }
    }
    
    
    if ([self checkIfOrderExists:detectionString]) {
        
        [self validOrderScreen:[self removeCharsAndAlphabet:detectionString]];
    }
    else
    {
        
        ///check this faizzzzzzzzz if not working
        self.textLabel.text = detectionString;
        
        [self invalidOrderScreen:self.textLabel.text];


    }
    
    
    
    
    self.highlightView.frame = highlightViewRect;
    
    if([self.delegate respondsToSelector:@selector(scanViewController:didSuccessfullyScan:)])
    {
        [self.delegate scanViewController:self didSuccessfullyScan:detectionString];
    }
    
  //  [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - Button Actions

- (IBAction)NewAction:(id)sender {
    
    [self resetScreen];
}

- (IBAction)pauseAction:(id)sender {
    
    [self stopScanning];
}

- (IBAction)resumeAction:(id)sender {
    [self startScanning];

}

-(void)resetScreen{
    
    
    scanDetected = NO;

    [self stopScanning];

    [self startScanning];
    
    [self setViewtext_searching];
    [self setBackgroundColors_searching];
    
}



-(void)invalidOrderScreen:(NSString *)OrderId{
    
    [self setViewtext_invalid:OrderId];
    [self setBackgroundColors_invalid];
    
    
}


-(void)validOrderScreen:(NSString *)OrderId{
    [self setViewtext_valid:OrderId];
    [self setBackgroundColors_valid];
    
    
}


#pragma mark - Set background colors

-(void)setBackgroundColors_invalid{
    if (@available(iOS 11.0, *)) {
        self.bottomView1.backgroundColor = [UIColor colorWithRed:0.90 green:0.22 blue:0.21 alpha:1.0];
        
        //Animate View for few seconds
        [self blinkView:self.bottomView1];
        
    } else {
        // Fallback on earlier versions
    }

}

-(void)setBackgroundColors_valid{
    if (@available(iOS 11.0, *)) {
        self.bottomView1.backgroundColor = [UIColor colorWithRed:0.30 green:0.69 blue:0.31 alpha:1.0];
        
        
        //Animate View for few seconds
        [self blinkView:self.bottomView1];
        
    } else {
        // Fallback on earlier versions
    }

}


-(void)setBackgroundColors_searching{
    
    if (@available(iOS 11.0, *)) {
        
        self.bottomView1.backgroundColor = [UIColor colorWithRed:0.02 green:0.66 blue:0.96 alpha:1.0];
    } else {
        // Fallback on earlier versions
    }
    
}


#pragma mark - Set Button colors



#pragma mark - Set text

-(void)setViewtext_invalid:(NSString *)OrderId{
    self.textLabel.text = [NSString stringWithFormat:@"Order # %@",OrderId ];
    self.ScannedStatus.text = @"Invalid Order";
}

-(void)setViewtext_valid:(NSString *)OrderId{
    self.textLabel.text = [NSString stringWithFormat:@"Order # %@",OrderId ];
    self.ScannedStatus.text = @"Valid Order";
}

-(void)setViewtext_searching{
    
    self.textLabel.text = @"Searching...";
    self.ScannedStatus.text = @"Scan valid QR code";
}



#pragma mark - Blinking Animation

-(void)blinkView:(UIView *)viewToAnimate{
    
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation setFromValue:[NSNumber numberWithFloat:1.0]];
    [animation setToValue:[NSNumber numberWithFloat:0.0]];
    [animation setDuration:0.5f];
    [animation setTimingFunction:[CAMediaTimingFunction
                                  functionWithName:kCAMediaTimingFunctionLinear]];
    [animation setAutoreverses:YES];
    [animation setRepeatCount:5];
    [[viewToAnimate layer] addAnimation:animation forKey:@"opacity"];
    
    
}







-(void)blinkView_infinite:(UIView *)viewToAnimate{
    
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation setFromValue:[NSNumber numberWithFloat:1.0]];
    [animation setToValue:[NSNumber numberWithFloat:0.0]];
    [animation setDuration:0.5f];
    [animation setTimingFunction:[CAMediaTimingFunction
                                  functionWithName:kCAMediaTimingFunctionLinear]];
    [animation setAutoreverses:YES];
    [animation setRepeatCount:20000];
    [[viewToAnimate layer] addAnimation:animation forKey:@"opacity"];
    
    
}
@end
