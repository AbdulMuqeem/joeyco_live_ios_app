//
//  ChatChannelMessagesViewController.h
//  Joey
//
//  Created by Katia Maeda on 2016-01-28.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "NavigationContainerViewController.h"
#import "Channel.h"

@interface ChatChannelMessagesViewController : NavigationContainerViewController

@property(nonatomic, strong) Channel *channel;

@end
