//
//  CameraViewController.h
//  Customer
//
//  Created by Katia Maeda on 2015-01-30.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^PictureSelectedCompletionBlock)(UIImage *);

@interface CameraViewController : UIViewController

@property (nonatomic, strong) PictureSelectedCompletionBlock pictureSelectedCompletion;
@property (nonatomic, strong) UIImage *image;

@end
