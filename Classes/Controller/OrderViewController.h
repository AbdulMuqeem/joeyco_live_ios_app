//
//  OrderViewController.h
//  Customer
//
//  Created by Katia Maeda on 2015-06-17.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "NavigationContainerViewController.h"

@interface OrderViewController : NavigationContainerViewController

-(void)getOrderList;

@end
