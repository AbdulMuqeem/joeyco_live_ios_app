//
//  ViewController.m
//  Twilio Voice with CallKit Quickstart - Objective-C
//
//  Copyright © 2016-2018 Twilio, Inc. All rights reserved.
//

#import "CallNow.h"
#import "CallNow_DialPadViewController.h"

//static NSString *const kYourServerBaseURLString = @"http://1cb1a670.ngrok.io";
//// If your token server is written in PHP, kAccessTokenEndpoint needs .php extension at the end. For example : /accessToken.php
//static NSString *const kAccessTokenEndpoint = @"/accessToken";

//
//static NSString *const kYourServerBaseURLString = @"https://joeyco.herokuapp.com";
static NSString *const kYourServerBaseURLString = @"https://joeyco-live.herokuapp.com";


// If your token server is written in PHP, kAccessTokenEndpoint needs .php extension at the end. For example : /accessToken.php
static NSString *const kAccessTokenEndpoint = @"/accessToken";

static NSString *const kIdentity = @"Joey";
static NSString *const kTwimlParamTo = @"to";

@interface CallNow () 

{
    dispatch_queue_t dq;

}


@end

@implementation CallNow




#pragma mark - AVAudioSession
- (void)toggleAudioRoute:(BOOL)toSpeaker {
    // The mode set by the Voice SDK is "VoiceChat" so the default audio route is the built-in receiver. Use port override to switch the route.
    NSError *error = nil;
    if (toSpeaker) {
        if (![[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error]) {
            NSLog(@"Unable to reroute audio: %@", [error localizedDescription]);
        }
    } else {
        if (![[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error]) {
            NSLog(@"Unable to reroute audio: %@", [error localizedDescription]);
        }
    }
}

#pragma mark - Icon spinning
- (void)startSpin {
    if (!self.isSpinning) {
        self.spinning = YES;
        [self spinWithOptions:UIViewAnimationOptionCurveEaseIn];
    }
}

- (void)stopSpin {
    self.spinning = NO;
}

- (void)spinWithOptions:(UIViewAnimationOptions)options {
    typeof(self) __weak weakSelf = self;

    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:options
                     animations:^{
                         typeof(self) __strong strongSelf = weakSelf;
                         strongSelf.iconView.transform = CGAffineTransformRotate(strongSelf.iconView.transform, M_PI / 2);
                     }
                     completion:^(BOOL finished) {
                         typeof(self) __strong strongSelf = weakSelf;
                         if (finished) {
                             if (strongSelf.isSpinning) {
                                 [strongSelf spinWithOptions:UIViewAnimationOptionCurveLinear];
                             } else if (options != UIViewAnimationOptionCurveEaseOut) {
                                 [strongSelf spinWithOptions:UIViewAnimationOptionCurveEaseOut];
                             }
                         }
                     }];
}


+(CallNow *) shared
{
    static CallNow *sharedInstance = nil;
    static dispatch_once_t onceToken;

    @synchronized(self) {
        
   if(sharedInstance == nil) {


    dispatch_once(&onceToken, ^{
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
        sharedInstance=[storyboard instantiateViewControllerWithIdentifier:@"CallNow"];
        

       
        
        
    });
    }
    }
    return sharedInstance;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
  
    //Alert view controller
    alertHelper = [[AlertHelper alloc] init];
    
    self.chronometer = [[MZTimerLabel alloc] initWithLabel:self.chronometerLabel];

    
    //[self InitCall];
}



/*
*
*Call Webservice - Call Time API
**/


-(void)UpdateCallTimeToServer{
    
    
    //Setting current time if unable to get proper time
    if ([self.start_time length] == 0) {
        self.start_time = [self getCurrentDate];
    }
    
    if ([self.end_time length] == 0) {
        self.end_time = [self getCurrentDate];
    }
    
    
    [json callTimeApi:self.Order_ID
              joey_id:self.Joey_ID
           start_time:self.start_time
             end_time:self.end_time];
}



-(NSString *)getCurrentDate{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    
    NSDate *currentDate = [NSDate date];
    return  [formatter stringFromDate:currentDate];

}




/**
 
 Call connecting
 */
-(void)callConnectingUI{
    
    self.CallStatus.text = @"Connecting...";

    
    [self.mainCallButton setImage:[UIImage imageNamed:@"call_end.png"] forState:UIControlStateNormal];
    
    self.mainCallButtonText.text = @"End Call";
}


/**
 
 Call disconnected button UI
 */
-(void)callEndedUI{
 
    [self.mainCallButton setImage:[UIImage imageNamed:@"call_start.png"] forState:UIControlStateNormal];
    
    self.mainCallButtonText.text = @"Start Call";
}



/**
 
 Call connected main button UI
 */
-(void)callStartedUI{
    
    [self.mainCallButton setImage:[UIImage imageNamed:@"call_end.png"] forState:UIControlStateNormal];
    
    self.mainCallButtonText.text = @"End Call";
}



-(void)viewWillAppear:(BOOL)animated{
    
    /*
     Init audio device only once using shared singleton
     ***/
    [CallNow shared];
    
    
    if (_isComingFromDialpadScreen && self.call == nil) {
    
    dispatch_async( dispatch_get_main_queue(), ^{

        
        //Call was disconnected and now shutting down this call UI
     [self dismissViewControllerAnimated:YES completion:nil] ;
    });
        
        }
    else if (self.call == nil ||  self.call.state == TVOCallStateConnecting) {
    
        
        [self callConnectingUI];
    
        
    }
}

/**
 This view have just appear
 */
-(void)viewDidAppear:(BOOL)animated{
    
    //If call is still alive then show "Tap to return to call " button otherwise hide it
    
    if (_isComingFromDialpadScreen && self.call && self.call.state == TVOCallStateConnected) {
      
        //Do nothing
        
    } else  if (self.call && self.call.state == TVOCallStateConnected) {
        
   
        /**
         Set UI if call is connected
         */
        [self callStartedUI];
        
        
        
     
        
        

  self.popupView = [ZHPopupView popUpDialogViewInView:nil
                                                        iconImg:[UIImage imageNamed:@"call_start"]
                                                backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                          title:@"Call on hold"
                                                        content:@"Do you want to resume call?"
                                                   buttonTitles:@[@"Start New call", @"Resume"]
                                            confirmBtnTextColor:nil otherBtnTextColor:nil
                                             buttonPressedBlock:^(NSInteger btnIdx) {
                                                 
                                                 if (btnIdx == 0) {
                                                     
                                                     [self InitCall];

                                                 }
                                                 else{
                                                     [self.popupView disappear];
                                                 }
                                                 
//                                                 [self clearAllTextField];
//
//                                                 [self.view.window endEditing:YES];
                                                 
                                                 
                                                 
                                             }];
    
    self.popupView.contentTextAlignment  = NSTextAlignmentCenter;
    
    [self.popupView present];
    }
    
    
    //if (self.call == nil ||  self.call.state == TVOCallStateConnecting) {
    if (self.call == nil ||  self.call.state == TVOCallStateConnecting) {
        
        [self InitCall];

        
    }
    
    
    
}


- (IBAction)backButtonAction:(id)sender {
    
    dispatch_async( dispatch_get_main_queue(), ^{

    ///Go back
    [self dismissViewControllerAnimated:YES completion:nil] ;
        
    });
}



-(id) init
{
    if(self = [super init])
    {
       self. observer = [[CXCallObserver alloc] init];
        // Hang on to a reference to the queue, as the
        // CXCallObserver only maintains a weak reference
        dq = dispatch_queue_create("my.ios10.call.status.queue", DISPATCH_QUEUE_SERIAL);
        [self.observer setDelegate: self queue: dq];
    }
    return self;
}


-(void)InitCall{
    
    json = [[JSONHelper alloc] init:self andDelegate:self];

    
    //self.PhoneNum = @"923452962859";
    
    
   // [TwilioVoice setLogLevel:TVOLogLevelVerbose];
    
   
    
    [self toggleUIState:YES showCallControl:NO];
    self.outgoingValue.delegate = self;
    
    [self configureCallKit];
    
    /*
        * The important thing to remember when providing a TVOAudioDevice is that the device must be set
        * before performing any other actions with the SDK (such as connecting a Call, or accepting an incoming Call).
        * In this case we've already initialized our own `TVODefaultAudioDevice` instance which we will now set.
        */
    
    if(self.audioDevice == nil){
       self.audioDevice = [TVODefaultAudioDevice audioDevice];
       TwilioVoice.audioDevice = self.audioDevice;
    }
//       
       self.activeCallInvites = [NSMutableDictionary dictionary];
       self.activeCalls = [NSMutableDictionary dictionary];
    
    
  
    
    //Place the call
    [self PlaceCall];

    
    //Check when your app is going to be ended
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ForceFulCallEnd) name:UIApplicationWillTerminateNotification object:nil];
}

- (void)configureCallKit {
    CXProviderConfiguration *configuration = [[CXProviderConfiguration alloc] initWithLocalizedName:@"Dialing..."];
    configuration.maximumCallGroups = 1;
    configuration.maximumCallsPerCallGroup = 1;
    UIImage *callkitIcon = [UIImage imageNamed:@"iconMask80"];
    configuration.iconTemplateImageData = UIImagePNGRepresentation(callkitIcon);

    _callKitProvider = [[CXProvider alloc] initWithConfiguration:configuration];
    [_callKitProvider setDelegate:self queue:nil];

    _callKitCallController = [[CXCallController alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    if (self.callKitProvider) {
        [self.callKitProvider invalidate];
    }
}

-(void)ForceFulCallEnd{
    if (self.call && self.call.state == TVOCallStateConnected) {
        [self.call disconnect];
        [self toggleUIState:NO showCallControl:NO];
        
        //Send updates to server
        [self UpdateCallTimeToServer];
    }
}




- (NSString *)fetchAccessToken {
    NSString *accessTokenEndpointWithIdentity = [NSString stringWithFormat:@"%@?identity=%@", kAccessTokenEndpoint, kIdentity];
    NSString *accessTokenURLString = [kYourServerBaseURLString stringByAppendingString:accessTokenEndpointWithIdentity];

    NSString *accessToken = [NSString stringWithContentsOfURL:[NSURL URLWithString:accessTokenURLString]
                                                     encoding:NSUTF8StringEncoding
                                                        error:nil];
    return accessToken;
}

- (IBAction)placeCall:(id)sender {
    [self PlaceCall];
}





- (void)checkRecordPermission:(void(^)(BOOL permissionGranted))completion {
    AVAudioSessionRecordPermission permissionStatus = [[AVAudioSession sharedInstance] recordPermission];
    switch (permissionStatus) {
        case AVAudioSessionRecordPermissionGranted:
            // Record permission already granted.
            completion(YES);
            break;
        case AVAudioSessionRecordPermissionDenied:
            // Record permission denied.
            completion(NO);
            break;
        case AVAudioSessionRecordPermissionUndetermined:
        {
            // Requesting record permission.
            // Optional: pop up app dialog to let the users know if they want to request.
            [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
                completion(granted);
            }];
            break;
        }
        default:
            completion(NO);
            break;
    }
}


/*
 
 Note the time when call was started and ended
 
 */



- (void)performEndCallActionWithUUID:(NSUUID *)uuid {
    if (uuid == nil) {
        return;
    }

    CXEndCallAction *endCallAction = [[CXEndCallAction alloc] initWithCallUUID:uuid];
    CXTransaction *transaction = [[CXTransaction alloc] initWithAction:endCallAction];

    [self.callKitCallController requestTransaction:transaction completion:^(NSError *error) {
        if (error) {
            NSLog(@"EndCallAction transaction request failed: %@", [error localizedDescription]);
        }
        else {
            NSLog(@"EndCallAction transaction request successful");
        }
    }];
}


#pragma mark - CallKit Actions
- (void)performStartCallActionWithUUID:(NSUUID *)uuid handle:(NSString *)handle {
    if (uuid == nil || handle == nil) {
        return;
    }

    CXHandle *callHandle = [[CXHandle alloc] initWithType:CXHandleTypeGeneric value:handle];
    CXStartCallAction *startCallAction = [[CXStartCallAction alloc] initWithCallUUID:uuid handle:callHandle];
    CXTransaction *transaction = [[CXTransaction alloc] initWithAction:startCallAction];

    [self.callKitCallController requestTransaction:transaction completion:^(NSError *error) {
        if (error) {
            NSLog(@"StartCallAction transaction request failed: %@", [error localizedDescription]);
        } else {
            NSLog(@"StartCallAction transaction request successful");

            CXCallUpdate *callUpdate = [[CXCallUpdate alloc] init];
            callUpdate.remoteHandle = callHandle;
            callUpdate.supportsDTMF = YES;
            callUpdate.supportsHolding = YES;
            callUpdate.supportsGrouping = NO;
            callUpdate.supportsUngrouping = NO;
            callUpdate.hasVideo = NO;

            [self.callKitProvider reportCallWithUUID:uuid updated:callUpdate];
        }
        
        
        
    }];
}

- (void)reportIncomingCallFrom:(NSString *) from withUUID:(NSUUID *)uuid {
    CXHandle *callHandle = [[CXHandle alloc] initWithType:CXHandleTypeGeneric value:from];

    CXCallUpdate *callUpdate = [[CXCallUpdate alloc] init];
    callUpdate.remoteHandle = callHandle;
    callUpdate.supportsDTMF = YES;
    callUpdate.supportsHolding = YES;
    callUpdate.supportsGrouping = NO;
    callUpdate.supportsUngrouping = NO;
    callUpdate.hasVideo = NO;

    [self.callKitProvider reportNewIncomingCallWithUUID:uuid update:callUpdate completion:^(NSError *error) {
        if (!error) {
            NSLog(@"Incoming call successfully reported.");

            // RCP: Workaround per https://forums.developer.apple.com/message/169511
         //   [TwilioVoice configureAudioSession];
            [self toggleAudioRoute:YES];
        }
        else {
            NSLog(@"Failed to report incoming call successfully: %@.", [error localizedDescription]);
        }
    }];
}


-(void)PlaceCall{
    
    //Remove instance of call
    self.call = nil;
    
//    if (self.call && self.call.state == TVOCallStateConnected) {
//        [self.call disconnect];
//        [self toggleUIState:NO showCallControl:NO];
//
//        //Send updates to server
//        [self UpdateCallTimeToServer];
//
//    } else {
//        NSUUID *uuid = [NSUUID UUID];
//        NSString *handle = @"Joey Call";
//
//        [self performStartCallActionWithUUID:uuid handle:handle];
//    }
    
    
    
    /*
     TWILIO 5.0 changes
     **/
    
    if (self.call != nil) {
        self.userInitiatedDisconnect = YES;
        [self performEndCallActionWithUUID:self.call.uuid];
        [self toggleUIState:NO showCallControl:NO];
    } else {
        NSUUID *uuid = [NSUUID UUID];
        NSString *handle = @"Joey Call";
        
        [self checkRecordPermission:^(BOOL permissionGranted) {
            if (!permissionGranted) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Voice Quick Start"
                                                                                         message:@"Microphone permission not granted."
                                                                                  preferredStyle:UIAlertControllerStyleAlert];

                typeof(self) __weak weakSelf = self;
                UIAlertAction *continueWithoutMic = [UIAlertAction actionWithTitle:@"Continue without microphone"
                                                                             style:UIAlertActionStyleDefault
                                                                           handler:^(UIAlertAction *action) {
                    typeof(self) __strong strongSelf = weakSelf;
                    [strongSelf performStartCallActionWithUUID:uuid handle:handle];
                }];
                [alertController addAction:continueWithoutMic];

                NSDictionary *openURLOptions = @{UIApplicationOpenURLOptionUniversalLinksOnly: @NO};
                UIAlertAction *goToSettings = [UIAlertAction actionWithTitle:@"Settings"
                                                                       style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction *action) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]
                                                       options:openURLOptions
                                             completionHandler:nil];
                }];
                [alertController addAction:goToSettings];
                
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                                 style:UIAlertActionStyleCancel
                                                               handler:^(UIAlertAction *action) {
                    typeof(self) __strong strongSelf = weakSelf;
                    [strongSelf toggleUIState:YES showCallControl:NO];
                    [strongSelf stopSpin];
                }];
                [alertController addAction:cancel];
                
                [self presentViewController:alertController animated:YES completion:nil];
            } else {
                [self performStartCallActionWithUUID:uuid handle:handle];
            }
        }];
    }
    
    
}

- (void)toggleUIState:(BOOL)isEnabled showCallControl:(BOOL)showCallControl {
    self.placeCallButton.enabled = isEnabled;
    if (showCallControl) {
        self.callControlView.hidden = NO;
        self.muteSwitch.on = NO;
        self.speakerSwitch.on = NO;
        
        
        
     

    } else {
        self.callControlView.hidden = YES;
    }
}

- (IBAction)muteSwitchToggled:(UISwitch *)sender {
    self.call.muted = sender.on;
}

- (IBAction)speakerSwitchToggled:(UISwitch *)sender {
    [self toggleAudioRoute:sender.on];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.outgoingValue resignFirstResponder];
    return YES;
}

//#pragma mark - PKPushRegistryDelegate
//- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(NSString *)type {
//    NSLog(@"pushRegistry:didUpdatePushCredentials:forType:");
//
//    if ([type isEqualToString:PKPushTypeVoIP]) {
//        self.deviceTokenString = [credentials.token description];
//        NSString *accessToken = [self fetchAccessToken];
//
//        [TwilioVoice registerWithAccessToken:accessToken
//                                 deviceToken:self.deviceTokenString
//                                  completion:^(NSError *error) {
//             if (error) {
//                 NSLog(@"An error occurred while registering: %@", [error localizedDescription]);
//             }
//             else {
//                 NSLog(@"Successfully registered for VoIP push notifications.");
//             }
//         }];
//    }
//}
//
//- (void)pushRegistry:(PKPushRegistry *)registry didInvalidatePushTokenForType:(PKPushType)type {
//    NSLog(@"pushRegistry:didInvalidatePushTokenForType:");
//
//    if ([type isEqualToString:PKPushTypeVoIP]) {
//        NSString *accessToken = [self fetchAccessToken];
//
//        [TwilioVoice unregisterWithAccessToken:accessToken
//                                   deviceToken:self.deviceTokenString
//                                    completion:^(NSError * _Nullable error) {
//            if (error) {
//                NSLog(@"An error occurred while unregistering: %@", [error localizedDescription]);
//            }
//            else {
//                NSLog(@"Successfully unregistered for VoIP push notifications.");
//            }
//        }];
//
//        self.deviceTokenString = nil;
//    }
//}
//



- (void)setupPushRegistry {
    self.voipRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
    self.voipRegistry.delegate = self;
    self.voipRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
}


//#pragma mark - PKPushRegistryDelegate
//- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(NSString *)type {
//    NSLog(@"pushRegistry:didUpdatePushCredentials:forType:");
//
//    if ([type isEqualToString:PKPushTypeVoIP]) {
//        if (self.pushKitEventDelegate && [self.pushKitEventDelegate respondsToSelector:@selector(credentialsUpdated:)]) {
//            [self.pushKitEventDelegate credentialsUpdated:credentials];
//        }
//    }
//}

//- (void)pushRegistry:(PKPushRegistry *)registry didInvalidatePushTokenForType:(PKPushType)type {
//    NSLog(@"pushRegistry:didInvalidatePushTokenForType:");
//
//    if ([type isEqualToString:PKPushTypeVoIP]) {
//        if (self.pushKitEventDelegate && [self.pushKitEventDelegate respondsToSelector:@selector(credentialsInvalidated)]) {
//            [self.pushKitEventDelegate credentialsInvalidated];
//        }
//    }
//}
//
//- (void)pushRegistry:(PKPushRegistry *)registry
//didReceiveIncomingPushWithPayload:(PKPushPayload *)payload
//             forType:(NSString *)type {
//    NSLog(@"pushRegistry:didReceiveIncomingPushWithPayload:forType:");
//
//    if (self.pushKitEventDelegate &&
//        [self.pushKitEventDelegate respondsToSelector:@selector(incomingPushReceived:withCompletionHandler:)]) {
//        [self.pushKitEventDelegate incomingPushReceived:payload withCompletionHandler:nil];
//    }
//}
//
///**
// * This delegate method is available on iOS 11 and above. Call the completion handler once the
// * notification payload is passed to the `TwilioVoice.handleNotification()` method.
// */
//- (void)pushRegistry:(PKPushRegistry *)registry
//didReceiveIncomingPushWithPayload:(PKPushPayload *)payload
//             forType:(PKPushType)type
//withCompletionHandler:(void (^)(void))completion {
//    NSLog(@"pushRegistry:didReceiveIncomingPushWithPayload:forType:withCompletionHandler:");
//
//    if (self.pushKitEventDelegate &&
//        [self.pushKitEventDelegate respondsToSelector:@selector(incomingPushReceived:withCompletionHandler:)]) {
//        [self.pushKitEventDelegate incomingPushReceived:payload withCompletionHandler:completion];
//    }
//
//    if ([[NSProcessInfo processInfo] operatingSystemVersion].majorVersion >= 13) {
//        /*
//         * The Voice SDK processes the call notification and returns the call invite synchronously. Report the incoming call to
//         * CallKit and fulfill the completion before exiting this callback method.
//         */
//        completion();
//    }
//}

////
////#pragma mark - PKPushRegistryDelegate
////- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(NSString *)type {
////    NSString *accessToken = [self fetchAccessToken];
////
////    [TwilioVoice registerWithAccessToken:accessToken
////                         deviceTokenData:credentials.token
////                              completion:^(NSError *error) {
////    }];
////}
////
////- (void)pushRegistry:(PKPushRegistry *)registry
////didReceiveIncomingPushWithPayload:(PKPushPayload *)payload
////             forType:(PKPushType)type
////withCompletionHandler:(void (^)(void))completion {
////    if ([payload.dictionaryPayload[@"twi_message_type"] isEqualToString:@"twilio.voice.cancel"]) {
////        CXHandle *callHandle = [[CXHandle alloc] initWithType:CXHandleTypeGeneric value:@"alice"];
////
////        CXCallUpdate *callUpdate = [[CXCallUpdate alloc] init];
////        callUpdate.remoteHandle = callHandle;
////        callUpdate.supportsDTMF = YES;
////        callUpdate.supportsHolding = YES;
////        callUpdate.supportsGrouping = NO;
////        callUpdate.supportsUngrouping = NO;
////        callUpdate.hasVideo = NO;
////
////        NSUUID *uuid = [NSUUID UUID];
////
////        [self.callKitProvider reportNewIncomingCallWithUUID:uuid update:callUpdate completion:^(NSError *error) {
////        }];
////
////        dispatch_async(dispatch_get_main_queue(), ^{
////            CXEndCallAction *endCallAction = [[CXEndCallAction alloc] initWithCallUUID:uuid];
////            CXTransaction *transaction = [[CXTransaction alloc] initWithAction:endCallAction];
////
////            [self.callKitCallController requestTransaction:transaction completion:^(NSError *error) {
////
////            }];
////        });
////
////        return;
////    }
////}
///**
// * Try using the `pushRegistry:didReceiveIncomingPushWithPayload:forType:withCompletionHandler:` method if
// * your application is targeting iOS 11. According to the docs, this delegate method is deprecated by Apple.
// */
//- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(NSString *)type {
//    NSLog(@"pushRegistry:didReceiveIncomingPushWithPayload:forType:");
//    if ([type isEqualToString:PKPushTypeVoIP]) {
//        [TwilioVoice handleNotification:payload.dictionaryPayload
//                               delegate:self];
//    }
//}
//
///**
// * This delegate method is available on iOS 11 and above. Call the completion handler once the
// * notification payload is passed to the `TwilioVoice.handleNotification()` method.
// */
//- (void)pushRegistry:(PKPushRegistry *)registry
//didReceiveIncomingPushWithPayload:(PKPushPayload *)payload
//             forType:(PKPushType)type
//withCompletionHandler:(void (^)(void))completion {
//    NSLog(@"pushRegistry:didReceiveIncomingPushWithPayload:forType:withCompletionHandler:");
//    if ([type isEqualToString:PKPushTypeVoIP]) {
//        [TwilioVoice handleNotification:payload.dictionaryPayload
//                               delegate:self];
//    }
//
//    completion();
//}

#pragma mark - TVONotificationDelegate
- (void)callInviteReceived:(TVOCallInvite *)callInvite {
//    if (callInvite.state == TVOCallInviteStatePending) {
//        [self handleCallInviteReceived:callInvite];
//    } else if (callInvite.state == TVOCallInviteStateCanceled) {
//        [self handleCallInviteCanceled:callInvite];
//    }
}

- (void)handleCallInviteReceived:(TVOCallInvite *)callInvite {
//    NSLog(@"callInviteReceived:");
//
//    if (self.callInvite && self.callInvite == TVOCallInviteStatePending) {
//        NSLog(@"Already a pending incoming call invite.");
//        NSLog(@"  >> Ignoring call from %@", callInvite.from);
//        return;
//    } else if (self.call) {
//        NSLog(@"Already an active call.");
//        NSLog(@"  >> Ignoring call from %@", callInvite.from);
//        return;
//    }
//
//    self.callInvite = callInvite;
//
//    [self reportIncomingCallFrom:@"Voice Bot" withUUID:callInvite.uuid];
}

- (void)handleCallInviteCanceled:(TVOCallInvite *)callInvite {
    NSLog(@"callInviteCanceled:");

    [self performEndCallActionWithUUID:callInvite.uuid];

    self.callInvite = nil;
}

- (void)notificationError:(NSError *)error {
    NSLog(@"notificationError: %@", [error localizedDescription]);
}

#pragma mark - TVOCallDelegate
- (void)callDidConnect:(TVOCall *)call {
    NSLog(@"callDidConnect:");
    
    
    //Set end call UI
    [self callStartedUI];
    
    self.CallStatus.text = @"Connected";
    
    //Start chronometer
    [self chronometerStart];
    
    //Show dialpad
    [self showDialPad];
    

    self.call = call;
    
    @try {
        self.callKitCompletionCallback(YES);
    }
    @catch (NSException *exception) {
        NSLog(@"**** TWILIO CALL CRASHED ");
    }
    @finally {
        
    }
    self.callKitCompletionCallback = nil;
    
    [self.placeCallButton setTitle:@"Hang Up" forState:UIControlStateNormal];
    
    [self toggleUIState:YES showCallControl:YES];
    
    //Initially set to unmute and speaker OFF
    self.call.muted =NO;
    [self toggleAudioRoute:NO];
    
    
    [self stopSpin];
    
    
    
}
//
//- (void)call:(TVOCall *)call didFailToConnectWithError:(NSError *)error {
    
- (void)call:(nonnull TVOCall *)call didFailToConnectWithError:(nonnull NSError *)error{

    NSLog(@"Call failed to connect: %ld", (long)error.code);
    
    self.CallStatus.text = @"Calling Failed";

    
    self.callKitCompletionCallback(NO);
    [self performEndCallActionWithUUID:call.uuid];
    [self callDisconnected];
}

-(void)call:(nonnull TVOCall *)call didDisconnectWithError:(nullable NSError *)error{
//
//- (void)call:(TVOCall *)call didDisconnectWithError:(NSError *)error {
    if (error) {
        NSLog(@"Call failed: %@", error);
        self.CallStatus.text = @"Calling Failed";

    } else {
        NSLog(@"Call disconnected");
        self.CallStatus.text = @"Call disconnected";

    }
    
    
    
    [self performEndCallActionWithUUID:call.uuid];
    [self callDisconnected];
}

- (void)callDisconnected {
    
//    NSDictionary *dict = [NSDictionary dictionaryWithObject:@"NO" forKey:@"callConnected"];
//
//    [[NSNotificationCenter defaultCenter]
//     postNotificationName:@"Notification_call_status" object:nil userInfo:dict];
//
    

    //Broadcast this call ended to navigation screen
    //[self.delegate callDisconnected:self];
    
    //Set end call UI
    [self callEndedUI];
    

    //Hide DialPad
    [self hideDialPad];
    
    
    self.call = nil;
    self.callKitCompletionCallback = nil;
    
    self.CallStatus.text = @"Call Ended";

    //Stop the timer
    [self chronometerStops];
    
    [self stopSpin];
    [self toggleUIState:YES showCallControl:NO];
    [self.placeCallButton setTitle:@"Call" forState:UIControlStateNormal];
    
    
    
    
    //Set Call end time
    self.end_time =  [self getCurrentDate];
    
    
    
    //Send updates to server
    [self UpdateCallTimeToServer];
    
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"Notification_call_status"
     object:self];
    
    
    
    //Reseting to Normal
    _isComingFromDialpadScreen = NO;
    
    ///Go back
    //[self dismissViewControllerAnimated:YES completion:nil] ;
    
    
    //Call has been ended , so if there are no pop ups already shown then you can show this pop up 
    
    if(self.popupView == nil){
    [self  showAlertCallEnded];
     }
         
}


#pragma mark - CXCallObserverDelegate

- (void)callObserver:(CXCallObserver *)callObserver callChanged:(CXCall *)call
{
    NSString* callId = [call.UUID UUIDString];
    
    if(!call.hasConnected && !call.hasEnded && !call.onHold)
    {
        if(call.outgoing)
        {
            // outgoing call is being dialling;
            self.CallStatus.text = @"Dialing...";

        }
       
    }
    else if(call.onHold)
    {
        // call is on hold;
        self.CallStatus.text = @"Call is on Hold";

    }
    else if(call.hasEnded)
    {
        // call has disconnected;
        self.CallStatus.text = @"Call Ended";

    }
    else if(call.hasConnected)
    {
        // call has been answered;
        self.CallStatus.text = @"Call Connected";

    }
}


#pragma mark - CXProviderDelegate
- (void)providerDidReset:(CXProvider *)provider {
    NSLog(@"providerDidReset:");
self.audioDevice.enabled = YES;}

- (void)providerDidBegin:(CXProvider *)provider {
    NSLog(@"providerDidBegin:");
}

- (void)provider:(CXProvider *)provider didActivateAudioSession:(AVAudioSession *)audioSession {
    NSLog(@"provider:didActivateAudioSession:");
    self.audioDevice.enabled = YES;
}

- (void)provider:(CXProvider *)provider didDeactivateAudioSession:(AVAudioSession *)audioSession {
    NSLog(@"provider:didDeactivateAudioSession:");
     self.audioDevice.enabled= NO;
}

- (void)provider:(CXProvider *)provider timedOutPerformingAction:(CXAction *)action {
    NSLog(@"provider:timedOutPerformingAction:");
}

- (void)provider:(CXProvider *)provider performStartCallAction:(CXStartCallAction *)action {
    NSLog(@"provider:performStartCallAction:");
    
    [self toggleUIState:NO showCallControl:NO];
    //[self startSpin];

   // [TwilioVoice configureAudioSession];
 //   [self toggleAudioRoute:YES];
//    TwilioVoice.audioEnabled = NO;
  
        self.audioDevice.enabled = NO;
       self.audioDevice.block();
    
    
    [self.callKitProvider reportOutgoingCallWithUUID:action.callUUID startedConnectingAtDate:[NSDate date]];
    
    __weak typeof(self) weakSelf = self;
    [self performVoiceCallWithUUID:action.callUUID client:nil completion:^(BOOL success) {
        __strong typeof(self) strongSelf = weakSelf;
        if (success) {
            
             [self toggleAudioRoute:YES];
            
            [strongSelf.callKitProvider reportOutgoingCallWithUUID:action.callUUID connectedAtDate:[NSDate date]];
            [action fulfill];
            
            
            /*
             Reporting call start time - For call time API
             */
            self.start_time = [self getCurrentDate];
            
            
        } else {
            [action fail];
        }
    }];
}

- (void)provider:(CXProvider *)provider performAnswerCallAction:(CXAnswerCallAction *)action {
    NSLog(@"provider:performAnswerCallAction:");

    // RCP: Workaround from https://forums.developer.apple.com/message/169511 suggests configuring audio in the
    //      completion block of the `reportNewIncomingCallWithUUID:update:completion:` method instead of in
    //      `provider:performAnswerCallAction:` per the WWDC examples.
    // [[TwilioVoice sharedInstance] configureAudioSession];

    NSAssert([self.callInvite.uuid isEqual:action.callUUID], @"We only support one Invite at a time.");
    
    self.audioDevice.enabled = NO;
    [self performAnswerVoiceCallWithUUID:action.callUUID completion:^(BOOL success) {
        if (success) {
            [action fulfill];
        } else {
            [action fail];
        }
    }];
    
    [action fulfill];
}

- (void)provider:(CXProvider *)provider performEndCallAction:(CXEndCallAction *)action {
    NSLog(@"provider:performEndCallAction:");

    
    
//    if (self.callInvite && self.callInvite.state == TVOCallInviteStatePending) {
//        [self.callInvite reject];
//        self.callInvite = nil;
//    } else if (self.call) {
//        [self.call disconnect];
//    }
//
//    [action fulfill];
    
    
    /*
     Twilio 5.0 changes
     **/
    
    TVOCallInvite *callInvite = self.activeCallInvites[action.callUUID.UUIDString];
    TVOCall *call = self.activeCalls[action.callUUID.UUIDString];

    if (callInvite) {
        [callInvite reject];
        [self.activeCallInvites removeObjectForKey:callInvite.uuid.UUIDString];
    } else if (self.call) {
        [self.call disconnect];
    } else {
        NSLog(@"Unknown UUID to perform end-call action with");
    }

    [action fulfill];
}

//- (void)provider:(CXProvider *)provider performSetHeldCallAction:(CXSetHeldCallAction *)action {
//
//
//    TVOCall *call = self.activeCalls[action.callUUID.UUIDString];
//       if (self.call) {
//           [self.call setOnHold:action.isOnHold];
//           [action fulfill];
//       } else {
//           [action fail];
//       }
//
//}
    
    

    - (void)provider:(CXProvider *)provider performSetHeldCallAction:(CXSetHeldCallAction *)action {
        TVOCall *call = self.activeCalls[action.callUUID.UUIDString];
        if (call) {
            [call setOnHold:action.isOnHold];
            [action fulfill];
        } else {
            [action fail];
        }
    }

    - (void)provider:(CXProvider *)provider performSetMutedCallAction:(CXSetMutedCallAction *)action {
        TVOCall *call = self.activeCalls[action.callUUID.UUIDString];
        if (call) {
            [call setMuted:action.isMuted];
            [action fulfill];
        } else {
            [action fail];
        }
    }




- (void)performVoiceCallWithUUID:(NSUUID *)uuid
                          client:(NSString *)client
                      completion:(void(^)(BOOL success))completionHandler {
    

    //Twilio 2.0
    
//    self.call = [TwilioVoice call:[self fetchAccessToken]
//                           params:@{kTwimlParamTo: self.PhoneNum}
//                             uuid:uuid
//                         delegate:self];
//
//    self.callKitCompletionCallback = completionHandler;
    
    
     //Twilio 5.0
    
    __weak typeof(self) weakSelf = self;
    TVOConnectOptions *connectOptions = [TVOConnectOptions optionsWithAccessToken:[self fetchAccessToken] block:^(TVOConnectOptionsBuilder *builder) {
        __strong typeof(self) strongSelf = weakSelf;
        builder.params = @{kTwimlParamTo:self.PhoneNum};
        builder.uuid = uuid;
    }];
    TVOCall *call = [TwilioVoice connectWithOptions:connectOptions delegate:self];
    if (call) {
        self.call = call;
        self.activeCalls[call.uuid.UUIDString] = call;
    }
    self.callKitCompletionCallback = completionHandler;
    
    
}

- (void)performAnswerVoiceCallWithUUID:(NSUUID *)uuid
                            completion:(void(^)(BOOL success))completionHandler {

    self.call = [self.callInvite acceptWithDelegate:self];
    self.callInvite = nil;
    self.callKitCompletionCallback = completionHandler;
}

- (IBAction)endCall:(id)sender {
    if (self.call && self.call.state == TVOCallStateConnected) {
        [self.call disconnect];
        [self toggleUIState:NO showCallControl:NO];
        
        //Send updates to server
        [self UpdateCallTimeToServer];
    }
    else  if (self.call && self.call.state == TVOCallStateRinging) {
          [self.call disconnect];
          [self toggleUIState:NO showCallControl:NO];
          
          //Send updates to server
          [self UpdateCallTimeToServer];
      }
    else  if ( self.call.state == TVOCallStateDisconnected) {
        
        
        //Call has been ended
           [self  showAlertCallEnded];
        
          // [self PlaceCall];
        
    }
    else  if ( self.call != nil) {
        
                [self.call disconnect];
               [self toggleUIState:NO showCallControl:NO];
               
               //Send updates to server
               [self UpdateCallTimeToServer];
    }
        
    else
    {
        
        ///Go back
        // [self dismissViewControllerAnimated:YES completion:nil] ;
        
        
        //Call has been ended
       // [self  showAlertCallEnded];
        
        
        
        if (self.call == nil ||  self.call.state == TVOCallStateConnecting) {
         
              [self PlaceCall];
             [self callConnectingUI];
         
             
         }
    }
}




/**
 Inform user that call has been ended by customer
 
 */

-(void)showAlertCallEnded{
    
    if(self.popupView != nil){
        
        [self.popupView disappear];

    }

    if(self.popupView == nil){

        NSLog(@"CREATING POP UP ******");

        
//        [alertHelper showAlertWithOneButton:@"Call Ended" message:@"Call has been disconnected" from:self buttonTitle:@"Ok" withHandler:^{
//
//
//                           //Stop the timer
//                            [self chronometerStops];
//
//                            //Hide DialPad
//                            [self hideDialPad];
//
//
//               if([self isBeingPresented]){
//                              [self dismissViewControllerAnimated:YES completion:nil];
//                        }
//                        //else if([self isMovingToParentViewController]){
//                            else {
//
//                                [self.navigationController popViewControllerAnimated:YES];
//                        }
//           }];
//
        
        self.popupView = [ZHPopupView popUpDialogViewInView:nil
                                                            iconImg:[UIImage imageNamed:@"call_end"]
                                                    backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                              title:@"Call Ended"
                                                            content:@"Call has been disconnected"
                                                       buttonTitles:@[@"Ok"]
                                                confirmBtnTextColor:nil otherBtnTextColor:nil
                                                 buttonPressedBlock:^(NSInteger btnIdx) {


                [self.popupView disappear];

               //Stop the timer
                [self chronometerStops];

                //Hide DialPad
                [self hideDialPad];

                dispatch_async( dispatch_get_main_queue(), ^{
                ///Go back
               [self dismissViewControllerAnimated:YES completion:^{

               }] ;
                });


            }];

        self.popupView.contentTextAlignment  = NSTextAlignmentCenter;

        [self.popupView present];
    }else
    {
        [self.popupView disappear];

    }

     
           
       
    
   
}

/*
Show dialpad
 */
-(void)showDialPad{
    self.dialButtonView.hidden = NO;
}

/*
 Hide Dialpad
 */
-(void)hideDialPad{
    self.dialButtonView.hidden = YES;
}

/**
 
 Chronometer Methods
 */

//CHRONOMETER STARTS
-(void)chronometerStart{
    
    self.chronometerLabel.hidden = NO;
    
   // self.chronometer  = nil;
    
    [self.chronometer reset];
     [self.chronometer start];
}

//CHRONOMETER STOPS
-(void)chronometerStops{
    
    self.chronometerLabel.hidden = YES;
    
    [self.chronometer reset];
}



- (IBAction)dialButtonMethod:(id)sender {
    
    if (self.call && self.call.state == TVOCallStateConnected) {

        CallNow_DialPadViewController * dialVC = [[CallNow_DialPadViewController alloc]init];
        dialVC.callNowVC = self;
       // dialVC.delegate =self;
    
       UINavigationController *passcodeNavigationController = [[UINavigationController alloc] initWithRootViewController:dialVC];

       //[self presentViewController:passcodeNavigationController animated:YES completion:nil];
        
        if (@available(iOS 13.0, *)) {
            [passcodeNavigationController setModalPresentationStyle: UIModalPresentationFullScreen];
        } else {
            // Fallback on earlier versions
        }
         [self presentViewController:passcodeNavigationController animated:YES completion:nil];
        
    }
    else
    {
        

        self.popupView = [ZHPopupView popUpDialogViewInView:nil
                                                            iconImg:[UIImage imageNamed:@"call_end"]
                                                    backgroundStyle:ZHPopupViewBackgroundType_Blur
                                                              title:@"Not on Call"
                                                            content:@"You are not on call"
                                                       buttonTitles:@[@"Ok"]
                                                confirmBtnTextColor:nil otherBtnTextColor:nil
                                                 buttonPressedBlock:^(NSInteger btnIdx) {
                                                     
                                                     
                                                    
                                                     
                                                 }];
        
        self.popupView.contentTextAlignment  = NSTextAlignmentCenter;
        
        [self.popupView present];
    }
}



@end
