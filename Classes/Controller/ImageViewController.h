//
//  VendorItemMoreDetailViewController.h
//  Customer
//
//  Created by Katia Maeda on 2014-11-07.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

@interface ImageViewController : UIViewController

@property (nonatomic, strong) UIImage *image;

@end
