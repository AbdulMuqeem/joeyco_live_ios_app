//
//  CameraViewController.h
//  Customer
//
//  Created by Katia Maeda on 2015-01-30.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZHPopupView.h"
#import "ItineraryOrder_Listing_ObjectDetail.h"

@protocol ImageConfirmDropOffDelegate;

typedef void (^PictureSelectedCompletionBlock)(UIImage *);

@interface CameraVC_takePhoto : UIViewController

{
    id<ImageConfirmDropOffDelegate> delegate;
}
@property(nonatomic,assign)BOOL is_itinerary;

@property (weak, nonatomic) id<ImageConfirmDropOffDelegate> delegate;

//Model Data of clicked obj
@property(nonatomic,strong)ItineraryOrder_Listing_ObjectDetail * modelData;

@property (nonatomic, strong) PictureSelectedCompletionBlock pictureSelectedCompletion;
@property (nonatomic, strong) UIImage *image;


@property(nonatomic,assign)int selectedIndexOnSheet;


//Order
@property(nonatomic,strong)Order * order;
@property(nonatomic,strong)Task *  order_task;


@property (weak, nonatomic) IBOutlet UIButton *closeDialog;

@property(nonatomic,strong)NSMutableArray * StatusObj;


- (IBAction)closeDialogAction:(id)sender;
- (IBAction)uploadPictureToServer:(id)sender;

@end
@protocol ImageConfirmDropOffDelegate

-(void)dropStatusCompletedUsingImage :(ItineraryOrder_Listing_ObjectDetail *)obj;

@end

