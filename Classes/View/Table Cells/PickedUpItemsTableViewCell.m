//
//  PickedUpItemsTableViewCell.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 28/07/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import "PickedUpItemsTableViewCell.h"

@implementation PickedUpItemsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
