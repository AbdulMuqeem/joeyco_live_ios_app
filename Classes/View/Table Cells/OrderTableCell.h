//
//  OrderTableCell.h
//  JoeyCo
//
//  Created by Katia Maeda on 2014-09-29.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "RoundedButton.h"

@interface OrderTableCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *orderNumTxt;
@property (nonatomic, weak) IBOutlet UILabel *readyInTxt;
@property (nonatomic, weak) IBOutlet UILabel *pickupTxt;
@property (nonatomic, weak) IBOutlet UILabel *droppOffTxt;
@property (nonatomic, weak) IBOutlet UILabel *travelDistanceTxt;
@property (nonatomic, weak) IBOutlet UILabel *travelTimeTxt;
@property (nonatomic, weak) IBOutlet RoundedButton *acceptBtn;

@end
