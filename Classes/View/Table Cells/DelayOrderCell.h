//
//  DelayOrderCell.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 16/11/2018.
//  Copyright © 2018 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DelayOrderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *reason_label;
@property (weak, nonatomic) IBOutlet UIButton *radioButton;



@end

NS_ASSUME_NONNULL_END
