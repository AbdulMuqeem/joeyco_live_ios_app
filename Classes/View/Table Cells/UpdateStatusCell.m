//
//  UpdateStatusCell.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 05/11/2018.
//  Copyright © 2018 JoeyCo. All rights reserved.
//

#import "UpdateStatusCell.h"

@implementation UpdateStatusCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
