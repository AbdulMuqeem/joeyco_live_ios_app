//
//  OrderCollectionCell.m
//  Joey
//
//  Created by Katia Maeda on 2015-03-17.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "OrderCollectionCell.h"

@implementation OrderCollectionCell

-(void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self setNeedsDisplay];
}

@end
