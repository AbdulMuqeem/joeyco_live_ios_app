//
//  CustomButton.h
//  Customer
//
//  Created by Katia Maeda on 2014-12-03.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomButton : UIButton

@property (nonatomic, strong) id buttonData;

@end
