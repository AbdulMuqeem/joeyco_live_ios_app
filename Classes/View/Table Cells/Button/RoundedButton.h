//
//  RoundedButton.h
//  Customer
//
//  Created by Katia Maeda on 2014-12-01.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomButton.h"

@interface RoundedButton : CustomButton

@end
