//
//  RoundedButton.m
//  Customer
//
//  Created by Katia Maeda on 2014-12-01.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "RoundedButton.h"

@implementation RoundedButton

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self)
    {
        self.layer.cornerRadius = 5.0;
        self.layer.masksToBounds = YES;
        
        [self setTitleColor:[UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1.0f] forState:UIControlStateNormal];
        
//        UIFont *titleFont = self.titleLabel.font;

//        [self.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic-Bold" size:titleFont.pointSize]];
    }
    return self;
}

+ (UIImage *) green:(UIView *)parent cornerRadius:(CGFloat)radius
{
    // Create Green Gradient
    UIColor *colorOne = [UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0];
    UIColor *colorTwo = [UIColor colorWithRed:0.68 green:0.79 blue:0.03 alpha:1.0];
    
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = colors;
    gradientLayer.locations = locations;
    
    // Create background
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, parent.frame.size.width, parent.frame.size.height)];
    gradientLayer.frame = view.bounds;
    [view.layer addSublayer:gradientLayer];
    view.layer.cornerRadius = radius;
    view.layer.masksToBounds = YES;
    
    // Create image
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *greenButton = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return greenButton;
}

@end
