//
//  FourLabelsTableViewCell.h
//  Joey
//
//  Created by Katia Maeda on 2016-01-27.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomButton.h"

@interface FourLabelsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *label1;
@property (nonatomic, weak) IBOutlet UILabel *label2;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *label2Height;
@property (nonatomic, weak) IBOutlet UILabel *label3;
@property (nonatomic, weak) IBOutlet UILabel *label4;
@property (nonatomic, weak) IBOutlet CustomButton *image;

@end
