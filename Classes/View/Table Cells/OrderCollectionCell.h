//
//  OrderCollectionCell.h
//  Joey
//
//  Created by Katia Maeda on 2015-03-17.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderCollectionCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *orderNumberTxt;
@property (nonatomic, weak) IBOutlet UILabel *readyInTxt;
@property (nonatomic, weak) IBOutlet UILabel *travelDistanceTxt;
@property (nonatomic, weak) IBOutlet UILabel *travelTimeTxt;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIView *scrollContentView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *contentViewHeight;

@end
