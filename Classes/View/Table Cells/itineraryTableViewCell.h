//
//  itineraryTableViewCell.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 15/05/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface itineraryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *view_item_vertical_bar;

@property (weak, nonatomic) IBOutlet UILabel *type_item;


@property (weak, nonatomic) IBOutlet UILabel *route_num;
@property (weak, nonatomic) IBOutlet UILabel *DeliveryWindow;
@property (weak, nonatomic) IBOutlet UILabel *merchantOrder_num;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *address_line1;
@property (weak, nonatomic) IBOutlet UILabel *address_line2;

@property (weak, nonatomic) IBOutlet UIButton *waze_btn;
@property (weak, nonatomic) IBOutlet UIButton *checklist_btn;
@property (weak, nonatomic) IBOutlet UIButton *callnow_btn;


@property (weak, nonatomic) IBOutlet UIButton *return_btn;
@property (weak, nonatomic) IBOutlet UIButton *mark_status;
@property (weak, nonatomic) IBOutlet UIButton *openLocation_btn;

@property (weak, nonatomic) IBOutlet UIView *View_return_order;
@property (weak, nonatomic) IBOutlet UIView *has_pickedView;

@end

NS_ASSUME_NONNULL_END
