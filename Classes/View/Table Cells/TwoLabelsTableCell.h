//
//  TwoLabelsTableCell.h
//  Joey
//
//  Created by Katia Maeda on 2015-02-19.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomButton.h"

@interface TwoLabelsTableCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *label1;
@property (nonatomic, weak) IBOutlet UILabel *label2;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *label2Width;

@property (nonatomic, weak) IBOutlet UITextView *textView1;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *textView1Height;

@property (nonatomic, weak) IBOutlet CustomButton *button;

@end
