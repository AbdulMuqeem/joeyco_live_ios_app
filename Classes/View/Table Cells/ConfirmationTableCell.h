//
//  ConfirmationTableCell.h
//  Joey
//
//  Created by Katia Maeda on 2015-02-20.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "RoundedButton.h"
#import "Confirmation.h"
#import "CustomTextField.h"

@interface ConfirmationTableCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleTxt;
@property (nonatomic, weak) IBOutlet UITextView *descriptionTxt;
@property (nonatomic, weak) IBOutlet CustomTextField *editField;

@property (nonatomic, weak) IBOutlet RoundedButton *confirmationButton;

@property (nonatomic, weak) Confirmation *confirmation;

@end
