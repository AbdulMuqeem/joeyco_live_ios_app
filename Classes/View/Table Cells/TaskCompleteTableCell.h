//
//  TaskCompleteTableCell.h
//  Joey
//
//  Created by Katia Maeda on 2015-02-20.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "RoundedButton.h"

@interface TaskCompleteTableCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *taskType;
@property (nonatomic, weak) IBOutlet UILabel *taskName;
@property (nonatomic, weak) IBOutlet UILabel *taskAddress;
@property (nonatomic, weak) IBOutlet UILabel *taskSuite;
@property (nonatomic, weak) IBOutlet UILabel *taskBuzzer;
@property (nonatomic, weak) IBOutlet UILabel *taskWeight;
@property (nonatomic, weak) IBOutlet UILabel *taskPhone;
@property (nonatomic, weak) IBOutlet UILabel *taskDescription;

@property (nonatomic, weak) IBOutlet RoundedButton *taskChecklist;
@property (nonatomic, weak) IBOutlet UILabel *taskCompleted;

@end
