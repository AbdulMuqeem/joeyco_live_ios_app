//
//  PickedUpItemsTableViewCell.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 28/07/2020.
//  Copyright © 2020 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PickedUpItemsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label_title;
@property (weak, nonatomic) IBOutlet UILabel *label_tracking_ID;

@end

NS_ASSUME_NONNULL_END
