//
//  UpdateStatusCell.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 05/11/2018.
//  Copyright © 2018 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UpdateStatusCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *Cell_status_description;
@property (weak, nonatomic) IBOutlet UIImageView *cell_status_image;
@property (weak, nonatomic) IBOutlet UIButton *Cell_statusUpdate_button;





@end

NS_ASSUME_NONNULL_END
