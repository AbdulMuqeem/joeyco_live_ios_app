//
//  GraySegmentedControl.h
//  Customer
//
//  Created by Katia Maeda on 2014-12-22.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GraySegmentedControl : UISegmentedControl

@end
