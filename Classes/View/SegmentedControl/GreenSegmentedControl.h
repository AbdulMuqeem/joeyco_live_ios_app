//
//  GreenSegmentedControl.h
//  Customer
//
//  Created by Katia Maeda on 2014-10-19.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GreenSegmentedControl : UISegmentedControl
{
    NSMutableArray *segmentsViews;
}

@end
