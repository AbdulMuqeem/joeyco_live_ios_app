//
//  GraySegmentedControl.m
//  Customer
//
//  Created by Katia Maeda on 2014-12-22.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "GraySegmentedControl.h"

@implementation GraySegmentedControl

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self)
    {
        
        NSDictionary *attributes = [NSDictionary dictionaryWithObjects:@[[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0f]] forKeys:@[NSForegroundColorAttributeName]];
        [self setTitleTextAttributes:attributes forState:UIControlStateNormal];
        [self setTitleTextAttributes:attributes forState:UIControlStateSelected];
//        [self setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"CenturyGothic" size:15.0f], NSForegroundColorAttributeName: [UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1.0f]} forState: UIControlStateNormal];
//        [self setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"CenturyGothic-Bold" size:15.0f], NSForegroundColorAttributeName: [UIColor colorWithRed:1 green:1 blue:1 alpha:1.0f]} forState: UIControlStateSelected];
    }
    return self;
}

@end
