//
//  GreenSegmentedControl.m
//  Customer
//
//  Created by Katia Maeda on 2014-10-19.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "GreenSegmentedControl.h"

@implementation GreenSegmentedControl

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self)
    {
        self.tintColor = [UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0];
        self.backgroundColor = [UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1.0];

        [self setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"CenturyGothic-Bold" size:15.0f], NSForegroundColorAttributeName: [UIColor whiteColor]} forState: UIControlStateNormal];
        [self setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"CenturyGothic-Bold" size:15.0f], NSForegroundColorAttributeName: [UIColor whiteColor]} forState: UIControlStateSelected];
        
        [self.layer setCornerRadius:5.0f];
        
        [self initSegmentsArray];

        [self addObserver:self forKeyPath:@"selectedSegmentIndex" options:NSKeyValueObservingOptionNew context:nil];
        self.selectedSegmentIndex = 0;

        [self setDividerImage:[UIImage imageNamed:@"segmented-divider.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [self setDividerImage:[UIImage imageNamed:@"segmented-divider.png"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [self setDividerImage:[UIImage imageNamed:@"segmented-divider.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    }
    return self;
}

-(void)dealloc
{
    [self removeObserver:self forKeyPath:@"selectedSegmentIndex"];
}

-(void)initSegmentsArray
{
    segmentsViews = [NSMutableArray array];
    for (int i = (int)self.numberOfSegments - 1; i >= 0; i--)
    {
        [segmentsViews addObject:[self.subviews objectAtIndex:i]];
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"selectedSegmentIndex"])
    {
        if (segmentsViews.count > 0)
        {
            for (int i = 0; i < self.numberOfSegments; i++)
            {
                if (i == self.selectedSegmentIndex)
                {
                    [[segmentsViews objectAtIndex:i] setTintColor:[UIColor colorWithRed:0.73 green:0.84 blue:0.03 alpha:1.0]];
                }
                else
                {
                    [[segmentsViews objectAtIndex:i] setTintColor:[UIColor clearColor]];
                }
            }
        }
    }
}

@end
