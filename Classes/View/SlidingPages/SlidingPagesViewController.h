//
//  SlidingPagesViewController.h
//  JoeyCo
//
//  Created by Katia Maeda on 2014-10-09.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

@protocol SlidingPagesDelegate <NSObject>

-(void)contentViewHeight:(CGFloat)height;

@end

@interface SlidingPagesViewController : UIViewController<UIScrollViewDelegate>
{
    bool viewDidAppearHasBeenCalled;
    UIScrollView *bottomScrollView;
}

@property (nonatomic, weak) id<SlidingPagesDataSource> dataSource;
@property (nonatomic, weak) id<SlidingPagesDelegate> delegate;

/**  @property initialPageNumber
 *   @brief Sets the scroller to scroll to a specific page initially
 *   Sets the scroller to scroll to a specific page initially (either on the first load, or afrer calling reloadPages). Default is 0. **/
@property (nonatomic) int initialPageNumber;

-(void)scrollToPage:(NSInteger)page animated:(BOOL)animated;
-(NSInteger)currentPage;

@end

