
//
//  SlidingPagesViewController.m
//  JoeyCo
//
//  Created by Katia Maeda on 2014-10-09.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "Prefix.pch"

@interface SlidingPagesViewController ()
{
    NSInteger currentPage;
}

@end

@implementation SlidingPagesViewController

/**
 Initalises the control and sets all the default values for the user-settable properties.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //set defaults
        self.initialPageNumber = 0;
    }
    return self;
}

/**
 Initialse the top and bottom scrollers (but don't populate them with pages yet)
 */
- (void)viewDidLoad
{
    [super viewDidLoad];

    int nextYPosition = 0;
    
    //set up the bottom scroller (for the content to go in)
    bottomScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, nextYPosition, self.view.frame.size.width, self.view.frame.size.height-nextYPosition)];
    bottomScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
    bottomScrollView.showsVerticalScrollIndicator = NO;
    bottomScrollView.showsHorizontalScrollIndicator = NO;
    bottomScrollView.directionalLockEnabled = YES;
    bottomScrollView.delegate = self; //move the top scroller proportionally as you drag the bottom.
    bottomScrollView.alwaysBounceVertical = NO;
    bottomScrollView.scrollEnabled = NO;
    [self.view addSubview:bottomScrollView];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!viewDidAppearHasBeenCalled)
    {
        viewDidAppearHasBeenCalled = YES;
        [self reloadPages];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/**
 Goes through the datasource and finds all the pages, then populates the topScrollView and bottomScrollView with all the pages and headers.
 
 It clears any of the views in both scrollViews first, so if you need to reload all the pages with new data from the dataSource for some reason, you can call this method.
 */
-(void)reloadPages
{
    if (!self.dataSource)
    {
        [NSException raise:@"TTSlidingPagesController data source missing" format:@"There was no data source set for the TTSlidingPagesControlller. You must set the .dataSource property on TTSlidingPagesController to an object instance that implements TTSlidingPagesDataSource, also make sure you do this before the view will be loaded (so before you add it as a subview to any other view that is about to appear)"];
    }
    
    //remove any existing items from the subviews
    [bottomScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    //remove any existing items from the view hierarchy
    for (UIViewController* subViewController in self.childViewControllers)
    {
        [subViewController willMoveToParentViewController:nil];
        [subViewController removeFromParentViewController];
    }
    
    //get the number of pages
    int numOfPages = [self.dataSource numberOfPagesInSlidingPages];
    
    //keep track of where next to put items in each scroller
    int nextXPosition = 0;

    //loop through each page and add it to the scroller
    for (int i=0; i<numOfPages; i++)
    {
        //bottom scroller add-----
        //set the default width of the page
        int pageWidth = bottomScrollView.frame.size.width;
        
        UIViewController *page = [self.dataSource pageForSlidingPagesAtIndex:i];//get the page
        UIView *contentView = page.view;
        
        //make a container view (putting it inside a container view because if the contentView uses any autolayout it doesn't work well with the .transform property that the zoom animation uses. The container view shields it from this).
        UIView *containerView = [[UIView alloc] init];
        
        //put the container view in the right position, y is always 0, x is incremented with each item you add (it is a horizontal scroller).
        containerView.frame = CGRectMake(nextXPosition, 0, pageWidth, bottomScrollView.frame.size.height);
        nextXPosition = nextXPosition + containerView.frame.size.width;
        
        //put the content view inside the container view
        contentView.frame = CGRectMake(0, 0, pageWidth, bottomScrollView.frame.size.height);
        [containerView addSubview:contentView];
        
        //add the container view to the scroll view
        [bottomScrollView addSubview:containerView];
        
        if (page)
        {
            [self addChildViewController:page];
            [page didMoveToParentViewController:self];
        }
    }
    
    //now set the content size of the scroller to be as wide as nextXPosition (we can know that nextXPosition is also the width of the scroller)
    bottomScrollView.contentSize = CGSizeMake(nextXPosition, bottomScrollView.frame.size.height);

    //scroll to the initialpage
    [self scrollToPage:self.initialPageNumber animated:NO];
}


/**
 Gets number of the page currently displayed in the bottom scroller (zero based - so starting at 0 for the first page).
 
 @return Returns the number of the page currently displayed in the bottom scroller (zero based - so starting at 0 for the first page).
 */
-(int)getCurrentDisplayedPage
{
    //sum through all the views until you get to a position that matches the offset then that's what page youre on (each view can be a different width)
    int page = 0;
    int currentXPosition = 0;
    while (currentXPosition <= bottomScrollView.contentOffset.x && currentXPosition < bottomScrollView.contentSize.width)
    {
        int pageWidth = [self getWidthOfPage:page];

        if ((currentXPosition + pageWidth / 2) <= bottomScrollView.contentOffset.x)
        {
            page++;
        }
        
        currentXPosition += pageWidth;
    }
    
    return page;
}

/**
 Gets the x position of the requested page in the bottom scroller. For example, if you ask for page 5, and page 5 starts at the contentOffset 520px in the bottom scroller, this will return 520.
 
 @param page The page number requested.
 @return Returns the x position of the requested page in the bottom scroller
 */
-(int)getXPositionOfPage:(NSInteger)page
{
    //each view could in theory have a different width
    int currentTotal = 0;
    for (int curPage = 0; curPage < page; curPage++)
    {
        currentTotal += [self getWidthOfPage:curPage];
    }
    
    return currentTotal;
}

/**
 Gets the width of a specific page in the bottom scroll view. Most of the time this will be the width of the scrollview itself, but if you have widthForPageOnSlidingPagesViewController implemented on the datasource it might be different - hence this method.
 
 @param page The page number requested.
 @return Returns the width of the page requested.
 */
-(int)getWidthOfPage:(int)page
{
    return bottomScrollView.frame.size.width;
}

/**
 Scrolls the bottom scorller (content scroller) to a particular page number.
 
 @param page The page number to scroll to.
 @param animated Whether the scroll should be animated to move along to the page (YES) or just directly scroll to the page (NO)
 */
-(void)scrollToPage:(NSInteger)page animated:(BOOL)animated
{
    //scroll to the page
    currentPage = page;
    CGFloat posX = [self getXPositionOfPage:page];
    [bottomScrollView setContentOffset: CGPointMake(posX,0) animated:animated];
}

-(NSInteger)currentPage
{
    return currentPage;
}

#pragma mark Some delegate methods for handling rotation.

-(void)didRotate
{
//    currentPageBeforeRotation = [self getCurrentDisplayedPage];
}

-(void)viewDidLayoutSubviews
{
    CGRect frame;
    int nextXPosition = 0;
    int page = 0;
    for (UIView *view in bottomScrollView.subviews)
    {
        view.transform = CGAffineTransformIdentity;
        frame = view.frame;
        frame.size.width = [self getWidthOfPage:page];
        frame.size.height = bottomScrollView.frame.size.height;
        frame.origin.x = nextXPosition;
        frame.origin.y = 0;
        page++;
        nextXPosition += frame.size.width;
        view.frame = frame;
    }
    bottomScrollView.contentSize = CGSizeMake(nextXPosition, bottomScrollView.frame.size.height);
    
    [self scrollToPage:currentPage animated:YES];
    
    //set it back to the same page as it was before (the contentoffset will be different now the widths are different)
//    int contentOffsetWidth = [self getXPositionOfPage:0];
//    bottomScrollView.contentOffset = CGPointMake(contentOffsetWidth, 0);
}

#pragma mark UIScrollView delegate
//
//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    if (!decelerate)
//    {
//        int currentPage = [self getCurrentDisplayedPage];
//        [self scrollToPage:currentPage animated:YES];
//    }
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    int currentPage = [self getCurrentDisplayedPage];
//
//    //store the page you were on so if you have a rotate event, or you come back to this view you know what page to start at. (for example from a navigation controller), the viewDidLayoutSubviews method will know which page to navigate to (for example if the screen was portrait when you left, then you changed to landscape, and navigate back, then viewDidLayoutSubviews will need to change all the sizes of the views, but still know what page to set the offset to)
//    currentPageBeforeRotation = currentPage;
//    
//    [self scrollToPage:currentPage animated:NO];
//}

#pragma mark property setters - for when need to do fancy things as well as set the value

-(void)setDataSource:(id<SlidingPagesDataSource>)dataSource
{
    _dataSource = dataSource;
}


@end
