//
//  SlidingPagesDataSource.h
//  JoeyCo
//
//  Created by Katia Maeda on 2014-10-09.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

@protocol SlidingPagesDataSource <NSObject>

-(int)numberOfPagesInSlidingPages;
-(UIViewController *)pageForSlidingPagesAtIndex:(int)index;
-(NSString *)titleForSlidingPagesAtIndex:(int)index;

@end
