//
//  SignatureView.h
//  Joey
//
//  Created by Katia Maeda on 2016-02-24.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignatureView : UIView

- (UIImage *)getSignatureImage;
- (void)clearSignature;

@end
