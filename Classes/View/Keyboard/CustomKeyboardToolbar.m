//
//  CustomKeyboardToolbar.m
//  Customer
//
//  Created by Katia Maeda on 2014-11-18.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "CustomKeyboardToolbar.h"

#define KEYBOARD_TOOLBAR_HEIGHT 40

@interface CustomKeyboardToolbar ()

@property (nonatomic, weak) UIView *superView;
@property (nonatomic, weak) UIScrollView *scrollView;
@property (nonatomic) CGRect viewOriginalFrame;
@property (nonatomic) CGSize viewOriginalContent;
@property (nonatomic) CGFloat keyboardHeight;

@property (nonatomic, strong) UIToolbar* keyboardToolbar;

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation CustomKeyboardToolbar

-(id)initWithView:(UIView *)superView
{
    self = [super initWithFrame:CGRectMake(0, 0, superView.frame.size.width, KEYBOARD_TOOLBAR_HEIGHT)];
    if (self) {
        self.superView = superView;
        if ([self.superView isKindOfClass:[UIScrollView class]])
        {
            self.scrollView = (UIScrollView *)superView;
        }
        
        self.viewOriginalFrame = superView.frame;
        self.keyboardToolbar =[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, superView.frame.size.width, 40)];
        
        UIBarButtonItem *previousButton = [[UIBarButtonItem alloc]initWithTitle:@" < " style:UIBarButtonItemStylePlain target:self action:@selector(keyboardPreviousButton)];
        UIBarButtonItem *nextButton = [[UIBarButtonItem alloc]initWithTitle:@" > " style:UIBarButtonItemStylePlain target:self action:@selector(keyboardNextButton)];
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
        self.keyboardToolbar.items = @[previousButton, nextButton, flexSpace, doneButton];
        [self.keyboardToolbar sizeToFit];
        
        [self addSubview:self.keyboardToolbar];
    }
    return self;
}

-(void)setScrollView:(UIScrollView *)scrollView
{
    _scrollView = scrollView;
    self.superView = scrollView;
    self.viewOriginalFrame = scrollView.frame;
    self.keyboardToolbar =[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, scrollView.frame.size.width, 40)];
}

-(void)dealloc
{
    [self.timer invalidate];
}

-(int)indexThatHasFocus
{
    int indexHasFocus = 0;
    for (int i = 0; i < self.textFields.count; i++)
    {
        UIResponder *textField = [self.textFields objectAtIndex:i];
        if ([textField isFirstResponder])
        {
            indexHasFocus = i;
            break;
        }
    }
    return indexHasFocus;
}

-(void)keyboardPreviousButton
{
    NSInteger indexPrevious = [self indexThatHasFocus] - 1;
    if (indexPrevious < 0) indexPrevious = self.textFields.count - 1;
    
    UITextField *textField = [self.textFields objectAtIndex:indexPrevious];
    [textField becomeFirstResponder];
}

-(void)keyboardNextButton
{
    int indexNext = [self indexThatHasFocus] + 1;
    if (indexNext >= self.textFields.count) indexNext = 0;
    
    UITextField *textField = [self.textFields objectAtIndex:indexNext];
    [textField becomeFirstResponder];
}

-(void)hideKeyboard
{
    [self resignAll];
}

-(void)resignAll
{
    for (UITextField *textField in self.textFields)
    {
        [textField resignFirstResponder];
    }
}

-(CGFloat)getHeyboardHeight
{
    return self.keyboardHeight;
}

-(void)keyboardWillShow:(NSNotification*)notification
{
    NSDictionary *keyboardInfo = [notification userInfo];
    CGRect keyboardFrameEnd = [[keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [self.timer invalidate];
    
    self.keyboardHeight = keyboardFrameEnd.size.height + KEYBOARD_TOOLBAR_HEIGHT;
    
    if (self.viewOriginalContent.height <= 0)
    {
        CGSize size = self.scrollView.contentSize;
        self.viewOriginalContent = CGSizeMake(size.width, size.height + self.keyboardHeight);
    }
    
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.scrollView.contentSize = self.viewOriginalContent;
                     }
                     completion:nil
     ];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:.2 target:self selector:@selector(hideKeyboardAnimation) userInfo:nil repeats:NO];
}

-(void)hideKeyboardAnimation
{
    if (self.viewOriginalContent.height <= 0)
    {
        return;
    }
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.scrollView.contentSize = CGSizeMake(self.viewOriginalContent.width, self.viewOriginalContent.height - self.keyboardHeight);
                     }
                     completion:^(BOOL finished) {
                         self.viewOriginalContent = CGSizeMake(self.viewOriginalContent.width, 0);
                         self.keyboardHeight = 0;
                     }];
}

-(void)updateContentHeight:(CGFloat)newHeight
{
    if (self.viewOriginalContent.height <= 0)
    {
        return;
    }
    
    self.viewOriginalContent = CGSizeMake(self.viewOriginalContent.width, newHeight);
}

@end
