//
//  CustomKeyboardToolbar.h
//  Customer
//
//  Created by Katia Maeda on 2014-11-18.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

typedef enum
{
    KeyboardVisibilityStatusHidden,
    KeyboardVisibilityStatusShowingStatusBar,
    KeyboardVisibilityStatusShowingAll,
} KeyboardVisibilityStatus;

@interface CustomKeyboardToolbar : UIView

@property (nonatomic, strong) NSArray *textFields;

-(id)initWithView:(UIView *)superView;
-(void)setScrollView:(UIScrollView *)scrollView;
-(void)keyboardWillShow:(NSNotification*)notification;
-(void)keyboardWillHide:(NSNotification *)notification;
-(void)updateContentHeight:(CGFloat)newHeight;

-(void)hideKeyboard;
-(CGFloat)getHeyboardHeight;

@end
