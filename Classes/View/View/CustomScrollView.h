//
//  CustomScrollView.h
//  Customer
//
//  Created by Katia Maeda on 2015-06-12.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomScrollView : UIScrollView

@end
