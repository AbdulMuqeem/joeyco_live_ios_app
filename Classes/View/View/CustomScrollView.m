//
//  CustomScrollView.m
//  Customer
//
//  Created by Katia Maeda on 2015-06-12.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "CustomScrollView.h"


@interface CustomScrollView () {
    BOOL blockContentOffset;
}

@end

@implementation CustomScrollView

-(void)scrollRectToVisible:(CGRect)rect animated:(BOOL)animated
{
    CGPoint scrollPoint = CGPointMake(0, rect.origin.y);
    scrollPoint.y -= 50;

    if (scrollPoint.y > self.contentSize.height - self.frame.size.height)
    {
        scrollPoint.y = self.contentSize.height - self.frame.size.height;
    }

    [self setContentOffset:scrollPoint animated:YES];
}

//-(void)setContentSize:(CGSize)contentSize
//{
//    blockContentOffset = YES;
//    [super setContentSize:contentSize];
//    [self performSelector:@selector(unblockContentOffset) withObject:nil afterDelay:.5];
//}
//
//-(void)unblockContentOffset
//{
//    blockContentOffset = NO;
//}
//
//-(void)setContentOffset:(CGPoint)contentOffset
//{
//    if (blockContentOffset)
//    {
//        return;
//    }
//    [super setContentOffset:contentOffset];
//}

@end
