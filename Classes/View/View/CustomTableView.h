//
//  CustomTableView.h
//  Joey
//
//  Created by Katia Maeda on 2016-02-05.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableView : UITableView

@end
