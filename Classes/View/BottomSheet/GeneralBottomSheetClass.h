//
//  GeneralBottomSheetClass.h
//  Joey
//
//  Created by Muhammad Faiz Masroor on 29/03/2019.
//  Copyright © 2019 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GeneralBottomSheetTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface GeneralBottomSheetClass : UIViewController


@property(nonatomic,strong)NSMutableArray * dataSourceArray;

@property (weak, nonatomic) IBOutlet UITableView *bottomSheetTableView;
@property(nonatomic,assign)int selectedIndex;


- (IBAction)closeButton:(id)sender;

@end

NS_ASSUME_NONNULL_END
