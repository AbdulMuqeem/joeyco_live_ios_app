//
//  GeneralBottomSheetClass.m
//  Joey
//
//  Created by Muhammad Faiz Masroor on 29/03/2019.
//  Copyright © 2019 JoeyCo. All rights reserved.
//

#import "GeneralBottomSheetClass.h"

@interface GeneralBottomSheetClass ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation GeneralBottomSheetClass

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



-(void) viewWillAppear:(BOOL)animated{
    self.preferredContentSize= CGSizeMake(self.view.frame.size.width, [self createHeight:self.dataSourceArray]);
}

-(CGFloat)createHeight:(NSMutableArray *)arr{
    
    CGFloat cellHeight = 130;
    
    if ([arr count] == 1) {
        
        cellHeight = 130;
        return cellHeight;
    }
    else  if ([arr count] == 2) {
        cellHeight = 180;
        return cellHeight;
        
        
    }
    else  if ([arr count] == 3) {
        cellHeight = 250;
        return cellHeight;
        
        
    }
    else  if ([arr count] == 4) {
        cellHeight = 290;
        return cellHeight;
        
    }
    
    else  if ([arr count] > 4) {
        cellHeight = 320;
        return cellHeight;
        
    }
    
    return cellHeight;
    
}

- (IBAction)closeButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"GeneralCelllReuse";
    
    GeneralBottomSheetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (!cell) cell = [[GeneralBottomSheetTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    __weak GeneralBottomSheetTableViewCell *weakCell = cell;
    
    
//    OrderStatus * CancelModel = [self.dataSourceArray objectAtIndex:indexPath.row];
//    cell.reason_label.text = CancelModel.orderDescription;
     cell.reason_label.text = [self.dataSourceArray objectAtIndex:indexPath.row];
    
    
    //Setting check and uncheck image
    if (_selectedIndex == indexPath.row) {
       // [cell.radioButton  setImage:[UIImage imageNamed:@"radio-checked.png"] forState:UIControlStateNormal];

        [cell.radioButton  setImage:[UIImage imageNamed:@"radio-unchecked.png"] forState:UIControlStateNormal];

    }
    else{
        [cell.radioButton  setImage:[UIImage imageNamed:@"radio-unchecked.png"] forState:UIControlStateNormal];
        
    }
    
    return weakCell;
    
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSourceArray count];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    /**
     Sending tapped data*/
    [NSNotificationCenter.defaultCenter postNotificationName:@"getTappedData" object:[self.dataSourceArray objectAtIndex:indexPath.row]];
    
    
     [self dismissViewControllerAnimated:YES completion:nil];
    // [self updateToServer];
    
     NSLog(@"DID SELECT ROW");

}

- (IBAction)RadioButtonTapped:(id)sender {
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.bottomSheetTableView];
    NSIndexPath *indexPath = [self.bottomSheetTableView indexPathForRowAtPoint:buttonPosition];
    
    NSLog(@"ROW TAPPED : %ld",(long)indexPath.row);
    
    
    //Saving selected index
    self.selectedIndex =(int) indexPath.row;
    
   
    
    
    /**
     Sending tapped data*/
    [NSNotificationCenter.defaultCenter postNotificationName:@"getTappedData" object:[self.dataSourceArray objectAtIndex:self.selectedIndex]];
    
    
     [self.bottomSheetTableView reloadData];

    
    [self dismissViewControllerAnimated:YES completion:nil];

    
}


/*
 Update To server
 */
- (void)updateToServer{
    
}

@end
