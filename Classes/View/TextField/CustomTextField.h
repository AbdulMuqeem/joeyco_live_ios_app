//
//  CustomTextField.h
//  Customer
//
//  Created by Katia Maeda on 2015-01-27.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import <UIKit/UIKit.h>

#define FIELD_TEXT "text"
#define FIELD_EMAIL "email"
#define FIELD_MONEY "money"
#define FIELD_PIN "pin"
#define FIELD_POSTAL_CODE "postalCode"

@interface CustomTextField : UITextField

@property (nonatomic, weak) UIImage *defaultBackground;
@property (nonatomic, strong) NSString *fieldType;

-(void)setRedBackground;
-(void)setDefaultBackground;

@end
