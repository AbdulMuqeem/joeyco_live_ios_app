//
//  CustomTextField.m
//  Customer
//
//  Created by Katia Maeda on 2015-01-27.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self)
    {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        self.leftView = paddingView;
        self.leftViewMode = UITextFieldViewModeAlways;
        
        self.defaultBackground = self.background;
    }
    return self;
}

-(void)setRedBackground
{
    [self setBackground:[self redBackgroundImage]];
}

-(void)setDefaultBackground
{
    [self setBackground:self.defaultBackground];
}

-(UIImage *)redBackgroundImage
{
    UIColor *color = [UIColor colorWithRed:1 green:.56 blue:.47 alpha:1.0];
    
    // Create background
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [view setBackgroundColor:color];
//    view.layer.cornerRadius = 4.0;
//    view.layer.masksToBounds = YES;
    
    // Create image
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
