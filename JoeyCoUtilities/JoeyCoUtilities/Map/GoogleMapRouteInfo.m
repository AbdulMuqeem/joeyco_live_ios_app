//
//  GoogleMapRouteInfo.m
//  Joey
//
//  Created by Katia Maeda on 2015-02-12.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "GoogleMapRouteInfo.h"

@implementation GoogleMapRouteInfo

-(void)addTotalDistance:(NSNumber *)totalDistance
{
    self.totalDistance = [NSNumber numberWithLong:([self.totalDistance longValue] + [totalDistance longValue])];
}

-(void)addTotalDuration:(NSNumber *)totalDuration
{
    self.totalDuration = [NSNumber numberWithLong:([self.totalDuration longValue] + [totalDuration longValue])];
}

@end
