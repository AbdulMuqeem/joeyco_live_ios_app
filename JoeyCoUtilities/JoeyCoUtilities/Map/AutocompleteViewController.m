//
//  AutocompleteViewController.m
//  Customer
//
//  Created by Katia Maeda on 2016-04-18.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "AutocompleteViewController.h"

@interface AutocompleteViewController () <GMSAutocompleteResultsViewControllerDelegate, UISearchBarDelegate>
{
    UISearchController *_searchController;
    GMSAutocompleteResultsViewController *_acViewController;
}

@end

@implementation AutocompleteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    _acViewController = [[GMSAutocompleteResultsViewController alloc] init];
    _acViewController.delegate = self;
    
    _searchController = [[UISearchController alloc] initWithSearchResultsController:_acViewController];
    _searchController.hidesNavigationBarDuringPresentation = NO;
    _searchController.dimsBackgroundDuringPresentation = YES;
    
    _searchController.searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    _searchController.searchBar.delegate = self;
    [_searchController.searchBar sizeToFit];
    self.navigationItem.titleView = _searchController.searchBar;
    self.definesPresentationContext = YES;
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    // Work around a UISearchController bug that doesn't reposition the table view correctly when
    // rotating to landscape.
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    _searchController.searchResultsUpdater = _acViewController;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        _searchController.modalPresentationStyle = UIModalPresentationPopover;
    } else {
        _searchController.modalPresentationStyle = UIModalPresentationFullScreen;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [self performSelector:@selector(callSearchBar) withObject:NULL afterDelay:0.2];
    [super viewDidAppear:animated];
}

-(void)callSearchBar{
    [_searchController.searchBar becomeFirstResponder];
}

#pragma mark - UISearchBarDelegate
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - GMSAutocompleteResultsViewControllerDelegate

-(void)resultsController:(GMSAutocompleteResultsViewController *)resultsController didAutocompleteWithPlace:(GMSPlace *)place {
    _searchController.active = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didAutocompleteWithPlace:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate didAutocompleteWithPlace:place];
        });
    }
}

-(void)resultsController:(GMSAutocompleteResultsViewController *)resultsController didFailAutocompleteWithError:(NSError *)error
{
//    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)didRequestAutocompletePredictionsForResultsController:(GMSAutocompleteResultsViewController *)resultsController
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

-(void)didUpdateAutocompletePredictionsForResultsController:(GMSAutocompleteResultsViewController *)resultsController
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
