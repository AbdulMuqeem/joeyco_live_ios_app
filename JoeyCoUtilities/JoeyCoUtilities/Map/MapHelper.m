//
//  GoogleMapDirection.m
//  Joey
//
//  Created by Katia Maeda on 2014-12-17.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "MapHelper.h"
#import <JoeyCoUtilities/NSDictionaryHelper.h>

//#define kDirectionsURL @"http://maps.googleapis.com/maps/api/directions/json?"
#define kDirectionsURL @"https://maps.googleapis.com/maps/api/directions/json?"

//#define map_api_key @"AIzaSyCgvW-OI6o8iv56qIzLR6KA0H3J8hC-ACs"
#define map_api_key @"AIzaSyDCjLC_NDa34ly8UcT9xMd8tXiPdaLP2FE"



@implementation MapHelper

+(NSString *)googleMapUrl:(NSArray *)locations travelMode:(TravelMode)travelMode
{
    NSUInteger locationsCount = [locations count];
    
    NSMutableArray *locationStrings = [NSMutableArray new];
    
    for (CLLocation *location in locations)
    {
        [locationStrings addObject:[[NSString alloc] initWithFormat:@"%f,%f", location.coordinate.latitude, location.coordinate.longitude]];
    }
    
    NSString *origin = [locationStrings objectAtIndex:0];
    NSString *destination = [locationStrings lastObject];
//    NSMutableString *url = [NSMutableString stringWithFormat:@"%@origin=%@&destination=%@&sensor=false&units=metric&avoid=tolls", kDirectionsURL, origin, destination];
    
        NSMutableString *url = [NSMutableString stringWithFormat:@"%@origin=%@&destination=%@&sensor=false&units=metric&avoid=tolls&key=%@", kDirectionsURL, origin, destination,map_api_key];
    
    if (locationsCount > 2)
    {
        [url appendString:@"&waypoints=optimize:false"];
        for (int i = 1; i < [locationStrings count] - 1; i++)
        {
            [url appendFormat:@"|%@", [locationStrings objectAtIndex:i]];
        }
    }
    
    switch (travelMode)
    {
        case TravelModeWalking: [url appendString:@"&mode=walking"]; break;
        case TravelModeBicycling: [url appendString:@"&mode=bicycling"]; break;
        case TravelModeTransit: [url appendString:@"&mode=transit"]; break;
        default: [url appendString:@"&mode=driving"]; break;
    }
    
    url = [NSMutableString stringWithString:[url stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
    return url;
}

+(void)getDirectionBetweenLocations:(NSArray *)locations travelMode:(TravelMode)travelMode succeeded:(void(^)(GMSPolyline *polyline))success failed:(void (^)(NSString *errorMsg))failure
{
    if ([locations count] < 2) return;
    
    NSString *url = [self googleMapUrl:locations travelMode:travelMode];

    [self fetchDataFrom:[NSURL URLWithString:url] withCompletionHandler:^(NSMutableDictionary *json, NSString *errorMsg) {
        if (errorMsg)
        {
            failure(errorMsg);
            return;
        }
        
        NSArray *routesArray = [json objectForKey:@"routes"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([routesArray count] > 0)
            {
                NSDictionary *routeDict = [routesArray objectAtIndex:0];
                NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                GMSPath *path = [GMSPath pathFromEncodedPath:points];
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
                success(polyline);
            }
            else
            {
                failure(@"Failed to get directions from Google Maps.");
            }
        });
    }];
}

+(void)getGoogleMapRouteInfo:(NSArray *)locations travelMode:(TravelMode)travelMode succeeded:(void(^)(GoogleMapRouteInfo *info))success failed:(void (^)(NSString *errorMsg))failure
{
    if ([locations count] < 2) return;
    
    NSString *url = [self googleMapUrl:locations travelMode:travelMode];

    [self fetchDataFrom:[NSURL URLWithString:url] withCompletionHandler:^(NSMutableDictionary *json, NSString *errorMsg) {
        if (errorMsg)
        {
            failure(errorMsg);
            return;
        }
        
        NSArray *routesArray = [json objectForKey:@"routes"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([routesArray count] > 0)
            {
                NSDictionary *routeDict = [routesArray objectAtIndex:0];
                GoogleMapRouteInfo *info = [[GoogleMapRouteInfo alloc] init];
                
                info.durations = [NSMutableArray array];
                info.distances = [NSMutableArray array];
                info.totalDistance = 0;
                info.totalDuration = 0;
                
                NSArray *legsArray = [routeDict arrayForKey:@"legs"];
                for (int i=0; i < [legsArray count]; i++)
                {
                    NSDictionary *legDict = [legsArray objectAtIndex:i];
                    NSDictionary *distanceDict = [legDict objectForKey:@"distance"];
                    NSNumber *distance = [distanceDict numberForKey:@"value"];
                    [info.distances addObject:distance];
                    [info addTotalDistance:distance];
                    
                    NSDictionary *durationDict = [legDict objectForKey:@"duration"];
                    NSNumber *duration = [durationDict numberForKey:@"value"];
                    [info.durations addObject:duration];
                    [info addTotalDuration:duration];
                }
                
                NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                GMSPath *path = [GMSPath pathFromEncodedPath:points];
                info.polyline = [GMSPolyline polylineWithPath:path];
                success(info);
            }
            else
            {
                failure(@"Failed to get data from Google Maps.");
            }
        });
    }];
}

+(void)fetchDataFrom:(NSURL *)url withCompletionHandler:(void (^)(NSMutableDictionary *json, NSString *errorMsg))completionHandler
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    // Create a data task object to perform the data downloading.
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *errorMsg = nil;
        NSMutableDictionary *json = nil;
        
        if (error)
        {
            // If any error occurs then just display its description on the console.
            NSLog(@"%@", [error localizedDescription]);
            errorMsg = [error localizedDescription];
        }
        else
        {
            NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
            
            if (HTTPStatusCode > 200)
            {
                NSLog(@"HTTP status code = %ld", (long)HTTPStatusCode);
                NSLog(@"jsonData:%@", [[NSString alloc] initWithData:data encoding:4]);
                errorMsg = [[NSString alloc] initWithData:data encoding:4];
            }
            else if (!data)
            {
                NSLog(@"Data null with code=%ld", (long)HTTPStatusCode);
                errorMsg = [NSString stringWithFormat:@"Data null with code=%ld", (long)HTTPStatusCode];
            }
            else
            {
                NSError *error;
                json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if (error)
                {
                    NSLog(@"%@", errorMsg);
                    errorMsg = [error localizedDescription];
                }
            }
        }
        
        completionHandler(json, errorMsg);
    }];
    
    [task resume];
}

+(double)getDistanceBetweenLocations:(CLLocationCoordinate2D)coord1 and:(CLLocationCoordinate2D) coord2
{
    CLLocation* location1 = [[CLLocation alloc] initWithLatitude: coord1.latitude longitude: coord1.longitude];
    CLLocation* location2 = [[CLLocation alloc] initWithLatitude: coord2.latitude longitude: coord2.longitude];
    return [location1 distanceFromLocation: location2];
}

+(void)verifyLocationAuthorization
{
    if(![CLLocationManager locationServicesEnabled])
    {
        NSString *settingButton = nil;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            settingButton = NSLocalizedString(@"Settings", nil);
        }
        
        NSString *message = NSLocalizedString(@"You will need to turn on location services to use the app.", nil);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:message delegate:self cancelButtonTitle: nil otherButtonTitles:settingButton, nil];

        alertView.tag=121;
        
        //Checking status of how many pop ups are being displayed
        if([MapHelper doesAlertViewExist] == NO){
            
            [alertView show];
            
        }
        
        return;
    }
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted)
    {
        NSString *settingButton = nil;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            settingButton = NSLocalizedString(@"Settings", nil);
        }
        
        NSString *message = NSLocalizedString(@"You will need to turn on location services to use the app.", nil);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:message delegate:self cancelButtonTitle:nil otherButtonTitles:settingButton, nil];
        alertView.tag=121;
        alertView.cancelButtonIndex = -1;

        //Checking status of how many pop ups are being displayed
        if([MapHelper doesAlertViewExist] == NO){
            
            [alertView show];
            
        }        return;
    }
}

+(BOOL)verifyLocationAuthorizationWithBooleanStatus
{
    BOOL locationStatus = NO;
    
    if(![CLLocationManager locationServicesEnabled])
    {
        NSString *settingButton = nil;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            settingButton = NSLocalizedString(@"Settings", nil);
        }
        
        NSString *message = NSLocalizedString(@"You will need to turn on location services to use the app.", nil);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:message delegate:self cancelButtonTitle:nil otherButtonTitles:settingButton, nil];
        alertView.tag=121;
        alertView.cancelButtonIndex = -1;

        //Checking status of how many pop ups are being displayed
        if([MapHelper doesAlertViewExist] == NO){
            
            [alertView show];
            
        }
        locationStatus = NO;
      //  return locationStatus;
    }
    else{
        locationStatus = YES;
     //   return locationStatus;

    }
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted)
    {
        NSString *settingButton = nil;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            settingButton = NSLocalizedString(@"Settings", nil);
        }
        
        NSString *message = NSLocalizedString(@"You will need to turn on location services to use the app.", nil);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:message delegate:self cancelButtonTitle:nil otherButtonTitles:settingButton, nil];
        alertView.tag=121;
        alertView.cancelButtonIndex = -1;

        //Checking status of how many pop ups are being displayed
        if([MapHelper doesAlertViewExist] == NO){
            
            [alertView show];
            
        }
        
        locationStatus = NO;
        return locationStatus;
    }
    else{
        
        
        
        locationStatus = locationStatus == YES ? YES: NO;
        return locationStatus;

    }
}

//
//+(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (alertView.tag == 121 && buttonIndex == 1)
//    {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//    }
//}


+(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 121 && buttonIndex == 0)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

+(NSString *)stringFromDistance:(NSInteger)distance
{
    return [NSString stringWithFormat:@"%d.%d km", (int)(distance / 1000), (int)(distance % 1000)/ 100];
}

+(GMSCoordinateBounds *)mapFitAllMarkers:(NSArray*)markers
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];

    for (GMSMarker *marker in markers)
    {
        bounds = [bounds includingCoordinate:marker.position];
    }
    
    return bounds;
}

+(BOOL) doesAlertViewExist
{
    if ([[UIApplication sharedApplication].keyWindow isMemberOfClass:[UIWindow class]])
    {
        return NO;//AlertView does not exist on current window
    }
    return YES;//AlertView exist on current window
}

@end
