//
//  CryptographyHelper.m
//  JoeyCoUtilities
//
//  Created by Katia Maeda on 2016-01-12.
//  Copyright © 2016 JoeyCo. All rights reserved.
//

#import "CryptographyHelper.h"
#import <CommonCrypto/CommonHMAC.h>

@implementation CryptographyHelper

+(NSString *)getHashWithKey:(NSString *)keyString data:(NSString *)data
{
    NSData * dataIn = [data dataUsingEncoding:NSASCIIStringEncoding];
    NSData * key = [keyString dataUsingEncoding:NSASCIIStringEncoding];
    
    NSMutableData *macOut = [NSMutableData dataWithLength:CC_SHA512_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA512, key.bytes, key.length, dataIn.bytes, dataIn.length, macOut.mutableBytes);
    const unsigned char *buffer = (const unsigned char *)[macOut bytes];
    
    NSString *HMAC = [NSMutableString stringWithCapacity:macOut.length * 2];
    for (int i = 0; i < macOut.length; ++i)
    {
        HMAC = [HMAC stringByAppendingFormat:@"%02lx", (unsigned long)buffer[i]];
    }
    
    return HMAC;
}



/**
 Cryptography
 SHA- 256 api
 */


+(NSString*)sha256HashFor:(NSString*)input
{
    const char* str = [input UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
    {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

@end
