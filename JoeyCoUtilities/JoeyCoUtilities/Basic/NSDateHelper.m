//
//  NSDateHelper.m
//  Joey
//
//  Created by Katia Maeda on 2015-01-16.
//  Copyright (c) 2015 JoeyCo. All rights reserved.
//

#import "NSDateHelper.h"

@implementation NSDateHelper

+(BOOL)isToday:(NSDate *)date
{
    return [self isEqualTo:date plus:0];
}

+(BOOL)isTomorrow:(NSDate *)date
{
    return [self isEqualTo:date plus:1];
}

+(BOOL)isEqualTo:(NSDate *)date plus:(int)plusDate
{
    NSUInteger units = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:units fromDate:date];
    
    NSDate *today = [NSDate date];
    NSDateComponents *todayComponents = [[NSCalendar currentCalendar] components:units fromDate:today];

    return ((todayComponents.year == dateComponents.year) &&
            (todayComponents.month == dateComponents.month) &&
            (todayComponents.day+plusDate == dateComponents.day));
}

+(BOOL)isLater:(NSDate *)date plusSeconds:(int)seconds
{
    NSDate *startTimeLate = [date dateByAddingTimeInterval:seconds];
    NSDate *now = [NSDate date];
    NSDate *laterDate = [now laterDate:startTimeLate];
    return (laterDate == now);
}

+(BOOL)isLater:(NSDate *)date plusMinutes:(int)minutes
{
    return [self isLater:date plusSeconds:minutes*60];
}

+(BOOL)isLater:(NSDate *)date plusHours:(int)hours
{
    return [self isLater:date plusMinutes:hours*60];
}

+(NSDate *)date:(NSDate *)date plusSeconds:(int)seconds
{
    return [date dateByAddingTimeInterval:seconds];
}

+(NSDate *)date:(NSDate *)date plusMinutes:(int)minutes
{
    return [date dateByAddingTimeInterval:minutes*60];
}

+(NSDate *)date:(NSDate *)date plusDays:(int)days
{
    return [self date:date plusMinutes:days*24*60];
}

+(NSDate *)dateFromString:(NSString *)stringDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [formatter dateFromString:stringDate];
}

+(NSDate *)dateFromUTCString:(NSString *)stringDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [formatter setTimeZone:gmt];
    return [formatter dateFromString:stringDate];
}

+(NSString *)stringFromDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [formatter stringFromDate:date];
}

+(NSString *)stringDateFromDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM dd, yyyy"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [formatter stringFromDate:date];
}

+(NSString *)stringDateFromDate2:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE MMM, dd"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [formatter stringFromDate:date];
}

+(NSString *)stringDateFromDate3:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [formatter stringFromDate:date];
}

+(NSString *)stringDateFromDate4:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM dd"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [formatter stringFromDate:date];
}

+(NSString *)stringDateTimeFromDate4:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd hh.mm a"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [formatter stringFromDate:date];
}

+(NSString *)stringDateTimeFromDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM dd, yyyy HH:mm"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [formatter stringFromDate:date];
}

+(NSString *)stringDateTimeFromDate2:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy - HH:mm"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [formatter stringFromDate:date];
}

+(NSString *)stringDateTimeFromDate3:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMyy"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [formatter stringFromDate:date];
}

+(NSString *)stringTimeFromDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"h:mm a"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [formatter stringFromDate:date];
}

+(NSString *)stringTimeFromDate2:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [formatter stringFromDate:date];
}

+(NSDate *)dateFromNumber:(NSInteger)integerDate
{
    if (integerDate <= 0)
    {
        return nil;
    }
    
    return [NSDate dateWithTimeIntervalSince1970:integerDate];
}

+(NSString *)stringFromCountDown:(NSInteger)countDown
{
    long hours, minutes, seconds;
    hours = countDown / 3600;
    minutes = (countDown % 3600) / 60;
    seconds = (countDown %3600) % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", hours, minutes, seconds];
}

+(NSString *)stringFromTime:(NSInteger)time
{
    long minutes = time / 60;
    long hour = minutes / 60;
    minutes = minutes % 60;
    
    if (hour > 0)
    {
        return [NSString stringWithFormat:@"%02ld:%02ld mins", hour, minutes];
    }

    return [NSString stringWithFormat:@"%02ld mins", minutes];
}

+(NSInteger)getTime:(NSDate *)date
{
    NSUInteger units = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:units fromDate:date];
    return dateComponents.hour * 60 * 60 + dateComponents.minute * 60 + dateComponents.second;
}

+(NSInteger)getFirstHour:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *stringDate = [NSString stringWithFormat:@"%@ 00:00:00", [formatter stringFromDate:date]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *firstDate = [formatter dateFromString:stringDate];
    return [firstDate timeIntervalSince1970];
}

+(NSInteger)getLastHour:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *stringDate = [NSString stringWithFormat:@"%@ 23:59:59", [formatter stringFromDate:date]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *lastDate = [formatter dateFromString:stringDate];
    return [lastDate timeIntervalSince1970];
}

+(NSString *)getFirstHourString:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [NSString stringWithFormat:@"%@ 00:00:00", [formatter stringFromDate:date]];
}

+(NSString *)getLastHourString:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return [NSString stringWithFormat:@"%@ 23:59:59", [formatter stringFromDate:date]];
}

@end
