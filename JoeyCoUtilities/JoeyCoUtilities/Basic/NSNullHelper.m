//
//  NSNumber+Helper.m
//  Joey
//
//  Created by Katia Maeda on 2014-11-06.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "NSNullHelper.h"

@implementation NSNull(Helper)

- (float)floatValue
{
    return 0;
}

- (int)intValue
{
    return 0;
}

- (BOOL)boolValue
{
    return NO;
}

@end
