//
//  Common.h
//  JoeyCo
//
//  Created by Katia Maeda on 2014-09-27.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#ifndef JoeyCo_Common_h
#define JoeyCo_Common_h

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#endif
