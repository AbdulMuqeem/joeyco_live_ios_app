//
//  DatabaseHelper.m
//  Joey
//
//  Created by Katia Maeda on 2014-11-24.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "DatabaseHelper.h"

@implementation DatabaseHelper

static DatabaseHelper *instance;
static NSString *databasePath;
static NSString *databaseName = @"joeyco_joey_db.db";
const NSInteger databaseVersion = 6;

#pragma mark - Database lifecycle

+(DatabaseHelper *)shared
{
     @synchronized(self)
     {
          if (!instance)
          {
               instance = [[DatabaseHelper alloc] init];
               
               // Get the database from the application bundle.
               NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
               databasePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:databaseName];
               NSLog(@"local do banco: %@", databasePath);
               
               [instance migrateToSchemaFromVersion:[instance userVersion] toVersion:databaseVersion];
          }
          return instance;
     }
}

-(int)userVersion
{
     sqlite3_stmt *statement;
     int dbVersion = -1;
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = @"PRAGMA user_version;";
          const char *query_stmt = [querySQL UTF8String];
          if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               while(sqlite3_step(statement) == SQLITE_ROW)
               {
                    dbVersion = sqlite3_column_int(statement, 0);
                    //                NSLog(@"%s: version %d", __FUNCTION__, databaseVersion);
               }
               //            NSLog(@"%s: the databaseVersion is: %d", __FUNCTION__, databaseVersion);
               sqlite3_finalize(statement);
          }
          
          sqlite3_close(database);
     }
     
     return dbVersion;
}

-(void)setUserVersion:(int)version
{
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          char *errMsg;
          NSString *querySQL = [NSString stringWithFormat:@"PRAGMA user_version = %d", version];
          const char *sql_stmt = [querySQL UTF8String];
          
          if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
          {
               NSLog(@"%s: Failed to set user_version", __FUNCTION__);
          }
          
          sqlite3_close(database);
     }
}

- (void)migrateToSchemaFromVersion:(int)fromVersion toVersion:(int)toVersion
{
     if (fromVersion >= toVersion)
     {
          return;
     }
     
     char *errMsg = NULL;
     NSError *error;
     
     if (fromVersion <= 0)
     {
          // Delete if exists
          error = [self deleteDatabase];
     }
     
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = nil;
          const char *sql_stmt = nil;
          switch (fromVersion)
          {
               case -1: case 0:
                    errMsg = [self createDatabase];
               case 1:
                    querySQL = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN %@ REAL; ALTER TABLE %@ ADD COLUMN %@ REAL; ALTER TABLE %@ ADD COLUMN %@ REAL;", confirmationTable, confirmationFieldOrderId, taskTable, taskFieldLatitude, taskTable, taskFieldLongitude];
                    sql_stmt = [querySQL UTF8String];
                    sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg);
               case 2:
                    querySQL = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN %@ REAL; ALTER TABLE %@ ADD COLUMN %@ REAL;", joeyTable, joeyFieldSettingRoute, joeyTable, joeyFieldSettingLargeFont];
                    sql_stmt = [querySQL UTF8String];
                    sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg);
               case 3:
                    querySQL = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN %@ REAL; ALTER TABLE %@ ADD COLUMN %@ REAL;", joeyTable, joeyFieldDuty, joeyTable, joeyFieldVehicleId];
                    sql_stmt = [querySQL UTF8String];
                    sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg);
               case 4:
                    querySQL = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (%@ REAL PRIMARY KEY, %@ TEXT); CREATE TABLE IF NOT EXISTS %@ (%@ REAL PRIMARY KEY, %@ TEXT, %@ TEXT, %@ REAL, %@ REAL, %@ REAL); ALTER TABLE %@ ADD COLUMN %@ DATETIME; ALTER TABLE %@ ADD COLUMN %@ TEXT; ALTER TABLE %@ ADD COLUMN %@ TEXT; DELETE FROM %@; DELETE FROM %@; DELETE FROM %@; DELETE FROM %@;", vehicleTable, vehicleFieldId, vehicleFieldName, zoneTable, zoneFieldId, zoneFieldNum, zoneFieldName, zoneFieldLatitude, zoneFieldLongitude, zoneFieldRadius, metaDataTable, metaDataFieldDate, joeyTable, joeyFieldPublicKey, joeyTable, joeyFieldPrivateKey, orderTable, taskTable, confirmationTable, joeyTable];
                    sql_stmt = [querySQL UTF8String];
                    sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg);
               case 5:
                    querySQL = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN %@ TEXT; ALTER TABLE %@ ADD COLUMN %@ TEXT;", metaDataTable, metaDataFieldHome, metaDataTable, metaDataFieldSignUp];
                    sql_stmt = [querySQL UTF8String];
                    sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg);
          }
          sqlite3_close(database);
     }
     
     if (errMsg || error)
     {
          NSLog(@"%s: Error trying to upgrade from %d, to %d: %@", __FUNCTION__, fromVersion, toVersion, [NSString stringWithUTF8String:errMsg]);
          [self setUserVersion:-1];
     }
     else
     {
          [self setUserVersion:toVersion];
     }
}

-(NSError *)deleteDatabase
{
     NSError *error;
     NSFileManager *fileManager = [NSFileManager defaultManager];
     if ([fileManager fileExistsAtPath: databasePath])
     {
          
          [fileManager removeItemAtPath:databasePath error:&error];
          if (error)
          {
               NSLog(@"%s: Failed to delete database: %@", __FUNCTION__, [error localizedDescription]);
          }
     }
     return error;
}

-(char *)createDatabase
{
     char *errMsg;
     NSString *joeySQL = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (%@ REAL PRIMARY KEY, %@ TEXT, %@ TEXT, %@ TEXT, %@ TEXT, %@ TEXT, %@ TEXT, %@ REAL); ", joeyTable, joeyFieldId, joeyFieldFirstName, joeyFieldLastName, joeyFieldEmail, joeyFieldPhone, joeyFieldNotes, joeyFieldVehicle, joeyFieldSettingMap];
     NSString *settingsSQL = [NSString stringWithFormat:@" CREATE TABLE IF NOT EXISTS %@ (%@ TEXT PRIMARY KEY, %@ TEXT); ", settingsTable, settingsFieldId, settingsFieldValue];
     NSString *metaOptionSQL = [NSString stringWithFormat:@" CREATE TABLE IF NOT EXISTS %@ (%@ TEXT PRIMARY KEY, %@ TEXT); ", metaOptionTable, metaOptionFieldId, metaOptionFieldValue];
     NSString *metaStatusSQL = [NSString stringWithFormat:@" CREATE TABLE IF NOT EXISTS %@ (%@ TEXT PRIMARY KEY, %@ TEXT); ", metaStatusTable, metaStatusFieldId, metaStatusFieldValue];
     NSString *metaDataSQL = [NSString stringWithFormat:@" CREATE TABLE IF NOT EXISTS %@ (local_id INTEGER PRIMARY KEY AUTOINCREMENT, %@ TEXT, %@ TEXT); ", metaDataTable, metaDataFieldPrivacy, metaDataFieldTerms];
     NSString *confirmationSQL = [NSString stringWithFormat:@" CREATE TABLE IF NOT EXISTS %@ (%@ REAL PRIMARY KEY, %@ REAL, %@ TEXT, %@ TEXT, %@ TEXT, %@ TEXT, %@ TEXT, %@ TEXT, %@ REAL, %@ BLOB, %@ TEXT); ", confirmationTable, confirmationFieldId, confirmationFieldTaskId, confirmationFieldName, confirmationFieldTitle, confirmationFieldDescription, confirmationFieldInputType, confirmationFieldUrl, confirmationFieldMethod, confirmationFieldStatus, confirmationFieldData, confirmationFieldInputField];
     NSString *orderSQL = [NSString stringWithFormat:@" CREATE TABLE IF NOT EXISTS %@ (%@ REAL PRIMARY KEY, %@ TEXT, %@ REAL, %@ REAL, %@ REAL, %@ REAL, %@ REAL, %@ REAL, %@ REAL, %@ REAL, %@ REAL); ", orderTable, orderFieldId, orderFieldNum, orderFieldStatus, orderFieldDistance, orderFieldDistanceAllowance, orderFieldDistanceCharge, orderFieldTotalCharge, orderFieldSubtotal, orderFieldTax, orderFieldTotal, orderFieldTip];
     NSString *taskSQL = [NSString stringWithFormat:@" CREATE TABLE IF NOT EXISTS %@ (%@ REAL PRIMARY KEY, %@ REAL, %@ TEXT, %@ TEXT, %@ TEXT, %@ REAL, %@ TEXT, %@ TEXT, %@ TEXT, %@ TEXT, %@ TEXT, %@ TEXT, %@ TEXT, %@ REAL, %@ REAL, %@ REAL, %@ REAL, %@ REAL, %@ REAL); ", taskTable, taskFieldId, taskFieldOrderId, taskFieldPin, taskFieldType, taskFieldDescription, taskFieldDueTime, taskFieldContactName, taskFieldContactPhone, taskFieldContactEmail, taskFieldAddress, taskFieldSuite, taskFieldBuzzer, taskFieldPostalCode, taskFieldCharge, taskFieldService, taskFieldWeightValue, taskFieldWeightUnit, taskFieldPaymentType, taskFieldPaymentAmount];
     NSString *zoneSQL = [NSString stringWithFormat:@" CREATE TABLE IF NOT EXISTS %@ (%@ REAL PRIMARY KEY, %@ TEXT, %@ DATETIME, %@ DATETIME, %@ DATETIME, %@ DATETIME, %@ REAL, %@ TEXT, %@ REAL, %@ REAL, %@ REAL); ", shiftTable, shiftFieldId, shiftFieldNum, shiftFieldStartTime, shiftFieldEndTime, shiftFieldRealStartTime, shiftFieldRealEndTime, shiftFieldZoneId, shiftFieldZoneNum, shiftFieldZoneLatitude, shiftFieldZoneLongitude, shiftFieldZoneRadius];
     
     NSMutableString *querySQL = [NSMutableString stringWithString:joeySQL];
     [querySQL appendString:settingsSQL];
     [querySQL appendString:metaOptionSQL];
     [querySQL appendString:metaStatusSQL];
     [querySQL appendString:metaDataSQL];
     [querySQL appendString:confirmationSQL];
     [querySQL appendString:orderSQL];
     [querySQL appendString:taskSQL];
     [querySQL appendString:zoneSQL];
     
     // TODO deletar depois
     [querySQL appendString:@" CREATE TABLE IF NOT EXISTS USER_LOCATION (LOCAL_TIME DATETIME, LATITUDE REAL, LONGITUDE REAL); "];
     
     const char *sql_stmt = [querySQL UTF8String];
     
     if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
     {
          NSLog(@"%s: Failed to create table", __FUNCTION__);
     }
     
     return errMsg;
}

#pragma mark - Joey

-(void)insertJoey:(Joey *)joey
{
     [self deleteJoeyAndOrderData];
     
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *firstName = joey.firstName;
          if ([joey.displayName isEqualToString:@"full"])
          {
               firstName = [NSString stringWithFormat:@"%@ %@", joey.firstName, joey.lastName];
          }
          else if ([joey.displayName isEqualToString:@"abbr"])
          {
               firstName = [NSString stringWithFormat:@"%@ %c.", joey.firstName, [joey.lastName characterAtIndex:0]];
          }
          else if ([joey.displayName isEqualToString:@"nickname"])
          {
               firstName = joey.nickName;
          }
          
          NSString *querySQL = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@) VALUES (\"%f\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%f\", \"%ld\", \"%f\", \"%@\", \"%@\")", joeyTable, joeyFieldId, joeyFieldFirstName, joeyFieldLastName, joeyFieldEmail, joeyFieldPhone, joeyFieldNotes, joeyFieldVehicleId, joeyFieldSettingMap, joeyFieldDuty, joeyFieldPublicKey, joeyFieldPrivateKey, [joey.joeyId doubleValue], firstName, joey.lastName, joey.email, joey.phone, joey.about, [joey.vehicleId doubleValue], (long)MapTypeSettingNormal, [joey.duty doubleValue], joey.publicKey, joey.privateKey];
          const char *query_stmt = [querySQL UTF8String];
          sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
          if (sqlite3_step(statement) == SQLITE_DONE)
          {
               
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
}

-(void)updateJoey:(Joey *)joey
{
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          char *errMsg = NULL;
          const char *sql_stmt = nil;
          
          NSString *firstName = joey.firstName;
          if ([joey.displayName isEqualToString:@"full"])
          {
               firstName = [NSString stringWithFormat:@"%@ %@", joey.firstName, joey.lastName];
          }
          else if ([joey.displayName isEqualToString:@"abbr"])
          {
               firstName = [NSString stringWithFormat:@"%@ %c.", joey.firstName, [joey.lastName characterAtIndex:0]];
          }
          else if ([joey.displayName isEqualToString:@"nickname"])
          {
               firstName = joey.nickName;
          }

          NSString *querySQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ = \"%d\"; UPDATE %@ SET %@ = \"%d\"; UPDATE %@ SET %@ = \"%@\"; ", joeyTable, joeyFieldVehicleId, [joey.vehicleId intValue], joeyTable, joeyFieldDuty, [joey.duty intValue], joeyTable, joeyFieldFirstName, firstName];
          sql_stmt = [querySQL UTF8String];
          sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg);
          sqlite3_close(database);
     }
}

-(Joey *)getJoey
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     Joey *joey = nil;
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@ FROM %@", joeyFieldId, joeyFieldFirstName, joeyFieldLastName, joeyFieldEmail, joeyFieldPhone, joeyFieldNotes, joeyFieldVehicleId, joeyFieldDuty, joeyTable];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               if (sqlite3_step(statement) == SQLITE_ROW)
               {
                    joey = [[Joey alloc] init];
                    joey.joeyId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 0)];
                    
                    char *joeyFirstName = (char *)sqlite3_column_text(statement, 1);
                    joey.firstName = (joeyFirstName) ? [NSString stringWithUTF8String:joeyFirstName] : @"";
                    char *joeyLastName = (char *)sqlite3_column_text(statement, 2);
                    joey.lastName = (joeyLastName) ? [NSString stringWithUTF8String:joeyLastName] : @"";
                    char *joeyEmail = (char *)sqlite3_column_text(statement, 3);
                    joey.email = (joeyEmail) ? [NSString stringWithUTF8String:joeyEmail] : @"";
                    char *joeyPhone = (char *)sqlite3_column_text(statement, 4);
                    joey.phone = (joeyPhone) ? [NSString stringWithUTF8String:joeyPhone] : @"";
                    char *joeyNotes = (char *)sqlite3_column_text(statement, 5);
                    joey.about = (joeyNotes) ? [NSString stringWithUTF8String:joeyNotes] : @"";
                    joey.vehicleId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 6)];
                    joey.duty = [NSNumber numberWithDouble:sqlite3_column_double(statement, 7)];
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return joey;
}

-(void)deleteJoeyAndOrderData
{
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          char *errMsg;
          NSString *querySQL = [NSString stringWithFormat:@"DELETE FROM %@; DELETE FROM %@; DELETE FROM %@; DELETE FROM %@;", orderTable, taskTable, confirmationTable, joeyTable];
          const char *sql_stmt = [querySQL UTF8String];
          
          if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
          {
               NSLog(@"%s: Failed to delete meta data", __FUNCTION__);
          }
          
          sqlite3_close(database);
     }
}

-(MapTypeSetting)getMapTypeSetting
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     MapTypeSetting type = MapTypeSettingNormal;
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@ FROM %@", joeyFieldSettingMap, joeyTable];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               if (sqlite3_step(statement) == SQLITE_ROW)
               {
                    double mapSetting = sqlite3_column_double(statement, 0);
                    if (mapSetting == MapTypeSettingHybrid)
                    {
                         type = MapTypeSettingHybrid;
                    }
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return type;
}

-(int)getMapRouteData
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     int route = 0;
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@ FROM %@", joeyFieldSettingRoute, joeyTable];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               if (sqlite3_step(statement) == SQLITE_ROW)
               {
                    double mapSetting = sqlite3_column_double(statement, 0);
                    route = (mapSetting == 0) ? 0 : 1;
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return route;
}

-(BOOL)getDisplayLargeFontSize
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     BOOL showLargeFont = NO;
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@ FROM %@", joeyFieldSettingLargeFont, joeyTable];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               if (sqlite3_step(statement) == SQLITE_ROW)
               {
                    double mapSetting = sqlite3_column_double(statement, 0);
                    showLargeFont = (mapSetting == 0) ? NO : YES;
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return showLargeFont;
}

-(void)updateSetting:(NSString *)name value:(int)value
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ = \"%d\"", joeyTable, name, value];
          const char *query_stmt = [querySQL UTF8String];
          sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
          if (sqlite3_step(statement) == SQLITE_DONE)
          {
               
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
}

-(NSString *)getPublicKey
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     NSString *publicKey = nil;
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@ FROM %@", joeyFieldPublicKey, joeyTable];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               if (sqlite3_step(statement) == SQLITE_ROW)
               {
                    char *data = (char *)sqlite3_column_text(statement, 0);
                    publicKey = (data) ? [NSString stringWithUTF8String:data] : @"";
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return publicKey;
}

-(NSString *)getPrivateKey
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     NSString *privateKey = nil;
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@ FROM %@", joeyFieldPrivateKey, joeyTable];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               if (sqlite3_step(statement) == SQLITE_ROW)
               {
                    char *data = (char *)sqlite3_column_text(statement, 0);
                    privateKey = (data) ? [NSString stringWithUTF8String:data] : @"";
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return privateKey;
}

#pragma mark - Settings

-(void)deleteSetting
{
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          char *errMsg;
          NSString *querySQL = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ IS NOT NULL", settingsTable, settingsFieldId];
          const char *sql_stmt = [querySQL UTF8String];
          
          if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
          {
               NSLog(@"%s: Failed to delete setting", __FUNCTION__);
          }
          
          sqlite3_close(database);
     }
}

-(void)insertSetting:(NSString *)settingId value:(NSString *)settingValue
{
     [self deleteSetting];
     
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSMutableString stringWithFormat:@"INSERT INTO %@ (%@, %@) VALUES (\"%@\", \"%@\"); ", settingsTable, settingsFieldId, settingsFieldValue, settingId, settingValue];
          const char *sql_stmt = [querySQL UTF8String];
          
          char *errMsg;
          if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
          {
               NSLog(@"%s: Failed to insert setting", __FUNCTION__);
          }
          
          sqlite3_close(database);
     }
}

-(NSString *)getSetting:(NSString *)settingId
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     NSString *value = nil;
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE %@ = \"%@\"", settingsFieldValue, settingsTable, settingsFieldId, settingId];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               if(sqlite3_step(statement) == SQLITE_ROW)
               {
                    char *data = (char *)sqlite3_column_text(statement, 0);
                    value = (data) ? [NSString stringWithUTF8String:data] : @"";
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return value;
}

#pragma mark - Link data

-(void)deleteMetaData
{
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          char *errMsg;
          NSString *querySQL = [NSString stringWithFormat:@"DELETE FROM %@; DELETE FROM %@; DELETE FROM %@;", metaDataTable, metaOptionTable, metaStatusTable];
          const char *sql_stmt = [querySQL UTF8String];
          
          if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
          {
               NSLog(@"%s: Failed to delete meta data", __FUNCTION__);
          }
          
          sqlite3_close(database);
     }
}

-(void)insertMetaData:(MetaData *)metaData
{
     [self deleteMetaData];
     
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSMutableString *querySQL = [NSMutableString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@) VALUES (\"%@\", \"%@\", \"%@\", \"%@\", \"%@\"); ", metaDataTable, metaDataFieldPrivacy, metaDataFieldTerms, metaDataFieldHome, metaDataFieldSignUp, metaDataFieldDate, metaData.privacyLink, metaData.termsLink, metaData.homeLink, metaData.signupLink, [NSDateHelper stringFromDate:[NSDate date]]];

          for (MetaOption *metaOption in metaData.options)
          {
               NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@) VALUES (\"%@\", \"%@\"); ", metaOptionTable, metaOptionFieldId, metaOptionFieldValue, metaOption.optionId, metaOption.value];
               [querySQL appendString:sql];
          }
          
          for (MetaStatus *metaStatus in metaData.status)
          {
               NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@) VALUES (\"%@\", \"%f\"); ", metaStatusTable, metaStatusFieldId, metaStatusFieldValue, metaStatus.statusId, [metaStatus.value doubleValue]];
               [querySQL appendString:sql];
          }
          
          const char *sql_stmt = [querySQL UTF8String];
          
          char *errMsg;
          if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
          {
               NSLog(@"%s: Failed to insert meta data", __FUNCTION__);
          }
          
          sqlite3_close(database);
     }
}

-(MetaData *)getMetaData
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     MetaData *metaData = nil;
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@", metaDataFieldPrivacy, metaDataFieldTerms, metaDataFieldHome, metaDataFieldSignUp, metaDataFieldDate, metaDataTable];

          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               while(sqlite3_step(statement) == SQLITE_ROW)
               {
                    metaData = [[MetaData alloc] init];
                    char *privacy = (char *)sqlite3_column_text(statement, 0);
                    metaData.privacyLink = (privacy) ? [NSString stringWithUTF8String:privacy] : @"";
                    char *terms = (char *)sqlite3_column_text(statement, 1);
                    metaData.termsLink = (terms) ? [NSString stringWithUTF8String:terms] : @"";
                    char *home = (char *)sqlite3_column_text(statement, 2);
                    metaData.homeLink = (home) ? [NSString stringWithUTF8String:home] : @"";
                    char *signup = (char *)sqlite3_column_text(statement, 3);
                    metaData.signupLink = (signup) ? [NSString stringWithUTF8String:signup] : @"";
                    char *time = (char *)sqlite3_column_text(statement, 4);
                    metaData.lastUpdated = (time) ? [NSDateHelper dateFromString:[NSString stringWithUTF8String:time]] : nil;
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return metaData;
}

-(MetaData *)getFullMetaData
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     MetaData *metaData = nil;
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@ FROM %@", metaDataFieldPrivacy, metaDataFieldTerms, metaDataFieldHome, metaDataFieldSignUp, metaDataFieldDate, metaDataTable];

          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               while(sqlite3_step(statement) == SQLITE_ROW)
               {
                    metaData = [[MetaData alloc] init];
                    char *privacy = (char *)sqlite3_column_text(statement, 0);
                    metaData.privacyLink = (privacy) ? [NSString stringWithUTF8String:privacy] : @"";
                    char *terms = (char *)sqlite3_column_text(statement, 1);
                    metaData.termsLink = (terms) ? [NSString stringWithUTF8String:terms] : @"";
                    char *home = (char *)sqlite3_column_text(statement, 2);
                    metaData.homeLink = (home) ? [NSString stringWithUTF8String:home] : @"";
                    char *signup = (char *)sqlite3_column_text(statement, 3);
                    metaData.signupLink = (signup) ? [NSString stringWithUTF8String:signup] : @"";
                    char *time = (char *)sqlite3_column_text(statement, 4);
                    metaData.lastUpdated = (time) ? [NSDateHelper dateFromString:[NSString stringWithUTF8String:time]] : nil;
                    
                    metaData.vehicles = [self getVehicles];
                    metaData.zones = [self getZones];
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }

     return metaData;
}

#pragma mark - Meta data Status

-(MetaStatus *)getMetaStatusByKey:(NSString *)key
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     MetaStatus *metaStatus = nil;
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@, %@ FROM %@ WHERE %@ = \"%@\"", metaStatusFieldId, metaStatusFieldValue, metaStatusTable, metaStatusFieldId, key];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               if (sqlite3_step(statement) == SQLITE_ROW)
               {
                    metaStatus = [[MetaStatus alloc] init];
                    
                    char *statusId = (char *)sqlite3_column_text(statement, 0);
                    metaStatus.statusId = (statusId) ? [NSString stringWithUTF8String:statusId] : @"";
                    metaStatus.value = [NSNumber numberWithDouble:sqlite3_column_double(statement, 1)];
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return metaStatus;
}

#pragma mark - Meta data Options

-(MetaOption *)getMetaOptionByKey:(NSString *)key
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     MetaOption *metaOption = nil;
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@, %@ FROM %@ WHERE %@ = \"%@\"", metaOptionFieldId, metaOptionFieldValue, metaOptionTable, metaOptionFieldId, key];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               if (sqlite3_step(statement) == SQLITE_ROW)
               {
                    metaOption = [[MetaOption alloc] init];
                    
                    char *optionId = (char *)sqlite3_column_text(statement, 0);
                    metaOption.optionId = (optionId) ? [NSString stringWithUTF8String:optionId] : @"";
                    char *optionValue = (char *)sqlite3_column_text(statement, 1);
                    metaOption.value = (optionValue) ? [NSString stringWithUTF8String:optionValue] : @"";
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return metaOption;
}

#pragma mark - Order

-(void)insertOrders:(NSMutableArray *)orders
{
     [self deleteOrders];
     
     for (Order *order in orders)
     {
          [self insertTableOrder:order];
          for (Task *task in order.tasks)
          {
               [self insertTask:task];
               for (Confirmation *confirmation in task.confirmations)
               {
                    [self insertConfirmation:confirmation];
               }
          }
     }
}

-(void)insertTableOrder:(Order *)order
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@) VALUES (\"%f\", \"%@\", \"%f\", \"%f\",\"%f\", \"%f\", \"%f\", \"%f\", \"%f\", \"%f\", \"%f\")", orderTable, orderFieldId, orderFieldNum, orderFieldStatus, orderFieldDistance, orderFieldDistanceAllowance, orderFieldDistanceCharge, orderFieldTotalCharge, orderFieldSubtotal, orderFieldTax, orderFieldTotal, orderFieldTip, [order.orderId doubleValue], order.num, [order.status doubleValue], [order.distance doubleValue], [order.distanceAllowance doubleValue], [order.distanceCharge doubleValue], [order.totalTaskCharge doubleValue], [order.subtotal doubleValue], [order.tax doubleValue], [order.total doubleValue], [order.tip doubleValue]];
          const char *query_stmt = [querySQL UTF8String];
          sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
          if (sqlite3_step(statement) == SQLITE_DONE)
          {
               
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
}



-(NSMutableArray *)getListOfAllOrders
{
     sqlite3_stmt *statement;
     NSMutableArray *orders = [NSMutableArray array];

     const char *dbpath = [databasePath UTF8String];
     
     
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM %@",orderTable];
          
          const char *query_stmt = [querySQL UTF8String];
          sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               while (sqlite3_step(statement) == SQLITE_ROW)
               {
                    Order *order = [[Order alloc] init];
                    order.orderId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 0)];
                    
                    char *orderNum = (char *)sqlite3_column_text(statement, 1);
                    order.num = (orderNum) ? [NSString stringWithUTF8String:orderNum] : @"";
                    order.status = [NSNumber numberWithDouble:sqlite3_column_double(statement, 2)];
                    order.distance = [NSNumber numberWithDouble:sqlite3_column_double(statement, 3)];
                    order.distanceAllowance = [NSNumber numberWithDouble:sqlite3_column_double(statement, 4)];
                    order.distanceCharge = [NSNumber numberWithDouble:sqlite3_column_double(statement, 5)];
                    order.totalTaskCharge = [NSNumber numberWithDouble:sqlite3_column_double(statement, 6)];
                    order.subtotal = [NSNumber numberWithDouble:sqlite3_column_double(statement, 7)];
                    order.tax = [NSNumber numberWithDouble:sqlite3_column_double(statement, 8)];
                    order.total = [NSNumber numberWithDouble:sqlite3_column_double(statement, 9)];
                    char *orderTip = (char *)sqlite3_column_text(statement, 10);
                    order.tip = (orderTip) ? [NSString stringWithUTF8String:orderTip] : @"";
                    
                    [orders addObject:order];
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     return orders;

}


-(NSMutableArray *)getOfflineOrders
{
     NSMutableArray *confirmations = [[DatabaseHelper shared] getConfirmationsWith:ConfirmationStatusNotConfirmed];
     if (confirmations.count <= 0)
     {
          return nil;
     }
     
     NSMutableArray *orderIds = [NSMutableArray array];
     NSMutableString *ids = [NSMutableString string];
     for (Confirmation *confirmation in confirmations)
     {
          if (![orderIds containsObject:confirmation.orderId])
          {
               [orderIds addObject:confirmation.orderId];
               [ids appendString:[NSString stringWithFormat:@"%f, ", [confirmation.orderId doubleValue]]];
          }
     }
     
     ids = [NSMutableString stringWithString:[ids substringToIndex:ids.length-2]];
     
     NSMutableArray *orders = [self getOrdersWith:ids];
     for (Order *order in orders)
     {
          order.tasks = [self getTasksFromOrderId:order.orderId];
          
          for (Task *task in order.tasks)
          {
               task.order = order;
               task.confirmations = [self getConfirmationsFromTaskId:task.taskId];
          }
     }
     
     return orders;
}

-(NSMutableArray *)getOrdersWith:(NSString *)orderIds
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     NSMutableArray *orders = [NSMutableArray array];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ IN (%@)", orderFieldId, orderFieldNum, orderFieldStatus, orderFieldDistance, orderFieldDistanceAllowance, orderFieldDistanceCharge, orderFieldTotalCharge, orderFieldSubtotal, orderFieldTax, orderFieldTotal, orderFieldTip, orderTable, orderFieldId, orderIds];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               while (sqlite3_step(statement) == SQLITE_ROW)
               {
                    Order *order = [[Order alloc] init];
                    order.orderId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 0)];
                    
                    char *orderNum = (char *)sqlite3_column_text(statement, 1);
                    order.num = (orderNum) ? [NSString stringWithUTF8String:orderNum] : @"";
                    order.status = [NSNumber numberWithDouble:sqlite3_column_double(statement, 2)];
                    order.distance = [NSNumber numberWithDouble:sqlite3_column_double(statement, 3)];
                    order.distanceAllowance = [NSNumber numberWithDouble:sqlite3_column_double(statement, 4)];
                    order.distanceCharge = [NSNumber numberWithDouble:sqlite3_column_double(statement, 5)];
                    order.totalTaskCharge = [NSNumber numberWithDouble:sqlite3_column_double(statement, 6)];
                    order.subtotal = [NSNumber numberWithDouble:sqlite3_column_double(statement, 7)];
                    order.tax = [NSNumber numberWithDouble:sqlite3_column_double(statement, 8)];
                    order.total = [NSNumber numberWithDouble:sqlite3_column_double(statement, 9)];
                    char *orderTip = (char *)sqlite3_column_text(statement, 10);
                    order.tip = (orderTip) ? [NSString stringWithUTF8String:orderTip] : @"";
                    
                    [orders addObject:order];
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return orders;
}

-(void)deleteOrders
{
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          char *errMsg;
          NSString *querySQL = [NSString stringWithFormat:@"DELETE FROM %@; DELETE FROM %@; DELETE FROM %@; ", orderTable, taskTable, confirmationTable];
          const char *sql_stmt = [querySQL UTF8String];
          
          if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
          {
               NSLog(@"%s: Failed to delete order", __FUNCTION__);
          }
          
          sqlite3_close(database);
     }
}

-(void)deleteOrderIfPossible
{
     NSMutableArray *confirmations = [[DatabaseHelper shared] getConfirmationsWith:ConfirmationStatusNotSent];
     if (confirmations.count > 0)
     {
          return;
     }
     
     [self deleteOrders];
}

#pragma mark - Task

-(void)insertTask:(Task *)task
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@) VALUES (\"%f\", \"%f\", \"%@\", \"%@\", \"%@\", \"%f\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%f\", \"%f\", \"%f\", \"%f\", \"%f\", \"%@\", \"%@\", \"%f\")", taskTable, taskFieldId, taskFieldOrderId, taskFieldPin, taskFieldType, taskFieldDescription, taskFieldDueTime, taskFieldContactName, taskFieldContactPhone, taskFieldContactEmail, taskFieldAddress, taskFieldSuite, taskFieldBuzzer, taskFieldPostalCode, taskFieldLatitude, taskFieldLongitude, taskFieldCharge, taskFieldService, taskFieldWeightValue, taskFieldWeightUnit, taskFieldPaymentType, taskFieldPaymentAmount, [task.taskId doubleValue], [task.orderId doubleValue], task.pin, task.type, task.taskDescription, [task.dueTime doubleValue], task.contact.name, task.contact.phone, task.contact.email, task.location.address.name, task.location.suite.name, task.location.buzzer.name, task.location.postalCode.name, [task.location.locationLatitude doubleValue], [task.location.locationLongitude doubleValue], [task.charge doubleValue], [task.serviceCharge doubleValue], [task.weightValue doubleValue], task.weightUnit, task.paymentType, [task.paymentAmount doubleValue]];
          const char *query_stmt = [querySQL UTF8String];
          sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
          if (sqlite3_step(statement) == SQLITE_DONE)
          {
               
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
}

-(NSMutableArray *)getTasksFromOrderId:(NSNumber *)orderId
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     NSMutableArray *tasks = [NSMutableArray array];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = \"%f\"", taskFieldId, taskFieldOrderId, taskFieldPin, taskFieldType, taskFieldDescription, taskFieldDueTime, taskFieldContactName, taskFieldContactPhone, taskFieldContactEmail, taskFieldAddress, taskFieldSuite, taskFieldBuzzer, taskFieldPostalCode, taskFieldLatitude, taskFieldLongitude, taskFieldCharge, taskFieldService, taskFieldWeightValue, taskFieldWeightUnit, taskFieldPaymentType, taskFieldPaymentAmount, taskTable, taskFieldOrderId, [orderId doubleValue]];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               while(sqlite3_step(statement) == SQLITE_ROW)
               {
                    Task *task = [[Task alloc] init];
                    task.taskId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 0)];
                    task.orderId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 1)];
                    
                    char *taskPin = (char *)sqlite3_column_text(statement, 2);
                    task.pin = (taskPin) ? [NSString stringWithUTF8String:taskPin] : @"";
                    char *taskType = (char *)sqlite3_column_text(statement, 3);
                    task.type = (taskType) ? [NSString stringWithUTF8String:taskType] : @"";
                    char *taskDescription = (char *)sqlite3_column_text(statement, 4);
                    task.taskDescription = (taskDescription) ? [NSString stringWithUTF8String:taskDescription] : @"";
                    task.dueTime = [NSNumber numberWithDouble:sqlite3_column_double(statement, 5)];
                    
                    Contact *contact = [[Contact alloc] init];
                    char *contactName = (char *)sqlite3_column_text(statement, 6);
                    contact.name = (contactName) ? [NSString stringWithUTF8String:contactName] : @"";
                    char *contactPhone = (char *)sqlite3_column_text(statement, 7);
                    contact.phone = (contactPhone) ? [NSString stringWithUTF8String:contactPhone] : @"";
                    char *contactEmail = (char *)sqlite3_column_text(statement, 8);
                    contact.email = (contactEmail) ? [NSString stringWithUTF8String:contactEmail] : @"";
                    task.contact = contact;
                    
                    Address *address = [[Address alloc] init];
                    char *locationAddress = (char *)sqlite3_column_text(statement, 9);
                    address.address = (locationAddress) ? [[AddressComponent alloc] init:[NSString stringWithUTF8String:locationAddress] type:@ADDRESS_ADDRESS] : nil;
                    char *locationSuite = (char *)sqlite3_column_text(statement, 10);
                    address.suite = (locationSuite) ? [[AddressComponent alloc] init:[NSString stringWithUTF8String:locationSuite] type:@ADDRESS_SUITE] : nil;
                    char *locationBuzzer = (char *)sqlite3_column_text(statement, 11);
                    address.buzzer = (locationBuzzer) ? [[AddressComponent alloc] init:[NSString stringWithUTF8String:locationBuzzer] type:@ADDRESS_BUZZER] : nil;
                    char *locationPostalCode = (char *)sqlite3_column_text(statement, 12);
                    address.postalCode = (locationPostalCode) ? [[AddressComponent alloc] init:[NSString stringWithUTF8String:locationPostalCode] type:@ADDRESS_POSTAL_CODE] : nil;
                    address.locationLatitude = [NSNumber numberWithDouble:sqlite3_column_double(statement, 13)];
                    address.locationLongitude = [NSNumber numberWithDouble:sqlite3_column_double(statement, 14)];
                    task.location = address;
                    
                    task.charge = [NSNumber numberWithDouble:sqlite3_column_double(statement, 15)];
                    task.serviceCharge = [NSNumber numberWithDouble:sqlite3_column_double(statement, 16)];
                    task.weightValue = [NSNumber numberWithDouble:sqlite3_column_double(statement, 17)];
                    char *weightUnit = (char *)sqlite3_column_text(statement, 18);
                    task.weightUnit = (weightUnit) ? [NSString stringWithUTF8String:weightUnit] : @"";
                    
                    char *paymentType = (char *)sqlite3_column_text(statement, 19);
                    task.paymentType = (paymentType) ? [NSString stringWithUTF8String:paymentType] : @"";
                    task.paymentAmount = [NSNumber numberWithDouble:sqlite3_column_double(statement, 20)];
                    
                    [tasks addObject:task];
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return tasks;
}

#pragma mark - Confirmation

-(void)insertConfirmation:(Confirmation *)confirmation
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@, %@) VALUES (\"%f\", \"%f\", \"%f\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%ld\")", confirmationTable, confirmationFieldId, confirmationFieldTaskId, confirmationFieldOrderId, confirmationFieldName, confirmationFieldTitle, confirmationFieldDescription, confirmationFieldInputType, confirmationFieldUrl, confirmationFieldMethod, confirmationFieldStatus, [confirmation.confirmationId doubleValue], [confirmation.taskId doubleValue], [confirmation.orderId doubleValue], confirmation.name, confirmation.title, confirmation.confirmationDescription, confirmation.inputType, confirmation.url, confirmation.method, [confirmation.isConfirmed boolValue] ? (long)ConfirmationStatusSent : (long)ConfirmationStatusNotConfirmed];
          const char *query_stmt = [querySQL UTF8String];
          sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
          if (sqlite3_step(statement) == SQLITE_DONE)
          {
               
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
}

-(NSMutableArray *)getConfirmationsFromTaskId:(NSNumber *)taskId
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     NSMutableArray *confirmations = [NSMutableArray array];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          
      
          
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = \"%f\"", confirmationFieldId, confirmationFieldTaskId, confirmationFieldOrderId, confirmationFieldName, confirmationFieldTitle, confirmationFieldDescription, confirmationFieldInputType, confirmationFieldUrl, confirmationFieldMethod, confirmationFieldData, confirmationFieldInputField,confirmationFieldStatus, confirmationTable, confirmationFieldTaskId, [taskId doubleValue]];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               while (sqlite3_step(statement) == SQLITE_ROW)
               {
                    Confirmation *confirmation = [[Confirmation alloc] init];
                    confirmation.confirmationId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 0)];
                    confirmation.taskId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 1)];
                    confirmation.orderId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 2)];
                    
                    char *name = (char *)sqlite3_column_text(statement, 3);
                    confirmation.name = (name) ? [NSString stringWithUTF8String:name] : @"";
                    char *title = (char *)sqlite3_column_text(statement, 4);
                    confirmation.title = (title) ? [NSString stringWithUTF8String:title] : @"";
                    char *description = (char *)sqlite3_column_text(statement, 5);
                    confirmation.confirmationDescription = (description) ? [NSString stringWithUTF8String:description] : @"";
                    char *inputType = (char *)sqlite3_column_text(statement, 6);
                    confirmation.inputType = (inputType) ? [NSString stringWithUTF8String:inputType] : @"";
                    char *url = (char *)sqlite3_column_text(statement, 7);
                    confirmation.url = (url) ? [NSString stringWithUTF8String:url] : @"";
                    char *method = (char *)sqlite3_column_text(statement, 8);
                    confirmation.method = (method) ? [NSString stringWithUTF8String:method] : @"";
                    
                    confirmation.savedImage = [[NSData alloc] initWithBytes:sqlite3_column_blob(statement, 9) length:sqlite3_column_bytes(statement, 9)];
                  
                    char *inputField = (char *)sqlite3_column_text(statement, 10);
                    confirmation.savedInputField = (inputField) ? [NSString stringWithUTF8String:inputField] : @"";
                    
                    confirmation.isConfirmed = [NSNumber numberWithDouble:sqlite3_column_double(statement, 11)];
                    
                    [confirmations addObject:confirmation];
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return confirmations;
}

//
//-(NSMutableArray *)getConfirmationsFromTaskId:(NSNumber *)taskId
//{
//     sqlite3_stmt *statement;
//     const char *dbpath = [databasePath UTF8String];
//     NSMutableArray *confirmations = [NSMutableArray array];
//
//     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
//     {
//          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = \"%f\"", confirmationFieldId, confirmationFieldTaskId, confirmationFieldOrderId, confirmationFieldName, confirmationFieldTitle, confirmationFieldDescription, confirmationFieldInputType, confirmationFieldUrl, confirmationFieldMethod, confirmationFieldData, confirmationFieldInputField, confirmationTable, confirmationFieldTaskId, [taskId doubleValue]];
//          const char *query_stmt = [querySQL UTF8String];
//          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
//          {
//               while (sqlite3_step(statement) == SQLITE_ROW)
//               {
//                    Confirmation *confirmation = [[Confirmation alloc] init];
//                    confirmation.confirmationId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 0)];
//                    confirmation.taskId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 1)];
//                    confirmation.orderId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 2)];
//
//                    char *name = (char *)sqlite3_column_text(statement, 3);
//                    confirmation.name = (name) ? [NSString stringWithUTF8String:name] : @"";
//                    char *title = (char *)sqlite3_column_text(statement, 4);
//                    confirmation.title = (title) ? [NSString stringWithUTF8String:title] : @"";
//                    char *description = (char *)sqlite3_column_text(statement, 5);
//                    confirmation.confirmationDescription = (description) ? [NSString stringWithUTF8String:description] : @"";
//                    char *inputType = (char *)sqlite3_column_text(statement, 6);
//                    confirmation.inputType = (inputType) ? [NSString stringWithUTF8String:inputType] : @"";
//                    char *url = (char *)sqlite3_column_text(statement, 7);
//                    confirmation.url = (url) ? [NSString stringWithUTF8String:url] : @"";
//                    char *method = (char *)sqlite3_column_text(statement, 8);
//                    confirmation.method = (method) ? [NSString stringWithUTF8String:method] : @"";
//
//                    confirmation.savedImage = [[NSData alloc] initWithBytes:sqlite3_column_blob(statement, 9) length:sqlite3_column_bytes(statement, 9)];
//                    char *inputField = (char *)sqlite3_column_text(statement, 10);
//                    confirmation.savedInputField = (inputField) ? [NSString stringWithUTF8String:inputField] : @"";
//
//                    [confirmations addObject:confirmation];
//               }
//          }
//
//          sqlite3_finalize(statement);
//          sqlite3_close(database);
//     }
//
//     return confirmations;
//}

-(NSMutableArray *)getConfirmationsWith:(ConfirmationStatus)status
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     NSMutableArray *confirmations = [NSMutableArray array];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@ WHERE %@ = \"%ld\"", confirmationFieldId, confirmationFieldTaskId, confirmationFieldOrderId, confirmationFieldName, confirmationFieldTitle, confirmationFieldDescription, confirmationFieldInputType, confirmationFieldUrl, confirmationFieldMethod, confirmationFieldData, confirmationFieldInputField, confirmationTable, confirmationFieldStatus, (long)status];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               while (sqlite3_step(statement) == SQLITE_ROW)
               {
                    Confirmation *confirmation = [[Confirmation alloc] init];
                    confirmation.confirmationId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 0)];
                    confirmation.taskId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 1)];
                    confirmation.orderId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 2)];
                    
                    char *name = (char *)sqlite3_column_text(statement, 3);
                    confirmation.name = (name) ? [NSString stringWithUTF8String:name] : @"";
                    char *title = (char *)sqlite3_column_text(statement, 4);
                    confirmation.title = (title) ? [NSString stringWithUTF8String:title] : @"";
                    char *description = (char *)sqlite3_column_text(statement, 5);
                    confirmation.confirmationDescription = (description) ? [NSString stringWithUTF8String:description] : @"";
                    char *inputType = (char *)sqlite3_column_text(statement, 6);
                    confirmation.inputType = (inputType) ? [NSString stringWithUTF8String:inputType] : @"";
                    char *url = (char *)sqlite3_column_text(statement, 7);
                    confirmation.url = (url) ? [NSString stringWithUTF8String:url] : @"";
                    char *method = (char *)sqlite3_column_text(statement, 8);
                    confirmation.method = (method) ? [NSString stringWithUTF8String:method] : @"";
                    
                    confirmation.savedImage = [[NSData alloc] initWithBytes:sqlite3_column_blob(statement, 9) length:sqlite3_column_bytes(statement, 9)];
                    char *inputField = (char *)sqlite3_column_text(statement, 10);
                    confirmation.savedInputField = (inputField) ? [NSString stringWithUTF8String:inputField] : @"";
                    
                    [confirmations addObject:confirmation];
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return confirmations;
}

-(void)updateOfflineConfirmation:(NSNumber *)confirmationId inputType:(NSString *)inputType
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ = \"%ld\", %@ = \"%@\" WHERE %@ = \"%f\"", confirmationTable, confirmationFieldStatus, (long)ConfirmationStatusNotSent, confirmationFieldInputField, inputType, confirmationFieldId, [confirmationId doubleValue]];
          const char *query_stmt = [querySQL UTF8String];
          sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
          if (sqlite3_step(statement) == SQLITE_DONE)
          {
               
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
}

-(void)updateConfirmation:(NSNumber *)confirmationId isConfirmed:(ConfirmationStatus)status
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ = \"%ld\" WHERE %@ = \"%f\"", confirmationTable, confirmationFieldStatus, (long)status, confirmationFieldId, [confirmationId doubleValue]];
          const char *query_stmt = [querySQL UTF8String];
          sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
          if (sqlite3_step(statement) == SQLITE_DONE)
          {
               
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
}

-(void)updateConfirmation:(NSNumber *)confirmationId image:(UIImage *)image
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ = ?, %@ = \"%ld\" WHERE %@ = \"%f\"", confirmationTable, confirmationFieldData, confirmationFieldStatus, (long)ConfirmationStatusNotSent, confirmationFieldId, [confirmationId doubleValue]];
          const char *query_stmt = [querySQL UTF8String];

          NSData *imageData = [NSData dataWithData: UIImagePNGRepresentation(image)];
          if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               sqlite3_bind_blob(statement, 1, [imageData bytes], (int)[imageData length], NULL);
               sqlite3_step(statement);
          }
          
          sqlite3_reset(statement);
          sqlite3_close(database);
     }
}

#pragma mark - Shift

-(void)insertShift:(Shift *)shift
{
     Shift *savedShift = [[DatabaseHelper shared] getShift:shift.shiftId];
     if (savedShift)
     {
          [self updateShift:shift];
          return;
     }
     
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@) VALUES (\"%f\", \"%@\", \"%@\", \"%@\",\"%@\", \"%@\", \"%f\", \"%@\", \"%f\", \"%f\", \"%f\")", shiftTable, shiftFieldId, shiftFieldNum, shiftFieldStartTime, shiftFieldEndTime, shiftFieldRealStartTime, shiftFieldRealEndTime, shiftFieldZoneId, shiftFieldZoneNum, shiftFieldZoneLatitude, shiftFieldZoneLongitude, shiftFieldZoneRadius, [shift.shiftId doubleValue], shift.num, [NSDateHelper stringFromDate:shift.startTime], [NSDateHelper stringFromDate:shift.endTime], [NSDateHelper stringFromDate:shift.realStartTime], [NSDateHelper stringFromDate:shift.realEndTime], [shift.zone.zoneId doubleValue], shift.zone.num, [shift.zone.centerLatitude doubleValue], [shift.zone.centerLongitude doubleValue], [shift.zone.radius doubleValue]];
          const char *query_stmt = [querySQL UTF8String];
          sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
          if (sqlite3_step(statement) == SQLITE_DONE)
          {
               
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
}

-(void)updateShift:(Shift *)shift
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ = \"%@\", %@ = \"%@\", %@ = \"%@\", %@ = \"%@\", %@ = \"%@\", %@ = \"%f\", %@ = \"%@\", %@ = \"%f\", %@ = \"%f\", %@ = \"%f\" WHERE %@ = \"%f\"", shiftTable, shiftFieldNum, shift.num, shiftFieldStartTime, [NSDateHelper stringFromDate:shift.startTime], shiftFieldEndTime, [NSDateHelper stringFromDate:shift.endTime], shiftFieldRealStartTime, [NSDateHelper stringFromDate:shift.realStartTime], shiftFieldRealEndTime, [NSDateHelper stringFromDate:shift.realEndTime], shiftFieldZoneId, [shift.zone.zoneId doubleValue], shiftFieldZoneNum, shift.zone.num, shiftFieldZoneLatitude, [shift.zone.centerLatitude doubleValue], shiftFieldZoneLongitude, [shift.zone.centerLongitude doubleValue], shiftFieldZoneRadius, [shift.zone.radius doubleValue], shiftFieldId, [shift.shiftId doubleValue]];
          
          const char *query_stmt = [querySQL UTF8String];
          sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
          if (sqlite3_step(statement) == SQLITE_DONE)
          {
               
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
}

-(Shift *)getShift:(NSNumber *)shiftId
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     Shift *shift = nil;
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSMutableString *querySQL = [NSMutableString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ FROM %@", shiftFieldId, shiftFieldNum, shiftFieldStartTime, shiftFieldEndTime, shiftFieldRealStartTime, shiftFieldRealEndTime, shiftFieldZoneId, shiftFieldZoneNum, shiftFieldZoneLatitude, shiftFieldZoneLongitude, shiftFieldZoneRadius, shiftTable];
          
          if ([shiftId longValue] > 0)
          {
               NSString *where = [NSString stringWithFormat:@" WHERE %@ = \"%f\"", shiftFieldId, [shiftId doubleValue]];
               [querySQL appendString:where];
          }
          
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               if (sqlite3_step(statement) == SQLITE_ROW)
               {
                    shift = [[Shift alloc] init];
                    shift.shiftId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 0)];
                    
                    char *shiftNum = (char *)sqlite3_column_text(statement, 1);
                    shift.num = (shiftNum) ? [NSString stringWithUTF8String:shiftNum] : @"";
                    char *startTime = (char *)sqlite3_column_text(statement, 2);
                    shift.startTime = (startTime) ? [NSDateHelper dateFromString:[NSString stringWithUTF8String:startTime]] : nil;
                    char *endTime = (char *)sqlite3_column_text(statement, 3);
                    shift.endTime = (endTime) ? [NSDateHelper dateFromString:[NSString stringWithUTF8String:endTime]] : nil;
                    char *realStartTime = (char *)sqlite3_column_text(statement, 4);
                    shift.realStartTime = (realStartTime) ? [NSDateHelper dateFromString:[NSString stringWithUTF8String:realStartTime]] : nil;
                    char *realEndTime = (char *)sqlite3_column_text(statement, 5);
                    shift.realEndTime = (realEndTime) ? [NSDateHelper dateFromString:[NSString stringWithUTF8String:realEndTime]] : nil;
                    
                    shift.zone = [[Zone alloc] init];
                    shift.zone.zoneId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 6)];
                    char *zoneNum = (char *)sqlite3_column_text(statement, 7);
                    shift.zone.num = (zoneNum) ? [NSString stringWithUTF8String:zoneNum] : @"";
                    shift.zone.centerLatitude = [NSNumber numberWithDouble:sqlite3_column_double(statement, 8)];
                    shift.zone.centerLongitude = [NSNumber numberWithDouble:sqlite3_column_double(statement, 9)];
                    shift.zone.radius = [NSNumber numberWithDouble:sqlite3_column_double(statement, 10)];
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return shift;
}

-(void)deleteOldShifts:(NSString *)startTime
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ < \"%@\"", shiftTable, shiftFieldStartTime, startTime];
          const char *query_stmt = [querySQL UTF8String];
          sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL);
          if (sqlite3_step(statement) == SQLITE_DONE)
          {
               
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
}

#pragma mark - Vehicle

-(void)deleteVehicles
{
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          char *errMsg;
          NSString *querySQL = [NSString stringWithFormat:@"DELETE FROM %@;", vehicleTable];
          const char *sql_stmt = [querySQL UTF8String];
          
          if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
          {
               NSLog(@"%s: Failed to delete vehicles", __FUNCTION__);
          }
          
          sqlite3_close(database);
     }
}

-(void)insertVehicles:(NSArray *)vehicles
{
     [self deleteVehicles];
     
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSMutableString *querySQL = [NSMutableString string];
          
          for (Vehicle *vehicle in vehicles)
          {
               NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@) VALUES (\"%@\", \"%@\"); ", vehicleTable, vehicleFieldId, vehicleFieldName, vehicle.vehicleId, vehicle.name];
               [querySQL appendString:sql];
          }
     
          const char *sql_stmt = [querySQL UTF8String];
          
          char *errMsg;
          if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
          {
               NSLog(@"%s: Failed to insert vehicle", __FUNCTION__);
          }
          
          sqlite3_close(database);
     }
}

-(NSArray *)getVehicles
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     NSMutableArray *vehicles = [NSMutableArray array];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@, %@ FROM %@", vehicleFieldId, vehicleFieldName, vehicleTable];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               while (sqlite3_step(statement) == SQLITE_ROW)
               {
                    Vehicle *vehicle = [[Vehicle alloc] init];
                    vehicle.vehicleId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 0)];

                    char *name = (char *)sqlite3_column_text(statement, 1);
                    vehicle.name = (name) ? [NSString stringWithUTF8String:name] : @"";

                    [vehicles addObject:vehicle];
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return vehicles;
}

#pragma mark - Zones

-(void)deleteZones
{
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          char *errMsg;
          NSString *querySQL = [NSString stringWithFormat:@"DELETE FROM %@;", zoneTable];
          const char *sql_stmt = [querySQL UTF8String];
          
          if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
          {
               NSLog(@"%s: Failed to delete zones", __FUNCTION__);
          }
          
          sqlite3_close(database);
     }
}

-(void)insertZones:(NSArray *)zones
{
     [self deleteZones];
     
     const char *dbpath = [databasePath UTF8String];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSMutableString *querySQL = [NSMutableString string];
          
          for (Zone *zone in zones)
          {
               NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@) VALUES (\"%f\", \"%@\", \"%@\", \"%f\", \"%f\", \"%f\"); ", zoneTable, zoneFieldId, zoneFieldNum, zoneFieldName, zoneFieldLatitude, zoneFieldLongitude, zoneFieldRadius, [zone.zoneId doubleValue], zone.num, zone.name, [zone.centerLatitude doubleValue], [zone.centerLongitude doubleValue], [zone.radius doubleValue]];
               [querySQL appendString:sql];
          }
          
          const char *sql_stmt = [querySQL UTF8String];
          
          char *errMsg;
          if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
          {
               NSLog(@"%s: Failed to insert zone", __FUNCTION__);
          }
          
          sqlite3_close(database);
     }
}

-(NSArray *)getZones
{
     sqlite3_stmt *statement;
     const char *dbpath = [databasePath UTF8String];
     NSMutableArray *zones = [NSMutableArray array];
     
     if (sqlite3_open(dbpath, &database) == SQLITE_OK)
     {
          NSString *querySQL = [NSString stringWithFormat:@"SELECT %@, %@, %@, %@, %@, %@ FROM %@", zoneFieldId, zoneFieldNum, zoneFieldName, zoneFieldLatitude, zoneFieldLongitude, zoneFieldRadius, zoneTable];
          const char *query_stmt = [querySQL UTF8String];
          if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
          {
               while (sqlite3_step(statement) == SQLITE_ROW)
               {
                    Zone *zone = [[Zone alloc] init];
                    zone.zoneId = [NSNumber numberWithDouble:sqlite3_column_double(statement, 0)];
                    char *zoneNum = (char *)sqlite3_column_text(statement, 1);
                    zone.num = (zoneNum) ? [NSString stringWithUTF8String:zoneNum] : @"";
                    char *zoneName = (char *)sqlite3_column_text(statement, 2);
                    zone.name = (zoneName) ? [NSString stringWithUTF8String:zoneName] : @"";
                    zone.centerLatitude = [NSNumber numberWithDouble:sqlite3_column_double(statement, 3)];
                    zone.centerLongitude = [NSNumber numberWithDouble:sqlite3_column_double(statement, 4)];
                    zone.radius = [NSNumber numberWithDouble:sqlite3_column_double(statement, 5)];

                    [zones addObject:zone];
               }
          }
          
          sqlite3_finalize(statement);
          sqlite3_close(database);
     }
     
     return zones;
}

@end
