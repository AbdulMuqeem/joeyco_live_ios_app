//
//  DatabaseHelper.h
//  Joey
//
//  Created by Katia Maeda on 2014-11-24.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <sqlite3.h>
#import <JoeyCoUtilities/JoeyCoUtilities.h>
#import "Joey.h"
#import "Address.h"
#import "AddressComponent.h"
#import "MetaData.h"
#import "Order.h"
#import "Task.h"
#import "Confirmation.h"
#import "Message.h"
#import "Shift.h"
#import "Info.h"

static const NSString *joeyTable = @"JOEY";
static const NSString *joeyFieldId = @"joey_id";
static const NSString *joeyFieldFirstName = @"first_name";
static const NSString *joeyFieldLastName = @"last_name";
static const NSString *joeyFieldEmail = @"email";
static const NSString *joeyFieldPhone = @"phone";
static const NSString *joeyFieldNotes = @"notes";
static const NSString *joeyFieldVehicle = @"vehicle";
static const NSString *joeyFieldVehicleId = @"vehicleId";
static const NSString *joeyFieldSettingMap = @"map_setting";
static const NSString *joeyFieldSettingRoute = @"map_route";
static const NSString *joeyFieldSettingLargeFont = @"large_font_size";
static const NSString *joeyFieldDuty = @"duty";
static const NSString *joeyFieldPublicKey = @"public_key";
static const NSString *joeyFieldPrivateKey = @"private_key";

static const NSString *settingsTable = @"SETTINGS";
static const NSString *settingsFieldId = @"id";
static const NSString *settingsFieldValue = @"value";

static const NSString *metaOptionTable = @"META_OPTION";
static const NSString *metaOptionFieldId = @"id";
static const NSString *metaOptionFieldValue = @"value";

static const NSString *metaStatusTable = @"META_STATUS";
static const NSString *metaStatusFieldId = @"id";
static const NSString *metaStatusFieldValue = @"value";

static const NSString *metaDataTable = @"META_DATA";
static const NSString *metaDataFieldPrivacy = @"privacy";
static const NSString *metaDataFieldTerms = @"terms";
static const NSString *metaDataFieldHome = @"home";
static const NSString *metaDataFieldSignUp = @"signup";

static const NSString *metaDataFieldDate = @"date";

static const NSString *confirmationTable = @"CONFIRMATION_TABLE";
static const NSString *confirmationFieldId = @"confirmation_id";
static const NSString *confirmationFieldTaskId = @"task_id";
static const NSString *confirmationFieldOrderId = @"order_id";
static const NSString *confirmationFieldName = @"name";
static const NSString *confirmationFieldTitle = @"title";
static const NSString *confirmationFieldDescription = @"description";
static const NSString *confirmationFieldInputType = @"input_type";
static const NSString *confirmationFieldUrl = @"url";
static const NSString *confirmationFieldMethod = @"method";
static const NSString *confirmationFieldStatus = @"status";
static const NSString *confirmationFieldData = @"input_data";
static const NSString *confirmationFieldInputField = @"input_field";


static const NSString *orderTable = @"ORDER_TABLE";
static const NSString *orderFieldId = @"order_id";
static const NSString *orderFieldNum = @"num";
static const NSString *orderFieldStatus = @"status";
static const NSString *orderFieldDistance = @"distance";
static const NSString *orderFieldDistanceAllowance = @"distance_allowance";
static const NSString *orderFieldDistanceCharge = @"distance_charge";
static const NSString *orderFieldTotalCharge = @"total_task_charge";
static const NSString *orderFieldSubtotal = @"subtotal";
static const NSString *orderFieldTax = @"tax";
static const NSString *orderFieldTotal = @"total_value";
static const NSString *orderFieldTip = @"tip";

static const NSString *taskTable = @"TASK";
static const NSString *taskFieldId = @"task_id";
static const NSString *taskFieldOrderId = @"order_id";
static const NSString *taskFieldPin = @"pin";
static const NSString *taskFieldType = @"type";
static const NSString *taskFieldDescription = @"description";
static const NSString *taskFieldDueTime = @"due_time";
static const NSString *taskFieldContactName = @"contact_name";
static const NSString *taskFieldContactPhone = @"contact_phone";
static const NSString *taskFieldContactEmail = @"contact_email";
static const NSString *taskFieldAddress = @"address";
static const NSString *taskFieldSuite = @"suite";
static const NSString *taskFieldBuzzer = @"buzzer";
static const NSString *taskFieldPostalCode = @"postal_code";
static const NSString *taskFieldLatitude = @"zone_latitude";
static const NSString *taskFieldLongitude = @"zone_longitude";
static const NSString *taskFieldCharge = @"payment_charge";
static const NSString *taskFieldService = @"payment_service_charge";
static const NSString *taskFieldWeightValue = @"weight_value";
static const NSString *taskFieldWeightUnit = @"weight_unit";
static const NSString *taskFieldPaymentType = @"payment_type";
static const NSString *taskFieldPaymentAmount = @"payment_amount";

static const NSString *shiftTable = @"SHIFT";
static const NSString *shiftFieldId = @"shift_id";
static const NSString *shiftFieldNum = @"num";
static const NSString *shiftFieldStartTime = @"start_time";
static const NSString *shiftFieldEndTime = @"end_time";
static const NSString *shiftFieldRealStartTime = @"real_start_time";
static const NSString *shiftFieldRealEndTime = @"real_end_time";
static const NSString *shiftFieldZoneId = @"zone_id";
static const NSString *shiftFieldZoneNum = @"zone_num";
static const NSString *shiftFieldZoneLatitude = @"zone_latitude";
static const NSString *shiftFieldZoneLongitude = @"zone_longitude";
static const NSString *shiftFieldZoneRadius = @"zone_radius";

static const NSString *vehicleTable = @"VEHICLE";
static const NSString *vehicleFieldId = @"id";
static const NSString *vehicleFieldName = @"name";

static const NSString *zoneTable = @"ZONE";
static const NSString *zoneFieldId = @"id";
static const NSString *zoneFieldNum = @"num";
static const NSString *zoneFieldName = @"name";
static const NSString *zoneFieldLatitude = @"latitude";
static const NSString *zoneFieldLongitude = @"longitude";
static const NSString *zoneFieldRadius = @"radius";

@interface DatabaseHelper : NSObject
{
    sqlite3 *database;
}

+(DatabaseHelper *)shared;

-(void)insertJoey:(Joey *)joey;
-(void)updateJoey:(Joey *)joey;
-(Joey *)getJoey;
-(void)deleteJoeyAndOrderData;
-(MapTypeSetting)getMapTypeSetting;
-(int)getMapRouteData;
-(BOOL)getDisplayLargeFontSize;
-(void)updateSetting:(NSString *)name value:(int)value;
-(NSString *)getPublicKey;
-(NSString *)getPrivateKey;

-(void)insertSetting:(NSString *)settingId value:(NSString *)settingValue;
-(NSString *)getSetting:(NSString *)settingId;
-(void)deleteSetting;

-(void)insertMetaData:(MetaData *)metaData;
-(MetaData *)getMetaData;
-(MetaData *)getFullMetaData;
-(MetaStatus *)getMetaStatusByKey:(NSString *)key;
-(MetaOption *)getMetaOptionByKey:(NSString *)key;

-(void)insertOrders:(NSMutableArray *)orders;
-(void)deleteOrderIfPossible;
-(NSMutableArray *)getOfflineOrders;
-(NSMutableArray *)getTasksFromOrderId:(NSNumber *)orderId;
-(NSMutableArray *)getConfirmationsWith:(ConfirmationStatus)status;
-(NSMutableArray *)getConfirmationsFromTaskId:(NSNumber *)taskId;
-(void)updateConfirmation:(NSNumber *)confirmationId isConfirmed:(ConfirmationStatus)status;
-(void)updateOfflineConfirmation:(NSNumber *)confirmationId inputType:(NSString *)inputType;
-(void)updateConfirmation:(NSNumber *)confirmationId image:(UIImage *)image;

-(void)insertShift:(Shift *)shift;
-(Shift *)getShift:(NSNumber *)shiftId;
-(void)deleteOldShifts:(NSString *)startTime;

-(NSArray *)getVehicles;
-(void)insertVehicles:(NSArray *)vehicles;

-(void)insertZones:(NSArray *)zones;
-(NSArray *)getZones;


//Get List of all orders
-(NSMutableArray *)getListOfAllOrders;

@end
