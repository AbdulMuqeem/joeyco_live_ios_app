//
//  UrlInfo.h
//  Joey
//
//  Created by Katia Maeda on 2014-10-22.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UrlInfo : NSObject

+(NSNumber *)getJoeyId;
+(void)setJoeyId:(NSInteger)newId;
+(NSString *)getDefaultUrl;
+(NSString *)apiAuthentication;
+(NSString *)apiAuthenticationForUrlChecking;
+(NSString *)getEnvironment;


+(NSString *)TestUrlForIpChecking;

+(NSString *)signInUrl;
+(NSString *)logoutUrl;

+(NSString *)metaDataUrl:(double)latitude longitude:(double)longitude;
+(NSString *)personalUrl;
+(NSString *)registerDeviceUrl;
+(NSString *)sendLocationUrl;

+(NSString *)orderListUrl:(NSString *)type;
+(NSString *)acceptOrderUrl:(int)orderId;
+(NSString *)messageUrl;
+(NSString *)reportFailUrl:(int)taskId;

+(NSString *)nextShiftUrl;
+(NSString *)startShiftUrl:(int)shiftId;
+(NSString *)endShiftUrl:(int)shiftId;
+(NSString *)schedules:(int)zoneId vehicle:(int)vehicleId start:(int)startDate end:(int)endDate;
+(NSString *)scheduleJoey:(int)shiftId;
+(NSString *)removeScheduleJoey:(int)shiftId;
+(NSString *)fullSchedule:(int)startDate endDate:(int)endDate;

+(NSString *)addDepositUrl;
+(NSString *)cashOnHandUrl;

+(NSString *)startDutyUrl;
+(NSString *)endDutyUrl;

+(NSString *)chatChannelsUrl;
+(NSString *)chatChannelMessagesUrl:(int)channelId startDate:(long)startDate endDate:(long)endDate;
+(NSString *)chatChannelNewMessageUrl:(int)channelId;

+(NSString *)summary:(NSInteger)startDate endDate:(NSInteger)endDate;

+(NSString *)signup;


+(NSString *)callTimeApiUrl;

+(NSString *)complainSectionApiUrl;

//Update System Status
+(NSString *)updateSystemStatus;

//itinerary Orders
+(NSString *)itineraryOrders;


//itinerary Orders -bulk scan
+(NSString *)itineraryOrdersBulkScan;

//Single Drop off
+(NSString *)singleDropOffScan;

//Digital Manifest api - Fetch data
+(NSString *)digitalManifestFetchData;

//Digital Manifest SIGN update
+(NSString *)digitalManifestSignUpdate;





#pragma mark - SCAN-AT-PICKUP

+(NSString *)storePickup;

+(NSString *)storePickupList;

+(NSString *)hubBulkDeliverFromStore;

#pragma mark - INFORMATION WINDOW

+(NSString *)informationWindow:(NSNumber *)sprintID;

+(NSString *)getQuestion:(NSNumber *)vendorID;


//Faq api - Fetch data
+(NSString *)faqFetchData;

//Faq Description api - Fetch data
+(NSString *)faqDescFetchData:(int)sprintId;

//+(NSString)callTimeApi:(NSString *)order_id joey_id:(NSString *)joey_id start_time:(NSString *)start_time end_time:(NSString *)end_time;

@end
