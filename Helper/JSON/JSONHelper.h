//
//  JSONHelper.h
//  Joey
//
//  Created by Katia Maeda on 2014-10-22.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "OrderStatus.h"


#define ACTION_Ip_Test "actionIpTest"

#define ACTION_SIGN_IN "actionSignIn"
#define ACTION_LOGOUT "actionLogout"
#define ACTION_GET_META_DATA "actionGetMetaData"
#define ACTION_PERSONAL_DATA "actionPersonalData"
#define ACTION_SAVE_PERSONAL_DATA "actionSavePersonalData"
#define ACTION_REGISTER_DEVICE "actionRegisterDevice"
#define ACTION_SEND_LOCATION "actionSendLocation"
#define ACTION_GET_ORDER_LIST "actionGetOrderList"
#define ACTION_ACCEPT_ORDER "actionAcceptOrder"
#define ACTION_CONFIRMATION "actionConfirmation"
#define ACTION_OFFLINE_CONFIRMATION "actionOfflineConfirmation"
#define ACTION_SEND_MESSAGE "actionSendMessage"
#define ACTION_REPORT_FAIL "actionReportFail"
#define ACTION_GET_SHIFTS "actionGetShifts"
#define ACTION_START_SHIFT "actionStartShift"
#define ACTION_END_SHIFT "actionEndShift"
#define ACTION_GET_SCHEDULES "actionGetSchedules"
#define ACTION_ADD_JOEY_SCHEDULE "actionAddJoeySchedule"
#define ACTION_REMOVE_JOEY_SCHEDULE "actionRemoveJoeySchedule"
#define ACTION_GET_FULL_SCHEDULE "actionGetFullSchedule"
#define ACTION_ADD_DEPOSIT "actionAddDeposit"
#define ACTION_CASH_ON_HAND "actionCashOnHand"
#define ACTION_START_DUTY "actionStartDuty"
#define ACTION_END_DUTY "actionEndDuty"
#define ACTION_CHAT_CHANNELS "actionChatChannels"
#define ACTION_CHAT_MESSAGES "actionChatMessages"
#define ACTION_CHAT_NEW_MESSAGE "actionChatNewMessage"
#define ACTION_SUMMARY "actionSummary"
#define ACTION_SIGN_UP "actionSignUp"
#define ACTION_CALL_TIME_API "actionCallTimeApi"
#define ACTION_SYSTEM_STATUS_API "actionSystemStatusApi"
#define ACTION_SYSTEM_STATUS_API_IMG_UPLOAD "actionSystemStatusImgUpload"

#define ACTION_ITINERARY_API "actionItinerary"


#define ACTION_COMPLAIN_SECTION_API "actionComplainSectionApi"
#define ACTION_ITINERARY_BULK_SCAN "actionItineraryBulkScanApi"
#define ACTION_ITINERARY_BULK_SCAN_success "actionItineraryBulkScanApi_success"
#define ACTION_ITINERARY_BULK_SCAN_badRequest "actionItineraryBulkScanApi_badRequest"

#define ACTION_SINGLE_DROP_OFF "actionSingleDropOff"



#define ACTION_digital_Manifest_fetch_API "actionDigitalManifest_fetch"

#define ACTION_digital_Manifest_sign_API "actionDigitalManifest_sign"

#define ACTION_get_question_API "actionGetQuestion_quiz"

#define ACTION_storePickup "actionStorePickup"
#define ACTION_storePickupList "actionStorePickupList"
#define ACTION_hubDeliverByJoey "actionHubDeliverByJoey"

#define ACTION_informationWindow "actionInformationWindow"
#define ACTION_faq_fetch_API "actionFaq_fetch"
#define ACTION_faqDesc_fetch_API "actionFaqDesc_fetch"


/*MM SUPERMARKET API */
#define ACTION_GET_SPRINTS "MM_getSprints"
#define ACTION_UPDATE_Status "MM_updateStatus"



@protocol JSONHelperDelegate <NSObject>
@required
-(void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list;
@optional
-(void)errorMessage:(NSString *)action message:(NSString *)message;
-(void)errorMessage:(NSString *)action message:(NSString *)message fields:(NSArray *)fieldsArray;
-(void)uploadData:(int64_t)totalSent totalToSend:(int64_t)totalToSend;
@end

@interface JSONHelper : NSObject

@property (nonatomic, weak) id<JSONHelperDelegate> delegate;

-(BOOL)hasInternetConnection:(BOOL)showAlert;

-(id)init:(UIViewController *)viewController andDelegate:(id<JSONHelperDelegate>)delegate;


//Used for finding out the ip address for nightly server
-(void)CheckIp;

-(void)signIn:(NSString *)joeyId password:(NSString *)password;
-(void)logout:(NSString *)registrationId;
-(void)metaData:(double)latitude longitude:(double)longitude;
-(void)personal;
-(void)savePersonal:(NSMutableDictionary *)jsonDictionary;
-(void)registerDevice:(NSString *)registrationId;
-(void)sendLocation:(double)latitude longitude:(double)longitude;
-(NSString *)ExtractAddressAndSendLocation:(double)latitude longitude:(double)longitude;

-(void)orderList:(NSString *)type;
-(void)acceptOrder:(int)orderId pickupInfo:(NSMutableDictionary *)jsonDictionary;
-(void)confirmation:(NSString *)url method:(NSString *)method confirmationInfo:(NSMutableDictionary *)jsonDictionary action:(NSString *)action;
-(void)messageUrl;
-(void)reportFail:(int)joeyId taskId:(int)taskId note:(NSString *)message;

-(void)nextShift;
-(void)startShift:(int)shiftId shiftInfo:(NSMutableDictionary *)jsonDictionary;
-(void)endShift:(int)shiftId shiftInfo:(NSMutableDictionary *)jsonDictionary;
-(void)getSchedules:(NSNumber *)zoneId vehicle:(NSNumber *)vehicleId start:(int)startDate end:(int)endDate;
-(void)addJoeySchedule:(NSNumber *)shiftId;
-(void)removeJoeySchedule:(NSNumber *)shiftId;
-(void)getFullSchedule:(int)startDate endDate:(int)endDate;

-(void)addDeposit:(NSMutableDictionary *)jsonDictionary;
-(void)cashOnHand;

-(void)startDuty:(NSMutableDictionary *)jsonDictionary;
-(void)endDuty;

-(void)chatChannels;
-(void)chatChannelMessages:(int)channelId startDate:(long)startDate endDate:(long)endDate;
-(void)chatNewMessage:(NSMutableDictionary *)jsonDictionary channelId:(int)channelId;

-(void)summary:(NSInteger)startDate endDate:(NSInteger)endDate;

-(void)signup:(NSMutableDictionary *)jsonDictionary;


//Call Time API
-(void)
callTimeApi:(NSString *)order_id
joey_id:(NSString *)joey_id
start_time:(NSString *)start_time
end_time:(NSString *)end_time;


//Complain Section Api
-(void)
complainSectionApi:(NSString *)order_id
joey_id:(NSString *)joey_id
issue_type:(NSString *)issue_type
desc_issue:(NSString *)desc_issue;


//Itinerary API
-(void)itineraryApi;


//Parse Order Status
-(OrderStatus *)ParseOrderStatusObject:(NSDictionary *)OrderData;


//MM SUPERMARKET API "S
-(void)updateMMAboutStatusUpdate_API:(NSString *)url;

//upate RETURN | DELAY status
-(void)updateSystemStatus:(NSString *)order_id statusCode:(NSString *)statusCode TaskId:(NSString *)TaskIdCode type:(NSString *)taskType;

//upload Image
-(void)updateSystemStatusImageUpload:(NSMutableDictionary *)jsonDictionary;


-(void)checkIfSprintExists_API:(NSString *)url;

-(void)parseSprints:(NSMutableDictionary *)json;

-(void)ItineraryBulkScanApi:(NSMutableArray *)tracking_ids;

-(void)SingleDropOff:(NSString *)tracking_id;

-(void)digitalManifestFetchApi;

-(void)getQuestionFetchApi;
-(void)getQuestionFetchApi:(NSNumber *)vendorID;


-(void)digitalManifestConfirmBySignatureApi:(NSData *)data;


-(void)storePickup:(NSString *)trackingID;

-(void)getStorePickupList;

-(void)hubDeliverByJoey;

-(void)faqFetchApi;
-(void)faqDescFetchApi:(int)sprintId;

-(void)informationWindow:(NSNumber *)sprintID;



@end


