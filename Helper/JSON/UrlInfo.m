//
//  UrlInfo.m
//  Joey
//
//  Created by Katia Maeda on 2014-10-22.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#define nightly @"https://api.nightly.joeyco.com"
#define staging @"https://api.staging.joeyco.com"
#define production @"https://api.joeyco.com"

#define defaultUrl production //Production URL
//#define defaultUrl staging //Staging URL

#define apiId @"api"
#define password @"api1243"

#define apiIdForUrlChecking @"jcopak"
#define passwordForUrlChecking @"6cf0b46ff3beb4c390218bcec67251c8"




#import "UrlInfo.h"

@implementation UrlInfo

static NSInteger joeyId = -1;

+(NSNumber *)getJoeyId
{
    return [NSNumber numberWithLong:joeyId];
}

+(void)setJoeyId:(NSInteger)newId
{
    joeyId = newId;
//    joeyId = 27558; //10080; //Static Joey ID
}

+(NSString *)getDefaultUrl
{
    return defaultUrl;
}

+(NSString *)getEnvironment
{
    if ([defaultUrl isEqualToString:nightly])
    {
        return @"n";
    }
    else if ([defaultUrl isEqualToString:staging])
    {
        return @"s";
    }
    
    return @"";
}

+(NSString *)apiAuthentication
{
    if ([defaultUrl isEqualToString:production])
    {
        return @"";
    }
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", apiId, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    return [NSString stringWithFormat: @"Basic %@", [authData base64EncodedStringWithOptions:0]];
}



+(NSString *)apiAuthenticationForUrlChecking
{
    if ([defaultUrl isEqualToString:production])
    {
        return @"";
    }
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", apiIdForUrlChecking, passwordForUrlChecking];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    return [NSString stringWithFormat: @"Basic_IpChecking %@", [authData base64EncodedStringWithOptions:0]];
}


+(NSString *)TestUrlForIpChecking
{
    return [NSString stringWithFormat:@"%@/check-ip", defaultUrl];
}


+(NSString *)signInUrl
{
    return [NSString stringWithFormat:@"%@/joeys/login", defaultUrl];
}

+(NSString *)logoutUrl
{
    return [NSString stringWithFormat:@"/joeys/%ld/logout", (long)joeyId];
}

+(NSString *)metaDataUrl:(double)latitude longitude:(double)longitude
{
    return [NSString stringWithFormat:@"%@/config/meta?platform=joey&latitude=%f&longitude=%f", defaultUrl, latitude, longitude];
}

+(NSString *)personalUrl
{
    return [NSString stringWithFormat:@"/joeys/%ld/personal", (long)joeyId];
}

+(NSString *)registerDeviceUrl
{
    return [NSString stringWithFormat:@"/device/register"];
}

+(NSString *)sendLocationUrl
{
    return [NSString stringWithFormat:@"/joeys/%ld/locations", (long)joeyId];
}

+(NSString *)orderListUrl:(NSString *)type
{
    return [NSString stringWithFormat:@"/joeys/%ld/orders?type=%@", (long)joeyId, type];
}

+(NSString *)acceptOrderUrl:(int)orderId
{
    return [NSString stringWithFormat:@"/sprints/%d/joey", orderId];
}

+(NSString *)messageUrl
{
    return [NSString stringWithFormat:@"/gcm/message/joeys/%ld", (long)joeyId];
}

+(NSString *)reportFailUrl:(int)taskId
{
    return [NSString stringWithFormat:@"/tasks/%d/fail", taskId];
}

+(NSString *)nextShiftUrl
{
    return [NSString stringWithFormat:@"/joeys/%ld/next-shift", (long)joeyId];
}

+(NSString *)startShiftUrl:(int)shiftId
{
    return [NSString stringWithFormat:@"/joeys/%ld/shift/%d/start", (long)joeyId, shiftId];
}

+(NSString *)endShiftUrl:(int)shiftId
{
    return [NSString stringWithFormat:@"/joeys/%ld/shift/%d/end", (long)joeyId, shiftId];
}

+(NSString *)schedules:(int)zoneId vehicle:(int)vehicleId start:(int)startDate end:(int)endDate
{
    return [NSString stringWithFormat:@"/schedules?joey_id=%ld&zone_id=%d&vehicle_id=%d&start=%d&end=%d", (long)joeyId, zoneId, vehicleId, startDate, endDate];
}

+(NSString *)scheduleJoey:(int)shiftId
{
    return [NSString stringWithFormat:@"/schedules/%d/joeys", shiftId];
}

+(NSString *)removeScheduleJoey:(int)shiftId
{
    return [NSString stringWithFormat:@"/schedules/%d/joeys/%ld", shiftId, (long)joeyId];
}

+(NSString *)fullSchedule:(int)startDate endDate:(int)endDate
{
    return [NSString stringWithFormat:@"/joeys/%ld/schedules?start_date=%d&end_date=%d", (long)joeyId, startDate, endDate];
}

+(NSString *)addDepositUrl
{
    return [NSString stringWithFormat:@"/joeys/%ld/cash/deposit", (long)joeyId];
}

+(NSString *)cashOnHandUrl
{
    return [NSString stringWithFormat:@"/joeys/%ld/cash-on-hand", (long)joeyId];
}

+(NSString *)startDutyUrl
{
    return [NSString stringWithFormat:@"/joeys/%ld/duty/start", (long)joeyId];
}

+(NSString *)endDutyUrl
{
    return [NSString stringWithFormat:@"/joeys/%ld/duty/end", (long)joeyId];
}

+(NSString *)chatChannelsUrl
{
    return [NSString stringWithFormat:@"/chat/channels?user_type=joey&user_id=%ld", (long)joeyId];
}

+(NSString *)chatChannelMessagesUrl:(int)channelId startDate:(long)startDate endDate:(long)endDate
{
    NSMutableString *url = [NSMutableString stringWithString:[NSString stringWithFormat:@"/chat/channels/%d/messages?user_type=joey&user_id=%ld&end_date=%ld", channelId, (long)joeyId, endDate]];
    if (startDate > 0)
    {
        [url appendString:[NSString stringWithFormat:@"&start_date=%ld", startDate]];
    }
    return url;
}

+(NSString *)chatChannelNewMessageUrl:(int)channelId
{
    return [NSString stringWithFormat:@"/chat/channels/%d/messages", channelId];
}

+(NSString *)summary:(NSInteger)startDate endDate:(NSInteger)endDate
{
    return [NSString stringWithFormat:@"/joeys/%ld/transactions?start=%ld&end=%ld", (long)joeyId, (long)startDate, (long)endDate];
}

+(NSString *)signup
{
    return [NSString stringWithFormat:@"%@/joeys", defaultUrl];
}



+(NSString *)callTimeApiUrl{
    
    return [NSString stringWithFormat:@"%@/order/calltime", defaultUrl];
}



+(NSString *)complainSectionApiUrl{
    
    return [NSString stringWithFormat:@"%@/joeys/complaints/new", defaultUrl];
}


+(NSString *)updateSystemStatus{
    
    return [NSString stringWithFormat:@"%@/update/status", defaultUrl];
}




+(NSString *)itineraryOrders{
    
return [NSString stringWithFormat:@"%@/joey/%ld/routes", defaultUrl,(long)joeyId];
//    return [NSString stringWithFormat:@"%@/joey/15288/routes", defaultUrl];
}



+(NSString *)digitalManifestFetchData{
    
    return [NSString stringWithFormat:@"%@/canadiantire/digital/manifest", defaultUrl];
}



+(NSString *)digitalManifestSignUpdate{
    
    
    return [NSString stringWithFormat:@"%@/vendor/pickup/signature", defaultUrl];
}




+(NSString *)itineraryOrdersBulkScan{
    
    return [NSString stringWithFormat:@"%@/route/pickup", defaultUrl];
}


+(NSString *)singleDropOffScan{
    
    return [NSString stringWithFormat:@"%@/order/confirm", defaultUrl];
}



#pragma mark - SCAN-AT-PICKUP

+(NSString *)storePickup{
    
    return [NSString stringWithFormat:@"%@/store/pickup", defaultUrl];
}


+(NSString *)storePickupList{
    
    return [NSString stringWithFormat:@"%@/joey/%ld/store/picks", defaultUrl,(long)joeyId];
}


+(NSString *)hubBulkDeliverFromStore{
    
    return [NSString stringWithFormat:@"%@/hub/deliver", defaultUrl];
}

#pragma mark - INFORMATION WINDOW

+(NSString *)informationWindow:(NSNumber *)sprintID
{
    
    
    //return [NSString stringWithFormat:@"%@/order/580902/customer/info", defaultUrl];

    return [NSString stringWithFormat:@"%@/order/%@/customer/info", defaultUrl,sprintID];
}


+(NSString *)getQuestion:(NSNumber *)vendorID
{
//return [NSString stringWithFormat:@"%@/order/580902/customer/info", defaultUrl];
    return [NSString stringWithFormat:@"%@/category/quiz/%@", defaultUrl,vendorID];
}

+(NSString *)faqFetchData{
    
    return [NSString stringWithFormat:@"%@/faq/vendors", defaultUrl];
}

+(NSString *)faqDescFetchData:(int)sprintId{
    return [NSString stringWithFormat:@"%@/faqs/%d", defaultUrl,(int)sprintId];
}


@end
