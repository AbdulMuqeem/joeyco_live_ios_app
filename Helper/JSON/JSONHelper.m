//  JSONHelper.m
//  Joey
//
//  Created by Katia Maeda on 2014-10-22.
//  Copyright (c) 2014 JoeyCo. All rights reserved.
//

#import "JSONHelper.h"
#import <JoeyCoUtilities/JoeyCoUtilities.h>
#import "UrlInfo.h"
#import "Joey.h"
#import "Address.h"
#import "AddressComponent.h"
#import "MetaData.h"
#import "Order.h"
#import "Task.h"
#import "Confirmation.h"
#import "Message.h"
#import "Shift.h"
#import "Info.h"
#import "DatabaseHelper.h"
#import "Vehicle.h"
#import "NavigationContainerViewController.h"
#import "Channel.h"
#import "Message.h"
#import "SummaryRecord.h"
#import "Summary.h"
#import "ItineraryOrder.h"
#import "ItineraryOrder_Listing_ObjectDetail.h"

@interface JSONHelper () <NSURLSessionTaskDelegate>
{
    AlertHelper *alertHelper;
    NSString *errorMsg;
}

@property (nonatomic, weak) UIViewController *viewController;

@end

@implementation JSONHelper

-(id)init:(UIViewController *)viewController andDelegate:(id<JSONHelperDelegate>)delegate
{
    self = [super init];
    
    if(self)
    {
        self.viewController = viewController;
        self.delegate = delegate;
        alertHelper = [[AlertHelper alloc] init];
    }
    
    return self;
}



-(void)dealloc
{
    [alertHelper dismissAlert];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

-(BOOL)hasInternetConnection
{
    return [self hasInternetConnection:YES];
}

-(BOOL)hasInternetConnection:(BOOL)showAlert
{
    Reachability *reachTest = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachTest  currentReachabilityStatus];
    
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
        // No internet
        if (showAlert && self.viewController)
        {
            NSLog(@"viewcontroller%@ ", self.viewController);
            [alertHelper showAlertWithOneButton:NSLocalizedString(@"Connection Error", nil) message:NSLocalizedString(@"The Internet connection appears to be offline.", nil) from:self.viewController buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        }
        
        return NO;
    }
    
    return YES;
}

-(void)showLoadingView
{
    if (self.viewController)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [alertHelper showLoadingView:self.viewController];
        });
    }
}

-(void)dismissLoadingView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [alertHelper dismissAlert];
    });
}

#pragma mark - NSURLSessionTaskDelegate

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(uploadData:totalToSend:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate uploadData:totalBytesSent totalToSend:totalBytesExpectedToSend];
        });
    }
}

-(void)sendData:(NSString *)urlString method:(NSString *)method action:(NSString *)action jsonData:(NSData *)jsonData withCompletionHandler:(void (^)(NSMutableDictionary *json))completionHandler
{
    NSString *privateKey = [[DatabaseHelper shared] getPrivateKey];
    if (!privateKey || [privateKey isEqualToString:@""])
    {
        [self dismissLoadingView];
        return;
    }
    
    NSURL *inputUrl = [NSURL URLWithString:urlString];
    if ([inputUrl host] && ![[inputUrl host] isEqualToString:@""])
    {
        urlString = [inputUrl relativePath];
    }
    
    NSString *publicKey = [[DatabaseHelper shared] getPublicKey];
    
    if ([urlString rangeOfString:@"?"].location == NSNotFound) {
        urlString = [NSString stringWithFormat:@"%@?key=%@", urlString, publicKey];
    } else {
        urlString = [NSString stringWithFormat:@"%@&key=%@", urlString, publicKey];
    }
    
    double timestamp = [[NSDate date] timeIntervalSince1970];
    NSString *signature = [self getSignatureString:method url:urlString timestamp:timestamp];
    NSString *signatureHash = [CryptographyHelper getHashWithKey:privateKey data:signature];
    NSString *hostname = [UrlInfo getDefaultUrl];
    
    NSString *urlFull = [NSString stringWithFormat:@"%@%@&signature=%@", hostname, urlString, signatureHash];
    NSLog(@"urlFull:%@", urlFull);
    NSURL *url = [NSURL URLWithString:urlFull];
    
    [self sendData:url method:method action:action timestamp:timestamp jsonData:jsonData withCompletionHandler:completionHandler];
}

-(void)sendData:(NSURL *)url method:(NSString *)method action:(NSString *)action timestamp:(double)timestamp jsonData:(NSData *)jsonData withCompletionHandler:(void (^)(NSMutableDictionary *json))completionHandler
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:method];
    [request setValue:[UrlInfo apiAuthentication] forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"iOS" forHTTPHeaderField:@"device-type"];

    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    [request setValue:language forHTTPHeaderField:@"Accept-Language"];
    [request setValue:[NSString stringWithFormat:@"%.0f", timestamp] forHTTPHeaderField:@"Jco-Timestamp"];
    
    //    NSLog(@"SIGNUP_details_Authorization : %@",[UrlInfo apiAuthentication]);
    //    NSLog(@"SIGNUP_Jco-Timestamp : %@",[NSString stringWithFormat:@"%.0f", timestamp]);
    
    if (jsonData)
    {
        [request setHTTPBody:jsonData];
    }
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    // Create a data task object to perform the data downloading.
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error)
        {
            NSLog(@"%@", [error localizedDescription]);
            [self dismissLoadingView];
            
            // If any error occurs then just display its description on the console.
            if (self.delegate && [self.delegate respondsToSelector:@selector(errorMessage:message:)])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.delegate errorMessage:action message:[error localizedDescription]];
                });
            }
        }
        else
        {
            NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
            
            if (HTTPStatusCode >= 400)
            {
                NSLog(@"HTTP status code = %ld", (long)HTTPStatusCode);
                NSLog(@"jsonData:%@", [[NSString alloc] initWithData:data encoding:4]);
                
                [self dismissLoadingView];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(errorMessage:message:)])
                {
                    NSError *error;
                    NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    
                    if (error)
                    {
                        NSLog(@"%@", [error localizedDescription]);
                    }
                    
                    NSDictionary *responseData = [json dictionaryForKey:@"response"];
                    if (responseData)
                    {
                        NSString *errorResponse = [responseData objForKey:@"error"];
                        if (errorResponse && ![errorResponse isEqualToString:@""])
                        {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.delegate errorMessage:action message:errorResponse];
                            });
                        }
                        else
                        {
                            NSString *msg = [responseData objForKey:@"message"];
                            if (msg && ![msg isEqualToString:@""])
                            {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.delegate errorMessage:action message:msg];
                                });
                            }
                            else if ([self.delegate respondsToSelector:@selector(errorMessage:message:fields:)])
                            {
                                errorMsg = @"";
                                NSArray *dictArray = [self parseErrorDictionary:responseData];
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.delegate errorMessage:action message:errorMsg fields:dictArray];
                                });
                            }
                        }
                    }
                    else
                    {
                        NSString *message = [json objForKey:@"response"];
                        if (message)
                        {
                            //                            if (HTTPStatusCode == 403)
                            //                            {
                            //                                dispatch_async(dispatch_get_main_queue(), ^{
                            //                                    if ([self.viewController isKindOfClass:[NavigationContainerViewController class]])
                            //                                    {
                            //                                        [alertHelper showAlertWithOneButton:@"Error" message:message from:self.viewController buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:^{
                            //                                            NavigationContainerViewController *controller = (NavigationContainerViewController *)self.viewController;
                            //                                            [controller logout];
                            //                                        }];
                            //
                            //                                    }
                            //                                });
                            //                                return;
                            //                            }
                            //                            else
                            //                            {
                            if ([message isKindOfClass:[NSString class]])
                            {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.delegate errorMessage:action message:message];
                                });
                            }
                            else if ([message isKindOfClass:[NSArray class]])
                            {
                                NSArray *array = (NSArray *)message;
                                NSMutableString *msg = [NSMutableString string];
                                for (NSObject *obj in array)
                                {
                                    [msg appendString:[obj description]];
                                }
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.delegate errorMessage:action message:msg];
                                });
                            }
                            //                            }
                        }
                        //                        else
                        //                        {
                        //                            dispatch_async(dispatch_get_main_queue(), ^{
                        //                                [self.delegate errorMessage:action message:@"Error"];
                        //                            });
                        //                        }
                    }
                }
                return;
            }
            
            if (!data)
            {
                NSLog(@"Data null with code=%ld", (long)HTTPStatusCode);
                [self dismissLoadingView];
                return;
            }
            
            NSError *error;
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
//            NSLog(@"Meta Data Dictionary=%@", json);
            
            //            NSLog(@"data:%@", [[NSString alloc] initWithData:data encoding:4]);
            
            if (error)
            {
                NSLog(@"data:%@", [[NSString alloc] initWithData:data encoding:4]);
                [self dismissLoadingView];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.delegate errorMessage:action message:[error localizedDescription]];
                });
            }
            else
            {
                [self dismissLoadingView];
                completionHandler(json);
            }
        }
    }];
    
    [task resume];
}



//
//-(void)sendData_IpCheck:(NSURL *)url method:(NSString *)method action:(NSString *)action timestamp:(double)timestamp jsonData:(NSData *)jsonData withCompletionHandler:(void (^)(NSMutableDictionary *json))completionHandler
//{
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:method];
//    [request setValue:[UrlInfo apiAuthenticationForUrlChecking] forHTTPHeaderField:@"Authorization"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
//    [request setValue:language forHTTPHeaderField:@"Accept-Language"];
//    [request setValue:[NSString stringWithFormat:@"%.0f", timestamp] forHTTPHeaderField:@"Jco-Timestamp"];
//
//    NSLog(@"SIGNUP_details_Authorization : %@",[UrlInfo apiAuthentication]);
//    NSLog(@"SIGNUP_Jco-Timestamp : %@",[NSString stringWithFormat:@"%.0f", timestamp]);
//
//    if (jsonData)
//    {
//        [request setHTTPBody:jsonData];
//    }
//
//    NSLog(@"AUth_ipChecking : %@",[UrlInfo apiAuthenticationForUrlChecking]);
//
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
//
//    // Create a data task object to perform the data downloading.
//    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//        NSLog(@"IP_CHECKING Request reply: %@", requestReply);
//    }] resume];
//
//}
-(NSString *)getSignatureString:(NSString *)method url:(NSString *)url timestamp:(double)timestamp
{
    return [NSString stringWithFormat:@"%@\napplication/json\n%.0f\n%@", method, timestamp, url];
}

#pragma mark - signIn

-(void)signIn:(NSString *)joeyId password:(NSString *)password
{
    if (!joeyId || !password) {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Sign In Error" , nil) message:NSLocalizedString(@"Problem with required parameters", nil) from:self.viewController buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    if (![self hasInternetConnection]) return;
    
    [self showLoadingView];
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"username":joeyId, @"password":password}];
    
    NSString *url = [UrlInfo signInUrl];
    NSLog(@"SignIn_URL : %@", url); //print url
    NSLog(@"SignIn_Dictionary : %@", jsonDictionary); //print dictionary
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    
    float timestamp = [[NSDate date] timeIntervalSince1970];
    [self sendData:[NSURL URLWithString:url] method:@"POST" action:@ACTION_SIGN_IN timestamp:timestamp jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseSignInData:) withObject:json];
    }];
}

-(void)parseSignInData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    NSLog(@"json sign in=%@", json);
    
    Joey *joey = [self parseJoey:responseData];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_SIGN_IN objects:[NSMutableArray arrayWithArray:@[joey]]];
        });
    }
}

#pragma mark - logout

-(void)logout:(NSString *)registrationId
{
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    [self showLoadingView];
    
    if (!registrationId)
    {
        registrationId = @"";
    }
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"registration_id":registrationId}];
    
    NSString *url = [UrlInfo logoutUrl];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self sendData:url method:@"POST" action:@ACTION_LOGOUT jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseLogoutData:) withObject:json];
    }];
}

-(void)parseLogoutData:(NSMutableDictionary *)json
{
    // delete user data from database
    [[DatabaseHelper shared] deleteJoeyAndOrderData];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_LOGOUT objects:nil];
        });
    }
}



#pragma mark - Ip Check

-(void)CheckIp
{
    if (![self hasInternetConnection]) return;
    
    NSURL *url = [NSURL URLWithString:@"https://api.nightly.joeyco.com/tasks"];
    
    NSLog(@"NIGHTLY_IP_TESTING : %@",url);

//    float timestamp = [[NSDate date] timeIntervalSince1970];
//
//    //username and password value
//    NSString *username = @"jcopak";
//    NSString *password = @"6cf0b46ff3beb4c390218bcec67251c8";
//
//    //HTTP Basic Authentication
//    NSString *authenticationString = [NSString stringWithFormat:@"%@:%@", username, password];
//    NSData *authenticationData = [authenticationString dataUsingEncoding:NSASCIIStringEncoding];
//    NSString *authenticationValue = [authenticationData base64Encoding];
//
//    //Set up your request
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://admin.nightly.joeyco.com/check-ip"]];
//
//    // Set your user login credentials
//    [request setValue:[NSString stringWithFormat:@"Basic %@", authenticationValue] forHTTPHeaderField:@"Authorization"];
//
//    // Send your request asynchronously
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *responseCode, NSData *responseData, NSError *responseError) {
//        NSString *charlieSendString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
//
//        NSLog(@"IP_CHECKINNN : %@",charlieSendString);
//
//
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:charlieSendString delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:nil, nil];
//        [alertView show];
//
//        if ([responseData length] > 0 && responseError == nil){
//            //logic here
//        }else if ([responseData length] == 0 && responseError == nil){
//            NSLog(@"data error: %@", responseError);
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Error accessing the data" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
//            [alert show];
//        }else if (responseError != nil && responseError.code == NSURLErrorTimedOut){
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"connection timeout" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
//            [alert show];
//        }else if (responseError != nil){
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"data download error" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
//            [alert show];
//        }
//
//
        
        
 //   }];
  

////    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://admin.nightly.joeyco.com/check-ip"]];
////    [request setHTTPMethod:@"HEAD"];
////    [NSURLConnection sendAsynchronousRequest:request
////                                       queue:[NSOperationQueue mainQueue]
////                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
////                               NSDictionary *headers = [(NSHTTPURLResponse *)response allHeaderFields];
////
////                               NSLog(@"Headers of Http : %@",headers);
////
////                           }];
////
//
//
//    url = [NSURL URLWithString:@"https://admin.nightly.joeyco.com/check-ip"];
//    NSURLRequest *request = [NSURLRequest requestWithURL: url];
//    NSHTTPURLResponse *response;
//    [NSURLConnection sendSynchronousRequest: request returningResponse: &response error: nil];
//    if ([response respondsToSelector:@selector(allHeaderFields)]) {
//        NSDictionary *dictionary = [response allHeaderFields];
//         NSLog(@"Headers of Http : %@",dictionary);
//    }
//
//
    
        double timestamp = [[NSDate date] timeIntervalSince1970];
        [self sendData:url method:@"POST" action:@ACTION_Ip_Test timestamp:timestamp jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        NSLog(@"NIGHTLY_IP_TESTING_JSON : %@",json);
    }];

                                                                              
  }

#pragma mark - MetaData

-(void)metaData:(double)latitude longitude:(double)longitude
{
    if (![self hasInternetConnection]) return;
    
    NSString *url = [UrlInfo metaDataUrl:latitude longitude:longitude];
    
    NSLog(@"JSON META %@", url);
    
    float timestamp = [[NSDate date] timeIntervalSince1970];
    [self sendData:[NSURL URLWithString:url] method:@"GET" action:@ACTION_GET_META_DATA timestamp:timestamp jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseMetaDataData:) withObject:json];
    }];
}

-(void)parseMetaDataData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    MetaData *metaData = [self parseMetaData:responseData];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_GET_META_DATA objects:[NSMutableArray arrayWithArray:@[metaData]]];
        });
    }
}

#pragma mark - Personal

-(void)personal
{
    if (![self hasInternetConnection]) return;
    
    NSString *url = [UrlInfo personalUrl];
    
    [self sendData:url method:@"GET" action:@ACTION_PERSONAL_DATA jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parsePersonalData:) withObject:json];
    }];
}

-(void)parsePersonalData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    Joey *joey = [self parseJoey:responseData];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_PERSONAL_DATA objects:[NSMutableArray arrayWithArray:@[joey]]];
        });
    }
}

#pragma mark - Save Personal

-(void)savePersonal:(NSMutableDictionary *)jsonDictionary
{
    if (![self hasInternetConnection]) return;
    
    [self showLoadingView];
    
    NSString *url = [UrlInfo personalUrl];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self sendData:url method:@"PUT" action:@ACTION_SAVE_PERSONAL_DATA jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseSavePersonalData:) withObject:json];
    }];
}

-(void)parseSavePersonalData:(NSMutableDictionary *)json
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_SAVE_PERSONAL_DATA objects:nil];
        });
    }
}

#pragma mark - registerDevice

-(void)registerDevice:(NSString *)registrationId
{
    if (!registrationId) {
        [alertHelper showAlertWithOneButton:NSLocalizedString(@"Register Error", nil) message:NSLocalizedString(@"Problem with required parameters", nil) from:self.viewController buttonTitle:NSLocalizedString(@"Ok", nil) withHandler:nil];
        return;
    }
    
    if (![self hasInternetConnection]) return;
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
//    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"device_uuid":uuid,
//                                                                                          @"device_id":registrationId,
//                                                                                          @"user_type":@"joey",
//                                                                                          @"user_id":[UrlInfo getJoeyId],
//                                                                                          @"service":@"apns"
//                                                                                          @"service":@"apns"}];
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"device_uuid":uuid,
                                                                                          @"device_id":registrationId,
                                                                                          @"user_type":@"joey",
                                                                                          @"user_id":[UrlInfo getJoeyId],
                                                                                          @"service":@"apns"}];
    
    NSString *url = [UrlInfo registerDeviceUrl];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self sendData:url method:@"POST" action:@ACTION_REGISTER_DEVICE jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseRegisterDeviceData:) withObject:json];
    }];
}

-(void)parseRegisterDeviceData:(NSMutableDictionary *)json
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_REGISTER_DEVICE objects:nil];
        });
    }
}

#pragma mark - sendLocation

-(void)sendLocation:(double)latitude longitude:(double)longitude
{
    
    //NSLog(@"REverse_geocoding: %@",[self ExtractAddress:43.681352 longitude:-79.57202700000001]);
    
    
    
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *UserAddress;
    
    if (![defaults objectForKey:@"UserAddress"]) {
        UserAddress= @"";
    }
    else{
        UserAddress= [defaults objectForKey:@"UserAddress"];
        
    }
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"lat":[NSNumber numberWithDouble:latitude], @"lon":[NSNumber numberWithDouble:longitude] , @"loc":UserAddress }];
    
    NSLog(@"jsonData_updatedToServer:%@ , Lat :%@ Lng :%@ ", UserAddress,[NSNumber numberWithDouble:latitude],[NSNumber numberWithDouble:longitude]);
    
    NSString *url = [UrlInfo sendLocationUrl];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"SEND_Location_error :%@", [error localizedDescription]);
    }
    
    [self sendData:url method:@"POST" action:@ACTION_SEND_LOCATION jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseSendLocationData:) withObject:json];
    }];
}


-(NSString *)ExtractAddressAndSendLocation:(double)latitude longitude:(double)longitude
{
    __block NSString *address= @"";
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!placemarks) {
             // handle error
         }
         
         if(placemarks && placemarks.count > 0)
         {
             CLPlacemark *placemark= [placemarks objectAtIndex:0];
             address = [NSString stringWithFormat:@"%@ %@,%@ %@", [placemark subThoroughfare],[placemark thoroughfare],[placemark locality], [placemark administrativeArea]];
             
             NSLog(@"REverse_geocoding(insideBlock): %@",address);
             
             //Save Address in user defaults
             
             NSString *valueToSave = address;
             [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"UserAddress"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             
             
             //Send Location Coordinates to server
             [self sendLocation:latitude longitude:longitude];
             
             
             //             // you have the address.
             //             // do something with it.
             //             [[NSNotificationCenter defaultCenter] postNotificationName:@"JoeyDidReceiveAddressNotification"
             //                                                                 object:self
             //                                                               userInfo:@{ @"address" : address }];
         }
     }];
    
    return address;
    
}

-(void)parseSendLocationData:(NSMutableDictionary *)json
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_SEND_LOCATION objects:nil];
        });
    }
}

#pragma mark - orderList

-(void)orderList:(NSString *)type
{
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    //    [self showLoadingView];
    
    NSString *url = [UrlInfo orderListUrl:type];
    
    [self sendData:url method:@"GET" action:@ACTION_GET_ORDER_LIST jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseOrderListData:) withObject:json];
    }];
}

-(void)parseOrderListData:(NSMutableDictionary *)json
{
    NSLog(@"json:%@", json);
    
    NSArray *responseData = [json arrayForKey:@"response"];
    if (!responseData) return;
    
    NSMutableArray *orders = [self parseOrders:responseData];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_GET_ORDER_LIST objects:orders];
        });
    }
}




#pragma mark - acceptOrder

-(void)acceptOrder:(int)orderId pickupInfo:(NSMutableDictionary *)jsonDictionary
{
    if (![self hasInternetConnection]) return;
    
    [self showLoadingView];
    
    NSString *url = [UrlInfo acceptOrderUrl:orderId];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self sendData:url method:@"PUT" action:@ACTION_ACCEPT_ORDER jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseAcceptOrderData:) withObject:json];
    }];
}

-(void)parseAcceptOrderData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    NSString *message = [responseData objForKey:@"message"];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_ACCEPT_ORDER objects:[NSMutableArray arrayWithArray:@[message]]];
        });
    }
}

#pragma mark - confirmation

-(void)confirmation:(NSString *)url method:(NSString *)method confirmationInfo:(NSMutableDictionary *)jsonDictionary action:(NSString *)action
{
    if (![self hasInternetConnection]) return;
    
    [self showLoadingView];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self sendData:url method:method action:action jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        if ([action isEqualToString:@ACTION_CONFIRMATION])
        {
            [self performSelector:@selector(parseConfirmationData:) withObject:json];
        }
        else
        {
            [self performSelector:@selector(parseOfflineConfirmationData:) withObject:json];
        }
    }];
}

-(void)parseConfirmationData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    Confirmation *confirmation = [self parseConfirmation:responseData];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_CONFIRMATION objects:[NSMutableArray arrayWithArray:@[confirmation]]];
        });
    }
}

-(void)parseOfflineConfirmationData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    Confirmation *confirmation = [self parseConfirmation:responseData];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_OFFLINE_CONFIRMATION objects:[NSMutableArray arrayWithArray:@[confirmation]]];
        });
    }
}

#pragma mark - messageUrl

-(void)messageUrl
{
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    [self showLoadingView];
    
    NSString *url = [UrlInfo messageUrl];
    
    [self sendData:url method:@"GET" action:@ACTION_SEND_MESSAGE jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseMessageData:) withObject:json];
    }];
}

-(void)parseMessageData:(NSMutableDictionary *)json
{
    NSArray *responseData = [json arrayForKey:@"response"];
    
    NSMutableArray *messages = [NSMutableArray array];
    for (int i=0; i < [responseData count]; i++)
    {
        NSDictionary *messageData = [responseData objectAtIndex:i];
        Message *message = [[Message alloc] init];
        message.message = [messageData objForKey:@"message"];
        message.date = [NSDateHelper dateFromNumber:[[messageData numberForKey:@"date"] integerValue]];
        [messages addObject:message];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_SEND_MESSAGE objects:messages];
        });
    }
}

#pragma mark - reportFail

-(void)reportFail:(int)joeyId taskId:(int)taskId note:(NSString *)message
{
    if (![self hasInternetConnection]) return;
    
    [self showLoadingView];
    
    if (!message)
    {
        message = @"";
    }
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":[NSNumber numberWithInt:joeyId], @"note":message}];
    
    NSString *url = [UrlInfo reportFailUrl:taskId];
    
    // Convert the dictionary into a JSON data object.
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self sendData:url method:@"PUT" action:@ACTION_REPORT_FAIL jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseReportFailData:) withObject:json];
    }];
}

-(void)parseReportFailData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    NSString *message = [responseData objForKey:@"messages"];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_REPORT_FAIL objects:[NSMutableArray arrayWithArray:@[message]]];
        });
    }
}

#pragma mark - nextShift

-(void)nextShift
{
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    [self showLoadingView];
    
    NSString *url = [UrlInfo nextShiftUrl];
    
    [self sendData:url method:@"GET" action:@ACTION_GET_SHIFTS jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseNextShiftData:) withObject:json];
    }];
}

-(void)parseNextShiftData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    NSMutableArray *returnArray = nil;
    if (responseData)
    {
        Shift *shift = [self parseShift:responseData];
        returnArray = [NSMutableArray arrayWithArray:@[shift]];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_GET_SHIFTS objects:returnArray];
        });
    }
}

#pragma mark - startShift

-(void)startShift:(int)shiftId shiftInfo:(NSMutableDictionary *)jsonDictionary
{
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    [self showLoadingView];
    
    NSString *url = [UrlInfo startShiftUrl:shiftId];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self sendData:url method:@"POST" action:@ACTION_START_SHIFT jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseStartShiftData:) withObject:json];
    }];
}

-(void)parseStartShiftData:(NSMutableDictionary *)json
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_START_SHIFT objects:nil];
        });
    }
}

#pragma mark - endShift

-(void)endShift:(int)shiftId shiftInfo:(NSMutableDictionary *)jsonDictionary
{
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    [self showLoadingView];
    
    NSString *url = [UrlInfo endShiftUrl:shiftId];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self sendData:url method:@"POST" action:@ACTION_END_SHIFT jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseEndShiftData:) withObject:json];
    }];
}

-(void)parseEndShiftData:(NSMutableDictionary *)json
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_END_SHIFT objects:nil];
        });
    }
}

#pragma mark - get schedules

-(void)getSchedules:(NSNumber *)zoneId vehicle:(NSNumber *)vehicleId start:(int)startDate end:(int)endDate
{
    if (![self hasInternetConnection]) return;
    
    //    [self showLoadingView];
    
    NSString *url = [UrlInfo schedules:[zoneId intValue] vehicle:[vehicleId intValue] start:startDate end:endDate];
    
    [self sendData:url method:@"GET" action:@ACTION_GET_SCHEDULES jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseSchedulesData:) withObject:json];
    }];
}

-(void)parseSchedulesData:(NSMutableDictionary *)json
{
    NSArray *responseData = [json arrayForKey:@"response"];
    
    NSMutableArray *shifts = [NSMutableArray array];
    for (int i=0; i < [responseData count]; i++)
    {
        NSDictionary *shiftData = [responseData objectAtIndex:i];
        Shift *shift = [self parseShift:shiftData];
        [shifts addObject:shift];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_GET_SCHEDULES objects:shifts];
        });
    }
}

#pragma mark - add schedule Joey

-(void)addJoeySchedule:(NSNumber *)shiftId
{
    if (![self hasInternetConnection]) return;
    
    [self showLoadingView];
    
    NSString *url = [UrlInfo scheduleJoey:[shiftId intValue]];
    
    NSError * error = nil;
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":[UrlInfo getJoeyId]}];
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self sendData:url method:@"POST" action:@ACTION_ADD_JOEY_SCHEDULE jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseAddJoeyScheduleData:) withObject:json];
    }];
}

-(void)parseAddJoeyScheduleData:(NSMutableDictionary *)json
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_ADD_JOEY_SCHEDULE objects:nil];
        });
    }
}

#pragma mark - remove schedule Joey

-(void)removeJoeySchedule:(NSNumber *)shiftId
{
    if (![self hasInternetConnection]) return;
    
    [self showLoadingView];
    
    NSString *url = [UrlInfo removeScheduleJoey:[shiftId intValue]];
    
    [self sendData:url method:@"DELETE" action:@ACTION_REMOVE_JOEY_SCHEDULE jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseRemoveJoeyScheduleData:) withObject:json];
    }];
}

-(void)parseRemoveJoeyScheduleData:(NSMutableDictionary *)json
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_REMOVE_JOEY_SCHEDULE objects:nil];
        });
    }
}

#pragma mark - get full schedule

-(void)getFullSchedule:(int)startDate endDate:(int)endDate
{
    if (![self hasInternetConnection]) return;
    
    [self showLoadingView];
    
    NSString *url = [UrlInfo fullSchedule:startDate endDate:endDate];
    
    [self sendData:url method:@"GET" action:@ACTION_GET_FULL_SCHEDULE jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseFullScheduleData:) withObject:json];
    }];
}

-(void)parseFullScheduleData:(NSMutableDictionary *)json
{
    NSArray *responseData = [json arrayForKey:@"response"];
    
    NSMutableArray *shifts = [NSMutableArray array];
    for (int i=0; i < [responseData count]; i++)
    {
        NSDictionary *shiftData = [responseData objectAtIndex:i];
        Shift *shift = [self parseShift:shiftData];
        [shifts addObject:shift];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_GET_FULL_SCHEDULE objects:shifts];
        });
    }
}

#pragma mark - addDeposit

-(void)addDeposit:(NSMutableDictionary *)jsonDictionary
{
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    [self showLoadingView];
    
    NSString *url = [UrlInfo addDepositUrl];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self sendData:url method:@"POST" action:@ACTION_ADD_DEPOSIT jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseAddDepositData:) withObject:json];
    }];
}

-(void)parseAddDepositData:(NSMutableDictionary *)json
{
    NSString *message = [json objForKey:@"response"];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_ADD_DEPOSIT objects:[NSMutableArray arrayWithArray:@[message]]];
        });
    }
}

#pragma mark - cashOnHand

-(void)cashOnHand
{
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    [self showLoadingView];
    
    NSString *url = [UrlInfo cashOnHandUrl];
    
    [self sendData:url method:@"GET" action:@ACTION_CASH_ON_HAND jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseCashOnHandData:) withObject:json];
    }];
}

-(void)parseCashOnHandData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    NSMutableArray *cashArray = [self parseCashInfo:responseData];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_CASH_ON_HAND objects:cashArray];
        });
    }
}

#pragma mark - start duty

-(void)startDuty:(NSMutableDictionary *)jsonDictionary
{
    if (![self hasInternetConnection]) return;
    
    NSString *url = [UrlInfo startDutyUrl];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self sendData:url method:@"POST" action:@ACTION_START_DUTY jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseStartDutyData:) withObject:json];
    }];
}

-(void)parseStartDutyData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    NSString *message = [responseData objForKey:@"message"];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_START_DUTY objects:[NSMutableArray arrayWithArray:@[message]]];
        });
    }
}

#pragma mark - end duty

-(void)endDuty
{
    if (![self hasInternetConnection]) return;
    
    NSString *url = [UrlInfo endDutyUrl];
    
    [self sendData:url method:@"POST" action:@ACTION_END_DUTY jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseEndDutyData:) withObject:json];
    }];
}

-(void)parseEndDutyData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    NSString *message = [responseData objForKey:@"message"];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_END_DUTY objects:[NSMutableArray arrayWithArray:@[message]]];
        });
    }
}

#pragma mark - get chat channels

-(void)chatChannels
{
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    //    [self showLoadingView];
    
    NSString *url = [UrlInfo chatChannelsUrl];
    
    [self sendData:url method:@"GET" action:@ACTION_CHAT_CHANNELS jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseChatChannelsData:) withObject:json];
    }];
}

-(void)parseChatChannelsData:(NSMutableDictionary *)json
{
    NSArray *responseData = [json arrayForKey:@"response"];
    if (!responseData) return;
    
    NSMutableArray *channelsArray = [self parseChannels:responseData];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_CHAT_CHANNELS objects:channelsArray];
        });
    }
}

#pragma mark - get chat channel message

-(void)chatChannelMessages:(int)channelId startDate:(long)startDate endDate:(long)endDate
{
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    //    [self showLoadingView];
    
    NSString *url = [UrlInfo chatChannelMessagesUrl:channelId startDate:startDate endDate:endDate+2];
    
    [self sendData:url method:@"GET" action:@ACTION_CHAT_MESSAGES jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseChatChannelMessagesData:) withObject:json];
    }];
}

-(void)parseChatChannelMessagesData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    NSArray *messagesData = [responseData arrayForKey:@"messages"];
    NSMutableArray *messagesArray = [self parseMessages:messagesData];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_CHAT_MESSAGES objects:messagesArray];
        });
    }
}

#pragma mark - new Message

-(void)chatNewMessage:(NSMutableDictionary *)jsonDictionary channelId:(int)channelId
{
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    //    [self showLoadingView];
    
    NSString *url = [UrlInfo chatChannelNewMessageUrl:channelId];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self sendData:url method:@"POST" action:@ACTION_CHAT_NEW_MESSAGE jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseChatNewMessageData:) withObject:json];
    }];
}

-(void)parseChatNewMessageData:(NSMutableDictionary *)json
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_CHAT_NEW_MESSAGE objects:nil];
        });
    }
}

#pragma mark - Summary transactions

-(void)summary:(NSInteger)startDate endDate:(NSInteger)endDate
{
    if (![self hasInternetConnection]) return;
    if ([[UrlInfo getJoeyId] intValue] <= 0) return;
    
    NSString *url = [UrlInfo summary:startDate endDate:endDate];
    
    [self sendData:url method:@"GET" action:@ACTION_SUMMARY jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseSummaryData:) withObject:json];
    }];
}

-(void)parseSummaryData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    NSArray *shiftsData = [responseData arrayForKey:@"shifts"];
    NSMutableArray *shifts = [NSMutableArray array];
    for (int i=0; i < [shiftsData count]; i++)
    {
        NSDictionary *shiftData = [shiftsData objectAtIndex:i];
        Shift *shift = [self parseShift:shiftData];
        [shifts addObject:shift];
    }
    
    NSDictionary *transactionsData = [responseData dictionaryForKey:@"transaction_report"];
    Summary *sumary = [self parseSummary:transactionsData];
    sumary.shifts = shifts;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_SUMMARY objects:[NSMutableArray arrayWithArray:@[sumary]]];
        });
    }
}

#pragma mark - Sign up

-(void)signup:(NSMutableDictionary *)jsonDictionary
{
    if (![self hasInternetConnection]) return;
    
    [self showLoadingView];
    
    NSString *url = [UrlInfo signup];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
    if (error)
    {
        // If any error occurs then just display its description on the console.
        NSLog(@"%@", [error localizedDescription]);
    }
    
    float timestamp = [[NSDate date] timeIntervalSince1970];
    
    //
    //    NSLog(@"SIGNUP_URL : %@",[NSURL URLWithString:url]);
    //    NSLog(@"SIGNUP_timestamp : %f",timestamp);
    
    
    [self sendData:[NSURL URLWithString:url] method:@"POST" action:@ACTION_SIGN_UP timestamp:timestamp jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseSignUpData:) withObject:json];
    }];
}

-(void)parseSignUpData:(NSMutableDictionary *)json
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_SIGN_UP objects:nil];
        });
    }
}

#pragma mark - Parsers

-(NSArray *)parseErrorDictionary:(NSDictionary *)data
{
    NSMutableArray *returnArray = [NSMutableArray array];
    for (int j=0; j < [data.allKeys count]; j++)
    {
        NSString *fieldName = [data.allKeys objectAtIndex:j];
        id obj = [data objForKey:fieldName];
        if ([obj isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *objDict = (NSDictionary *)obj;
            NSArray *dictArray = [self parseErrorDictionary:objDict];
            
            for (NSString *key in dictArray)
            {
                [returnArray addObject:[NSString stringWithFormat:@"%@-%@", fieldName, key]];
            }
        }
        else if ([obj isKindOfClass:[NSArray class]])
        {
            NSArray *objArray = (NSArray *)obj;
            for (NSString *error in objArray)
            {
                errorMsg = [errorMsg stringByAppendingString:[NSString stringWithFormat:@"%@\n", error]];
            }
            
            [returnArray addObject:fieldName];
        }
    }
    
    return returnArray;
}

-(Joey *)parseJoey:(NSDictionary *)joeyData
{
    Joey *joey = [[Joey alloc] init];
    NSNumber *joeyId = [joeyData numberForKey:@"id"];
    if ([joeyId intValue] <= 0)
    {
        joeyId = [UrlInfo getJoeyId];
    }
    joey.joeyId = joeyId;
    joey.firstName = [joeyData objForKey:@"first_name"];
    joey.lastName = [joeyData objForKey:@"last_name"];
    joey.nickName = [joeyData objForKey:@"nickname"];
    joey.displayName = [joeyData objForKey:@"display_name"];
    joey.email = [joeyData objForKey:@"email"];
    joey.phone = [joeyData objForKey:@"phone"];
    joey.vehicleId = [joeyData numberForKey:@"vehicle_id"];
    joey.duty = [joeyData numberForKey:@"on_duty"];
    joey.about = [joeyData objForKey:@"about"];
    joey.image = [joeyData objForKey:@"image"];
    joey.publicKey = [joeyData objForKey:@"public_key"];
    joey.privateKey = [joeyData objForKey:@"private_key"];
    joey.address = [joeyData objForKey:@"address"];
    
    NSDictionary *vendorLocationData = [joeyData dictionaryForKey:@"location"];
    joey.location = [self parseAddress:vendorLocationData];
    
    return joey;
}

-(Address *)parseAddress:(NSDictionary *)addressData
{
    Address *address = [[Address alloc] init];
    
    NSDictionary *coordinatesData = [addressData dictionaryForKey:@"coordinates"];
    if (coordinatesData)
    {
        address.locationLatitude = [coordinatesData numberForKey:@"lat"];
        address.locationLongitude = [coordinatesData numberForKey:@"lon"];
    }
    
    NSArray *addressComponents = [addressData arrayForKey:@"address_components"];
    for (int k=0; k < [addressComponents count]; k++)
    {
        NSDictionary *addressComponent = [addressComponents objectAtIndex:k];
        [address setAddressComponent:[self parseAddressComponent:addressComponent]];
    }
    return address;
}

//To Detect if its business/Residential
-(NSString *)GetAddressType:(NSDictionary *)addressData
{
    
    NSString * AddressType;
    
    @try {
        
        AddressType = [[addressData objectForKey:@"type"] isEqual:[NSNull null]] ? @"" : [addressData objectForKey:@"type"]  ;
    }
    @catch (NSException *exception) {
        
        AddressType= @"";
    }
    
    return AddressType;
}

-(AddressComponent *)parseAddressComponent:(NSDictionary *)addressComponent
{
    AddressComponent *component = [[AddressComponent alloc] init];
    component.name = [addressComponent objForKey:@"name"];
    component.code = [addressComponent objForKey:@"code"];
    component.type = [addressComponent objForKey:@"type"];
    return component;
}

-(MetaData *)parseMetaData:(NSDictionary *)metaDataData
{
    MetaData *metaData = [[MetaData alloc] init];
    
    //setting dropOff/pickup location radius from API
    if ([metaDataData objectForKey:@"at_location_radius"]) {
        
        [[NSUserDefaults standardUserDefaults] setInteger:[[metaDataData objectForKey:@"at_location_radius"]intValue] forKey:@"LOCATION_RADIUS"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    NSDictionary *linksData = [metaDataData dictionaryForKey:@"links"];
    if (linksData)
    {
        metaData.privacyLink = [linksData objForKey:@"privacy"];
        metaData.termsLink = [linksData objForKey:@"terms"];
        metaData.homeLink = [linksData objForKey:@"home"];
        metaData.signupLink = [linksData objForKey:@"signup"];
    }
    
    NSDictionary *optionsData = [metaDataData dictionaryForKey:@"options"];
    metaData.options = [NSMutableArray array];
    if (optionsData)
    {
        for (int j=0; j < [optionsData.allKeys count]; j++)
        {
            NSString *key = [optionsData.allKeys objectAtIndex:j];
            NSString *value = [optionsData objectForKey:key];
            
            [metaData.options addObject:[[MetaOption alloc] init:key value:value]];
        }
    }
    
    NSDictionary *statusesData = [metaDataData dictionaryForKey:@"statuses"];
    metaData.status = [NSMutableArray array];
    if (statusesData)
    {
        for (int j=0; j < [statusesData.allKeys count]; j++)
        {
            NSString *key = [statusesData.allKeys objectAtIndex:j];
            NSNumber *value = [statusesData numberForKey:key];
            
            [metaData.status addObject:[[MetaStatus alloc] init:key value:value]];
        }
    }
    
    NSArray *vehiclesData = [metaDataData arrayForKey:@"vehicles"];
    NSMutableArray *vehicles = [NSMutableArray array];
    if (vehiclesData)
    {
        for (int i=0; i < [vehiclesData count]; i++)
        {
            NSDictionary *vehicleData = [vehiclesData objectAtIndex:i];
            [vehicles addObject:[self parseVehicle: vehicleData]];
            metaData.vehicles = vehicles;
        }
    }
    
    NSArray *zonesData = [metaDataData arrayForKey:@"zones"];
    NSMutableArray *zones = [NSMutableArray array];
    if (zonesData)
    {
        for (int i=0; i < [zonesData count]; i++)
        {
            NSDictionary *zoneData = [zonesData objectAtIndex:i];
            [zones addObject:[self parseZone: zoneData]];
        }
        metaData.zones = zones;
    }
    
    return metaData;
}

-(NSMutableArray *)parseOrders:(NSArray *)ordersData
{
    NSMutableArray *orders = [NSMutableArray array];
    for (int i=0; i < [ordersData count]; i++)
    {
        NSDictionary *orderData = [ordersData objectAtIndex:i];
        
        Order *order = [[Order alloc] init];
        order.orderId = [orderData numberForKey:@"id"];
        order.num = [orderData objForKey:@"num"];
        order.remaintime = [orderData objForKey:@"remaintime"];

        order.status = [orderData numberForKey:@"status"];
        order.distance = [orderData numberForKey:@"distance"];
        order.distanceAllowance = [orderData numberForKey:@"distance_allowance"];
        
        /****************
         NEW
         */
        
        order.cancellation_status = [orderData objForKey:@"return_status"];
        order.delay_status = [orderData objForKey:@"delay_status"];
        order.pickup_status = [orderData objForKey:@"pickup_status"];
        order.dropoff_status = [orderData objForKey:@"dropoff_status"];

        
        
        /****************
         NEW
         */
        
        NSDictionary *durationData = [orderData dictionaryForKey:@"duration"];
        if (durationData)
        {
            order.eta = [durationData numberForKey:@"eta"];
        }
        
        NSDictionary *joeyData = [orderData dictionaryForKey:@"joey"];
        if (joeyData)
        {
            order.joey = [self parseJoey:joeyData];
        }
        
        order.distanceCharge = [orderData numberForKey:@"distance_charge"];
        order.totalTaskCharge = [orderData numberForKey:@"total_task_charge"];
        order.subtotal = [orderData numberForKey:@"subtotal"];
        order.tax = [orderData numberForKey:@"tax"];
        order.total = [orderData numberForKey:@"total"];
        order.tip = [orderData objForKey:@"tip"];
        
        order.tasks = [NSMutableArray array];
        NSArray *tasksArray = [orderData arrayForKey:@"tasks"];
        for (int k=0; k < [tasksArray count]; k++)
        {
            NSDictionary *taskData = [tasksArray objectAtIndex:k];
            [order.tasks addObject:[self parseTask:taskData order:order]];
        }
        
        [orders addObject:order];
    }
    
    return orders;
}

-(Task *)parseTask:(NSDictionary *)taskData order:(Order *)order
{
    Task *task = [[Task alloc] init];
    task.taskId = [taskData numberForKey:@"id"];
    task.order = order;
    task.orderId = order.orderId;
    task.num = [taskData objForKey:@"num"];

    task.pin = [taskData objForKey:@"pin"];
    
    task.merchant_order_num = [taskData objForKey:@"merchant_order_num"];
    task.start_time = [taskData objForKey:@"start_time"];
    task.end_time = [taskData objForKey:@"end_time"];
    task.address_line2 = [taskData objForKey:@"address_line2"];


    task.type = [taskData objForKey:@"type"];
    task.taskDescription = [taskData objForKey:@"copy"];
    task.dueTime = [taskData numberForKey:@"due_time"];
    
    NSDictionary *sprintData = [taskData dictionaryForKey:@"sprint"];
    task.sprintId = [sprintData numberForKey:@"id"];
    
    NSDictionary *statusData = [taskData dictionaryForKey:@"status"];
    task.statusId = [statusData numberForKey:@"id"];
    
    NSDictionary *contactData = [taskData dictionaryForKey:@"contact"];
    task.contact = [self parseContact:contactData];
    
    NSDictionary *locationData = [taskData dictionaryForKey:@"location"];
    task.location = [self parseAddress:locationData];
    
    //Business/Residential
    task.location.AddressType = [self GetAddressType:locationData];
    
    NSDictionary *paymentData = [taskData dictionaryForKey:@"payment"];
    task.paymentType = [paymentData objForKey:@"type"];
    task.paymentAmount = [paymentData numberForKey:@"amount"];
    
    task.charge = [taskData numberForKey:@"charge"];
    task.serviceCharge = [taskData numberForKey:@"payment_service_charge"];
    task.weightCharge = [taskData numberForKey:@"weight_charge"];
    
    NSDictionary *weightData = [taskData dictionaryForKey:@"weight"];
    if (weightData)
    {
        task.weightValue = [weightData numberForKey:@"unit"];
        task.weightUnit = [weightData objForKey:@"value"];
    }
    
    task.confirmations = [NSMutableArray array];
    NSArray *confirmationsArray = [taskData arrayForKey:@"confirmations"];
    for (int k=0; k < [confirmationsArray count]; k++)
    {
        NSDictionary *confirmationData = [confirmationsArray objectAtIndex:k];
        [task.confirmations addObject:[self parseConfirmation:confirmationData task:task]];
    }
    
    return task;
}

-(Confirmation *)parseConfirmation:(NSDictionary *)confirmationData task:(Task *)task
{
    Confirmation *confirmation = [self parseConfirmation:confirmationData];
    confirmation.task = task;
    confirmation.taskId = task.taskId;
    confirmation.orderId = task.orderId;
    return confirmation;
}

-(Confirmation *)parseConfirmation:(NSDictionary *)confirmationData
{
    Confirmation *confirmation = [[Confirmation alloc] init];
    confirmation.confirmationId = [confirmationData numberForKey:@"id"];
    confirmation.name = [confirmationData objForKey:@"name"];
    confirmation.title = [confirmationData objForKey:@"title"];
    confirmation.confirmationDescription = [confirmationData objForKey:@"description"];
    confirmation.inputType = [confirmationData objForKey:@"inputType"];
    confirmation.isConfirmed = [confirmationData numberForKey:@"confirmed"];
    
    NSDictionary *actionResourceData = [confirmationData dictionaryForKey:@"action_resource"];
    if (actionResourceData)
    {
        confirmation.url = [actionResourceData objForKey:@"url"];
        confirmation.method = [actionResourceData objForKey:@"method"];
    }
    
    return confirmation;
}

-(Shift *)parseShift:(NSDictionary *)shiftData
{
    Shift *shift = [[Shift alloc] init];
    shift.shiftId = [shiftData numberForKey:@"id"];
    shift.num = [shiftData objForKey:@"num"];
    
    shift.startTime = [NSDateHelper dateFromNumber:[[shiftData numberForKey:@"start_date"] integerValue]];
    shift.endTime = [NSDateHelper dateFromNumber:[[shiftData numberForKey:@"end_date"] integerValue]];
    
    shift.realStartTime = [NSDateHelper dateFromNumber:[[shiftData objForKey:@"joey_start_date"] integerValue]];
    shift.realEndTime = [NSDateHelper dateFromNumber:[[shiftData objForKey:@"joey_end_date"] integerValue]];
    
    // remove on the next version
    if (!shift.startTime) {
        shift.startTime = [NSDateHelper dateFromUTCString:[shiftData objForKey:@"start_time"]];
    }
    if (!shift.endTime) {
        shift.endTime = [NSDateHelper dateFromUTCString:[shiftData objForKey:@"end_time"]];
    }
    if (!shift.realStartTime) {
        shift.realStartTime = [NSDateHelper dateFromUTCString:[shiftData objForKey:@"joey_start_time"]];
    }
    if (!shift.realEndTime) {
        shift.realEndTime = [NSDateHelper dateFromUTCString:[shiftData objForKey:@"joey_end_time"]];
    }
    
    NSDictionary *zoneData = [shiftData dictionaryForKey:@"zone"];
    NSString *zoneString = [shiftData objForKey:@"zone_num"];
    if (zoneData)
    {
        shift.zone = [self parseZone:zoneData];
    }
    else if (zoneString && ![zoneString isEqualToString:@""])
    {
        Zone *zone = [[Zone alloc] init];
        zone.num = zoneString;
        shift.zone = zone;
    }
    
    shift.occupancy = [shiftData numberForKey:@"occupancy"];
    shift.capacity = [shiftData numberForKey:@"capacity"];
    
    shift.can_cancel = [shiftData numberForKey:@"can_cancel"]; //newly added

    
    NSDictionary *vehicleData = [shiftData dictionaryForKey:@"vehicle"];
    shift.vehicle = [self parseVehicle:vehicleData];
    
    NSMutableArray *joeys = [NSMutableArray array];
    NSArray *joeysArray = [shiftData arrayForKey:@"joeys"];
    for (int k=0; k < [joeysArray count]; k++)
    {
        [joeys addObject:[joeysArray objectAtIndex:k]];
        
    }
    shift.joeys = joeys;
    
    NSDictionary *wageData = [shiftData dictionaryForKey:@"wage"];
    shift.wage = [wageData numberForKey:@"value"];
    
    shift.joeyNotes = [shiftData objForKey:@"joey_notes"];
    
    return shift;
}

-(Zone *)parseZone:(NSDictionary *)zoneData
{
    Zone *zone = [[Zone alloc] init];
    zone.zoneId = [zoneData numberForKey:@"id"];
    zone.num = [zoneData objForKey:@"num"];
    zone.name = [zoneData objForKey:@"name"];
    zone.radius = [zoneData numberForKey:@"radius"];
    zone.centerLatitude = [zoneData numberForKey:@"latitude"];
    zone.centerLongitude = [zoneData numberForKey:@"longitude"];
    
    // remove on the next version
    if (!zone.centerLatitude || !zone.centerLatitude)
    {
        NSDictionary *centerData = [zoneData dictionaryForKey:@"center"];
        if (centerData)
        {
            zone.centerLatitude = [centerData numberForKey:@"latitude"];
            zone.centerLongitude = [centerData numberForKey:@"longitude"];
        }
    }
    
    return zone;
}

-(NSMutableArray *)parseCashInfo:(NSDictionary *)cashData
{
    NSMutableArray *cashArray = [NSMutableArray array];
    
    NSString *cashOnHand = [cashData objForKey:@"cash_on_hand"];
    if (cashOnHand && ![cashOnHand isEqualToString:@""])
    {
        Info *info = [[Info alloc] init];
        info.title = NSLocalizedString(@"Cash on Hand", nil);
        info.value = cashOnHand;
        [cashArray addObject:info];
    }
    
    NSString *amountPending = [cashData objForKey:@"total_amount_pending"];
    if (amountPending && ![amountPending isEqualToString:@""])
    {
        Info *info = [[Info alloc] init];
        info.title = NSLocalizedString(@"Deposits pending approval", nil);
        info.value = amountPending;
        [cashArray addObject:info];
    }
    
    NSString *amountToDeposit = [cashData objForKey:@"amount_to_deposit"];
    if (amountToDeposit && ![amountToDeposit isEqualToString:@""])
    {
        Info *info = [[Info alloc] init];
        info.title = NSLocalizedString(@"Amount to deposit", nil);
        info.value = amountToDeposit;
        [cashArray addObject:info];
    }
    
    NSDate *cashDate = [NSDateHelper dateFromUTCString:[cashData objForKey:@"last_deposited"]];
    if (cashDate)
    {
        Info *info = [[Info alloc] init];
        info.title = NSLocalizedString(@"Last deposited", nil);
        info.value = [NSDateHelper stringDateFromDate:cashDate];
        [cashArray addObject:info];
    }
    
    return cashArray;
}

-(Vehicle *)parseVehicle:(NSDictionary *)vehicleData
{
    Vehicle *vehicle = [[Vehicle alloc] init];
    vehicle.vehicleId = [vehicleData numberForKey:@"id"];
    vehicle.name = [vehicleData objForKey:@"name"];
    vehicle.price = [vehicleData numberForKey:@"price"];
    return vehicle;
}

-(NSMutableArray *)parseChannels:(NSArray *)channelsData
{
    NSMutableArray *channels = [NSMutableArray array];
    for (int i=0; i < [channelsData count]; i++)
    {
        NSDictionary *channelData = [channelsData objectAtIndex:i];
        
        Channel *channel = [[Channel alloc] init];
        channel.channelId = [channelData numberForKey:@"id"];
        channel.name = [channelData objForKey:@"name"];
        
        NSArray *messagesArray = [channelData arrayForKey:@"messages"];
        if ([messagesArray count] > 0)
        {
            NSDictionary *messageData = [messagesArray lastObject];
            channel.lastMessage = [self parseMessage:messageData];
        }
        
        [channels addObject:channel];
    }
    
    return channels;
}

-(Message *)parseMessage:(NSDictionary *)messageData
{
    Message *message = [[Message alloc] init];
    if (!messageData)
    {
        return message;
    }
    
    NSString *messageString = [messageData objForKey:@"message"];
    NSString *textConverted = [messageString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    message.message = textConverted;
    
    message.messageId = [messageData numberForKey:@"id"];
    
    message.type = [messageData objForKey:@"type"];
    message.date = [NSDateHelper dateFromNumber:[[messageData numberForKey:@"date"] integerValue]];
    
    NSDictionary *contactData = [messageData objForKey:@"user"];
    message.contact = [self parseContact:contactData];
    
    return message;
}

-(Contact *)parseContact:(NSDictionary *)contactData
{
    Contact *contact = [[Contact alloc] init];
    if (!contactData)
    {
        return contact;
    }
    contact.contactId = [contactData objForKey:@"id"];
    contact.name = [contactData objForKey:@"name"];
    contact.phone = [contactData objForKey:@"phone"];
    contact.email = [contactData objForKey:@"email"];
    contact.type = [contactData objForKey:@"type"];
    
    return contact;
}

-(NSMutableArray *)parseMessages:(NSArray *)messagesData
{
    NSMutableArray *messages = [NSMutableArray array];
    for (int i=0; i < [messagesData count]; i++)
    {
        NSDictionary *messageData = [messagesData objectAtIndex:i];
        Message *message = [self parseMessage:messageData];
        [messages addObject:message];
    }
    
    return messages;
}

-(NSNumber *)parseMoney:(NSDictionary *)moneyData
{
    if (!moneyData)
    {
        return 0;
    }
    return [moneyData objectForKey:@"value"];
}

-(Summary *)parseSummary:(NSDictionary *)summaryData
{
    Summary *summary = [[Summary alloc] init];
    if (!summaryData)
    {
        return summary;
    }
    summary.startDate = [NSDateHelper dateFromNumber:[[summaryData numberForKey:@"start_date"] integerValue]];
    summary.endDate = [NSDateHelper dateFromNumber:[[summaryData numberForKey:@"end_date"] integerValue]];
    summary.totalDistance = [summaryData numberForKey:@"total_distance"];
    summary.totalDuration = [summaryData objForKey:@"total_duration"];
    summary.numCustom = [summaryData numberForKey:@"num_custom"];
    summary.numRemotePos = [summaryData numberForKey:@"num_remote_pos"];
    summary.numQuick = [summaryData numberForKey:@"num_quick"];
    summary.numThirdParty = [summaryData numberForKey:@"num_third_party"];
    summary.numCustomer = [summaryData numberForKey:@"num_customer"];
    summary.numTotalOrders = [summaryData numberForKey:@"num_total_orders"];
    summary.numCompletedShifts = [summaryData numberForKey:@"num_completed_shifts"];
    summary.numTransfers = [summaryData numberForKey:@"num_transfers"];
    summary.numStatements = [summaryData numberForKey:@"num_statements"];
    
    summary.cashCollected = [self parseMoney:[summaryData dictionaryForKey:@"cash_collected"]];
    summary.paymentsMade = [self parseMoney:[summaryData dictionaryForKey:@"payments_made"]];
    summary.earnings = [self parseMoney:[summaryData dictionaryForKey:@"earnings"]];
    summary.wages = [self parseMoney:[summaryData dictionaryForKey:@"wages"]];
    summary.wagesOwed = [self parseMoney:[summaryData dictionaryForKey:@"wages_owed"]];
    summary.tips = [self parseMoney:[summaryData dictionaryForKey:@"tips"]];
    summary.transfers = [self parseMoney:[summaryData dictionaryForKey:@"transfers"]];
    summary.adjustments = [self parseMoney:[summaryData dictionaryForKey:@"adjustments"]];
    summary.planPayments = [self parseMoney:[summaryData dictionaryForKey:@"plan_payments"]];
    summary.invoicePayments = [self parseMoney:[summaryData dictionaryForKey:@"invoice_payments"]];
    summary.statementPayments = [self parseMoney:[summaryData dictionaryForKey:@"statement_payments"]];
    summary.bonuses = [self parseMoney:[summaryData dictionaryForKey:@"bonuses"]];
    
    summary.totalHours = [summaryData objForKey:@"total_hours"];
    summary.ordersPerHour = [summaryData objForKey:@"orders_per_hour"];
    summary.earningsPerHour = [self parseMoney:[summaryData dictionaryForKey:@"earnings_per_hour"]];
    summary.startingBalance = [self parseMoney:[summaryData dictionaryForKey:@"starting_balance"]];
    summary.endingBalance = [self parseMoney:[summaryData dictionaryForKey:@"ending_balance"]];
    summary.averageDuration = [summaryData objForKey:@"average_duration"];
    summary.averageDistance = [summaryData numberForKey:@"average_distance"];
    summary.maxDistance = [summaryData numberForKey:@"max_distance"];
    
    summary.records = [NSMutableArray array];
    NSArray *recordsArray = [summaryData arrayForKey:@"records"];
    for (int k=0; k < [recordsArray count]; k++)
    {
        NSDictionary *recordData = [recordsArray objectAtIndex:k];
        [summary.records addObject:[self parseSummaryRecord:recordData]];
    }
    
    return summary;
}

-(SummaryRecord *)parseSummaryRecord:(NSDictionary *)recordData
{
    SummaryRecord *summaryRecord = [[SummaryRecord alloc] init];
    if (!recordData)
    {
        return summaryRecord;
    }
    summaryRecord.payment_method = [recordData numberForKey:@"payment_method"];
    summaryRecord.amount = [self parseMoney:[recordData dictionaryForKey:@"amount"]];
    summaryRecord.balance = [self parseMoney:[recordData dictionaryForKey:@"balance"]];
    summaryRecord.type = [recordData objForKey:@"type"];
    summaryRecord.recordsDescription = [recordData objForKey:@"description"];
    summaryRecord.recordsId = [recordData numberForKey:@"id"];
    summaryRecord.date = [NSDateHelper dateFromNumber:[[recordData numberForKey:@"date"] integerValue]];
    summaryRecord.num = [recordData objForKey:@"num"];
    summaryRecord.shift = [recordData objForKey:@"shift"];
    summaryRecord.distance = [recordData numberForKey:@"distance"];
    summaryRecord.duration = [recordData objForKey:@"duration"];
    
    return summaryRecord;
}



#pragma mark - Call time API

-(void)callTimeApi:(NSString *)order_id joey_id:(NSString *)joey_id start_time:(NSString *)start_time end_time:(NSString *)end_time
{
  
    if (![self hasInternetConnection]) return;
    
    NSString *url = [UrlInfo callTimeApiUrl];
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"order_id":order_id,
                                                                                          @"joey_id":joey_id,
                                                                                          @"start_time":start_time,
                                                                                          @"end_time":end_time,
                                                                                         @"type":@"dropoff"

                                                                                          }];

    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];

    
    [self sendData:url method:@"POST" action:@ACTION_CALL_TIME_API jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseCallTimeResponseData:) withObject:json];
    }];
    
}



#pragma mark - Complain Section API

-(void)complainSectionApi:(NSString *)order_id
                  joey_id:(NSString *)joey_id
               issue_type:(NSString *)issue_type
                 desc_issue:(NSString *)desc_issue
{
    
    if (![self hasInternetConnection]) return;
    
    NSString *url = [UrlInfo complainSectionApiUrl];
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"order_id":order_id,
                                                                                          @"joey_id":joey_id,
                                                                                          @"type":issue_type,
                                                                                          @"description":desc_issue
                                                                                          
                                                                                          }];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    
    
    [self sendData:url method:@"POST" action:@ACTION_COMPLAIN_SECTION_API jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseComplainSectionResponseData:) withObject:json];
    }];
    
}


#pragma mark - Itinerary API

-(void)itineraryApi
                
{
    
    if (![self hasInternetConnection]) return;

       //Show Loader
       [self showLoadingView ];
       
    NSString *url = [UrlInfo itineraryOrders];
    
   
    
  // Convert the dictionary into a JSON data object.
  //  NSError * error = nil;
  
    [self sendData:url method:@"GET" action:@ACTION_ITINERARY_API jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
          [self performSelector:@selector(parseItineraryListData:) withObject:json];
      }];
    
}




-(void)parseItineraryListData:(NSMutableDictionary *)json
{
    NSLog(@"itineary_json:%@", json);
    
    NSDictionary *responseData = [json objectForKey:@"response"];
    if (!responseData) return;
    
    NSMutableArray *hubArray = [self parseItineraryObjectDetail:[responseData arrayForKey:@"hub"]];
    NSMutableArray *storeArray = [self parseItineraryObjectDetail:[responseData arrayForKey:@"store"]];

    NSMutableArray *pickup_delay_status = [self parseStatus:[responseData arrayForKey:@"pickup_delay_status"]];
    NSMutableArray *dropoff_delay_status = [self parseStatus:[responseData arrayForKey:@"dropoff_delay_status"]];

    NSMutableArray *pickup_return_status = [self parseStatus:[responseData arrayForKey:@"pickup_return_status"]];
    NSMutableArray *dropoff_return_status = [self parseStatus:[responseData arrayForKey:@"dropoff_return_status"]];

    NSMutableArray *dropoff_status = [self parseStatus:[responseData arrayForKey:@"dropoff_status"]];
    

    ItineraryOrder * itinerary = [[ItineraryOrder alloc]init];

    itinerary.arr_itineraryDetailObj_Hub = hubArray;
    itinerary.arr_itineraryDetailObj_store= storeArray;
    
    itinerary.pickup_delay_status = pickup_delay_status;
    itinerary.dropoff_delay_status = dropoff_delay_status;

    itinerary.pickup_return_status = pickup_return_status;
    itinerary.dropoff_return_status = dropoff_return_status;
    
    itinerary.dropoff_status = dropoff_status;

    //Adding above object into array and then passing it
    NSMutableArray *dataArray = [[NSMutableArray alloc]init];
    [dataArray addObject:itinerary];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_ITINERARY_API objects:dataArray];
        });
    }
}







#pragma mark - System Status API (RETURN | DELAY | DROPOFF UPDATE) - WITHOUT TYPE PARAM



-(void)updateSystemStatus:(NSString *)order_id statusCode:(NSString *)statusCode TaskId:(NSString *)TaskIdCode type:(NSString *)taskType

{
    
    
    if (![self hasInternetConnection]) return;

    //Show Loader
    [self showLoadingView ];


    NSString *url = [UrlInfo updateSystemStatus];

    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"order_id":order_id,
                                                                                          @"status_id":statusCode,
                                                                                          @"joey_id": [UrlInfo getJoeyId],
                                                                                          @"task_id":TaskIdCode,
                                                                                          @"image" : @"",
                                                                                          @"type":taskType
                                                                                          }];

    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];


    [self sendData:url method:@"PUT" action:@ACTION_SYSTEM_STATUS_API jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        
        NSLog(@"SYSTEM_STATUS_API : %@",json);
        [self performSelector:@selector(parseUpdateStatusResponseData:) withObject:json];
    }];
    
}


#pragma mark - System Status API - For Image Upload

-(void)updateSystemStatusImageUpload:(NSMutableDictionary *)jsonDictionary
{
    
    if (![self hasInternetConnection]) return;
    
    NSString *url = [UrlInfo updateSystemStatus];
    
    [self showLoadingView];

    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    
    
    [self sendData:url method:@"PUT" action:@ACTION_SYSTEM_STATUS_API_IMG_UPLOAD jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        
        NSLog(@"SYSTEM_STATUS_API : %@",json);
        [self performSelector:@selector(parseImageUploadResponseData:) withObject:json];
    }];
    
}




/**
 UPDATE STATUS RESPONSE
 */

-(void)parseUpdateStatusResponseData:(NSMutableDictionary *)json
{
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_SYSTEM_STATUS_API objects:[json objectForKey:@"response"]];
        });
    }
    
}


/**
 UPLOAD IMAGE RESPONSE
 */
-(void)parseImageUploadResponseData:(NSMutableDictionary *)json
{
  
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_SYSTEM_STATUS_API_IMG_UPLOAD objects:[json objectForKey:@"response"]];
        });
    }
   
}



-(void)parseSystemStatusResponseData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;
    
    Joey *joey = [self parseJoey:responseData];
    //
    //    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    //    {
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            [self.delegate parsedObjects:@ACTION_PERSONAL_DATA objects:[NSMutableArray arrayWithArray:@[joey]]];
    //        });
    //    }
}




-(void)parseCallTimeResponseData:(NSMutableDictionary *)json
{
    NSDictionary *responseData = [json dictionaryForKey:@"response"];
    if (!responseData) return;

    Joey *joey = [self parseJoey:responseData];
//
//    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self.delegate parsedObjects:@ACTION_PERSONAL_DATA objects:[NSMutableArray arrayWithArray:@[joey]]];
//        });
//    }
}





-(void)parseComplainSectionResponseData:(NSMutableDictionary *)json
{
//    NSDictionary *responseData = [json dictionaryForKey:@"response"];
//    if (!responseData) return;
//
//    Joey *joey = [self parseJoey:responseData];
//
//    NSLog(@"COMPLAIN_SECTION : %@",joey.joeyId);
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_COMPLAIN_SECTION_API objects:[json objectForKey:@"response"]];
        });
    }
    //
    //    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    //    {
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            [self.delegate parsedObjects:@ACTION_PERSONAL_DATA objects:[NSMutableArray arrayWithArray:@[joey]]];
    //        });
    //    }
}







/**
 MM SUPERMARKET API
 
 -- Update Status api
 */


#pragma mark - MM SUPERMARKET  API

-(void)updateMMAboutStatusUpdate_API:(NSString *)url
{
    
    if (![self hasInternetConnection]) return;
    
    NSString *API_URL = url;
    
 
    
    /**
     Checking Sprint ID and then calling Update Status API
     */
    
    [self checkIfSprintExists_API];
    
    
    
//    /**
//     Iterate SPRINT ID / ORDER ID and then call update status update API
//     */
//    
//    [self sendData:API_URL method:@"POST" action:@ACTION_UPDATE_Status jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
//        [self performSelector:@selector(parseMMStatusAPI:) withObject:json];
//    }];
//    
    
   
}



/**

 MM SUPERMARKET API
 
 -Get Sprints
 */

-(void)checkIfSprintExists_API
{
    if (![self hasInternetConnection]) return;
    
    
    /**
     MM SUPERMARKET API
     */
    
    NSString *API_URL = @"http://dev-delivery-services.mmfoodmarket.com/api/joeyco/getsprints";
    
    NSLog(@"JSON META %@", API_URL);
    
    [self sendData:[NSURL URLWithString:API_URL] method:@"GET" action:@ACTION_GET_SPRINTS timestamp:0.0 jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
        
        [self performSelector:@selector(parseSprints:) withObject:json];
    }];
}




-(void)parseSprints:(NSMutableDictionary *)json
{
//    NSDictionary *responseData = [json dictionaryForKey:@"response"];
//    if (!responseData) return;
    
    
    
//
//    NSArray * values = [json allValues];
//    if (!values) return;

    NSArray *userData = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingAllowFragments error:nil];

    NSMutableArray *creditCards = [NSMutableArray array];
    for (NSDictionary *user in userData) {
        [creditCards addObject:[user objectForKey:@"sprintId"]];
    }
    
}



/*

 Parse Order Status
 */


-(OrderStatus *)ParseOrderStatusObject:(NSDictionary *)OrderData{
    
    OrderStatus * orderStatus = [[OrderStatus alloc]init];
    orderStatus.orderStatusID = [OrderData objForKey:@"id"];
    orderStatus.orderActive = [OrderData objForKey:@"active"];
    orderStatus.orderDescription = [OrderData objForKey:@"description"];
    
    return orderStatus;

    
}



/*

 ITINERARY ORDERS
 Parse itinerary Object Detail
 */


-(ItineraryOrder_Listing_ObjectDetail *)ParseItineraryObject:(NSDictionary *)data{
    
    ItineraryOrder_Listing_ObjectDetail * obj = [[ItineraryOrder_Listing_ObjectDetail alloc]init];
    obj.taskId = [data objForKey:@"task_id"];
    obj.sprintId = [data objForKey:@"sprint_id"];
    obj.num = [data objForKey:@"num"];
    
    obj.confirm_signature = [[data objForKey:@"confirm_signature"] boolValue];
    obj.confirm_pin = [[data objForKey:@"confirm_pin"] boolValue];
    obj.confirm_image = [[data objForKey:@"confirm_image"] boolValue];
    
    obj.has_picked = [[data objForKey:@"has_picked"] boolValue];
    obj.returned = [[data objForKey:@"returned"] boolValue];


    obj.hub_title = [data objForKey:@"hub_title"];
    obj.hub_address = [data objForKey:@"hub_address"];
    obj.type = [data objForKey:@"type"];
    
    obj.start_time = [data objForKey:@"start_time"];
    obj.end_time = [data objForKey:@"end_time"];
    obj.merchant_order_num = [data objForKey:@"merchant_order_num"];
    obj.tracking_id = [data objForKey:@"tracking_id"];

    
    obj.contact = [data objForKey:@"contact"];
    obj.location = [data objForKey:@"location"];
    obj.confirmations = [data objForKey:@"confirmations"];

    
    return obj;

    
}


/*
 
 ITNEARARY ORDERS
 
 Parse status for Itinerary object
 
 For pickup delay
 For Dropoff Delay
 
 For Pickup Return
 For Dropoff Return
 
 For Drop off status
 
 */
-(NSMutableArray *)parseStatus:(NSArray *)statusArray
{
    NSMutableArray *parsedArray = [NSMutableArray array];
    OrderStatus * orderStatus;
    
       for (int i=0; i < [statusArray count]; i++)
       {
           NSDictionary *statusData = [statusArray objectAtIndex:i];
           orderStatus =[self ParseOrderStatusObject:statusData];
           [parsedArray addObject:orderStatus];
        }
    return parsedArray;

}

-(NSMutableArray *)parseItineraryObjectDetail:(NSArray *)data
{
    NSMutableArray *parsedArray = [NSMutableArray array];
    ItineraryOrder_Listing_ObjectDetail * detail;
    
       for (int i=0; i < [data count]; i++)
       {
           NSDictionary *obj = [data objectAtIndex:i];
           detail =[self ParseItineraryObject:obj];
           [parsedArray addObject:detail];
        }
    return parsedArray;

}


#pragma mark - Itinerary orders - Bulk Pickup


-(void)ItineraryBulkScanApi:(NSMutableArray *)tracking_ids
                
{
    
    if (![self hasInternetConnection]) return;

       //Show Loader
       [self showLoadingView ];
       
    NSString *url = [UrlInfo itineraryOrdersBulkScan];
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"items_pick":tracking_ids
                                                                                          }];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    
    
    [self sendData:url method:@"POST" action:@ACTION_ITINERARY_BULK_SCAN jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseItineraryBulkScanResponseData:) withObject:json];
    }];
    
}




-(void)parseItineraryBulkScanResponseData:(NSMutableDictionary *)json
{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_ITINERARY_BULK_SCAN objects:[json objectForKey:@"response"]];
        });
    }
}





#pragma mark - Itinerary orders - SINGLE Drop OFF


-(void)SingleDropOff:(NSString *)tracking_id
                
{
    
    if (![self hasInternetConnection]) return;

    //Show Loader
    [self showLoadingView ];
    
    
    NSString *url = [UrlInfo singleDropOffScan];
    
    NSMutableDictionary *jsonDictionary = [[NSMutableDictionary alloc]init];
    [jsonDictionary setObject:tracking_id forKey:@"tracking_id"];
    
    // Convert the dictionary into a JSON data object.
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    
    
    [self sendData:url method:@"POST" action:@ACTION_SINGLE_DROP_OFF jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
        [self performSelector:@selector(parseSingleDropOffScanResponseData:) withObject:json];
    }];
    
}




-(void)parseSingleDropOffScanResponseData:(NSMutableDictionary *)json
{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_SINGLE_DROP_OFF objects:[json objectForKey:@"response"]];
        });
    }
}



#pragma mark - DIGITAL MANIFEST API - FETCH DATA

-(void)digitalManifestFetchApi
                
{
    
    if (![self hasInternetConnection]) return;

       //Show Loader
       [self showLoadingView ];
       
    NSString *url = [UrlInfo digitalManifestFetchData];
    
  
    [self sendData:url method:@"GET" action:@ACTION_digital_Manifest_fetch_API jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
          [self performSelector:@selector(parseDMListData:) withObject:json];
      }];
    
}

-(void)parseDMListData:(NSMutableDictionary *)json{
    
    NSMutableArray *responseData = [json arrayForKey:@"response"];
      // if (!responseData) return;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
      {
          dispatch_async(dispatch_get_main_queue(), ^{
              [self.delegate parsedObjects:@ACTION_digital_Manifest_fetch_API objects:responseData];
          });
      }
}


#pragma mark - DIGITAL MANIFEST API - CONFIRM BY SIGNATURE

-(void)digitalManifestConfirmBySignatureApi:(NSData *)data
                
{
    
    if (![self hasInternetConnection]) return;

       //Show Loader
       [self showLoadingView ];
       
    NSString *url = [UrlInfo digitalManifestSignUpdate];
    
  // Convert the dictionary into a JSON data object.
//     NSError * error = nil;
//     NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
//     //    NSLog(@"jsonData:%@", [[NSString alloc] initWithData:jsonData encoding:4]);
//     if (error)
//     {
//         // If any error occurs then just display its description on the console.
//         NSLog(@"%@", [error localizedDescription]);
//     }
    [self sendData:url method:@"POST" action:@ACTION_digital_Manifest_sign_API jsonData:data withCompletionHandler:^(NSMutableDictionary *json) {
          [self performSelector:@selector(parseDMSignUpdateResponseData:) withObject:json];
      }];
    
}

-(void)parseDMSignUpdateResponseData:(NSMutableDictionary *)json{
    
    

    NSMutableArray *responseData = [[NSMutableArray alloc]init];
    [responseData addObject:@"Confirmation has been completed"];
    
      // if (!responseData) return;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
      {
          dispatch_async(dispatch_get_main_queue(), ^{
              [self.delegate parsedObjects:@ACTION_digital_Manifest_sign_API objects:responseData];
          });
      }
}


#pragma mark - Scan-At-Pickup

-(void)storePickup:(NSString *)trackingID{
     if (![self hasInternetConnection]) return;

           //Show Loader
           [self showLoadingView ];
           
        NSString *url = [UrlInfo storePickup];
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":[UrlInfo getJoeyId], @"tracking_id":trackingID}];
  
    // Convert the dictionary into a JSON data object.
       NSError * error = nil;
       NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
       
    
        [self sendData:url method:@"POST" action:@ACTION_storePickup jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
              [self performSelector:@selector(parsestorePickupResponseData:) withObject:json];
          }];
}


-(void)parsestorePickupResponseData:(NSMutableDictionary *)json{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
      {
          dispatch_async(dispatch_get_main_queue(), ^{
              [self.delegate parsedObjects:@ACTION_storePickup objects:[json objectForKey:@"response"]];
          });
      }
}


-(void)getStorePickupList{
     if (![self hasInternetConnection]) return;

           //Show Loader
           [self showLoadingView ];
           
        NSString *url = [UrlInfo storePickupList];
    
        [self sendData:url method:@"GET" action:@ACTION_storePickupList jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
              [self performSelector:@selector(parseStorePickupListData:) withObject:json];
          }];
}



-(void)parseStorePickupListData:(NSMutableDictionary *)json
{
    NSLog(@"storePickupData_json:%@", json);
    
    NSMutableArray *responseData = [[json arrayForKey:@"response"] mutableCopy];
    if (!responseData) return;
    
  
  
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_storePickupList objects:responseData];
        });
    }
}




-(void)hubDeliverByJoey{
     if (![self hasInternetConnection]) return;

           //Show Loader
           [self showLoadingView ];
           
    NSString *url = [UrlInfo hubBulkDeliverFromStore];
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"joey_id":[UrlInfo getJoeyId]}];

    // Convert the dictionary into a JSON data object.
       NSError * error = nil;
       NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    
    
        [self sendData:url method:@"POST" action:@ACTION_hubDeliverByJoey jsonData:jsonData withCompletionHandler:^(NSMutableDictionary *json) {
              [self performSelector:@selector(parsehubDeliverByJoeyResponseData:) withObject:json];
          }];
}


-(void)parsehubDeliverByJoeyResponseData:(NSMutableDictionary *)json{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
      {
          dispatch_async(dispatch_get_main_queue(), ^{
              [self.delegate parsedObjects:@ACTION_hubDeliverByJoey objects:[json objectForKey:@"response"]];
          });
      }
}



#pragma mark - INFORMATION WINDOW


-(void)informationWindow:(NSNumber *)sprintID{
    
    if (![self hasInternetConnection]) return;

            //Show Loader
            [self showLoadingView ];
            
         NSString *url = [UrlInfo informationWindow:sprintID];
     
         [self sendData:url method:@"GET" action:@ACTION_informationWindow jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
               [self performSelector:@selector(parseInformationWindowData:) withObject:json];
           }];
}


-(void)getQuestionFetchApi :(NSNumber *)vendorID
{
    
    if (![self hasInternetConnection]) return;

       //Show Loader
       [self showLoadingView ];
       
    NSString *url = [UrlInfo getQuestion:vendorID];
    
  
    [self sendData:url method:@"GET" action:@ACTION_get_question_API jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
          [self performSelector:@selector(parseGetQuestionListData:) withObject:json];
      }];
    
}

-(void)parseGetQuestionListData:(NSMutableDictionary *)json{
    
    NSMutableArray *responseData = [json arrayForKey:@"response"];
      // if (!responseData) return;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
      {
          dispatch_async(dispatch_get_main_queue(), ^{
              [self.delegate parsedObjects:@ACTION_get_question_API objects:responseData];
          });
      }
}

-(void)parseInformationWindowData:(NSMutableDictionary *)json
{
  
    
    NSMutableArray *responseData = [[NSMutableArray alloc]initWithObjects:[[json objForKey:@"response"] mutableCopy], nil];
    if (!responseData) return;
    
  
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate parsedObjects:@ACTION_informationWindow objects:responseData];
        });
    }
}
-(void)faqFetchApi
                
{
    
    if (![self hasInternetConnection]) return;

       //Show Loader
       [self showLoadingView ];
       
    NSString *url = [UrlInfo faqFetchData];
    
    [self sendData:url method:@"GET" action:@ACTION_faq_fetch_API jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
          [self performSelector:@selector(parseFaqListData:) withObject:json];
      }];
    
}

-(void)parseFaqListData:(NSMutableDictionary *)json{
    
    NSMutableArray *responseData = [json arrayForKey:@"response"];
      // if (!responseData) return;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
      {
          dispatch_async(dispatch_get_main_queue(), ^{
              [self.delegate parsedObjects:@ACTION_faq_fetch_API objects:responseData];
          });
      }
}

#pragma mark - FAQ API - FETCH DATA

-(void)faqDescFetchApi:(int)sprintId
                
{
    
    if (![self hasInternetConnection]) return;

       //Show Loader
       [self showLoadingView ];
       
    NSString *url = [UrlInfo faqDescFetchData:sprintId];
    
    [self sendData:url method:@"GET" action:@ACTION_faqDesc_fetch_API jsonData:nil withCompletionHandler:^(NSMutableDictionary *json) {
          [self performSelector:@selector(parseFaqDescListData:) withObject:json];
      }];
    
}

-(void)parseFaqDescListData:(NSMutableDictionary *)json{
    
    NSMutableArray *responseData = [json arrayForKey:@"response"];
      // if (!responseData) return;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(parsedObjects:objects:)])
      {
          dispatch_async(dispatch_get_main_queue(), ^{
              [self.delegate parsedObjects:@ACTION_faqDesc_fetch_API objects:responseData];
          });
      }
}
@end
