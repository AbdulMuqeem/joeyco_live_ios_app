//
//  LocationManager.h
//  Location
//
//  Created by Rick
//  Copyright (c) 2014 Location. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#define LocationManagerUpdateLocation "LocationManagerDidUpdateLocation"
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


@interface LocationManager : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSTimer *delay10Seconds;
@property (nonatomic) BOOL inBackground;

+(LocationManager *)shared;
-(CLLocation *)lastKnownLocation;
-(void)startUpdatingLocation;
-(void)startBackgroundMode;
-(void)startLaunchMode;
-(void)stopUpdatingLocation;
-(void)endBackgroundMode;

@end

@interface Location : NSObject 

@property (nonatomic, strong) NSDate *time;
@property (nonatomic, copy) CLLocation *location;

@end
