//
//  LocationManager.m
//  Location
//
//  Created by Rick
//  Copyright (c) 2014 Location All rights reserved.
//

#import "LocationManager.h"
#import <JoeyCoUtilities/JoeyCoUtilities.h>
#import "JSONHelper.h"

#define LATITUDE @"latitude"
#define LONGITUDE @"longitude"
#define ACCURACY @"theAccuracy"

@interface LocationManager () <JSONHelperDelegate>
{
    NSMutableArray *locationArray;
    JSONHelper *json;
}

@property (nonatomic, strong) CLLocationManager *managerActive;
@property (nonatomic, strong) CLLocationManager *managerBackground;
@property (nonatomic, copy) CLLocation *lastKnownLocation;

@end

@implementation LocationManager

static LocationManager *instance;
static const double DEFAULT_DISTANCE_FILTER = 50.00;

+(LocationManager *) shared
{
    if (!instance)
    {
        instance = [[LocationManager alloc] init];
        
    }
    return instance;
}

-(id)init
{
    if (self = [super init])
    {
        // Clear current value to be sure we start fresh
        self.lastKnownLocation = nil;
        self.inBackground = NO;

        json = [[JSONHelper alloc] init:nil andDelegate:self];
        
        self.managerActive = [[CLLocationManager alloc]init];
        self.managerActive.delegate = self;
      //  self.managerActive.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        self.managerBackground.desiredAccuracy = kCLLocationAccuracyBestForNavigation;

      //  self.managerActive.distanceFilter = DEFAULT_DISTANCE_FILTER;
        
        self.managerActive.distanceFilter = kCLDistanceFilterNone;
        self.managerActive.pausesLocationUpdatesAutomatically = NO;
        self.managerActive.allowsBackgroundLocationUpdates = YES;
        self.managerActive.showsBackgroundLocationIndicator = YES;


        self.managerActive.activityType = CLActivityTypeAutomotiveNavigation;
        
        self.managerBackground = [[CLLocationManager alloc]init];
        self.managerBackground.delegate = self;
      //  self.managerBackground.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        self.managerBackground.desiredAccuracy = kCLLocationAccuracyBestForNavigation;

        self.managerBackground.activityType = CLActivityTypeAutomotiveNavigation;
        self.managerBackground.pausesLocationUpdatesAutomatically = NO;
        self.managerBackground.allowsBackgroundLocationUpdates = YES;
        self.managerBackground.showsBackgroundLocationIndicator = YES;
        self.managerBackground.distanceFilter = kCLDistanceFilterNone;


        
        
    }
    return self;
}

-(CLLocation *)lastKnownLocation
{
    return _lastKnownLocation;
}

-(void)dealloc
{
    [self.timer invalidate];
    [self.delay10Seconds invalidate];
}

#pragma mark - Public Methods

/**
 * Starts updating the location in realtime,
 * Stops the background monitoring.
 * Called when the application is launched to the foreground
 */

-(void)startUpdatingLocation
{
    self.inBackground = NO;

    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    if (self.delay10Seconds)
    {
        [self.delay10Seconds invalidate];
        self.delay10Seconds = nil;
    }
    
    [self.managerActive stopUpdatingLocation];
    [self.managerBackground stopMonitoringSignificantLocationChanges];
    
    locationArray = [[NSMutableArray alloc] init];

    if ([self.managerActive respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.managerActive requestAlwaysAuthorization];
        });
    }
    
    [MapHelper verifyLocationAuthorization];
    
    if (self.inBackground)
    {
        [self.managerBackground startMonitoringSignificantLocationChanges];
    }
    else
    {
        [self.managerActive startUpdatingLocation];
    }
}

-(void)startBackgroundMode
{
    self.inBackground = YES;
    
    [self.managerActive stopUpdatingLocation];
    [self.managerBackground stopMonitoringSignificantLocationChanges];

    if ([self.managerBackground respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.managerBackground requestAlwaysAuthorization];
        });
    }
    [self.managerBackground startMonitoringSignificantLocationChanges];
}

-(void)endBackgroundMode
{
    self.inBackground = NO;
}

-(void)startLaunchMode
{
    self.inBackground = YES;
    
    self.managerBackground = [[CLLocationManager alloc]init];
    self.managerBackground.delegate = self;
//    self.managerBackground.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
//    self.managerBackground.activityType = CLActivityTypeOtherNavigation;
    self.managerBackground.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.managerBackground.activityType = CLActivityTypeAutomotiveNavigation;
    self.managerBackground.pausesLocationUpdatesAutomatically = NO;
    self.managerBackground.allowsBackgroundLocationUpdates = YES;
    self.managerBackground.showsBackgroundLocationIndicator = YES;
    self.managerBackground.distanceFilter = kCLDistanceFilterNone;


    
    if ([self.managerBackground respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.managerBackground requestAlwaysAuthorization];
        });
    }
    
    [self.managerBackground startMonitoringSignificantLocationChanges];
}

-(void)stopUpdatingLocation
{
    if (self.timer)
    {
        [self.timer invalidate];
        self.timer = nil;
    }
    if (self.delay10Seconds)
    {
        [self.delay10Seconds invalidate];
        self.delay10Seconds = nil;
    }
    
    [self.managerActive stopUpdatingLocation];
    [self.managerBackground stopMonitoringSignificantLocationChanges];
    
    [self updateLocationToServer];
}

-(void)stopService
{
    [self.managerActive stopUpdatingLocation];
    [self.managerBackground stopMonitoringSignificantLocationChanges];
    
    [self updateLocationToServer];
}

#pragma mark - Private Methods

/**
 ** Main delegate method for CLLocationManager.
 ** Called when location is updated - makes decisions about whether or not to update class instance variable currentLocation
 */
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    for(CLLocation *newLocation in locations)
    {
        NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
        if (locationAge > 60.0)
        {
            continue;
        }

        CLLocationDistance dist = [self.lastKnownLocation distanceFromLocation:newLocation];

        CLLocationCoordinate2D theLocation = newLocation.coordinate;
        
        //Select only valid location and also location with good accuracy
        if((!self.lastKnownLocation || dist > 50) && newLocation && newLocation.horizontalAccuracy > 0 && newLocation.horizontalAccuracy < 2000
           && theLocation.latitude != 0.0 && theLocation.longitude != 0.0)
        {
            [locationArray addObject:newLocation];
            [self saveLocationAndNotifyObservers:newLocation];
        }
    }

    //If the timer still valid, return it (Will not run the code below)
    if (self.timer)
    {
        return;
    }

    self.timer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(startUpdatingLocation) userInfo:nil repeats:NO];
    if (self.delay10Seconds)
    {
        [self.delay10Seconds invalidate];
        self.delay10Seconds = nil;
    }
    self.delay10Seconds = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(stopService) userInfo:nil repeats:NO];
}

// updates the property currentLocation and sends out notification. Calls stop CL from updating if appropriate
-(void)saveLocationAndNotifyObservers:(CLLocation *)locationToSave
{
    self.lastKnownLocation = locationToSave;

    dispatch_async(dispatch_get_main_queue(), ^{
        NSNotification *aNotification = [NSNotification notificationWithName:@LocationManagerUpdateLocation object:[locationToSave copy]];
        [[NSNotificationCenter defaultCenter] postNotification:aNotification];
    });
}

//Send the location to Server
- (void)updateLocationToServer
{
    
    
    NSLog(@"updating_location_to_server");
    
    // Find the best location from the array based on accuracy
    CLLocation *bestLocation = [locationArray firstObject];
    
    for(CLLocation *location in locationArray)
    {
        if(location.horizontalAccuracy < bestLocation.horizontalAccuracy)
        {
            bestLocation = location;
        }
    }
    
    if (bestLocation && json)
    {
       // [json sendLocation:bestLocation.coordinate.latitude longitude:bestLocation.coordinate.longitude];
        [json ExtractAddressAndSendLocation:bestLocation.coordinate.latitude longitude:bestLocation.coordinate.longitude];

    }
}

#pragma mark - JSONHelperDelegate

- (void)parsedObjects:(NSString *)action objects:(NSMutableArray *)list
{
    if ([action isEqualToString:@ACTION_SEND_LOCATION])
    {
        
    }
}

- (void)errorMessage:(NSString *)action message:(NSString *)message
{

}

@end

@implementation Location

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.time forKey:@"time"];
    [encoder encodeObject:self.location forKey:@"location"];

}
- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if( self != nil )
    {
        self.time = [decoder decodeObjectForKey:@"time"];
        self.location = [decoder decodeObjectForKey:@"location"];
    }
    return self;
}

@end
