#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

// TODO(b/161176336): Move this class to .. when FeatureCustom is released.

/** A model source indicating where the model is hosted remotely. */
NS_SWIFT_NAME(RemoteModelSource)
@interface MLKRemoteModelSource : NSObject

/** Unavailable. Use a subclass initializer. */
- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
