#import <Foundation/Foundation.h>

#import "MLKLocalModel.h"

NS_ASSUME_NONNULL_BEGIN

// TODO(b/161176336): Merge this category into `LocalModel` when FeatureCustom is released.

/** A category for supporting AutoML custom models. */
@interface MLKLocalModel (Custom)

/**
 * An absolute path to a model manifest file stored locally on the device. `nil` if the model does
 * not have a manifest.
 */
@property(nonatomic, copy, readonly, nullable) NSString *manifestPath;

/**
 * Creates a new instance with the given manifest file path of an AutoML Vision Edge model.
 *
 * @param manifestPath Absolute path to an AutoML Vision Edge model manifest stored locally on the
 *     device.
 * @return A new `LocalModel` instance. `nil` if the model manifest at the given `manifestPath` is
 *     either missing or invalid.
 */
- (nullable instancetype)initWithManifestPath:(NSString *)manifestPath;

@end

NS_ASSUME_NONNULL_END
